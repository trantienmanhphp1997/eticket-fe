export const environment = {
  production: true,
  test: true,
  fileDomain:'http://10.16.150.205:8000/eticket-api/storage/',
  domain: 'http://10.16.150.205:8000/eticket',
  fe_domain: 'http://10.16.150.205:8000/eticket/',
  gateway: 'http://10.16.150.205:8000/eticket-api/api/v1/',
  //domain: 'https://mobifone.smart-sales.vn/eticket',
  //gateway: 'https://mobifone.smart-sales.vn/api/v1/',
  androidDownloadUrl: 'https://play.google.com/store/apps/details?id=com.mbamc.buildingcares',
  iosDownloadUrl: 'https://apps.apple.com/us/app/building-cares/id1602344331',
  is_department_sync:true,
  is_user_sync:false,
  is_customer_sync : false,
  is_product_sync:false,
  login_type: 'sso', //'ticket', // sso or ticket
  sso_domain: 'http://10.16.150.205:8000',
  pusher: {
    key: 'd08b0eea34b5e8ea25d7',
    cluster: 'ap1',
  },
  firebaseConfig: {
    apiKey: 'AIzaSyBT2IXza1uoqjjyuZxIbPZlk301UqbBIcY',
    authDomain: 'eticket-dev-29dbd.firebaseapp.com',
    projectId: 'eticket-dev-29dbd',
    storageBucket: 'eticket-dev-29dbd.appspot.com',
    messagingSenderId: '774021134240',
    appId: '1:774021134240:web:4a1575150a8ec4304d96c5',
    measurementId: 'G-3BP3QHNQ47'
  }
};
