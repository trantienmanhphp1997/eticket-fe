// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false,
  test: true,
  fileDomain:'http://192.168.2.100:9064/storage/',
  domain: 'http://192.168.2.100:9064/',
  gateway: 'http://192.168.2.100:9064/api/v1/',
  fe_domain: 'http://localhost:4200/',
  androidDownloadUrl: 'https://play.google.com/store/apps/details?id=com.mbamc.buildingcares',
  iosDownloadUrl: 'https://apps.apple.com/us/app/building-cares/id1602344331',
  is_department_sync:true,
  is_user_sync:false,
  is_customer_sync : false,
  is_product_sync:false,
  login_type: 'ticket', // sso or ticket
  sso_domain: 'https://mobifone.smart-sales.vn',
  pusher: {
    key: 'd08b0eea34b5e8ea25d7',
    cluster: 'ap1',
  },
  firebaseConfig :{ 
    apiKey: "AIzaSyDDeY8543hidVLyDvs_k303TggcexWrF4k", 
    authDomain: "mobifone-eticket.firebaseapp.com", 
    projectId: "mobifone-eticket", 
    storageBucket: "mobifone-eticket.appspot.com", 
    messagingSenderId: "891917527008", 
    appId: "1:891917527008:web:e032b1ac39f48bc0706d85", 
    measurementId: "G-BR52GF58G2" 
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.


