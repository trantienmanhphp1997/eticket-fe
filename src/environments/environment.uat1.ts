export const environment = {
  production: true,
  test: true,
  domain: 'http://10.16.150.205:8000/eticket',
  fe_domain:'http://10.16.150.205:8000/eticket',
  gateway: 'http://10.16.150.205:8000/eticket-api/api/v1/',
  //domain: 'https://mobifone.smart-sales.vn/eticket',
  //gateway: 'https://mobifone.smart-sales.vn/api/v1/',
  androidDownloadUrl: 'https://play.google.com/store/apps/details?id=com.mbamc.buildingcares',
  iosDownloadUrl: 'https://apps.apple.com/us/app/building-cares/id1602344331',
  is_department_sync:false,
  is_user_sync:false,
  is_customer_sync : false,
  is_product_sync:false,
  login_type: 'ticket', //'ticket', // sso or ticket
  sso_domain: 'http://10.16.150.205:8000',
};

export const environment = {
  production: true,
  test: true,
  domain: 'http://10.3.15.23:9052',
  gateway: 'http://10.3.15.23:9051/api/v1/',
  //domain: 'https://mobifone.smart-sales.vn/eticket',
  //gateway: 'https://mobifone.smart-sales.vn/api/v1/',
  androidDownloadUrl: 'https://play.google.com/store/apps/details?id=com.mbamc.buildingcares',
  iosDownloadUrl: 'https://apps.apple.com/us/app/building-cares/id1602344331',
  is_department_sync:true,
  is_user_sync:true,
  is_customer_sync : true,
  is_product_sync:true,
  login_type: 'ticket', // sso or ticket
  sso_domain: 'https://mobifone.smart-sales.vn',
};



ng build --configuration=uat --base-href=/eticket/
