export const environment = {
  production: true,
  test: true,
  fileDomain:'https://smartsales.mobifone.vn/eticket-api/storage/',
  domain: 'https://smartsales.mobifone.vn/eticket',
  fe_domain : 'https://smartsales.mobifone.vn/eticket/',
  gateway: 'https://smartsales.mobifone.vn/eticket-api/api/v1/',
  androidDownloadUrl: 'https://play.google.com/store/apps/details?id=com.mbamc.buildingcares',
  iosDownloadUrl: 'https://apps.apple.com/us/app/building-cares/id1602344331',
  is_department_sync:true,
  is_user_sync:false,
  is_customer_sync : false,
  is_product_sync:false,
  login_type: 'sso', // sso or ticket
  sso_domain: 'https://smartsales.mobifone.vn',
  pusher: {
    key: 'd08b0eea34b5e8ea25d7',
    cluster: 'ap1',
  },
  firebaseConfig :{
    apiKey: "AIzaSyDDeY8543hidVLyDvs_k303TggcexWrF4k",
    authDomain: "mobifone-eticket.firebaseapp.com",
    projectId: "mobifone-eticket",
    storageBucket: "mobifone-eticket.appspot.com",
    messagingSenderId: "891917527008",
    appId: "1:891917527008:web:e032b1ac39f48bc0706d85",
    measurementId: "G-BR52GF58G2"
  }
};

