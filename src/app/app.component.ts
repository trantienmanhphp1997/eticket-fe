import { AfterViewChecked, ChangeDetectorRef, Component } from '@angular/core';
import { Event, NavigationError, NavigationStart, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LANGUAGE_VI } from './shared/constants/base.constant';
import { LOCAL_STORAGE } from './shared/constants/local-session-cookies.constants';
import { LoadingService } from './shared/service/helpers/loading.service';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter , MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DatePipe } from "@angular/common";
61


export interface DateParse {
  dateInput: string;
}
export type DateDisplay = DateParse & {
  monthYearLabel?: string;
  dateA11yLabel?: string;
  monthYearA11yLabel?: string;
};
export class CustomDateFormat {
  private _parse: DateParse = {
    dateInput: "DD/MM/YYYY"
  };
  private _display: DateDisplay = {
    dateInput: "DD/MM/YYYY",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMM YYYY"
  };

  set parse(parse: DateParse) {
    this._parse = Object.assign({}, this._parse, parse);
  }

  get parse(): DateParse {
    return this._parse;
  }

  set display(display: DateDisplay) {
    this._display = Object.assign({}, this._display, display);
  }

  get display(): DateDisplay {
    return this._display;
  }

  updateDateFormat(parse: DateParse, display?: DateDisplay) {
    this.parse = parse;
    if (!display) {
      display = parse;
    }
    this.display = display;
  }
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {provide: MAT_DATE_FORMATS,   useClass: CustomDateFormat},
    {provide: MAT_DATE_LOCALE, useValue: 'vi-VI' }
  ],
})
export class AppComponent implements AfterViewChecked {

  currentUrl = '';
  defaultLanguage: string = LANGUAGE_VI;

  constructor(
    public router: Router,
    public loadingService: LoadingService,
    protected localStorage: LocalStorageService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {
    const self = this;
    this.router.events.subscribe((routerEvent: Event) => {
      let language = self.localStorage.retrieve('language');
      if (!language) {
        language = self.defaultLanguage;
        self.localStorage.store(LOCAL_STORAGE.LANGUAGE, self.defaultLanguage);
      }
      self.setLanguage(language);

      if (routerEvent instanceof NavigationStart) {
        self.currentUrl = routerEvent.url.substring(routerEvent.url.lastIndexOf('/') + 1);
      }

      if (routerEvent instanceof NavigationError && routerEvent.error.status === 404) {
        this.router.navigate(['/404']);
      }

      window.scrollTo(0, 0);
    });
    
  }

  setLanguage(language: string): void {
    this.translate.setDefaultLang(language);
    this.translate.use(language);
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

}
