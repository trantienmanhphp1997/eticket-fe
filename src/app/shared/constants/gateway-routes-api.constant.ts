export const SERVICE = {
  IAM: 'api',
  BUILDING: '/building/api',
  STORAGE: '/storage/api',
  SYSTEM: '/system/api',
  NOTIFICATION: '/notification/api',
  TICKET: '/ticket/api',
  SURVEY: '/survey/api',
};

export const MODULE = {
  COMPLAINT: 'complaint'
};
