export const FILE_SIZE_DEFAULT = 1024 * 1024; // 1Mb
export const FILE_SIZE = 15;
export const FILE_SIZE_MAIL = 20;
export const MAX_FILE_SIZE = FILE_SIZE_DEFAULT * FILE_SIZE;
export const MAX_FILE_MAIL_SIZE = FILE_SIZE_DEFAULT * FILE_SIZE_MAIL;
export const MAX_IMAGE_SIZE = 5;
export const LIMIT_FILE = 5;

export const OWNER_TYPE = {
  NOTIFICATION: 'NOTIFICATION',
  TICKET: 'TICKET',
  COMPLAINT: 'COMPLAINT',
};
export const FALLBACK =
  'assets/images/no-image.jpg';

export const NO_IMAGE =
  'assets/images/no-image.jpg';
