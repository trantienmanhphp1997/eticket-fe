export const TRIGGER_ORDERBY = [
    { value: 'created_at', label: "Ngày tạo" },
    { value: 'updated_at', label: "Ngày cập nhật gần nhất" },
    { value: 'status', label: "Trạng thái" }
]
export const TRIGGER_SORTBY = [
    { value: 'desc', label: "Sắp xếp từ cao tới thấp" },
    { value: 'asc', label: "Sắp xếp từ thấp tới cao" }
]
export const TRIGGER_KEY = {
    SET_TICKET_STATUS: 'set_ticket_status',
    SET_TICKET_PRIORITY: 'set_ticket_priority',
    SET_TICKET_CATEGORY: 'change_category_id',
    SET_TICKET_NOTE: 'set_ticket_note',
    SET_PRODUCT_ID:"change_product_id",
    CC_TO: 'cc_to',
    FORWARD_TO: 'forward_to',
    MAIL_STAFF: 'mail_to_staff',
    MAIL_GROUP: 'mail_to_group',
    MAIL_REQUEST: 'mail_to_requester',
    SMS_STAFF: 'sms_to_staff',
    SMS_GROUP: 'sms_to_group',
    SMS_REQUEST: 'sms_to_requester',
}
export const TRIGGER_EDIT_TYPE = {
    SINGER_SELECT: 'single-select',
    MULTI_SELECT: 'multiple-select',
    INPUT_TEXT: 'TextInput',
    MAIL_FORM:'mail_form',
    SMS_FORM:'sms_form',
}
export const TRIGGER_ACTION =[TRIGGER_KEY.SET_TICKET_STATUS,TRIGGER_KEY.SET_TICKET_PRIORITY,TRIGGER_KEY.SET_TICKET_CATEGORY,TRIGGER_KEY.SET_PRODUCT_ID]