export const WAITING = 'Cảnh báo';
export const NOTIFI = 'Thông báo';
export const ASSIGNEE = 'giao việc';
export const STATE_CHANGE = 'chuyển trạng thái';
export const USER_CHANGE = 'chuyển người xử lý';
export const DUE = 'đến hạn';
export const OUT_OF_DATE = 'quá hạn';


export const NOTIFICATION_STATUS = [
  ASSIGNEE,
  STATE_CHANGE,
  USER_CHANGE
];
export const WAITING_STATUS = [
  DUE,
  OUT_OF_DATE,
];


