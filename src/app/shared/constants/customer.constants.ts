export const CUSTOMER_TYPE = [
  {
    label: 'model.customer.individual',
    value: '1',
  },
  {
    label: 'model.customer.enterprise',
    value: '2',
  }
];

export const INTERNAL = 'INTERNAL';
export const CUSTOMER = 'CUSTOMER';

const LEASED_LABEL = 'model.customer.leased';
const DEPOSIT_LABEL = 'model.customer.deposit';
const RETURNED_LABEL = 'model.customer.returned';

export const LEASED = 'LEASED';
export const DEPOSIT = 'DEPOSIT';
export const RETURNED = 'RETURNED';

export const LEASING_STATUS_CREATE = [
  {
    label: LEASED_LABEL,
    value: LEASED,
  },
  {
    label: DEPOSIT_LABEL,
    value: DEPOSIT,
  },
];

export const LEASING_STATUS = [
  {
    label: LEASED_LABEL,
    value: LEASED,
  },
  {
    label: DEPOSIT_LABEL,
    value: DEPOSIT,
  },
  {
    label: RETURNED_LABEL,
    value: RETURNED,
  },
];
