export const VALIDATORS = {
  USERNAME: '^[A-Za-z][A-Za-z0-9_@.]{1,50}$',
  PASSWORD: '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\\^$*.\\[\\]{}\\(\\)?\\-“!@#%&/,><\\’:;|_~`])\\S{8,}$',
  BLANK: '(\\s){0,}\\S+.*(\\s){0,}',
  EMAIL: '^(\\s){0,}[a-zA-Z][a-zA-Z0-9_\\.\-]{1,50}@[a-zA-Z0-9\-_]{2,}(\\.[a-zA-Z0-9\]{2,4}){1,2}(\\s){0,}$',
  PHONE: '^(\\+[0-9]+[\\- \\.]*)?(\\([0-9]+\\)[\\- \\.]*)?([0-9][0-9\\- \\.]+[0-9])$',
  CODE: '^([A-Za-z0-9]+)$',
  NUMBER: '^[0-9]*$',
  SPACE: '^(?![\\s-])[\\S\\s-]+$',
  NAME: '^([a-zA-Z0-9._(),?:;"\'/&!ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ/-\\s]+?)*$',
  TAX: '^([0-9\\-]+)$',
  ADDRESS: '^[A-Za-z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ \\.\\[\\]\\{\\}\\-\\/\\,\\_]*$',
  DEPT_NAME: '^([a-zA-Z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ]+[" "]?)*$',
};

export const LENGTH_VALIDATOR = {
  CODECUS_MAX_LENGTH: {
    MAX: 12,
  },
  USERNAME_MAX_LENGTH: {
    MAX: 50,
  },
  USERNAME_MIN_LENGTH: {
    MIN: 4,
  },
  NAME_MAX_LENGTH: {
    MAX: 200,
  },
  NAME_DEPARTMENT_MAX_LENGTH: {
    MAX: 40,
  },
  PASSWORD_MIN_LENGTH: {
    MIN: 8
  },
  PASSWORD_MAX_LENGTH: {
    MAX: 8
  },
  CODE_MAX_LENGTH: {
    MAX: 100,
  },
  PHONE_MAX_LENGTH: {
    MAX: 20,
  },
  EMAIL_MAX_LENGTH: {
    MAX: 50,
  },
  ENUM_MAX_LENGTH: {
    MAX: 20,
  },
  DESC_MAX_LENGTH: {
    MAX: 1000,
  },
  CONTENT_MAX_LENGTH: {
    MAX: 2000,
  },
  NOTE_MAX_LENGTH: {
    MAX: 1000,
  },
  TITLE_MAX_LENGTH: {
    MAX: 200,
  },
  ADDRESS_MAX_LENGTH: {
    MAX: 250,
  },
  ID_MIN_LENGTH: {
    MIN: 1
  },
  ID_MAX_LENGTH: {
    MAX: 36
  },
  VALUE_MAX_LENGTH: {
    MAX: 200
  },
  IDS_MAX_LENGTH: {
    MAX: 500
  },
  DES_MAX_LENGTH: {
    MAX : 500
  },
  BIRTH_MAX_LENGTH: {
    MAX: 100
  },
  GENDER_MAX_LENGTH: {
    MAX: 20
  },
  STATUS_MAX_LENGTH: {
    MAX: 20
  },
  OPINION_MAX_LENGTH: {
    MAX: 300
  },
  LABEL_MAX_LENGTH: {
    MAX: 200
  },
  TICKET_TITLE_LENGTH: {
    MAX: 200
  },
  TICKET_DESCRIPTION_LENGTH: {
    MAX: 500
  },
  TICKET_DESCRIPTION_LENGTH2: {
    MAX: 5000
  },
  SMS_CONTENT: {
    MAX: 70
  }
};
export const VALIDATOR_MESSAGE = [
  {
    key: 'required',
    label: 'error.required',
  },
  {
    key: 'minlength',
    label: 'error.minlength',
  },
  {
    key: 'maxlength',
    label: 'error.maxlength',
  },
  {
    key: 'pattern',
    label: 'error.pattern',
  },
  {
    key: 'email',
    label: 'error.email',
  },
  {
    key: 'min',
    label: 'error.min',
  },
  {
    key: 'max',
    label: 'error.max',
  }
];
