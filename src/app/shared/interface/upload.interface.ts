export interface Upload {
    progress: number
    state: 'pending' | 'active' | 'success' | 'exception',
    mediaBody?: any
}