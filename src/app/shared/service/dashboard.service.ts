import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AbstractService, EntityResponseType} from '@shared/service/common/abstract.service';
import {Observable} from 'rxjs';

import { FileService } from './file.service';

@Injectable({
  providedIn: 'root',
})
export class DashboardService extends AbstractService {


  constructor(
    protected http: HttpClient,
    // private fileService: FileService
  ) {
    super(http);
  }

  // findAll(loading = false): Observable<EntityResponseType> {
  //   return this.http
  //     .get<IBaseResponse>(`${this.resourceUrl}`, {
  //       observe: 'response', headers: CommonUtil.headers(loading)
  //     });
  // }

    /**
   * 
   * @param params : pageIndex, pageSize, parentId, searchKeyword
   * @param loading 
   * @returns 
   */
  getStastus(param:any,loading = true): Observable<EntityResponseType<any>>{
    return super.get<any>(`get-ticket-status-dashboard?quarter=${param?.quarter}&date=${param?.month}&from_date=${param?.from_date}&to_date=${param?.to_date}&year=${param?.year}`,{loading});
  }
  getDashboardProduct(param:any,loading = true): Observable<EntityResponseType<any>>{
    const params ={
      quarter:param?.quarter,
      date:param?.month,
      from_date:param?.from_date,
      to_date:param?.to_date,
      year: param?.year
    }
    return super.post<any>(`get-rate-ticket-reach-SLA-by-product?quarter=${param?.quarter}&date=${param?.month}&from_date=${param?.from_date}&to_date=${param?.to_date}&year=${param?.year}`,{loading});
  }
  getDashboardCountProduct(param:any,loading = true): Observable<EntityResponseType<any>>{
    return super.post<any>(`get-tickets-by-products?limit=0&department_id=${param?.department_id}&quarter=${param?.quarter}&date=${param?.month}&from_date=${param?.from_date}&to_date=${param?.to_date}&product_id=${param?.product_id}&year=${param?.year}`,{loading});
  }
  getDashboardWeek(param:any,loading = true): Observable<EntityResponseType<any>>{
    let period_time=param?.period_time ? `&period_time=${param?.period_time}`:'';
    return super.get<any>(`get-tickets-by-week?${period_time}`,{ loading});
  }

  getDashboardTicketPriority(param:any,loading = true): Observable<EntityResponseType<any>>{
   
    return super.post<any>(`get-rate-ticket-by-priotity?quarter=${param?.quarter}&date=${param?.month}&from_date=${param?.from_date}&to_date=${param?.to_date}&year=${param?.year}`,{ loading});
  }

  getDashboardTicketSLAPriority(param:any,loading = true): Observable<EntityResponseType<any>>{
    
    return super.post<any>(`get-rate-ticket-violate-SLA-by-priority?department_id=${param?.department_id}&quarter=${param?.quarter}&date=${param?.month}&from_date=${param?.from_date}&to_date=${param?.to_date}&product_id=${param?.product_id}&year=${param?.year}`,{ loading});
  }
  getDashboardCountTicketByYear(param:any,loading = true): Observable<EntityResponseType<any>>{
   
    return super.post<any>(`get-count-tickets-by-year?type=${param?.type}&department_id=${param?.department_id}&quarter=${param?.quarter}&date=${param?.month}&from_date=${param?.from_date}&to_date=${param?.to_date}&product_id=${param?.product_id}&year=${param?.year}`,{ loading});
  }
  
}
