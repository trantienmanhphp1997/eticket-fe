import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import { AbstractService, EntityResponseType } from './common/abstract.service';
import { Observable } from 'rxjs';
import { FileService } from './file.service';
import { environment } from '@env/environment';
@Injectable({
  providedIn: 'root'
})
export class ReportTicketService  extends AbstractService{
  constructor(
    protected http : HttpClient,
    private fileService: FileService,
  ) {
    super(http);
   }
   getAll(params: any, fillterDate: any, loading = true): Observable<EntityResponseType<any[]>>{
    let  orderBy = params.orderBy ? params.orderBy.replaceAll(',', ';') : 'created_at';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',', ';') : 'desc';
    let options = {
      limit: params.pageSize,
      page: params.pageIndex,
      search: params.keyword,
      fromDate: fillterDate?.from_date,
      toDate: fillterDate?.to_date,
      product_ids: params?.product_id,
      department_id:params?.department_id,
      user_ids:params?.user_ids,
      category_ids:params?.category_ids,
      source_ids:params?.source_ids,
      state_ids: params?.state_ids,
      priority_ids: params?.priority_ids,
      created_by_department_ids: params?.created_by_department_ids,
    };
   let  sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '';
    return super.get<any[]>(`ticket-handle-report?${sort}`, { params: options, loading });
  }
  getAllOutdate(params: any, fillterDate: any, loading = true): Observable<EntityResponseType<any[]>>{
    let  orderBy = params.orderBy ? params.orderBy.replaceAll(',', ';') : 'created_at';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',', ';') : 'desc';
    let options = {
      limit: params.pageSize,
      page: params.pageIndex,
      search: params.keyword,
      fromDate: fillterDate?.from_date,
      toDate: fillterDate?.to_date,
      product_ids: params?.product_id,
      department_id:params?.department_id,
      user_ids:params?.user_ids,
      category_ids:params?.category_ids,
      source_ids:params?.source_ids,
      state_ids: params?.state_ids,
      priority_ids: params?.priority_ids,
      created_by_department_ids: params?.created_by_department_ids,
    };
   let  sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '';
    return super.get<any[]>(`ticket-handle-expired?${sort}`, { params: options, loading });
  }
  exportTemplate(fillter: any): any {
    let options = {
      search: fillter.keyword,
      fromDate: fillter?.from_date,
      toDate: fillter?.to_date,
      product_ids: fillter?.product_id,
      department_id:fillter?.department_id,
      user_ids:fillter?.user_ids,
      category_ids:fillter?.category_ids,
      source_ids:fillter?.source_ids,
      state_ids: fillter?.state_ids,
      priority_ids: fillter?.priority_ids,
      created_by_department_ids: fillter?.created_by_department_ids,
    };
    let fillterAll = `orderBy=created_at&sortedBy=desc&search=${options.search}&toDate=${options.toDate}&fromDate=${options.fromDate}&product_ids=${options.product_ids}&department_id=${options.department_id}&user_ids=${options.user_ids}&category_ids=${options.category_ids}&source_ids=${options.source_ids}&state_ids=${options.state_ids}&priority_ids=${options.priority_ids}&created_by_department_ids=${options.created_by_department_ids}`
    this.fileService.downloadFileTemplateRTickets(`${environment.gateway}ticket-handle-report/export?type=execute&${fillterAll}`)
  }
  exportTemplate2(fillter: any): any {
    let options = {
      search: fillter.keyword,
      fromDate: fillter?.from_date,
      toDate: fillter?.to_date,
      product_ids: fillter?.product_id,
      department_id:fillter?.department_id,
      user_ids:fillter?.user_ids,
      category_ids:fillter?.category_ids,
      source_ids:fillter?.source_ids,
      state_ids: fillter?.state_ids,
      priority_ids: fillter?.priority_ids,
      created_by_department_ids: fillter?.created_by_department_ids,
    };
    let fillterAll = `orderBy=created_at&sortedBy=desc&search=${options.search}&toDate=${options.toDate}&fromDate=${options.fromDate}&product_ids=${options.product_ids}&department_id=${options.department_id}&user_ids=${options.user_ids}&category_ids=${options.category_ids}&source_ids=${options.source_ids}&state_ids=${options.state_ids}&priority_ids=${options.priority_ids}&created_by_department_ids=${options.created_by_department_ids}`
    this.fileService.downloadFileTemplateRTicketsOutdate(`${environment.gateway}ticket-handle-report/export?type=expired&${fillterAll}`)
  }
}
