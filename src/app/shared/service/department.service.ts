import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IBuilding } from '@shared/models/building.model';
import { AbstractService, EntityResponseType } from '@shared/service/common/abstract.service';
import { Observable } from 'rxjs';
import { SERVICE } from '../constants/gateway-routes-api.constant';
import { FileService } from './file.service';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root',
})
export class DepartmentService extends AbstractService {
  public resourceUrl = SERVICE.IAM;

  constructor(protected http: HttpClient,
    private fileService: FileService
    ) {
    super(http);
  }

  /**
   * 
   * @param params : pageIndex, pageSize, parentId, searchKeyword
   * @param loading 
   * @returns 
   */
  getDepartments(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let options = {
      parentIds: params.parentId,
      limit: params.pageSize,
      page: params.pageIndex,
      search: params.keyword,
      orderBy: params.orderBy ? params.orderBy.replaceAll(',', ';') : '',
      sortBy: params.sortBy ? params.sortBy.replaceAll(',', ';') : '',
      data_context:params.data_context
    }
    
    return super.get<any[]>(`departments`, { params: options, loading });
  }

  /**
   * 
   * @param params : pageIndex, pageSize, parentId, searchKeyword
   * @param loading 
   * @returns 
   */
  getAllDepartments(params:any,loading = false): Observable<EntityResponseType<any[]>> {
    let options = {
      excludeChild:params.excludeChild,
      data_context:params.data_context,
      selectedId:params?.selectedId,
      search: params?.search
    }
    return super.get<any[]>(`departments/all/tree?limit=0`,{params:options,loading });
  }

  /**
   * 
   * @param department 
   * @param loading 
   * @returns 
   */
  create(department: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`departments`, department, { loading });
  }

  /**
   * 
   * @param department 
   * @param loading 
   * @returns 
   */
  update(departmentId: any, department: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`departments/${departmentId}`, department, { loading });
  }

  delete(departmentIds: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`departments/multipledelele`, { departmentIds: departmentIds }, { loading });
  }
  find(departmentIds: any, loading = false): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`departments/${departmentIds}`, { loading });
  }
  export(params: any = null): any {
    let filterParent = params.parentIds ? `&parentIds=${params.parentIds}`:'';
    this.fileService.downloadFileDepartment(`${environment.gateway}departments/export?${filterParent}`)
  }
  getByRole(): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`get-departments-by-role`);
  }
  getAlltree(loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`get-departments-by-role-v2?limit=0&status=1`, { loading })
  }
}
