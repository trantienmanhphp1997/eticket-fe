import { Injectable } from '@angular/core';
import { AbstractService, EntityResponseType } from './common/abstract.service';
import { HttpClient } from '@angular/common/http';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProductCategoryService extends AbstractService{
  public resourceUrl = SERVICE.TICKET + '/tickets';
  constructor(
    protected http : HttpClient
  ) {
    super(http);
   }
   getAll(loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`productcategories?limit=0`, { loading })
  }
  create(productcategories: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`productcategories`,productcategories, { loading })
  }
  update(id: any, productcategories : any , loading = true): Observable<EntityResponseType<any[]>>{
    return super.patch<any[]>(`productcategories/${id}`, productcategories , { loading })
  }
  multiDelete(id: any , loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`productcategories/multipledelete`, { productCategoryIds : id }, {loading})
  }
  existed(code: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`productcategories/is-existed`, {code: code} , { loading })
  }
  getAllServiceTree(excludeChild:any,loading = true): Observable<EntityResponseType<any[]>>{
    var exclude =``;
    if(excludeChild){
      exclude =`&excludeChild=${excludeChild}`
    }
    return super.get<any[]>(`productcategories?limit=0${exclude}`, { loading })
  }
}
