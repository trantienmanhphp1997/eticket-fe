import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AbstractService, EntityResponseType } from './common/abstract.service';

@Injectable({
  providedIn: 'root'
})
export class ContactService extends AbstractService {

  constructor(protected http: HttpClient) {
    super(http);
  }
  create(contact: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`contacts`, contact, { loading });
  }
  update(contactId: any, contact: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`contacts/${contactId}`, contact, { loading });
  }
  getContactByCustomerId(customer_id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`contacts?customer_id=${customer_id}&limit=0&orderBy=name&sortedBy=asc`, { loading });
  }
  getContactByCustomerIdInCustomer(customer_id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`contacts?customer_id=${customer_id}&limit=0`, { loading });
  }
  getContactByCustomerIdTK(customer_id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`contacts?customer_id=${customer_id}&limit=0&orderBy=name&sortedBy=asc`, { loading });
  }
  delete(contactId: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.delete<any[]>(`contacts/${contactId}`, { loading });
  }
}