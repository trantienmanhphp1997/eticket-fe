import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AbstractService, EntityResponseType } from './common/abstract.service';

@Injectable({
  providedIn: 'root'
})
export class WorkFlowService extends AbstractService {

  constructor(protected http: HttpClient) {
    super(http);
  }

  getWorkFlows(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let options = {
      limit: params.pageSize,
      page: params.pageIndex,
      search: params.keyword,
      orderBy: params.orderBy ? params.orderBy.replaceAll(',', ';') : '',
      sortBy: params.sortBy ? params.sortBy.replaceAll(',', ';') : '',
      sort : params.orderBy && params.sortBy ? `&orderBy=${params.orderBy}&sortedBy=${params.sortBy}` : '&orderBy=order&sortedBy=asc'
    }
    let filterDepartment = params.department_id ? `&departmentId=${params.department_id}`:'';
    let filterServiceProduct = params.product_id ? `&productId=${params.product_id}`:'';
    return super.get<any[]>(`workflows?include=steps&${filterDepartment}${filterServiceProduct}`, { params: options, loading });
  }

  getWorkFlowsDetail(id: any, loading = true): Observable<EntityResponseType<any>> {
    return super.get<any>(`workflows/${id}?include=steps,transactions,stepConfigs`, { loading });
  }

  getStepConfigInfo (stepId: any, loading = true): Observable<EntityResponseType<any>> {
    return super.get<any>(`wfstepconfigs/find_by_step_id/${stepId}?include=department`, { loading });
  }
  createWorkFlowInfo(workFlowInfo: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`workflows`, workFlowInfo, { loading });
  }

  /**
  * 
  * @param workFLowId 
  * @param loading 
  * @returns 
  */
  update(workFLowId: any, workFlow: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`workflows/${workFLowId}?include=steps,transactions,stepConfigs`, workFlow, { loading });
  }
  getContactByCustomerId(customer_id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`contacts?customer_id=${customer_id}`, { loading });
  }
  delete(workId: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.delete<any[]>(`workflows/${workId}`, { loading });
  }
  deleteMulti(workId: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`workflows/multipledelete`,workId, { loading });
  }
  updateOrder(params:any,loading=true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`workflows/update-order`,params, { loading });
  }
  clone(workFLowId: any, workFlow: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`workflows/${workFLowId}/clone`, workFlow, { loading });
  }
}