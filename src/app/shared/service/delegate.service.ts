import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { idText } from 'typescript';
import { AbstractService, EntityResponseType } from './common/abstract.service';

@Injectable({
  providedIn: 'root'
})
export class DelegateService extends AbstractService {

  constructor(protected http: HttpClient) {
    super(http);
  }
  create(delegate: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`delegates`, delegate, { loading });
  }
  getDeledateByCustomerId(id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`delegates/${id}`, { loading });
  }
  update(delegateId: any, delegate: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`delegates/${delegateId}`, delegate, { loading });
  }
  delete(delegateId: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.delete<any[]>(`delegates/${delegateId}`, { loading });
  }
}