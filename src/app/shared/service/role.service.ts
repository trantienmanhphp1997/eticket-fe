import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import {
  AbstractService,
  EntityResponseType
} from '@shared/service/common/abstract.service';
import { Observable } from 'rxjs';
import { RoleRequest } from '../models/request/role-request.model';
import { IRole, Role } from '../models/role.model';
import { FileService } from './file.service';

@Injectable({
  providedIn: 'root',
})
export class RoleService extends AbstractService {
  public resourceUrl = 'roles';

  constructor(protected http: HttpClient,
    private fileService: FileService
    ) {
    super(http);
  }

  // findAll(loading = false): Observable<EntityResponseType<IRole>> {
  //   return super.get<IRole>(`${this.resourceUrl}`, {loading});
  // }

  // findCachedAll(loading = false): Observable<EntityResponseType<IRole>> {
  //   return super.get<IRole>(`${this.resourceUrl}/cached`, {loading});
  // }
  getRole(params: any, loading = false): Observable<EntityResponseType<any>> {
    const param ={search:params.search};
    // let searchSubString = params.search ? `&search=${params.search}` : '';
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=created_at&sortedBy=desc';
    return super.get<any>(`${this.resourceUrl}?limit=${params.pageSize}&page=${params.pageIndex}${sort}&include=${params.includes}`, {params:param, loading });
  }
  getRoleHasUserGroup(param :any,loading=true):Observable<EntityResponseType<any>> {
    let userGroup = param.userGroup ? `&userGroup=${param.userGroup}` : '';
    return super.get<any>(`roles?${userGroup}`, { loading });
  }
  create(role: Role, loading = false): Observable<EntityResponseType<IRole>> {
    return super.post<IRole>(`${this.resourceUrl}`, role, { loading });
  }

  update(
    role: Role,
    id: any,
    loading = false
  ): Observable<EntityResponseType<IRole>> {
    return super.patch<IRole>(`${this.resourceUrl}/${id}`, role, {
      loading,
    });
  }

  delete(id: any, loading = false): Observable<EntityResponseType<IRole>> {
    return super.delete<IRole>(`${this.resourceUrl}/${id}`,{ loading });
  }

  findById(id: any, loading = false): Observable<EntityResponseType<IRole>> {
    return super.get<IRole>(`${this.resourceUrl}/${id}`, { loading });
  }

  active(
    roleId: any,
    loading = false
  ): Observable<EntityResponseType<boolean>> {
    return super.post<boolean>(
      `${this.resourceUrl}/${roleId}/active`,
      {},
      { loading }
    );
  }

  inactive(
    roleId: any,
    loading = false
  ): Observable<EntityResponseType<boolean>> {
    return super.post<boolean>(
      `${this.resourceUrl}/${roleId}/inactive`,
      {},
      { loading }
    );
  }

  // Api lấy hết list role
  search(
    params?: RoleRequest,
    loading = false
  ): Observable<EntityResponseType<IRole[]>> {
    return super.get<IRole[]>(`${this.resourceUrl}`, { params, loading });
  }

  searchAutoComplete(
    params?: RoleRequest,
    loading = false
  ): Observable<EntityResponseType<IRole[]>> {
    return super.get<IRole[]>(`${this.resourceUrl}/auto-complete`, {
      params,
      loading,
    });
  }
  permissionOfRole(roleId: any, loading = false): Observable<EntityResponseType<IRole>> {
    return super.get<IRole>(`${this.resourceUrl}/${roleId}/permissions`, { loading });
  }
  multiDelete(roleIds:any, loading = true): Observable<EntityResponseType<IRole>>{
    return super.post<IRole>(`${this.resourceUrl}/multipledelete`, {roleIds: roleIds}, { loading });
  }
  checkExist(params:any, loading = true): Observable<EntityResponseType<IRole>>{
    return super.post<IRole>(`${this.resourceUrl}/is-existed`, params, { loading });
  }
  export(): any {
    this.fileService.downloadFileRole('http://192.168.2.100:9052/api/v1/departments/export')
  }
}