import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "@env/environment";
import { Observable, Subject } from "rxjs/Rx";
import { AbstractService, EntityResponseType } from "./common/abstract.service";
import Pusher from "pusher-js";
import { LOCAL_STORAGE } from "@shared/constants/local-session-cookies.constants";
import { LocalStorageService } from "ngx-webstorage";


@Injectable()

export class NotificationService extends AbstractService{

  pusher: any;
  channel: any;
  constructor(
    private localStorage: LocalStorageService,
    protected http: HttpClient
    ) {
    super(http);
    this.pusher = new Pusher(environment.pusher.key, {
      cluster: environment.pusher.cluster,
      forceTLS: true
    });
    // Pusher.logToConsole = true;
    const profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE); 
    this.channel = this.pusher.subscribe(`channel-${profile?.id}`);
  }
  getNotifi(param:any): Observable<EntityResponseType<any[]>> {
    let limit = param?.limit ?`&limit=${param?.limit}`:'';
    let page = param?.page ?`&page=${param?.page}`:'';
    return super.get<any[]>(`notifications?${limit}${page}`, { loading :false});
  }
  getNotifiAll(): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`notifications/mark-all-read`, { loading :true});
  }
  readNotifi(id:any): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`notifications/mark-read/${id}`, { loading :true});
  }
  sendMessage(msg: string) {
    // this.socket.emit('sendMessage', { message: msg });
  }

}
