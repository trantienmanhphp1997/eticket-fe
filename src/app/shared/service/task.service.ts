import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import { AbstractService, EntityResponseType } from '@shared/service/common/abstract.service';
import { Observable } from 'rxjs';
import { IChangePassword } from '../models/request/change-password-request.model';

@Injectable({
    providedIn: 'root',
})

export class TaskService extends AbstractService {
    constructor(
        protected http: HttpClient,
    ) {
        super(http);
    }

    getTasks(params:any, loading=true): Observable<EntityResponseType<any>>{
        let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
        let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
        let searchSubString = params.keyword ? `&search=${params.keyword}` : '';
        let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '';
        return super.get<any[]>(`tasks?limit=${params.pageSize}&page=${params.pageIndex}${sort}${searchSubString}`,{loading});
    }
    create(params :any,loading= true): Observable<EntityResponseType<any>>{
        return super.post<any[]>(`tasks/`,params, {loading} );
    }
    getTaskById(taskId :any,loading=true): Observable<EntityResponseType<any>>{
        return super.get<any[]>(`tasks/${taskId}`, {loading} );
    }
    updateTask(taskId:any,params:any,loading=true): Observable<EntityResponseType<any>>{
        return super.patch<any[]>(`tasks/${taskId}`,params, {loading} );
    }
    getTaskRelated(taskId :any,param :any): Observable<EntityResponseType<any>>{
        let ticketId = param.ticketId ? `getrelated?ticketId=${param.ticketId}`:'';
        return super.get<any[]>(`tasks/${taskId}/${ticketId}`);
    }

}