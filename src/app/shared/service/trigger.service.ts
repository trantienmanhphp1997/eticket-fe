import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AbstractService, EntityResponseType } from './common/abstract.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TriggerService extends AbstractService{

  constructor(protected http: HttpClient) {
    super(http);
  }

  getTrigger(params:any, loading=true): Observable<EntityResponseType<any>>{
    const param ={search:params.search};
    // let searchSubString = params.search ? `&search=${params.search}` : '';
    let include = params.include ?  `&include=${params.include}`:'';
    let orderBy = params.orderBy ? params.orderBy : '';
    let sortBy = params.sortBy ? params.sortBy  : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=order&sortedBy=asc';
    return super.get<any[]>(`triggers?limit=${params.pageSize}&page=${params.pageIndex}${sort}${include}`,{params:param,loading});
  }
  createTrigger(triggers:any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`triggers`, triggers, { loading });
  }
  updateTrigger(id:any,data: any, loading = false): Observable<EntityResponseType<any>> {
    return super.patch<any>(`triggers/${id}`, data, { loading });
  }
  deleteTrigger(id: any, loading = true): Observable<EntityResponseType<any>> {
    return super.delete<any>(`triggers/${id}`, { loading });
  }
  multiDeleteTrigger(id: any , loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`triggers/multipledelete`, { triggerIds : id }, {loading})
  }

  getAllConfigTrigger(params:any, loading=true): Observable<EntityResponseType<any>>{
    let include = params.include ?  `&include=${params.include}`:'';
    return super.get<any[]>(`appconfigs/triggers?page=${params.pageIndex}${include}`,loading);
  }
  getOptionsTriggerConfig(params:any,loading=false): Observable<EntityResponseType<any>>{
    let searchName= params?.searchName ? `&search=${params.searchName}` : '';
    let limit= params?.limit || 0;
    return super.get<any[]>(`${params.urlApi}?limit=${limit}${searchName}`,{loading});
  }
  findTriggerId(id:any, loading=false): Observable<EntityResponseType<any>>{
    return super.get<any[]>(`triggers/${id}`, { loading });
  }
  updateOrderTrigger(params: any, loading = false): Observable<EntityResponseType<any>> {
    return super.post<any>(`triggers/update-order`, params, { loading });
  }
}
