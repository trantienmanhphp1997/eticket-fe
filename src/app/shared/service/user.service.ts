import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {SERVICE} from '@shared/constants/gateway-routes-api.constant';
import {IUserLevel} from '@shared/models/user-level.models';
import {AbstractService, EntityResponseType} from '@shared/service/common/abstract.service';
import {Observable} from 'rxjs';

import {IChangePassword} from '../models/request/change-password-request.model';
import {IUserRequest} from '../models/request/user-request.model';
import {IUser, User} from '../models/user.model';
import { FileService } from './file.service';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService extends AbstractService {

  public resourceUrl = SERVICE.IAM + '/users';

  constructor(
    protected http: HttpClient,
    private fileService: FileService
  ) {
    super(http);
  }

  // findAll(loading = false): Observable<EntityResponseType> {
  //   return this.http
  //     .get<IBaseResponse>(`${this.resourceUrl}`, {
  //       observe: 'response', headers: CommonUtil.headers(loading)
  //     });
  // }

    /**
   * 
   * @param params : pageIndex, pageSize, parentId, searchKeyword
   * @param loading 
   * @returns 
   */
  getUser( param :any,loading = true): Observable<EntityResponseType<IUser[]>>{
    let include = param.include ?  `&include=${param.include}`:'';
    let fitlerbyParentPath = param.departmentIds ? `&departmentIds=${param.departmentIds}&getAll=true` : '';
    const params ={search:param.keyword};
    // let searchSubString = param.keyword ? `&search=${param.keyword}` : '';
    let filterDepartment = param.departmentId ? `&departmentId=${param.departmentId}`:'';
    let filterGroupUser = param.userGroupId ? `&userGroupId=${param.userGroupId}`:'';
    let filterStatus = param.status ? `&status=${param.status}`:'';
    let orderBy = param.orderBy ? param.orderBy.replaceAll(',',';') : '';
    let sortBy = param.sortBy ? param.sortBy.replaceAll(',',';')  : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '';
    let userids= param.excludeUser?`&excludeUser=${param.excludeUser}`:'';
    return super.get<IUser[]>(`users?limit=${param.pageSize}&page=${param.pageIndex}${include}${filterDepartment}${filterStatus}${filterGroupUser}${fitlerbyParentPath}${sort}${userids}`,{params:params, loading});
  }
  getAll(params:any,loading = true): Observable<EntityResponseType<IUser[]>>{
    return super.get<IUser[]>(`users`,{params,loading});
  }
  search(params?: IUserRequest, loading = false): Observable<EntityResponseType<IUser[]>> {
    return super.get<IUser[]>(`${this.resourceUrl}/search`, {params, loading});
  }
  /**
   * 
   * @param user 
   * @param loading 
   * @returns 
   */
  create(user: any, loading = true): Observable<EntityResponseType<IUser>> {
    return super.post<IUser>(`admins`,user, loading = true);
  }
  getUserOfGroup(params:any, groupids:any,loading=true): Observable<EntityResponseType<IUser>> {
    // let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
    // let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
    // let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '';
    return super.post<IUser>(`usergroups/refs?limit=${params.pageSize}&page=${params.pageIndex}`,{group_ids:groupids,loading})
  }

  createLdap(user: User, loading = false): Observable<EntityResponseType<IUser>> {
    return super.post<IUser>(`${this.resourceUrl}/customer`, user, {loading});
  }

  createContact(user: User, loading = false): Observable<EntityResponseType<IUser>> {
    return super.post<IUser>(`${this.resourceUrl}/contact`, user, {loading});
  }

    /**
   * 
   * @param userId 
   * @param loading 
   * @returns 
   */
  update(userId:any, user:any, loading = true): Observable<EntityResponseType<IUser>> {
    return super.patch<IUser>(`users/${userId}?include=avatarProfile`, user, loading = true);
  }

  delete(userId: any, loading = true): Observable<EntityResponseType<IUser>> {
    return super.post<IUser>(`users/multipledelete`, {userIds: userId},loading = true);
  }

  find(id: any, loading = false): Observable<EntityResponseType<IUser>> {
    return super.get<IUser>(`users/${id}?include=avatarProfile`, {loading});
  }

  // deleteUsers(ids: any, loading = false): Observable<EntityResponseType<Void>> {
  //   return super.post<Void>(`${this.resourceUrl}/delete-by-ids`, ids, {loading});
  // }

  active(userId: any, loading = false): Observable<EntityResponseType<boolean>> {
    return super.post<boolean>(`${this.resourceUrl}/${userId}/active`, {}, {loading});
  }

  inactive(userId: any, loading = false): Observable<EntityResponseType<boolean>> {
    return super.post<boolean>(`${this.resourceUrl}/${userId}/inactive`, {}, {loading});
  }

  changePassword(userId: any, params: IChangePassword, loading = true): Observable<EntityResponseType<IUser>> {
    return super.post<IUser>(`${this.resourceUrl}/${userId}/change-password`, params, {loading});
  }

  searchUsersAutoComplete(params?: any, loading = false): Observable<EntityResponseType<IUser[]>> {
    return super.get<IUser[]>(`${this.resourceUrl}/auto-complete`, {params, loading});
  }

  findCustomersByBuildings(ids: { ids: string[] }, loading = false): Observable<EntityResponseType<IUser[]>> {
    return super.post<IUser[]>(`${this.resourceUrl}/find-customer-by-building-ids`, ids, {loading});
  }

  findUserLevel(loading = false): Observable<EntityResponseType<IUserLevel[]>> {
    return super.get<IUserLevel[]>(`${this.resourceUrl}/user-level`, {loading});
  }

  findByUserIds(ids?: string[], loading = false): Observable<EntityResponseType<IUser[]>> {
    return super.post<IUser[]>(`${this.resourceUrl}/find-by-ids`, {ids}, {loading});
  }
  export(params:any): any {
    let filterDepartment = params.departmentId ? `&departmentId=${params.departmentId}`:'';
    let filterGroupUser = params.userGroupId ? `&userGroupId=${params.userGroupId}`:'';
    let filterStatus = params.status ? `&status=${params.status}`:'';
    this.fileService.downloadFileUser(`${environment.gateway}users/export?${filterDepartment}${filterGroupUser}${filterStatus}`)
  }
}
