import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AbstractService, EntityResponseType } from './common/abstract.service';
import { FileService } from './file.service';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends AbstractService {

  constructor(protected http: HttpClient,
    private fileService: FileService) {
    super(http);
  }

  getCustomer(params: any, loading = true): Observable<EntityResponseType<any>> {

    // let filterServiceProduct = params.product_id ? `${params.product_id}` : '';
    // let data = {
    //   limit: params.pageSize,
    //   page: params.pageIndex,
    //   search: params.search,
    //   include: params.include,
    //   orderBy: params.orderBy ? params.orderBy.replaceAll(',', ';') : '',
    //   sortBy: params.sortBy ? params.sortBy.replaceAll(',', ';') : '',
    //   sort: params.orderBy && params.sortBy ? `&orderBy=${params.orderBy}&sortedBy=${params.sortBy}` : '&orderBy=order&sortedBy=asc',
    //   product_id: filterServiceProduct
    // }
    let param ={search: params?.search};
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=created_at&sortedBy=desc';
    let include = params.include ? `&include=${params.include}`:'';
    let filterServiceProduct = params.product_id ? `&product_id=${params.product_id}`:'';

    return super.get<any[]>(`customers?limit=${params.pageSize}&page=${params.pageIndex}${include}${filterServiceProduct}${sort}`,{params:param, loading});
  }
  create(customer: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`customers`, customer, { loading });
  }
  update(customerId: any, customer: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`customers/${customerId}`, customer, { loading });
  }
  delete(customerId: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.delete<any[]>(`customers/${customerId}`, { loading });
  }
  multiDelete(customerIds: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`customers/multipledelete`, { customerIds: customerIds }, { loading });
  }
  checkExist(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`customers/is-existed`, params, { loading });
  }
  export(param:any): any {
    let filterServiceProduct = param.product_id ? `&product_id=${param.product_id}`:'';
    this.fileService.downloadFileCustomer(`${environment.gateway}customers/export?${filterServiceProduct}`)
  }
}
