import { HttpClient, HttpErrorResponse, HttpEvent, HttpEventType, HttpProgressEvent, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_API } from '@shared/constants/base.constant';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import { Upload } from '@shared/interface/upload.interface';
import { AbstractService } from '@shared/service/common/abstract.service';
import { Observable, throwError } from 'rxjs';
import { catchError, scan, distinctUntilChanged } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})

export class MediaService extends AbstractService {
    public resourceUrl = SERVICE.IAM + '/users';
    constructor(
        protected http: HttpClient,
    ) {
        super(http);
    }

    upload(name: string, profileImage: any): Observable<Upload> {
        var formData: any = new FormData();
        formData.append('key', name);
        formData.append('files[]', profileImage);
        const initialState: Upload = { state: 'pending', progress: 0 }
        return this.http
            .post(`${BASE_API}media/`, formData, {
                reportProgress: true,
                observe: 'events',
            }).pipe(scan(this.reduceState, initialState),
                distinctUntilChanged(
                    (a: any, b: any) => a.state === b.state && a.progress === b.progress
                ),catchError(this.errorMgmt))
    }
    errorMgmt(error: HttpErrorResponse) {
        console.log(error);

        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(() => {
            return errorMessage;
        });
    }
    isHttpResponse<T>(event: HttpEvent<T>): event is HttpResponse<T> {
        return event.type === HttpEventType.Response;
    }

    isHttpProgressEvent(
        event: HttpEvent<unknown>
    ): event is HttpProgressEvent {
        return (
            event.type === HttpEventType.DownloadProgress ||
            event.type === HttpEventType.UploadProgress
        );
    }

    reduceState = (upload: Upload, event: HttpEvent<unknown>): Upload => {
        if (this.isHttpProgressEvent(event)) {
            return {
                progress: event.total
                    ? Math.round((100 * event.loaded) / event.total)
                    : upload.progress,
                state: 'active',
            };
        }
        if (this.isHttpResponse(event)) {
            return {
                progress: 100,
                state: 'success',
                mediaBody: event.body
            };
        }
        return upload;
    };
}