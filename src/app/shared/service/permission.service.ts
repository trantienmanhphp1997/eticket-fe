import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import { AbstractService, EntityResponseType } from '@shared/service/common/abstract.service';
import { Observable } from 'rxjs';
import { IPermission } from './../models/permission.model';


@Injectable({
  providedIn: 'root',
})

export class PermissionService extends AbstractService{

  public resourceUrl = 'permissions';

  constructor(
    protected http: HttpClient,
  ) {
    super(http);
  }

  findAll(loading = false): Observable<EntityResponseType<IPermission[]>> {
    return super.get<IPermission[]>(`${this.resourceUrl}`, {loading});
  }
  assignPermissionToRole(params:any,loading=false): Observable<EntityResponseType<IPermission[]>> {
    return super.post<IPermission[]>(`${this.resourceUrl}/attach`, { role_id:params.role_id,permissions_ids:params.permissions_ids , loading });
  }
}
