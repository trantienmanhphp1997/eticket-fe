import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IBuilding } from '@shared/models/building.model';
import { AbstractService, EntityResponseType } from '@shared/service/common/abstract.service';
import { Observable } from 'rxjs';
import { SERVICE } from '../constants/gateway-routes-api.constant';
import { FileService } from './file.service';

@Injectable({
  providedIn: 'root',
})
export class UserGroupService extends AbstractService {
  public resourceUrl = SERVICE.IAM;

  constructor(protected http: HttpClient,
    private fileService: FileService) {
    super(http);
  }

  /**
   * 
   * @param params : pageIndex, pageSize, parentId, searchKeyword
   * @param loading 
   * @returns 
   */
  getUserGroups(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let fitlerbyParentPath = params.parentId ? `?parentIds=${params.parentId}&getAll=true` : '';
    const param ={search:params.keyword};
    // let searchSubString = params.keyword ? `&search=${params.keyword}` : ''
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=created_at&sortedBy=desc';
    let include = params.include ?  `&include=${params.include}`:'';
    return super.get<any[]>(`usergroups${fitlerbyParentPath}?limit=${params.pageSize}&page=${params.pageIndex}${include}${sort}`, { params:param,loading });
  }
  getDefaultUserGroup(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let is_default = params.is_default ? `&is_default=${params.is_default}` : ''
    return super.get<any[]>(`usergroups?${is_default}`, { loading });
  }

  getAllUserGroup(params: any ,loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`usergroups?data_context=edit_user_to_group&group_ref_user_id=${params.group_ref_user_id}&group_ref_group_id=${params.group_ref_group_id}`, {  loading });
  }

  getUserGroupRefs(user_ids:any,loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`usergroups/refs`, {user_ids},{loading});
  }

  getUserGroupAddUser(param:any,loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`usergroups/adduser`, {...param,loading});
  }
  getUserGroupEditOrDeleteUser(param:any,loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`usergroups/removeuser`, {...param,loading});
  }
 
  removeGroupUser(params: any,loading=true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`usergroups/removeuser`, params , {loading});
  }

  /**
   * 
   * @param params : pageIndex, pageSize, parentId, searchKeyword
   * @param loading 
   * @returns 
   */
   getAllRoles(loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`roles?limit=0`, { loading });
  }

  /**
   * 
   * @param usergroup 
   * @param loading 
   * @returns 
   */
  create(usergroup: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`usergroups`, usergroup, { loading });
  }

  addUserToGroup(group_ids:any,department_ids:any,user_ids:any, loading=true):Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`usergroups/adduser`, {group_ids:group_ids,user_ids:user_ids,department_ids:department_ids}, { loading });
  }
  getUserGroupUpdate(param:any,loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`usergroups/removeuser`, param, {loading});
  }
  findUserGroup(usergroupId: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`usergroups/${usergroupId}?include=roles,users`, { loading });
  }

  /**
   * 
   * @param usergroup 
   * @param loading 
   * @returns 
   */
  update(usergroupId: any, usergroup: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`usergroups/${usergroupId}`, usergroup, { loading });
  }

  deleteUserGroup(usergroupIds: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`usergroups/multipledelete`, {usergroupIds: usergroupIds}, { loading });
  }

  getAllUsers(loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`users`, { loading });
  }

  getAllDepartments(param:any,loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`departments?limit=${param.limit}&data_context=${param.data_context}`, { loading });
  }

  getAllUserGroupRefs(body: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`usergroups/refs?limit=0`, {group_ids: body}, { loading });
  }
  export(): any {
    this.fileService.downloadFileUserGroup('http://192.168.2.100:9052/api/v1/usergroups/export')
  }
}
