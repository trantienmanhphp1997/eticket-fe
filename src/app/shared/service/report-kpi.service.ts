import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { AbstractService, EntityResponseType } from './common/abstract.service';
import { FileService } from './file.service';

@Injectable({
  providedIn: 'root'
})
export class ReportKpiService extends AbstractService {

  private API_URL = environment.gateway;
  constructor(
    protected http: HttpClient,
    private fileService: FileService,) {
    super(http);
  }

  getReportKpi(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let options = {
      limit: params.pageSize,
      page: params.pageIndex,
      search: params.keyword,
      // orderBy: params.orderBy ? params.orderBy.replaceAll(',', ';') : '',
      // sortBy: params.sortBy ? params.sortBy.replaceAll(',', ';') : '',
      // sort: params.orderBy && params.sortBy ? `&orderBy=${params.orderBy}&sortedBy=${params.sortBy}` : '&orderBy=order&sortedBy=asc',
      from_date: params?.from_date,
      to_date: params?.to_date,
    }
    let filterDepartment = params.department_ids ? `&department_ids=${params.department_ids}` : '';
    let filterServiceProduct = params.product_ids ? `&product_ids=${params.product_ids}` : '';
    return super.get<any[]>(`kpi-handle-data?${filterDepartment}${filterServiceProduct}`, { params: options, loading });
  }
  getReportKpiMonth(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let options = {
      limit: params.pageSize,
      page: params.pageIndex,
      search: params.keyword,
      // orderBy: params.orderBy ? params.orderBy.replaceAll(',', ';') : '',
      // sortBy: params.sortBy ? params.sortBy.replaceAll(',', ';') : '',
      // sort: params.orderBy && params.sortBy ? `&orderBy=${params.orderBy}&sortedBy=${params.sortBy}` : '&orderBy=order&sortedBy=asc',
      date: params?.date,
    }
    
    let filterDepartment = params.department_ids ? `&department_ids=${params.department_ids}` : '';
    let filterServiceProduct = params.product_ids ? `&product_ids=${params.product_ids}` : '';
    return super.get<any[]>(`kpi-month-data?${filterDepartment}${filterServiceProduct}`, { params: options, loading });
  }
  getReportKpiYear(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let options = {
      limit: params.pageSize,
      page: params.pageIndex,
      search: params.keyword,
      date: params?.date,
    }
    // let orderBy= params.orderBy ? params.orderBy.replaceAll(',', ';') : '';
    // let sortBy= params.sortBy ? params.sortBy.replaceAll(',', ';') : '';
    // let sort= params.orderBy && params.sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=order&sortedBy=asc';
    let filterDepartment = params.department_ids ? `&department_ids=${params.department_ids}` : '';
    let filterServiceProduct = params.product_ids ? `&product_ids=${params.product_ids}` : '';
    return super.get<any[]>(`kpi-year-data?${filterDepartment}${filterServiceProduct}`, { params: options, loading });
  }
  exportKPIYear(params: any, loading = true): any {
    let search = params.keyword ? `&search=${params?.keyword}` : '';
    let filterDepartment = params.department_ids ? `&department_ids=${params.department_ids}` : '';
    let filterServiceProduct = params.product_ids ? `&product_ids=${params.product_ids}` : '';
    this.fileService.downloadFileReport(`${ this.API_URL}kpi-year-data/export?type=year&date=${params?.date}${search}${filterDepartment}${filterServiceProduct}`,'Báo cáo thống kê KPI theo năm')
  }
  exportKPIMonth(params: any, loading = true): any {
    let filterDepartment = params.department_ids ? `&department_ids=${params.department_ids}` : '';
    let filterServiceProduct = params.product_ids ? `&product_ids=${params.product_ids}` : '';
    let search = params.keyword ? `&search=${params?.keyword}` : '';
    this.fileService.downloadFileReport(`${ this.API_URL}kpi-month-data/export?type=month&date=${params?.date}${search}${filterDepartment}${filterServiceProduct}`,'Báo cáo thống kê KPI theo tháng')
  }
  exportKPI(params: any, loading = true): any {
    let filterDepartment = params.department_ids ? `&department_ids=${params.department_ids}` : '';
    let filterServiceProduct = params.product_ids ? `&product_ids=${params.product_ids}` : '';
    let fromdate = params.from_date ? `&from_date=${params?.from_date}` : '';
    let todate = params.to_date ? `&to_date=${params?.to_date}` : '';
    let search = params.keyword ? `&search=${params?.keyword}` : '';
    this.fileService.downloadFileReport(`${ this.API_URL}kpi-handle-data/export?type=handle${fromdate}${todate}${search}${filterDepartment}${filterServiceProduct}`,'Báo cáo thống kê KPI')
  }
}