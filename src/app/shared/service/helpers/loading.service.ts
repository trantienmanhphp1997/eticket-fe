import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {

  public isLoading = new BehaviorSubject<boolean>(false);

  constructor() {
  }

  /**
   * function show spinner
   *
   * @author chinh.phungduc
   * @date 2022-04-28
   */
  show(): void {
    this.isLoading.next(true);
  }

  /**
   * function hide spinner
   *
   * @author chinh.phungduc
   * @date 2022-04-28
   */
  hide(): void {
    this.isLoading.next(false);
  }
}
