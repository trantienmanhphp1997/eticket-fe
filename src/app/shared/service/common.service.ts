import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IBuilding } from '@shared/models/building.model';
import { AbstractService, EntityResponseType } from '@shared/service/common/abstract.service';
import { Observable } from 'rxjs';
import { SERVICE } from '../constants/gateway-routes-api.constant';

@Injectable({
  providedIn: 'root',
})
export class CommonService extends AbstractService {
  public resourceUrl = SERVICE.IAM;

  constructor(protected http: HttpClient) {
    super(http);
  }

  /**
   * 
   * @param params : pageIndex, pageSize, parentId, searchKeyword
   * @param loading 
   * @returns 
   */
  getProvinces(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let options = {
      search: params.keyword,
      limit: 0
    }
    
    return super.get<any[]>(`provinces`, { params: options, loading });
  }

  getDistricts(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let options = {
      search: params.keyword,
      province: params.province,
      limit: 0
    }
    
    return super.get<any[]>(`districts`, { params: options, loading });
  }

  getWards(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let options = {
      search: params.keyword,
      district: params.district,
      limit: 0
    }
    
    return super.get<any[]>(`wards`, { params: options, loading });
  }

  /**
   * 
   * @param params : pageIndex, pageSize, parentId, searchKeyword
   * @param loading 
   * @returns 
   */
  getAllDepartments(loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`departments?getAll=true`, { loading });
  }

  /**
   * 
   * @param department 
   * @param loading 
   * @returns 
   */
  create(department: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`departments`, department, { loading });
  }

  /**
   * 
   * @param department 
   * @param loading 
   * @returns 
   */
  update(departmentId: any, department: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`departments/${departmentId}`, department, { loading });
  }

  checkExisted(model:any, params: any, loading = true):  Promise<any> {
    return super.post<any[]>(`${model}/is-existed`, params, { loading }).toPromise();
  }
}
