import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AbstractService, EntityResponseType } from './common/abstract.service';
@Injectable({
  providedIn: 'root'
})
export class SlaService extends AbstractService{

  constructor(protected http: HttpClient) {
    super(http);
  }

  getSla(params:any, loading=true): Observable<EntityResponseType<any>>{
    const param ={search:params.search};
    // let searchSubString = params.search ? `&search=${params.search}` : '';
    let include = params.include ?  `&include=${params.include}`:'';
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=order&sortedBy=asc';
    let filterServiceProduct = params.product_id ? `&productId=${params.product_id}`:'';
    return super.get<any[]>(`slas?limit=${params.pageSize}&page=${params.pageIndex}${filterServiceProduct}${include}${sort}`,{params:param,loading});
  }
  updateSla(data: any, loading = false): Observable<EntityResponseType<any>> {
    return super.patch<any>(`slas/${(data.id)}`, data, { loading });
  }
  deleteSla(ids: any, loading = true): Observable<EntityResponseType<any>> {
    console.log(ids)
    return super.post<any[]>(`slas/multipledelete`, {slaIds: ids}, { loading });
  }
  create(data: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`slas`, data , { loading })
  }
  update(id: any , data: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.patch<any[]>(`slas/${id}`, data , { loading })
  }
  updateStatus(id: any , status: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.patch<any[]>(`slas/${id}`, { status : status} , { loading })
  }
  detail(id: any, param: any, loading = true): Observable<EntityResponseType<any[]>>{
    let include = param.include ?  `?include=${param.include}`:'';
    return super.get<any[]>(`slas/${id}${include}`, { loading })
  }
  updateOrder(params:any,loading=true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`slas/update-order`,params, { loading });
  }
}
