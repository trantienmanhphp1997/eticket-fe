import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { BOOLEAN_STRING } from '@shared/constants/common.constant';
import { IBaseResponse } from '@shared/models/base.model';
import CommonUtil from '@shared/utils/common-utils';
import { Observable } from 'rxjs/internal/Observable';

export type EntityResponseType<T> = HttpResponse<IBaseResponse<T>>;

@Injectable({
  providedIn: 'root',
})
export abstract class AbstractService {
  private BASE_API_URL = environment.gateway;

  protected readonly httpOptions: {
    observe: string;
    headers?:
      | HttpHeaders
      | {
          [header: string]: string | string[];
        };
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        };
  } = {
    observe: 'response',
    headers: {} as HttpHeaders,
    params: {} as HttpParams,
  };

  protected readonly httpOptionsFile: {
    responseType: string;
    headers?:
      | HttpHeaders
      | {
          [header: string]: string | string[];
        };
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        };
    observe: string;
  } = {
    responseType: 'blob',
    observe: 'response',
    headers: {} as HttpHeaders,
    params: {} as HttpParams,
  };

  constructor(protected http: HttpClient) {}

  /**
   * GET Request
   * 
   * @param {string} url 
   * @param {any} options 
   * @returns Observable<EntityResponseType<T> | any> 
   * @author chinh.phungduc
   * @date 2022-04-28
   */
  public get<T>(
    url: string,
    options?: any
  ): Observable<EntityResponseType<T> | any> {
    return this.http.get<IBaseResponse<T>>(
      `${this.BASE_API_URL}${url}`,
      this.httpOptionCustomize(options)
    );
  }

  /**
   * Get file request
   * 
   * @param {string} url 
   * @param {any} options 
   * @returns Observable<Blob | any>
   * @author chinh.phungduc
   * @date 2022-04-28
   */
  public getFile(
    url: string,
    options?: any
  ): Observable<Blob | any> {
    return this.http.get(
      `${this.BASE_API_URL}${url}`,
      this.httpOptionCustomize(options, true)
    );
  }

  /**
   * POST request
   * 
   * @param {string} url 
   * @param {string} body 
   * @param {any} options 
   * @returns Observable<EntityResponseType<T> | any>
   * @author chinh.phungduc
   * @date 2022-04-28
  */
  public post<T>(
    url: string,
    body: any,
    options?: any
  ): Observable<EntityResponseType<T> | any> {
    console.log(this.BASE_API_URL);
    
    return this.http.post<IBaseResponse<T>>(
      `${this.BASE_API_URL}${url}`,
      CommonUtil.optimalObjectParams(body),
      this.httpOptionCustomize(options)
    );
  }

   /**
   * POST file request
   * 
   * @param {string} url 
   * @param {string} body 
   * @param {any} options 
   * @returns Observable<Blob | any>
   * @author chinh.phungduc
   * @date 2022-04-28
  */
  public postFile(
    url: string,
    body: any,
    options?: any
  ): Observable<Blob | any> {
    return this.http.post(
      `${this.BASE_API_URL}${url}`,
      CommonUtil.optimalObjectParams(body),
      this.httpOptionCustomize(options, true)
    );
  }

   /**
   * patch request
   * 
   * @param {string} url 
   * @param {string} body 
   * @param {any} options 
   * @returns Observable<EntityResponseType<T> | any>
   * @author chinh.phungduc
   * @date 2022-04-28
  */
  public patch<T>(
    url: string,
    body: any,
    options?: any
  ): Observable<EntityResponseType<T> | any> {
    return this.http.patch<IBaseResponse<T>>(
      `${this.BASE_API_URL}${url}`,
      CommonUtil.optimalObjectParams(body),
      this.httpOptionCustomize(options)
    );
  }

   /**
   * DELETE request
   * 
   * @param {string} url 
   * @param {any} options 
   * @returns Observable<EntityResponseType<T> | any>
   * @author chinh.phungduc
   * @date 2022-04-28
  */
  public delete<T>(
    url: string,
    options?: any
  ): Observable<EntityResponseType<T> | any> {
    return this.http.delete<IBaseResponse<T>>(
      `${this.BASE_API_URL}${url}`,
      this.httpOptionCustomize(options)
    );
  }

  /**
   * httpOptionCustomize
   *
  
   * @param {any} options httpOptions
   * @param {boolean} httpFile
   * @note với các service cần set headers trong httpOptions thì tạo object json dạng {}, không dùng new HttpHeaders để tạo,
   * ví dụ tạo: headers: { xxx: xxx }
   * @returns any
   * @author chinh.phungduc
   * @date 2022-04-28
   */
  private httpOptionCustomize(options?: any, httpFile: boolean = false): any {
    const httpOptions = httpFile ? { ...this.httpOptionsFile } : { ...this.httpOptions };
    if (options?.observe) {
      httpOptions.observe = options.observe;
    }
    if (options?.loading !== false) {
      httpOptions.headers = new HttpHeaders({
        ...options?.headers,
        loading: BOOLEAN_STRING.TRUE,
      });
    }
    if (options?.params) {
      httpOptions.params = CommonUtil.optimalObjectParams(options?.params);
    }
    return httpOptions;
  }
}
