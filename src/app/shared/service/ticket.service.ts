import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import { ITicketComplaintRequest } from '@shared/models/request/ticlet-complaint-request.model';
import { IStatistical } from '@shared/models/statistical';
import { forkJoin, Observable } from 'rxjs';

import { Ticket } from '../models/ticket.model';
import { AbstractService, EntityResponseType } from './common/abstract.service';
import { FileService } from './file.service';
import { environment } from '@env/environment';
import { idText } from 'typescript';
@Injectable({
  providedIn: 'root'
})
export class TicketService extends AbstractService {
  public resourceUrl = SERVICE.TICKET + '/tickets';

  constructor(
    protected http: HttpClient,
    private fileService: FileService,
  ) {
    super(http);
  }

  search(options?: {}, loading = true): Observable<EntityResponseType<any[]>> {

    return super.get<any[]>(`tickets`, { params: options, loading });
  }
  getAll(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`tickets?limit=${params.pageSize}`, { params, loading });
  }
  getListTicketEndUser(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    // let searchSubString = params.keyword ? `&search=${params.keyword}` : '';
    const param ={search:params.keyword};
    let filterPriority = params.priority_id ? `&priority_id=${params.priority_id}` : '';
    let filterSla = params.sla_id ? `&sla_id=${params.sla_id}` : '';
    let filterState = params.state_id ? `&state_id=${params.state_id}` : '';
    let filterProduct = params.product_id ? `&product_id=${params.product_id}` : '';
    let filterFromDate = params.fromDate ? `&fromDate=${params.fromDate}` : '';
    let filterToDate = params.toDate ? `&toDate=${params.toDate}` : '';
    let filterSource = params.source_id ? `&source_id=${params.source_id}` : '';
    let filterCate = params.category_id ? `&category_id=${params.category_id}` : '';
    let key = params.key ? `&key=${params.key}` : '';
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',', ';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',', ';') : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=created_at&sortedBy=desc';
    return super.get<any[]>(`end-user/tickets?limit=${params.pageSize}&page=${params.pageIndex}${filterPriority}${filterSla}${filterState}${key}${sort}${filterFromDate}${filterToDate}${filterProduct}${filterSource}${filterCate}`, {params:param, loading });
  }
  fillter(options?: any, fillter?: any, loading = true): Observable<EntityResponseType<any[]>> {
    let search = options?.search ? `&search=${options.search}` : '';
    let state_id = options.state_id ? options.state_id : '';
    let product_id = options.product_id ? options.product_id : '';
    let priority_id = options.priority_id ? options.priority_id : '';
    let ticket_type = options.ticket_type ? options.ticket_type : '';
    let category_id = options.category_id ? options.category_id : '';
    let user_id = options.user_id ? options.user_id : '';
    let source_id = options.source_id ? options.source_id : '';
    let start_time = fillter?.start_time ? fillter?.start_time : '';
    let end_time = fillter?.end_time ? fillter?.end_time : '';
    let orderBy = fillter?.orderBy ? fillter?.orderBy.replaceAll(',', ';') : 'created_at';
    let sortBy = fillter?.sortBy ? fillter?.sortBy.replaceAll(',', ';') : 'desc';
    let require_id = options.require_id ? options.require_id : '';
    let customer_id = options.customer_id ? options.customer_id : '';
    let department_id = options.department_id ? options.department_id : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '';
    let filterTickets = options.quick_filter_type ? options.quick_filter_type : '';
    let creator_department_id = options.creator_department_id ? options.creator_department_id : '';
    let state = options.state ? options.state : '';
    let limit = options?.limit ?? 10;
    let page = options?.page ?? 1;
    let paginate = `&limit=${limit}&page=${page}`;
    return super.get<any[]>(`tickets?fromDate=${start_time}&toDate=${end_time}${search}&customer_id=${customer_id}&department_id=${department_id}&require_id=${require_id}&state_id=${state_id}&category_id=${category_id}&priority_id=${priority_id}&user_id=${user_id}&product_id=${product_id}&source_id=${source_id}&ticket_type=${ticket_type}&quick_filter_type=${filterTickets}&creator_department_id=${creator_department_id}&state=${state}${sort}${paginate}`, { params: fillter, loading });
  }
  multiDelete(id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`tickets/multipledelete`, { ticketIds: id }, { loading })
  }
  getTicketEndUserById(param: any, ticketId: any, loading = true): Observable<EntityResponseType<any[]>> {
    let include = param.include ? `&include=${param.include}` : '';
    return super.get<any[]>(`end-user/tickets/${ticketId}?${include}`, { loading });
  }
  updateTicetEndUser(ticketId: any, params: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any>(`end-user/tickets/${ticketId}`, params, loading = true);
  }
  customer(limit: number, keyword: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any>(`customers?orderBy=name&sortedBy=asc&search=${keyword}`, { params: { limit }, loading });
  }
  contact(id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any>(`customers/${id}?include=contacts,delegate`, { params: { limit: 0 }, loading });
  }
  loadMultipleDataForTicket(): Observable<any[]> {
    let response1 = super.get<any[]>(`products?orderBy=name&sortedBy=asc&status=1`, { params: { limit: 0 } });
    let response2 = super.get<any[]>(`ticketpriorities?orderBy=name&sortedBy=asc&status=1`, { params: { limit: 0 } });
    let response3 = super.get<any[]>(`ticketsources?orderBy=name&sortedBy=asc`, { params: { limit: 0 } });
    let response4 = super.get<any[]>(`ticketcategories?orderBy=name&sortedBy=asc&status=1`, { params: { limit: 0 } });
    let response5 = super.get<any[]>(`customers?orderBy=name&sortedBy=asc`, { params: { limit: 100 } });
    let response6 = super.get<any[]>(`users?data_context=assignee_ticket`, { params: { limit: 0 } });
    let response7 = super.get<any[]>(`ticketstates?orderBy=name&sortedBy=asc&status=1`, { params: { limit: 0 } });
    // Observable.forkJoin (RxJS 5) changes to just forkJoin() in RxJS 6
    return forkJoin([response1, response2, response3, response4, response5, response6, response7])
  }
  loadMultipleDataForTicketDetail(params: any = null): Observable<any[]> {
    let pinnedRecord = params?.product_id ? `&pinnedRecord=${params.product_id}` : '';
    let response1 = super.get<any[]>(`products?status=1${pinnedRecord}`, { params: { limit: 0 } });
    let response2 = super.get<any[]>(`ticketconfigs`, { });

    // Observable.forkJoin (RxJS 5) changes to just forkJoin() in RxJS 6
    return forkJoin([response1, response2])
  }
  getUserAssignee(ticketId: any, search: any): Observable<EntityResponseType<any>> {
    let ticketU = ticketId ? `&ticket_id=${ticketId}` : '';
    let searchText = search ? `&search=${search}` : '';
    return super.get<any>(`users?data_context=assignee_ticket${searchText}${ticketU}`, { params: { limit: 0 } });
  }
  create(ticket: Ticket, loading = false): Observable<EntityResponseType<any>> {
    return super.post<any>(`tickets`, ticket, { loading });
  }
  createTicketEndUser(params: any, loading = true): Observable<EntityResponseType<any>> {
    return super.post<any>(`end-user/tickets/`, params, { loading });
  }

  createByComplaint(ticket: ITicketComplaintRequest, loading = false): Observable<EntityResponseType<any>> {
    return super.post<any>(`${this.resourceUrl}/issued-by-complaint`, ticket, { loading });
  }

  update(ticket: Ticket, id: any, loading = false): Observable<EntityResponseType<any>> {

    return super.post<any>(`${this.resourceUrl}/${(id)}/update)`, ticket, { loading });
  }

  delete(id: any, loading = false): Observable<EntityResponseType<any>> {
    return super.post<any>(`${this.resourceUrl}/${id}/delete`, {}, { loading });
  }

  // deleteTickets(ids: any, isLoading = false): Observable<EntityResponseType> {
  //   return this.http
  //     .post<IBaseResponse>(`${this.resourceUrl}/delete-tickets`, ids, {observe: "response", headers: CommonUtil.headers(isLoading)});
  // }

  getById(id: any, loading = false): Observable<EntityResponseType<any>> {
    return super.get<any>(`${this.resourceUrl}/${id}`, { loading });
  }

  handleTicket(id: any, request: any, loading = false): Observable<EntityResponseType<any>> {
    return super.post<any>(`${this.resourceUrl}/${id}/handle`, request, { loading });
  }

  receiveTicket(id: any, request: any, loading = false): Observable<EntityResponseType<any>> {
    return super.post<any>(`${this.resourceUrl}/${id}/receive`, request, { loading });
  }

  completeTicket(id: any, request: any, loading = false): Observable<EntityResponseType<any>> {
    return super.post<any>(`${this.resourceUrl}/${id}/complete`, request, { loading });
  }

  getStatistical(options?: any, loading = false): Observable<EntityResponseType<IStatistical>> {
    return super.post<IStatistical>(`${this.resourceUrl}/statistic`, options, { loading });
  }
  detail(id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`tickets/${id}?include=files,transactions,workflow,internalComments,commentsWithCustomer`, { loading })
  }
  updateTickket(id: any, data: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`tickets/${id}`, data, { loading })
  }
  excuteTransaction(body: any, idTicket: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`tickets/${idTicket}/excutetransaction`, body, { loading })
  }
  task(idTicket: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`tasks?ticketId=${idTicket}`, { loading })
  }
  comment(id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`comment/tickets?include=replies,files&scope=internal&ticketId=${id}`, { loading })
  }
  editComment(id: any, body: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`comments/${id}`,body ,{ loading })
  }
  deleteComment(id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.delete<any[]>(`comments/${id}` ,{ loading })
  }
  commentTicket(id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`comment/tickets?include=replies,files&scope=with_customer&ticketId=${id}`, { loading })
  }
  createComment(data: any, loading =false): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`comments/ticket`, data, { loading })
  }
  log(id: any, params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',', ';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',', ';') : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=created_at&sortedBy=desc';
    return super.get<any[]>(`activitylogs?ticketId=${id}${sort}`, { loading })
  }

  ticketStatus(params: any,loading = true): Observable<EntityResponseType<any[]>> {
    let filterFromDate = params.start_time ? params.start_time : '';
    let filterToDate = params.end_time ? params.end_time : '';
    let state_id = params.state_id ? params.state_id : '';
    let user_id = params.user_id ? params.user_id : '';
    let product_id = params.product_id ? params.product_id : '';
    let priority_id = params.priority_id ? params.priority_id : '';
    let ticket_type = params.ticket_type ? params.ticket_type : '';
    let category_id = params.category_id ? params.category_id : '';
    let source_id = params.source_id ? params.source_id : '';
    let require_id = params.require_id ? params.require_id : '';
    let customer_id = params.customer_id ? params.customer_id : '';
    let department_id = params.department_id ? params.department_id : '';
    let quick_filter_type = params.quick_filter_type ? params.quick_filter_type :'';
    let search = params.search ? params.search: '';
    let creator_department_id = params.creator_department_id ?? '';
    return super.get<any[]>(`tickets-by-status?limit=0&fromDate=${filterFromDate}&toDate=${filterToDate}&search=${search}&quick_filter_type=${quick_filter_type}&customer_id=${customer_id}&department_id=${department_id}&require_id=${require_id}&state_id=${state_id}&category_id=${category_id}&priority_id=${priority_id}&user_id=${user_id}&product_id=${product_id}&source_id=${source_id}&ticket_type=${ticket_type}&creator_department_id=${creator_department_id}`, { loading })
  }
  sendMail(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`send-mail/ticket`,params, { loading })
  }
  reOpenTicket(id: any,params: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`tickets/re-open/${id}`,params, { loading })
  }
  assignTicket(id: any,params: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`ticket-assign-back-to-helpdesk/${id}`,params, { loading })
  }
  status(id:any,params: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`change-status-completed-tickets/${id}`, params,{ loading })
  }
  createMedia(files: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`media`, files, { loading })
  }
  exportTicketData(params: any, fillter?: any,loading = true): any {
    let fromDate = params?.start_time? params?.start_time :'';
    let toDate = params?.end_time? params?.end_time :'';
    let search = params?.search ? params?.search:'';
    let state_id = params?.state_id ? params?.state_id:'';
    let customer_id = params?.customer_id ? params?.customer_id:'';
    let require_id = params?.require_id ? params?.require_id:'';
    let category_id = params?.category_id ? params?.category_id:'';
    let priority_id = params?.priority_id ? params?.priority_id:'';
    let user_id = params?.user_id ? params?.user_id:'';
    let product_id = params?.product_id ? params?.product_id:'';
    let source_id = params?.source_id ? params?.source_id :'';
    let department_id = params?.department_id ? params?.department_id :'';
    let creator_department_id = params?.creator_department_id ? params?.creator_department_id :'';
    let ticket_type = params?.ticket_type ? params?.ticket_type :'';
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=created_at&sortedBy=desc';
    this.fileService.downloadFile(`${environment.gateway}tickets/export?limit=0&${sort}&fromDate=${fromDate}&toDate=${toDate}&search=${search}&state_id=${state_id}&customer_id=${customer_id}&require_id=${require_id}&category_id=${category_id}&priority_id=${priority_id}&user_id=${user_id}&product_id=${product_id}&source_id=${source_id}&department_id=${department_id}&creator_department_id=${creator_department_id}&ticket_type=${ticket_type}`)
  }
  exportTemplate( loading = true): any {
    this.fileService.downloadFileTemplate(`${environment.gateway}tickets/export-template`)
  }
  exportFileError(params:any,loading=true):any{
    let data = JSON.stringify(params);
    this.fileService.downloadFileError(`${environment.gateway}tickets/exportError`, {data})
  }
  importTicketData(file: any, loading = true): Observable<EntityResponseType<any[]>> {
    var formData: any = new FormData();
    formData.append('file', file);
    return super.post(`tickets/import`, formData, { loading })
  }
  getAllAssignee(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`users?status=1`, { params, loading });
  }
  getAllCustomer(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let name = params?.name ? `&searchName=${params?.name}` : '';
    let limit =  10;
    let orderBy = params.orderBy ? `&orderBy=${params.orderBy}`:`orderBy=name`;
    let sortBy = params.sortBy ? `&sortedBy=${params.sortBy}`:`sortedBy=asc`;
    return super.get<any[]>(`customers?limit=${limit}&${orderBy}&${sortBy}&status=1${name}`, { loading });
  }
  getAllCustomerDetail(params: any, loading = true): Observable<EntityResponseType<any[]>> {
    let name = params?.name ? `&searchTerm=${params?.name}` : '';
    let limit = params?.limit || 0;
    let pinnedRecord = params.pinnedRecord ? `&pinnedRecord=${params.pinnedRecord}` : '';
    let orderBy = ``;
    let sortBy = ``;
    if (params.skipSort == true) {
      orderBy = params.orderBy ? `&orderBy=${params.orderBy}`:`&orderBy=phone`;
      sortBy = params.sortBy ? `&sortedBy=${params.sortBy}`:`&sortedBy=asc`;
    }
    return super.get<any[]>(`customers?limit=${limit}${pinnedRecord}${orderBy}${sortBy}&status=1${name}`, { loading });
  }
  historyEmail(ticket_hash_id: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`mails?ticket_id=${ticket_hash_id}&limit=0`, { loading });
  }

  updateComment(comment_id: any,body:any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.patch<any[]>(`comments/${comment_id}`,body, { loading });
  }

  downloadFile(fileId: string): Observable<EntityResponseType<any[]>> {
    return super.getFile(`download-media/${fileId}`);
  }
}
