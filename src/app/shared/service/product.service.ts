import { Injectable } from '@angular/core';
import { AbstractService, EntityResponseType } from './common/abstract.service';
import { HttpClient } from '@angular/common/http';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import {Observable} from 'rxjs';
import { FileService } from './file.service';
import { environment } from '@env/environment';
@Injectable({
  providedIn: 'root'
})

export class ProductService extends AbstractService {
  public resourceUrl = SERVICE.TICKET + '/tickets';
  constructor(
    protected http : HttpClient,
    private fileService: FileService,
  ) {
    super(http);
   }
  search(params: any, loading = true): Observable<EntityResponseType<any[]>>{
    const param ={search:params.keyword};
    let fitlerbyParentPath = params.categoryIds ? `?categoryIds=${params.categoryIds}&getAll=true` : '';
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
    let limit = params.pageSize ? `&limit=${params.pageSize}` : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=created_at&sortedBy=desc';
    return super.get<any[]>(`products${fitlerbyParentPath}?${limit}&page=${params.pageIndex}${sort}`, {params:param, loading})
  }
  getAll(loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`products?limit=0&status=1`, { loading })
  }
  getAlltree(loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`products-v2?limit=0&status=1`, { loading })
  }
  getAllProductAdminDepartment(loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`get-all-product-admin-department?limit=0&orderBy=name&sortedBy=asc`, { loading })
  }
  getAllWithAlphabet(loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`products?limit=0&orderBy=name&sortedBy=asc`, { loading })
  }
  create(products: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`products`,products, { loading })
  }
  existed(code: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`products/is-existed`, {code : code} , { loading })
  }
  update(id: any ,products: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.patch<any[]>(`products/${id}`,products, { loading })
  }
  updateStauts(status: any, id: any , loading = true): Observable<EntityResponseType<any[]>>{
    return super.patch<any[]>(`products/${id}`,{ status : status }, { loading })
  }
  getDelete(id: any , loading = true): Observable<EntityResponseType<any[]>>{
    return super.delete<any[]>(`products/${id}`, { loading })
  }
  multiDelete(id: any , loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`products/multipledelete`, { productIds : id }, {loading})
  }
  getProduct(): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>("products?limit=0",)
  }
  export(): any {
    this.fileService.downloadFileProduct(`${environment.gateway}/products/export`)
  }
  importData(file: any, loading = true): Observable<EntityResponseType<any[]>> {
    var formData: any = new FormData();
    formData.append('file', file);
    return super.postFile(`products/import`, formData, { loading })
  }
  exportTemplate(): any {
    this.fileService.downloadFileTemplateProduct(`${environment.gateway}/products/export-template`)
  }
}
