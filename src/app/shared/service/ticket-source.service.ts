import { Injectable } from '@angular/core';
import { AbstractService, EntityResponseType } from './common/abstract.service';
import { HttpClient } from '@angular/common/http';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class TicketSourceService extends AbstractService {
  constructor(
    protected http : HttpClient
  ) {
    super(http);
   }
  search(params: any, loading = true): Observable<EntityResponseType<any[]>>{
    const param ={search:params.keyword};
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=order&sortedBy=asc';
    return super.get<any[]>(`ticketsources?limit=${params.pageSize}&page=${params.pageIndex}${sort}`, {params:param, loading })
  }
  getAll(loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`ticketsources`, { loading })
  }
  create(ticketcategories: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`ticketsources`,ticketcategories, { loading })
  }
  update(id: any ,ticketcategories: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.patch<any[]>(`ticketsources/${id}`,ticketcategories, { loading })
  }
  delete(ids: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`ticketsources/multipledelete`, {ticketsourceIds: ids}, { loading });
  }
  updateOrder(params: any, loading = false): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`ticketsources/update-order`, params, { loading });
  }
}
