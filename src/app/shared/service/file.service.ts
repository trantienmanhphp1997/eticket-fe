import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BOOLEAN_STRING } from '@shared/constants/common.constant';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import { IFile } from '@shared/models/file.model';
import { AuthService } from '@shared/service/auth/auth.service';
import { Observable } from 'rxjs';
import CommonUtil from '../utils/common-utils';
import { AbstractService, EntityResponseType } from './common/abstract.service';
@Injectable({
  providedIn: 'root'
})
export class FileService extends AbstractService {
  public resourceUrl = SERVICE.STORAGE + '/files';

  public publicUrl = SERVICE.STORAGE;

  constructor(
    protected http: HttpClient,
    protected httpClient: HttpClient,
    private authService: AuthService,
  ) {
    super(http);
  }

  /**
   * @description : check file exist by id
   * @param fileId
   * @return IFileResponse
   */
  ensureExistedFile(fileId: string): Observable<EntityResponseType<IFile>> {
    return super.get<IFile>(`${this.resourceUrl}/${fileId}`);
  }

  /**
   * @description : upload file
   * @return IFileResponse[]
   * @param fileIds
   */
  ensureExistedListFileId(fileIds: string[]): Observable<EntityResponseType<IFile[]>> {
    return super.post<IFile[]>(`${this.resourceUrl}/find-by-ids`, { fileIds });
  }

  /**
   * @description : upload single file
   * @param file
   * @param ownerId
   * @param ownerType
   */
  uploadFile(file: File, ownerId?: string, ownerType?: string): Observable<EntityResponseType<IFile>> {
    const formData = new FormData();
    formData.append('file', file);
    if (ownerId) {
      formData.append('ownerId', ownerId);
    }
    if (ownerType) {
      formData.append('ownerType', ownerType);
    }
    return super.post<IFile>(`${this.resourceUrl}/upload`, formData);
  }

  /**
   * @description : update list file
   * @param files
   * @param ownerId id of owner
   * @param ownerType type of owner
   * @return FileList
   */
  uploadListFile(files: File[], ownerId?: string, ownerType?: string): Observable<EntityResponseType<IFile>> {
    const formData = new FormData();
    if (!!ownerId) {
      formData.append('ownerId', ownerId);
    }
    if (!!ownerType) {
      formData.append('ownerType', ownerType);
    }
    files.forEach(file => {
      formData.append('files', file);
    });
    return super.post<IFile>(`${this.resourceUrl}/uploads`, formData);
  }

  uploadListFilePublic(files: File[], ownerId?: string, ownerType?: string): Observable<EntityResponseType<IFile>> {
    const formData = new FormData();
    if (!!ownerId) {
      formData.append('ownerId', ownerId);
    }
    if (!!ownerType) {
      formData.append('ownerType', ownerType);
    }
    files.forEach(file => {
      formData.append('files', file);
    });
    return super.post<IFile>(`${this.publicUrl}/public/files/uploads`, formData);
  }

  /**
   * @description : delete file
   * @param fileId
   */
  deleteFile(fileId: string): Observable<EntityResponseType<IFile>> {
    return super.delete<IFile>(`${this.resourceUrl}/${fileId}`);
  }

  /**
   * view resource file
   * @param fileId
   */
  viewFileResource(fileId: string): string {
    return `${this.resourceUrl}/${fileId}/view?token=${this.authService.getToken()}`;
  }

  // viewFileResource(fileId: string): Observable<EntityResponseType> {
  //   return this.httpClient.get<IBaseResponse>(`${this.resourceUrl}/${fileId}/view`, {observe: 'response'});
  // }

  /**
   * download resource file
   * @param fileId
   */
  downloadFileResource(fileId: string): string {
    return `${this.resourceUrl}/${fileId}/download?token=${this.authService.getToken()}`;
  }

  /**
   * search file
   * @param options
   */
  searchFile(options: any): Observable<EntityResponseType<IFile>> {
    // const httpParam = CommonUtil.formatParams(options);
    return super.get<IFile>(`${this.resourceUrl}/search`);
  }

  downloadFileByUrl(url: string,  loading: boolean,): Observable<any> {
    return this.httpClient.get(url, {
      headers: loading ? new HttpHeaders({
        loading: BOOLEAN_STRING.TRUE,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }) : new HttpHeaders(),
      responseType: 'blob',
      // observe: 'response'
    });
  }
  downloadFileByUrlError(url: string, data:any, loading: boolean): Observable<any> {
    return this.httpClient.post(url, data ,{
      headers: loading ? new HttpHeaders({
        loading: BOOLEAN_STRING.TRUE,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }) : new HttpHeaders(),
      responseType: 'blob',
      // observe: 'response'
    });
  }
  downloadFileUpload(file: any): void{
    this.downloadFileByUrl(file, false).subscribe(response => {
      let fileName = 'list-tickets';
      CommonUtil.downloadFileType(response, 'excel', fileName);
    });
  }
  downloadFile(file: any): void {
    this.downloadFileByUrl(file, false).subscribe(response => {
      let today = new Date();
      let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
      let fileName = 'Danh sách ticket' + ' ' + date;
      CommonUtil.downloadFileType(response, 'excel', fileName);
    });
  }
  downloadFileProduct(file: any): void {
    this.downloadFileByUrl(file, false).subscribe(response => {
      let today = new Date();
      let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
      let fileName = 'list-product' + ' ' + date;
      CommonUtil.downloadFileType(response, 'excel', fileName);
    });
  }
  downloadFileDepartment(file: any): void {
    this.downloadFileByUrl(file, false).subscribe(response => {
      let today = new Date();
      let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
      let fileName = 'Danh sách đơn vị' + ' ' + date;
      CommonUtil.downloadFileType(response, 'excel', fileName);
    });
  }
  downloadFileCustomer(file: any): void {
    this.downloadFileByUrl(file, false).subscribe(response => {
      let today = new Date();
      let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
      let fileName = 'Danh sách khách hàng' + ' ' + date;
      CommonUtil.downloadFileType(response, 'excel', fileName);
    });
  }
  downloadFileUser(file: any): void {
    this.downloadFileByUrl(file, false).subscribe(response => {
      let today = new Date();
      let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
      let fileName = 'Danh sách người dùng' + ' ' + date;
      CommonUtil.downloadFileType(response, 'excel', fileName);
    });
  }
  downloadFileRole(file: any): void {
    this.downloadFileByUrl(file, false).subscribe(response => {
      let today = new Date();
      let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
      let fileName = 'list-role' + ' ' + date;
      CommonUtil.downloadFileType(response, 'excel', fileName);
    });
  }
  downloadFileUserGroup(file: any): void {
    this.downloadFileByUrl(file, false).subscribe(response => {
      let today = new Date();
      let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
      let fileName = 'list-user-group' + ' ' + date;
      CommonUtil.downloadFileType(response, 'excel', fileName);
    });
  }
  downloadFileTemplateProduct(file: any): void {
    let fileName = 'product_template';
    this.downloadFileByUrl(file, false).subscribe(response => {
      CommonUtil.downloadFileType(response, 'excel', fileName);
    })
  }
  downloadFileTemplate(file: any): void {
    let fileName = 'tickets_template';
    this.downloadFileByUrl(file, false).subscribe(response => {
      CommonUtil.downloadFileType(response, 'excel', fileName);
    })
  }
  downloadFileError(file: any, data:any): void {
    let today = new Date();
    let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
    let fileName = 'tickets_error_' + date;
    this.downloadFileByUrlError(file, data, false).subscribe(response => {
      CommonUtil.downloadFileType(response, 'excel', fileName);
    })
  }
  downloadFileTemplateRTickets(file: any): void {
    let fileName = 'Báo cáo xử lý ticket';
    this.downloadFileByUrl(file, false).subscribe(response => {
      CommonUtil.downloadFileType(response, 'excel', fileName);
    })
  }
  downloadFileTemplateRTicketsOutdate(file: any): void {
    let fileName = 'Báo cáo chi tiết yêu cầu quá hạn';
    this.downloadFileByUrl(file, false).subscribe(response => {
      CommonUtil.downloadFileType(response, 'excel', fileName);
    })
  }
  downloadFileReport(file: any, name: any): void {
    this.downloadFileByUrl(file, false).subscribe(response => {
      let today = new Date();
      let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
      let fileName = name + ' ' + date;
      CommonUtil.downloadFileType(response, 'excel', fileName);
    });
  }
}
