import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AbstractService, EntityResponseType } from './common/abstract.service';

@Injectable({
  providedIn: 'root'
})
export class SlaInternalService extends AbstractService{

  constructor(protected http: HttpClient) {
    super(http);
  }
  getSlaInternal(id : any , params: any , loading=true): Observable<EntityResponseType<any>>{
    // let searchSubString = params.search ? `&search=${params.search}` : '';
    
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=created_at&sortedBy=desc';
    return super.get<any[]>(`slainternals/findbyworkflow/${id}?limit=${params.pageSize}&page=${params.pageIndex}`, { loading } );
  } 
  getWorkFlows(loading = true): Observable<EntityResponseType<any[]>> {
    return super.get<any[]>(`workflows`, {loading });
  }
  create(data: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`slainternals`, data , { loading })
  }
  delete(ids: any, loading = true): Observable<EntityResponseType<any>> {
    return super.post<any[]>(`slainternals/multipledelete`, {slaInternalIds: ids}, { loading });
  }
  updateWorflow(id: any, loading = true): Observable<EntityResponseType<any>> {
    return super.get<any>(`workflows/${id}?include=steps,transactions`, { loading });
  }
  detailSlaInternal(id: any ,  loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`slainternals/${id}`, { loading })
  }
  updateSlaInternal(id: any , data: any ,  loading = true): Observable<EntityResponseType<any[]>>{
    return super.patch<any[]>(`slainternals/${id}`,  data , { loading })
  }
}
