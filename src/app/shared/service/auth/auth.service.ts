import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { Observable, throwError } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import jwt_decode from 'jwt-decode';
import { API_IAM, BASE_API } from '../../constants/base.constant';
import { LOCAL_STORAGE, SESSION_STORAGE } from '../../constants/local-session-cookies.constants';
import { USER_CUSTOMER } from '../../constants/user.constant';
import { UserPrimary } from '../../models/user-primary.model';
import { User } from '../../models/user.model';
import CommonUtil from '../../utils/common-utils';
import { ToastService } from '../helpers/toast.service';
import { omit } from 'lodash';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { CookieService } from 'ngx-cookie-service';

type EntityResponseType = HttpResponse<any>;

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  public currentUser: any;
  private tokenPrivateKey?: string;
  permissionUser: any = [];

  constructor(
    private http: HttpClient,
    private $localStorage: LocalStorageService,
    private $sessionStorage: SessionStorageService,
    private translateService: TranslateService,
    private router: Router,
    protected toast: ToastService,
    private cookieService: CookieService
  ) {
  }

  myProfile(): Observable<EntityResponseType> {
    return this.http
      .get<any>(`${BASE_API}user/profile/?include=roles,departmentse,userGroups,`, { observe: 'response' });
  }

  myAuthorities(userId: string): Observable<EntityResponseType> {
    return this.http
      .get<any>(`${API_IAM}/users/${userId}/authorities`, { observe: 'response' });
  }

  storeProfile(redirectUrl?: string, isShowToast = true): void {
    this.myProfile().subscribe(response => {
      const currentUser = response?.body?.data
      this.currentUser = currentUser;

      this.permissionUser = this.currentUser.permissions.map((item: any) => {
        return item?.name;
      });
      
      if (currentUser.accountType === USER_CUSTOMER) {
        this.clear();
        this.toast.info('model.login.error.permissionNotAccess');
      } else {
        if (isShowToast) {
          this.toast.success(this.translateService.instant('model.login.success.authenticate'));
        }
        this.$localStorage.store(LOCAL_STORAGE.PROFILE, currentUser);
        if (currentUser.is_admin){
          currentUser.permission_group_code[0]
            switch(currentUser.permission_group_code[0]){
              case "access-dashboard":
                this.router.navigate([`admin/${ROUTER_UTILS.base.dashboard}`]);
                break;
              case "list-ticket":
                  this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}`]);
                  break;
              case "list-product":
                  this.router.navigate([`admin/${ROUTER_UTILS.serviceProducts.root}`]);
                  break;
              case "list-departmen":
                  this.router.navigate([`admin/${ROUTER_UTILS.organization.root}`]);
                  break;
              case "list-customer":
                  this.router.navigate([`admin/${ROUTER_UTILS.customer.root}`]);
                  break;
              case "list-users":
                  this.router.navigate([`admin/${ROUTER_UTILS.user.root}`]);
                  break;
                  default:
                  this.router.navigate([`${ROUTER_UTILS.endUser.root}`]);
                }
               
        }
        else {
          this.router.navigate([`${ROUTER_UTILS.endUser.root}`]);
        }
        // if (redirectUrl) {
        //   this.router.navigate([`${redirectUrl}`]);
        // }
      }
    });
  }

  login(email: string, password: string, rememberMe = false): Observable<any> {
    return this.http
      .post<any>(`${BASE_API}clients/web/login`, {
        email,
        password
      }, {
        headers: CommonUtil.headers(true)
      })
      .pipe(map(this.authenticateSuccess.bind(this, rememberMe)));
  }

  logout(): Observable<any> {
    const url = `${API_IAM}/logout`;
    let logoutRevokeRequest = {};
    const refreshToken = this.$localStorage.retrieve(LOCAL_STORAGE.REFRESH_TOKEN);
    if (refreshToken) {
      logoutRevokeRequest = {
        refreshToken,
      };
    }
    return this.http.post<any>(url, logoutRevokeRequest);
  }

  authenticateSuccess(rememberMe: boolean, response: any): void {
    const accessToken = response?.access_token;
    const refreshToken = response?.refresh_token;
    // const isSuccess = response?.success;
    // const decoder: any = jwt_decode(accessToken);
    // get user id from jwt token
    // this.tokenPrivateKey = decoder?.user_id;
    if (!accessToken) {
      this.toast.error(this.translateService.instant('model.login.error.unauthorized'));
      this.router.navigate(['/authentication/login']);
    } else {
      if (!!refreshToken) {
        // const endCodeRefreshToken = CommonUtil.encryptMessage(refreshToken, this.getTokenPrivateKey());
        const endCodeRefreshToken = refreshToken;
        this.$localStorage.store(LOCAL_STORAGE.REFRESH_TOKEN, endCodeRefreshToken);
      }
      if (accessToken) {
        this.storeAuthenticationToken(accessToken, rememberMe);
        return accessToken;
      }
    }
  }

  storeAuthenticationToken(jwt: any, rememberMe: boolean): void {
    // const endCodeAccessToken = CommonUtil.encryptMessage(jwt, this.getTokenPrivateKey());
    const endCodeAccessToken = jwt
    if (rememberMe) {
      this.$localStorage.store(LOCAL_STORAGE.JWT_TOKEN, endCodeAccessToken);
    } else {
      this.$sessionStorage.store(SESSION_STORAGE.JWT_TOKEN, endCodeAccessToken);
    }
  }

  clear(): void {
    this.cookieService.delete('token', '/');
    this.$localStorage.clear(LOCAL_STORAGE.PROFILE);
    this.$localStorage.clear(LOCAL_STORAGE.JWT_TOKEN);
    this.$sessionStorage.clear(SESSION_STORAGE.JWT_TOKEN);
    this.$localStorage.clear(LOCAL_STORAGE.REFRESH_TOKEN);
  }

  getCurrentUser(): User | null {
    const user: User = this.$localStorage.retrieve(LOCAL_STORAGE.PROFILE);
    return user ? user : null;
  }

  hasAnyAuthority(authorities: string[]): boolean {
    this.currentUser = this.getCurrentUser();
    // let roles: any = [];
    // roles = this.currentUser?.roles?.data.map((r: any) => { return r.name});
    // if (roles.includes('admin')) {
    //   return true;
    // }
    let containsAll = authorities.length > 0 ? false : true;
    let grantedPermissions: any = [];
    if (this.currentUser) {
      if (this.currentUser.permissions) {
        grantedPermissions = this.currentUser.permissions.map((aData: any) => {
          // grantedPermissions = grantedPermissions.concat(aData?.map((pm: any) => { return pm.name}));
          return aData.name;
        });
      }
      if (grantedPermissions && authorities.length > 0) {
        containsAll = authorities.some(element => grantedPermissions.includes(element));
      }
    }
    return containsAll;
  }


  hasPermissionAccess(authorities: string[], userLevel?: string[]): void {
    if (!!userLevel && userLevel?.length > 0) {
      this.currentUser = this.getCurrentUser();
      if (!userLevel.includes(this.currentUser?.userPrimary?.userLevel) && !this.currentUser?.isRoot) {
        window.history.back();
      }
    }
    const hasAnyAuthority = this.hasAnyAuthority(authorities);
    if (!hasAnyAuthority) {
      window.history.back();
    }
  }

  hasUserLevelAccess(userLevels: string[]): boolean {
    this.currentUser = this.getCurrentUser();
    const currentUserLevel = this.getCurrentUser()?.userPrimary?.userLevel as string;
    return !(!userLevels.includes(currentUserLevel) && !this.currentUser?.isRoot);
  }

  getToken(): string {
    const accessTokenEncode = this.$localStorage.retrieve(LOCAL_STORAGE.JWT_TOKEN)
      || this.$sessionStorage.retrieve(SESSION_STORAGE.JWT_TOKEN);
    // return CommonUtil.decryptMessage(accessTokenEncode, this.getTokenPrivateKey());
    return accessTokenEncode
  }

  refreshToken(refreshToken: any): Observable<any> {
    return this.http.post<any>(`${API_IAM}/refresh-token`, { refreshToken }, {
      headers: {
        Authorization: `Bearer ${refreshToken}`,
      }
    }).pipe(
      tap(
        (res) => {
          const endCodeRefreshToken = CommonUtil.encryptMessage(res?.data?.refreshToken, this.getTokenPrivateKey());
          this.$localStorage.store(LOCAL_STORAGE.REFRESH_TOKEN, endCodeRefreshToken);
          this.storeAuthenticationToken(res?.data?.accessToken, true);
        },
        (err) => throwError(err)
      )
    );
  }

  resetPassword(userId: string, newPassword: string, oldPassword: string, refreshToken: string): Observable<any> {
    return this.http
      .post<any>(`${API_IAM}/user/${userId}/change-password`,
        {
          newPassword,
          oldPassword,
          refreshToken
        });
  }

  getTokenPrivateKey(): string {
    let tokenPrivateKey = '';
    if (!this.tokenPrivateKey) {
      // nếu không có key tồn tại thì lấy key từ userProfile local storage
      // console.error('Token private key is not defined');
      const user = this.getCurrentUser();
      if (user) {
        tokenPrivateKey = user.id as string;
        this.tokenPrivateKey = tokenPrivateKey;
      } else {
        // nếu trong local storage không có user thì xoá hết thông tin profile
        console.error('User is not defined');
        this.clear();
      }
    } else {
      tokenPrivateKey = this.tokenPrivateKey;
    }
    return tokenPrivateKey;
  }
  sendToken(id:any): Observable<any> {
    return this.http.post<any>(`${BASE_API}init-app/store-token`, {device_token:id});
  }
}
