import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { AbstractService, EntityResponseType } from './common/abstract.service';
import { FileService } from './file.service';

@Injectable({
  providedIn: 'root'
})
export class ReportRequestUpgradeService extends AbstractService {
  private API_URL = environment.gateway;
  constructor(
    protected http: HttpClient,
    private fileService: FileService,) {
    super(http);
  }

  getReportRequestUpgrade(params: any, fillterDate: any, loading = true): Observable<EntityResponseType<any[]>>{
    let  orderBy = params.orderBy ? params.orderBy.replaceAll(',', ';') : 'created_at';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',', ';') : 'desc';
    let options = {
      limit: params.pageSize,
      page: params.pageIndex,
      search: params.keyword,
      fromDate: fillterDate?.from_date,
      toDate: fillterDate?.to_date,
      product_ids: params?.product_id,
      department_ids:params?.department_ids,
      user_ids:params?.user_ids,
      source_ids:params?.source_ids,
      state_ids: params?.state_ids,
      priority_ids: params?.priority_ids,
      created_bys: params?.created_bys,
      created_by_department_ids: params?.created_by_department_ids,
      is_feature: params?.is_feature
    };
   let  sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '';
    return super.get<any[]>(`report-request-upgrade?${sort}`, { params: options, loading });
  }

  exportReportData(params: any, loading = true): any {
    let options = {
        search: params.keyword,
        fromDate: params?.from_date,
        toDate: params?.to_date,
        product_ids: params?.product_id,
        department_ids:params?.department_ids,
        user_ids:params?.user_ids,
        source_ids:params?.source_ids,
        state_ids: params?.state_ids,
        priority_ids: params?.priority_ids,
        created_bys: params?.created_bys,
        created_by_department_ids: params?.created_by_department_ids,
        is_feature: params?.is_feature
    };
    let fillterAll = `orderBy=created_at&sortedBy=desc&search=${options.search}&toDate=${options.toDate}&fromDate=${options.fromDate}&product_ids=${options.product_ids}&department_id=${options.department_ids}&user_ids=${options.user_ids}&created_bys=${options.created_bys}&source_ids=${options.source_ids}&state_ids=${options.state_ids}&priority_ids=${options.priority_ids}&created_by_department_ids=${options.created_by_department_ids}&is_feature=${options.is_feature}`
    if (options.is_feature) {
        this.fileService.downloadFileReport(`${this.API_URL}report-request-upgrade/export?${fillterAll}`,'Báo cáo yêu cầu nâng cấp tính năng');
    } else {
        this.fileService.downloadFileReport(`${this.API_URL}report-request-upgrade/export?${fillterAll}`,'Báo cáo yêu cầu nâng cấp giao diện');
    }
  }
}
