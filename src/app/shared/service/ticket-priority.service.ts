import { Injectable } from '@angular/core';
import { AbstractService, EntityResponseType } from './common/abstract.service';
import { HttpClient } from '@angular/common/http';
import { SERVICE } from '@shared/constants/gateway-routes-api.constant';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class TicketPriorityService extends AbstractService {
  constructor(
    protected http : HttpClient
  ) {
    super(http);
   }
  search(params?: any, loading = true): Observable<EntityResponseType<any[]>>{
    const param ={search:params.keyword};
    // let searchSubString = params.keyword ? `&search=${params.keyword}` : '';
    let orderBy = params.orderBy ? params.orderBy.replaceAll(',',';') : '';
    let sortBy = params.sortBy ? params.sortBy.replaceAll(',',';')  : '';
    let sort = orderBy && sortBy ? `&orderBy=${orderBy}&sortedBy=${sortBy}` : '&orderBy=order&sortedBy=asc';
    return super.get<any[]>(`ticketpriorities?limit=${params.pageSize}&page=${params.pageIndex}${sort}`, {params:param, loading })
  }
  list(): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`ticketpriorities`)
  }
  getAll(loading = true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`ticketpriorities?status=1`, { loading })
  }
  getAllTicketPriority(params :any,loading=true): Observable<EntityResponseType<any[]>>{
    return super.get<any[]>(`ticketpriorities?limit=${params.pageSize}`, { params,loading })
  }
  create(ticketcategories: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.post<any[]>(`ticketpriorities`,ticketcategories, { loading })
  }
  update(id: any ,ticketcategories: any, loading = true): Observable<EntityResponseType<any[]>>{
    return super.patch<any[]>(`ticketpriorities/${id}`,ticketcategories, { loading })
  }
  delete(ids: any, loading = true): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`ticketpriorities/multipledelete`, {ticketpriorityIds: ids}, { loading });
  }
  updateOrder(params: any, loading = false): Observable<EntityResponseType<any[]>> {
    return super.post<any[]>(`ticketpriorities/update-order`, params, { loading });
  }
}
