import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.scss'],
})
export class ModalConfirmComponent implements OnInit {

  @Input() title = '';
  @Input() content = '';
  @Input() interpolateParams: object = {};
  @Input() okText = 'action.confirm';
  @Input() cancelText = 'action.cancel';
  @Input() width: string | number = '25%';
  @Input() isVisible = false;
  @Input() centered = true;

  @Input() showTitle = true;
  @Input() showClose = true;
  @Input() showBtnCancel = true;
  @Input() btnCentered = false;
  @Input() closable = true;
  @Input() callBack?: Function;
  @Output() emitter: EventEmitter<any> = new EventEmitter();

  constructor(private translateService: TranslateService,
    private modalRef:NzModalRef) {
  }

  ngOnInit(): void {
  }

  showContent(): string{
    return this.translateService.instant(this.content, this.interpolateParams);
  }

  handle(): void {
    if (this.callBack) {
      this.callBack();
    }
    this.modalRef.close()
    this.emitter.emit({
      success: true,
    });
  }

  cancel(): void {
    this.modalRef.close()
    this.emitter.emit({
      success: false,
    });
  }
}
