
import { AfterContentChecked, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ACTION } from '@shared/constants/common.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { NzModalRef,NzModalService } from 'ng-zorro-antd/modal';
import { TicketService } from '@shared/service/ticket.service';
import CommonUtil from '@shared/utils/common-utils';
import { ConfirmExportErrorComponent } from './confirm-export/confirm-export.component';

@Component({
    selector: 'app-import-modal',
    templateUrl: './import-modal.component.html',
    styleUrls: ['./import-modal.component.scss']
})
export class ImportModalComponent implements OnInit, AfterContentChecked {
    @Input() selectedDepartment: any = []
    @Input() isUpdate: boolean = false
    @Input() action: any = ACTION.CREATE
    dataFile: any;
    checkErrorUpload: boolean = false;
    checkErrorAmoutUpload:any = false;
    constructor(
        private ticketService: TicketService,
        private modalRef: NzModalRef,
        private cdr: ChangeDetectorRef,
        private modalService: NzModalService,
        private toast: ToastService,) { }

    ngOnInit() {

    }

    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }

    handleLoadFile(e: any) {

        this.dataFile = e[0];

    }
    onCancel(): void {
        this.modalRef.close({
            success: false,
            value: null,
        });
    }
    addEvent(e: any) {
        let errArr = e.filter((f: any) => f !== undefined);
        if (errArr.length > 0) {
            this.checkErrorUpload = false;
          } else {
            this.checkErrorUpload = true;
          }
      }
      addEvent1(e: any){
        if(e){
            this.checkErrorAmoutUpload = true;
        }else{
            this.checkErrorAmoutUpload = false; 
        }
    }
    exportTemplate() {
        this.ticketService.exportTemplate();
    }
    handleOk(): void {
        if(this.checkErrorUpload || this.checkErrorAmoutUpload){
            this.ticketService.importTicketData(this.dataFile, true).subscribe(res => {
                if (res?.body?.status == "success") {
                    
                    this.toast.success(`Import file thành công`);
                    this.modalRef.close({
                        success: true,
                        value: true,
                    });
                } else {
                    const base = CommonUtil.modalBase(ConfirmExportErrorComponent, { data:res }, '24%');
                    const modal: NzModalRef = this.modalService.create(base);
                    modal.afterClose.subscribe(result => {
                    });
                }
            }
            );
        }
        else{
            this.toast.error(`Không đúng định dạng file`);
        }
       
    }
}
