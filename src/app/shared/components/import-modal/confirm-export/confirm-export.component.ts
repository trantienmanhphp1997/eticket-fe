import { Component, Input, OnInit } from '@angular/core';
import { ACTION } from '@shared/constants/common.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TicketService } from '@shared/service/ticket.service';
import CommonUtil from '@shared/utils/common-utils';
@Component({
    selector: 'app-import-modal',
    templateUrl: './confirm-export.component.html',
    styleUrls: ['./confirm-export.component.css']
})
export class ConfirmExportErrorComponent implements OnInit {
    @Input() data:any;
    constructor(
        private modalRef: NzModalRef,
        private modalService: NzModalService,
        private toast: ToastService,
        private ticketService: TicketService,
    ) { }

    ngOnInit() {
    }

    onCancel(): void {
        this.modalRef.close({
            success: false,
            value: null,
        });
    }
    handleOk(): void {
        const params = {
            status:"error",
            message:{
                errorRequiredProductId:this.data.body.message?.errorRequiredProductId ? this.data.body.message?.errorRequiredProductId :"",
                errorRequiredSourceId:this.data.body.message?.errorRequiredSourceId ? this.data.body.message?.errorRequiredSourceId : "",
                errorRequiredCategoryId:this.data.body.message?.errorRequiredCategoryId ? this.data.body.message?.errorRequiredCategoryId :"",
                errorRequiredStateId:this.data.body.message?.errorRequiredStateId ? this.data.body.message?.errorRequiredStateId : "",
                errorRequiredTitle:this.data.body.message?.errorRequiredTitle ? this.data.body.message?.errorRequiredTitle : ""
            }
        }
        this.ticketService.exportFileError(params,true);
    }
}