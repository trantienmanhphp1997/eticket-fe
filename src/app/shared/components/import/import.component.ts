import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MAX_FILE_SIZE, FILE_SIZE, LIMIT_FILE } from '@shared/constants/file.constant';
import { CommonService } from '@shared/service/common.service';
import { FileService } from '@shared/service/file.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { MediaService } from '@shared/service/media.service';
import CommonUtil from '@shared/utils/common-utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {

  @Input() action = '';
  @Input() key: any;
  @Input() className = '';
  @Input() disable = false;
  @Input() isShow = true;
  @Input() easyUpload = true;
  @Input() multiple = false
  @Input() typeUpload = 'uploadToServer' // uploadToServer | uploadGetFile
  @Input() upload = false;
  @Input() acceptTypeFiles: string[] = ['default' || 'excel' ];
  @Input() acceptFileDescription: string = "Import file theo đúng định dạng"
  @Output() emitter: EventEmitter<any> = new EventEmitter();
  @Output() emitterError :any = new EventEmitter();
  @Output() emitterAmountError :any = new EventEmitter();
  errorFile: any;
  checkAmountFileUpload = '';
  errorAmount = false;
  readonly typeFiles = [

    {
      type: 'excel',
      value: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      icon: 'file-excel'
    },
    {
      type: 'excel',
      value: 'application/vnd.ms-excel',
      icon: 'file-excel'
    },


  ];
  acceptFiles: string[] = [];
  files: any = [];
  uploadState: any
  constructor(
    private translate: TranslateService,
    private mediaService: MediaService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toast: ToastService,
  ) {
  }

  ngOnInit(): void {
    this.filesAccept();
    // this.files();
  }

  handleChange(event: any) {
    const file = event.target.files[0];
    this.files = this.valid([file]);
    this.emitter.emit(this.files);
  }

  filesAccept(): void {
    if (this.acceptTypeFiles.includes('default')) {
      this.acceptFiles = this.typeFiles.map(file => file.value);
    } else {
      this.acceptFiles = this.typeFiles
        .filter(file => this.acceptTypeFiles.includes(file.type))
        .map(val => val.value);
    }
  }

  valid(files: any) {
    return files.length <= LIMIT_FILE && files.filter((file: any) => this.acceptFiles.includes(file?.type) && file?.size <= MAX_FILE_SIZE);
  }

  onFileChange(pFileList: any) {
    this.files = Array.from((Object.keys(pFileList).map((key: any) => pFileList[key])));
  }

  openFile(event: any) {
    const input = event.target as HTMLInputElement;

    if (!input.files?.length) {
      return;
    }
    this.files = this.multiple ? Array.from(this.files).concat(Array.from(input.files)) : Array.from(input.files);
    this.onUpload(this.files);

  }
  deleAllFile(){
      this.files = [];
  }
  deleteFile(f: any) {
    this.files = Array.from(this.files).filter(function (w: any) { return w.name != f.name });
    this.emitterError.emit(this.files.map((e:any) => e.error));
    this.emitter.emit(this.files.map((f: any) => f.id))
  }

  openConfirmDialog(pIndex: any): void {
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc chắn muốn xóa file này không ? `,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'25%',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.deleteFile(pIndex);
        this.onUpload(this.files);
      }
    });
  }

  deleteFromArray(index: any) {
    Array.from(this.files).splice(index, 1);
  }

  onUpload(file: any) {
    if(file.length > LIMIT_FILE){
      this.errorAmount = true;
      this.checkAmountFileUpload = 'Tải lên tối đa ' + LIMIT_FILE + ' File'
      this.emitterAmountError.emit(this.errorAmount);
    }else{
      this.errorAmount = false;
      this.emitterAmountError.emit(this.errorAmount);
    }
    switch (this.typeUpload) {
      case 'uploadToServer':
       file.forEach((element: any, index: number) => {
          this.files[index].icon = this.typeFiles.find((e: any) => e.value === element.type)?.icon || 'file-unknown'
          if (!this.acceptFiles.includes(element.type)) {
            this.files[index].error = 'File không đúng định dạng';
            // this.emitterError.emit(this.files[index].error);
            return
          }

          if (element.size > MAX_FILE_SIZE) {
            this.files[index].error = 'Dung lượng file vượt quá ' + FILE_SIZE + 'MB';
            // this.emitterError.emit(this.files[index].error);
            return
          }


          if (this.files[index].state === undefined || this.files[index].state === null) {
            this.mediaService.upload(this.key, element).subscribe((res: any) => {
              this.files[index].progress = res.progress,
                this.files[index].state = res.state

              if (res.state === 'success') {
                // console.log(res.mediaBody);

                this.files[index].id = res.mediaBody[0].data.id
                this.emitter.emit(this.files.map((f: any) => f?.id))
                // this.emitter.emit(res.mediaBody);
              }
            }, (err: any) => {
              this.files[index].state = 'exception'
              this.uploadState = { progress: 100, state: 'exception' }
              // this.emitterError.emit('Upload lỗi');
            })
          }
        });

        this.emitterError.emit(this.files.map((e:any) => e.error));
        break;
      case 'uploadGetFile':
          file.forEach((element: any, index: number) => {
            this.files[index].icon = this.typeFiles.find((e: any) => e.value === element.type)?.icon || 'file-unknown'
            if (!this.acceptFiles.includes(element.type)) {
              this.files[index].error = 'File không đúng định dạng';
              // this.emitterError.emit(this.files[index].error);
              return
            }

            if (element.size > MAX_FILE_SIZE) {
              this.files[index].error = 'Dung lượng file vượt quá ' + FILE_SIZE + 'MB';
              // this.emitterError.emit(this.files[index].error);
              return
            }

          });
          this.emitterError.emit(this.files.map((e:any) => e.error));
          this.emitter.emit(this.files);
        break;
      default:
        break;
    }


  }
  formatBytes(bytes: any, decimals = 2) {
    if (bytes === 0) return '0 Bytes'

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

}
