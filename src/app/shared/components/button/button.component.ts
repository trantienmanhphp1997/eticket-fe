import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mbf-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() isLoading: boolean = false;
  @Input() text = '';
  @Input() icon = '';
  @Input() iconResource = '';
  @Input() iconImageSuffix = 'png';
  @Input() disabled = false;
  @Input() tooltip = '';
  @Input() iconColor = 'text-black';
  @Input() type: 'primary' | 'default' | 'danger' | 'dashed' = 'primary';
  @Input() themeType: 'fill' | 'outline' | 'twotone' = 'outline';
  @Input() shapeType: 'circle' | 'radius' | 'default' = 'default';
  @Input() customClass = ''

  constructor() { }

  ngOnInit(): void {
  }

  getSource(): string {
    return `assets/images/button/${this.iconResource}.${this.iconImageSuffix}`;
  }

  getClassStyle(type: string, shapeType: string, customClass: string): string {
    return `ant-btn${type} btn-${shapeType} ${customClass}`
  }
}
