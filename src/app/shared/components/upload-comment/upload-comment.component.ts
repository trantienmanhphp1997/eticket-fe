import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MAX_FILE_SIZE, FILE_SIZE, LIMIT_FILE, MAX_FILE_MAIL_SIZE, FILE_SIZE_MAIL } from '@shared/constants/file.constant';
import { CommonService } from '@shared/service/common.service';
import { FileService } from '@shared/service/file.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { MediaService } from '@shared/service/media.service';
import CommonUtil from '@shared/utils/common-utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'mbf-upload-comment',
  templateUrl: './upload-comment.component.html',
  styleUrls: ['./upload-comment.component.css']
})
export class UploadCommentComponent implements OnInit {
  @Input() action = '';
  @Input() key: any;
  @Input() className = '';
  @Input() disable = false;
  @Input() isShow = true;
  @Input() easyUpload = true;
  @Input() multiple = false
  @Input() typeUpload = 'uploadToServer' // uploadToServer | uploadGetFile
  @Input() upload = false;
  @Input() isMail = '';
  @Input() acceptTypeFiles: string[] = ['default' ||'svg'|| 'webm' || 'gif'||'csv' ||'docx' || 'excel' || 'pdf' || 'image'|| 'audio' || 'video'||'zip' || 'txt'];
  @Input() acceptFileDescription: string = "JPG, PNG, XLSX, CSV, WEBM, GIF, SVG, DOCX, MP3, MP4, ZIP, RAR, TXT... or PDF"
  @Output() emitterComment: EventEmitter<any> = new EventEmitter();
  @Output() emitterErrorComment :any = new EventEmitter();
  @Output() emitterAmountError :any = new EventEmitter();
  @Output() emitterError :any = new EventEmitter();
  @Output() emitterUploading :any = new EventEmitter();
  checkAmountFileUpload = '';
  errorAmount = false;
  errorFile: any;
  readonly typeFiles = [
    {
      type: 'docx',
      value: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      icon: 'file-word'
    },
    {
      type: 'webm',
      value: 'video/webm',
      icon: 'file-word'
    },
    {
      type: 'svg',
      value: 'image/svg+xml',
      icon: 'file-word'
    },
    {
      type: 'gif',
      value: 'image/gif',
      icon: 'file-word'
    },
    {
      type: 'csv',
      value: 'text/csv',
      icon: 'file-word'
    },
    {
      type: 'docx',
      value: 'application/msword',
      icon: 'file-word'
    },
    {
      type: 'excel',
      value: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      icon: 'file-excel'
    },
    {
      type: 'excel',
      value: 'application/vnd.ms-excel',
      icon: 'file-excel'
    },
    {
      type: 'pdf',
      value: 'application/pdf',
      icon: 'file-pdf'
    },
    {
      type: 'image',
      value: 'image/jpeg',
      icon: 'file-image'
    },
    {
      type: 'image',
      value: 'image/png',
      icon: 'file-image'
    },
    {
      type:'audio/mpeg',
      value:'audio/mpeg',
      icon:'file-markdown',
    },
    {
      type:'video/mp4',
      value:'video/mp4',
      icon:'file-markdown',
    },
    {
      type:'application/x-zip-compressed',
      value:'application/x-zip-compressed',
      icon:'file-zip',
    },
    {
      type:'text/plain',
      value:'text/plain',
      icon:'file-text',
    }

  ];
  acceptFiles: string[] = [];
  filesComment: any = [];
  uploadState: any;
  checkFileUpload = '';
  errorMail = false;
  constructor(
    private translate: TranslateService,
    private mediaService: MediaService,
    private modalService: NzModalService,
    private translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.filesAccept();
    // this.files();

  }

  handleChange(event: any) {
    const file = event.target.files[0];
    this.filesComment = this.valid([file]);
    this.emitterComment.emit(this.filesComment);
  }
  filesAccept(): void {
    if (this.acceptTypeFiles.includes('default')) {
      this.acceptFiles = this.typeFiles.map(file => file.value);
    } else {
      this.acceptFiles = this.typeFiles
        .filter(file => this.acceptTypeFiles.includes(file.type))
        .map(val => val.value);
    }
  }

  valid(files: any) {
    return files.filter((file: any) => this.acceptFiles.includes(file?.type) && file?.size <= MAX_FILE_SIZE);
  }
  deleAllFile(){
    this.filesComment = [];
  }
  onFileChange(pFileList: any) {
    this.filesComment = Array.from((Object.keys(pFileList).map((key: any) => pFileList[key])));
  }

  openFile(event: any) {
    const input = event.target as HTMLInputElement;

    if (!input.files?.length) {
      return;
    }
    this.filesComment = this.multiple ? Array.from(this.filesComment).concat(Array.from(input.files)) : Array.from(input.files);
    this.onUpload(this.filesComment);

  }

  deleteFile(f: any) {
    this.filesComment = Array.from(this.filesComment).filter(function (w: any) { return w.name != f.name });
    this.emitterErrorComment.emit(this.filesComment.map((e:any) => e.error));
    this.emitterComment.emit(this.filesComment.map((f: any) => f.id))
    if (this.isMail) {
        let totalSize = 0;
        this.filesComment.forEach((element: any, index: number) => {
            totalSize += element.size;
        });
        if (totalSize > MAX_FILE_MAIL_SIZE) {
            this.errorMail = true;
            this.checkFileUpload = 'Tải tất cả file lên không quá ' + FILE_SIZE_MAIL +'MB'
            this.emitterError.emit(this.errorMail);
        } else {
            this.errorMail = false;
            this.emitterError.emit(this.errorMail);
        }
    }
    if(this.filesComment > LIMIT_FILE){
      this.errorAmount = true;
      this.checkAmountFileUpload = 'Tải lên tối đa ' + LIMIT_FILE +' File'
      this.emitterAmountError.emit(this.errorAmount);
    }else{
      this.errorAmount = false;
      this.emitterAmountError.emit(this.errorAmount);
    }
  }

  openConfirmDialog(pIndex: any): void {
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc chắn muốn xóa file này không ? `,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'25%',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.deleteFile(pIndex);
      }
    });
  }

  deleteFromArray(index: any) {
    Array.from(this.filesComment).splice(index, 1);
  }

  onUpload(file: any) {
    if(file.length > LIMIT_FILE){
      this.errorAmount = true;
      this.checkAmountFileUpload = 'Tải lên tối đa ' + LIMIT_FILE +' File'
      this.emitterAmountError.emit(this.errorAmount);
    } else {
      this.errorAmount = false;
      this.emitterAmountError.emit(this.errorAmount);
    }
    if (this.isMail) {
        let totalSize = 0;
        file.forEach((element: any, index: number) => {
            totalSize += element.size;
        });
        if (totalSize > MAX_FILE_MAIL_SIZE) {
            this.errorMail = true;
            this.checkFileUpload = 'Tải tất cả file lên không quá ' + FILE_SIZE_MAIL +'MB'
            this.emitterError.emit(this.errorMail);
        } else {
            this.errorMail = false;
            this.emitterError.emit(this.errorMail);
        }
    }
    switch (this.typeUpload) {
      case 'uploadToServer':
       file.forEach((element: any, index: number) => {

          this.filesComment[index].icon = this.typeFiles.find((e: any) => e.value === element.type)?.icon || 'file-unknown'
          if (!this.acceptFiles.includes(element.type)) {
            this.filesComment[index].error = 'File không đúng định dạng';
            // this.emitterError.emit(this.files[index].error);
            return
          }
          if (element.size > MAX_FILE_SIZE) {
            this.filesComment[index].error = 'Dung lượng file vượt quá '+ FILE_SIZE + 'MB';
            // this.emitterError.emit(this.files[index].error);
            return
          }

          if (this.filesComment[index].state === undefined || this.filesComment[index].state === null) {
            this.mediaService.upload(this.key, element).subscribe((res: any) => {
                this.emitterUploading.emit()
                this.filesComment[index].progress = res.progress;
                this.filesComment[index].state = res.state;
              if (res.state === 'success') {
                this.filesComment[index].id = res.mediaBody[0].data.id;
                this.emitterComment.emit(this.filesComment.map((f: any) => f?.id));
                // this.emitter.emit(res.mediaBody);
              }
            }, (err: any) => {
              this.filesComment[index].state = 'exception'
              this.uploadState = { progress: 100, state: 'exception' }
              // this.emitterErrorComment.emit('Upload lỗi');
            })
          }
        });

        this.emitterErrorComment.emit(this.filesComment.map((e:any) => e.error));
        break;
      case 'uploadGetFile':
        this.emitterComment.emit(this.filesComment);
        break;
      default:
        break;
    }


  }

  formatBytes(bytes: any, decimals = 2) {
    if (bytes === 0) return '0 Bytes'

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }
}
