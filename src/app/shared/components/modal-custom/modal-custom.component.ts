import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'modal-custom',
  templateUrl: './modal-custom.component.html',
  styleUrls: ['./modal-custom.component.css'],
})
export class ModalCustomComponent implements OnInit {

  @Input() title = '';
  @Input() content = '';
  @Input() interpolateParams: object = {};
  @Input() okText = 'action.confirm';
  @Input() cancelText = 'action.cancel';
  @Input() width: string | number = '25%';
  @Input() isVisible = false;
  @Input() centered = true;
  @Input() note  = '';
  @Input() requirer = '';

  @Input() showTitle = true;
  @Input() showClose = true;
  @Input() showBtnCancel = true;
  @Input() btnCentered = false;
  @Input() closable = true;
  @Input() callBack?: Function;
  @Input() cancellation_reason : any = '';
  checkReason :boolean=false;
  @Output() emitter: EventEmitter<any> = new EventEmitter();

  constructor(private translateService: TranslateService,
    private modalRef:NzModalRef) {
  }

  ngOnInit(): void {
  }

  showContent(): string{
    return this.translateService.instant(this.content, this.interpolateParams);
  }
  onChangeReason(data:any){
    this.cancellation_reason = data;
  }

  handle(): void {
    
    if(this.cancellation_reason.length ==0){
      this.checkReason = true;
      return;
    }
    if (this.callBack) {
      this.callBack();
    }
    this.modalRef.close()
    this.emitter.emit({
      success: true,
      cancellation_reason:this.cancellation_reason
    });
  }

  cancel(): void {
    this.modalRef.close()
    this.emitter.emit({
      success: false,
    });
  }
}
