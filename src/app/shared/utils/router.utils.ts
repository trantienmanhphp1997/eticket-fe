export const ROUTER_UTILS = {
  base: {
    home: 'admin',
    dashboard: 'dashboard',
    freeRoute: '**'
  },
  authentication: {
    root: 'authentication',
    login: 'login',
  },
  profile : {
    root: 'user-profile',
    view :'view-profile',
    detail: 'detail',
  },
  ticket: {
    root: 'ticket-management',
    create: 'create',
    view: 'view-ticket',
    detail: ':ticketId/detail',
  },
  organization: {
    root: 'organization-management',
    create: 'create',
    detail: ':organizationId/detail',
  },
  authorizationMng: {
    root: 'authorization-management',
    create: 'create',
    detail: ':authId/detail',
  },
  serviceProducts: {
    root: 'service-products-management',
    create: 'create',
    detail: ':serviceProductId/detail',
  },
  ticketConfiguration: {
    root: 'ticket-configuration',
    create: 'create',
    detail: ':tkConfigId/detail',
  },
  flowConfiguration: {
    root: 'flow-configuration',
    create: 'create',
    detail: ':flowConfigId/detail',
  },
  templateConfiguration: {
    root: 'template-configuration',
    create: 'create',
    detail: ':tempConfigId/detail',
  },
  workflowConfiguration: {
    root: 'workflow-configuration',
    create: 'create',
    detail: 'detail',
    listSla: 'sla-internal',
    clone:'clone',
    view:'view'
  },
  roleManagement: {
    root: 'role-management',
    create: 'create',
    detail: ':roleId/detail',
  },
  error: {
    notFound: '404',
    permissionDenied: '403',
    systemError: '500'
  },

  user: {
    root: 'user',
    create: 'create',
    detail: ':user/detail',
  },
  userGroup: {
    root: 'user-group-management',
    create: 'create',
    detail: ':group/detail',
  },
  endUser: {
    root: '',
    create: 'create',
    detail: ':endUser/detail',
  },
  endUserTicket: {
    root: 'ticket',
    create: 'create',
    detail: 'detail',
  },
  endUserProfile: {
    root: 'profile',
    detail: 'detail',
  },
  customer: {
    root: 'customer-management',
    create: 'create',
    detail: 'detail',
  },
  sla: {
    root: 'sla-management',
    create: 'create',
    view: 'view-sla',
    detail: 'detail',
  },
  slaInternal: {
    root: 'workflow-configuration/sla-internal-management',
    create: 'create',
    view: 'view-sla',
    detail: 'detail',
  },
  ticketSource: {
    root: 'ticket-source',
    create: 'create',
    detail: 'detail',
  },
  ticketPriority: {
    root: 'ticket-priority',
    create: 'create',
    detail: 'detail',
  },
  ticketCategory: {
    root: 'ticket-category',
    create: 'create',
    detail: 'detail',
  },
  ticketState: {
    root: 'ticket-state',
    create: 'create',
    detail: 'detail',
  },
  trigger: {
    root: 'trigger',
    create: 'create',
    detail: 'detail',
    edit:':id',
    view:'view/:id'
  },
  task: {
    root: 'task',
    create: 'create',
    detail: 'detail',
  },
  reportTickets: {
    root: 'report-tickets',
  },
  reportProcessTickets: {
    root: 'report-processs-tickets',
  },
  reportKpi: {
    root: 'report-kpi-tickets',
  },
  reportOutdate: {
    root: 'report-outdate-tickets',
  },
  reportRequestUpgrade: {
    root: 'report-request-upgrade',
  }
};
