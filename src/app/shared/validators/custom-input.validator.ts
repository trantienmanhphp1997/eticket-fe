import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { CommonService } from '@shared/service/common.service';
import { Observable } from 'rxjs/internal/Observable';


export class CustomInputValidator {

    static notBlankValidator(control: AbstractControl): { [key: string]: any } | null {
        // Luu y chi dung cho input string
        if (control.value) {
            if (control.value.trim() === '') {
                return { isBlank: true };
            }
        }
        return null;
    }

    static isNumberic(control: AbstractControl) {
        const val = control.value;
        // input type : number
        if (val !== null && val <= 0) {
            return { invalidNumber: true };
        }
        return null;
    }

    static isInteger(control: AbstractControl) {
        const val = control.value;
        // input type : number
        if (val !== null && (val < 0 || !Number.isInteger(val))) {
            return { invalidNumber: true };
        }
        return null;
    }

    static isPositiveInteger(control: AbstractControl) {
        const val = control.value;
        // input type : number
        if (val !== null && (val <= 0 || !Number.isInteger(val))) {
            return { invalidNumber: true };
        }
        return null;
    }

    static removeSpaces(control: AbstractControl) {
        if (control && control.value && !control.value.replace(/\s/g, '').length) {
            control.setValue('');
        }
        return null;
    }

    static isCode(control: AbstractControl): { [key: string]: any } | null {
        if (control.value) {
            var inValid = /[ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ\s\W]/g ;
            //$`~@#%^&*();,><":=+*
            if (inValid.test(control.value.trim())) {
                return { isNotCode: true };
            }
        }
        return null;
    }

    static isNumberPhone(control: AbstractControl): { [key: string]: any } | null {
        if (control.value) {
            var valid = /^[+]?\d+$/g ;
          
            if (!valid.test(control.value.trim())) {
                return { isNotNumberPhone: true };
            }
        }
        return null;
    }
    static removeAscent (str:string) {
        if (str === null || str === undefined) return str;
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        return str;
      }
    static isName (control: AbstractControl): { [key: string]: any } | null { 
        if (control.value) {
            var valid = /^[a-zA-Z0-9._(),?:;"\'/&! ]*$/g
          
            if (!valid.test(CustomInputValidator.removeAscent(control.value.trim()))) {
                return { pattern: true };
            }
        }
        return null;
      }
    /** 
     
     * @param model : This is name of model feature suchas: departments, role,...
     * @param field : fFeld to check exist in system
     * @param commonService : This is service to call api check existed
     * @param currentValue : 
     * @returns 
     */
    static hasExited (model: any, field: any, commonService: CommonService, currentValue?: any): AsyncValidatorFn {
        return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
            if (control.value && currentValue !== control.value) {
                let param: any = {}
                param[`${field}`] = control.value
                return commonService.checkExisted(model, param).then(
                    (users: any) => {
                    return (users &&  users.body === 1) ? { "hasExisted": true } : null;
                    }
                );
            }
            return Promise.reject(null)
        };
      }

      static isEmail(control: AbstractControl): { [key: string]: any } | null {
        if (control.value) {
            var valid = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
            if (!valid.test(control.value.trim())) {
                return { isNotEmail: true };
            }
        }
        return null;
    }
}
