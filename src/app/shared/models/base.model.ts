import { IMetaable } from '@shared/interface/metaable.interface';
import { IPageable } from '../interface/pageable.interface';

export interface IBaseResponse<T> {
  success?: boolean;
  code?: number | string;
  data?: T;
  meta?: IMetaable;
  message?: string;
  page?: IPageable;
  timestamp?: string | number | any;
  status?:string;
  massage?:string;
}

// export class BaseResponse implements IBaseResponse {
//   constructor(
//     public success?: boolean,
//     public code?: number | string,
//     public data?: [] | {},
//     public message?: string,
//     public page?: IPageable,
//     public timestamp?: string | number | any,
//   ) {
//     this.success = success;
//     this.code = code;
//     this.data = data;
//     this.message = message;
//     this.page = page;
//     this.timestamp = timestamp;
//   }
// }
