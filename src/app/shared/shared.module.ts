import { CommonModule, DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NzBackTopModule } from 'ng-zorro-antd/back-top';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { BadgeComponent } from './components/badge/badge.component';
import { ButtonActionComponent } from './components/button-action/button-action.component';
import { ButtonComponent } from './components/button/button.component';
import { EditorComponent } from './components/editor/editor.component';
import { FileComponent } from './components/file/file.component';
import { ModalFileComponent } from './components/modal-file/modal-file.component';
import { ModalComponent } from './components/modal/modal.component';
import { NoDataComponent } from './components/no-data/no-data.component';
import { NotBlankComponent } from './components/not-blank/not-blank.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { SelectAllComponent } from './components/select-all/select-all.component';
import { TableTitleComponent } from './components/table-title/table-title.component';
import { UploadSimpleComponent } from './components/upload-simple/upload-simple.component';
import { UploadComponent } from './components/upload/upload.component';
import { UploadCommentComponent } from './components/upload-comment/upload-comment.component';
import { UploadAvatarComponent } from './components/upload-ava/upload-ava.component';
import { ValidateMessageComponent } from './components/validate-message/validate-message.component';
import { DebounceClickDirective } from './directive/debounce-click.directive';
import { DebounceKeyupDirective } from './directive/debounce-keyup.directive';
import { DebounceDirective } from './directive/debounce.directive';
import { DecimalFormatterDirective } from './directive/decimal-number-formatter.directive';
import { HasRolesDirective } from './directive/has-roles.directive';
import { NumberFormatterDirective } from './directive/number-formatter.directive';
import { NumbersCharacterBasicDirective } from './directive/numbers-character-basic.directive';
import { NumbersOnlyDirective } from './directive/numbers-only.directive';
import {RemoveOptionTitleDirective} from './directive/remove-option-title.directive';
import {NzCarouselModule} from 'ng-zorro-antd/carousel';
import { CurrencyVNDPipe } from './pipe/currency.pipe';
import { FilePublicComponent } from './components/file-public/file-public.component';
import { HasUserLevelDirective } from './directive/has-user-level.directive';
import { NzTreeViewModule } from 'ng-zorro-antd/tree-view';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { NzTreeSelectModule } from 'ng-zorro-antd/tree-select';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzCommentModule } from 'ng-zorro-antd/comment';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { FileDragNDropDirective } from './directive/file-drag-n-drop.directive';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { ImportModalComponent } from './components/import-modal/import-modal.component';
import { ConfirmExportErrorComponent } from './components/import-modal/confirm-export/confirm-export.component';
import { HighlighPipe } from './pipe/highlight.pipe';
import { ModalCustomComponent } from './components/modal-custom/modal-custom.component';
import {ChartsModule} from 'ng2-charts';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NotificationService } from './service/notification.service';
import { SafeHtmlPipe } from './pipe/safehtml.pipe';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { ImportComponent } from './components/import/import.component';
import { ModalConfirmComponent } from './components/modal-confirm/modal-confirm.component';


@NgModule({
  declarations: [
    HasRolesDirective,
    DebounceDirective,
    NumberFormatterDirective,
    DecimalFormatterDirective,
    NumbersCharacterBasicDirective,
    DebounceKeyupDirective,
    DebounceClickDirective,
    NumbersOnlyDirective,
    CurrencyVNDPipe,
    ModalComponent,
    TableTitleComponent,
    UploadComponent,
    UploadCommentComponent,
    UploadAvatarComponent,
    EditorComponent,
    UploadSimpleComponent,
    ButtonComponent,
    BadgeComponent,
    ButtonActionComponent,
    PaginationComponent,
    FileComponent,
    FilePublicComponent,
    ModalFileComponent,
    SelectAllComponent,
    NoDataComponent,
    NotBlankComponent,
    ValidateMessageComponent,
    RemoveOptionTitleDirective,
    HasUserLevelDirective,
    ImportModalComponent,
    ConfirmExportErrorComponent,
    FileDragNDropDirective,
    HighlighPipe,
    ModalCustomComponent,
    SafeHtmlPipe,
    ImportComponent,
    ModalConfirmComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CKEditorModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    TranslateModule,
    NzNotificationModule,
    NzFormModule,
    NzRadioModule,
    NzTableModule,
    NzDividerModule,
    NzSwitchModule,
    NzIconModule,
    NzToolTipModule,
    NzTabsModule,
    NzPaginationModule,
    NzCardModule,
    NzButtonModule,
    NzBadgeModule,
    NzInputModule,
    NzDropDownModule,
    NzModalModule,
    NzCheckboxModule,
    NzPopoverModule,
    NzUploadModule,
    NzMessageModule,
    NzDatePickerModule,
    NzSelectModule,
    NzCollapseModule,
    NzEmptyModule,
    NzBreadCrumbModule,
    NzImageModule,
    NzBackTopModule,
    NzCarouselModule,
    NzTreeModule,
    NzTreeViewModule,
    NzTreeSelectModule,
    NzTagModule,
    NzAlertModule,
    NzDrawerModule,
    NzCommentModule,
    NzListModule,
    NzAvatarModule,
    NzTimelineModule,
    NzProgressModule,
    NzInputNumberModule,
    DragDropModule,
    ChartsModule,
    NzSpinModule,
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [
    HasRolesDirective,
    DecimalFormatterDirective,
    DebounceDirective,
    NumberFormatterDirective,
    DebounceKeyupDirective,
    DebounceClickDirective,
    NumbersCharacterBasicDirective,
    NumbersOnlyDirective,
    CurrencyVNDPipe,
    ModalComponent,
    PaginationComponent,
    NotBlankComponent,
    SelectAllComponent,
    NoDataComponent,
    ModalFileComponent,
    EditorComponent,
    ButtonActionComponent,
    FileComponent,
    FilePublicComponent,
    ButtonComponent,
    BadgeComponent,
    UploadSimpleComponent,
    UploadComponent,
    UploadCommentComponent,
    UploadAvatarComponent,
    ImportModalComponent,
    ConfirmExportErrorComponent,
    TableTitleComponent,
    CommonModule,
    FormsModule,
    CKEditorModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    TranslateModule,
    NzNotificationModule,
    NzFormModule,
    NzRadioModule,
    NzTableModule,
    NzDividerModule,
    NzSwitchModule,
    NzIconModule,
    NzToolTipModule,
    NzTabsModule,
    NzPaginationModule,
    NzCardModule,
    NzButtonModule,
    NzBadgeModule,
    NzInputModule,
    NzDropDownModule,
    NzModalModule,
    NzCheckboxModule,
    NzPopoverModule,
    NzUploadModule,
    NzMessageModule,
    NzDatePickerModule,
    NzSelectModule,
    NzCollapseModule,
    NzEmptyModule,
    NzBreadCrumbModule,
    NzImageModule,
    NzBackTopModule,
    ValidateMessageComponent,
    RemoveOptionTitleDirective,
    NzCarouselModule,
    HasUserLevelDirective,
    NzTreeModule,
    NzTreeViewModule,
    NzTreeSelectModule,
    NzTagModule,
    NzAlertModule,
    NzDrawerModule,
    NzListModule,
    NzCommentModule,
    NzAvatarModule,
    NzTimelineModule,
    FileDragNDropDirective,
    NzProgressModule,
    NzInputNumberModule,
    DragDropModule,
    HighlighPipe,
    ModalCustomComponent,
    ChartsModule,
    NzSpinModule,
    SafeHtmlPipe,
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatButtonModule,
    MatIconModule,
    ModalConfirmComponent
  ],
  providers: [
    DatePipe,
    NumbersOnlyDirective,
    DecimalFormatterDirective,
    DebounceDirective,
    NumberFormatterDirective,
    CurrencyVNDPipe,
    NumbersCharacterBasicDirective,
    NotificationService,
  ],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SharedModule {
  
}

