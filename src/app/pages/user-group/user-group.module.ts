import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { FormAddUsersComponent } from './form-add-users/form-add-users.component';
import { UserGroupDetailComponent } from './user-group-detail/user-group-detail.component';
import { UserGroupRoutingModule } from './user-group-routing.module';
import { UserGroupComponent } from './user-group.component';

@NgModule({
  declarations: [UserGroupComponent, UserGroupDetailComponent, FormAddUsersComponent],
  imports: [
    CommonModule,
    SharedModule,
    UserGroupRoutingModule,
  ],
  exports: [UserGroupComponent]
})
export class UserGroupModule { }
