import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { UserGroupComponent } from './user-group.component';

const routes: Routes = [
  {
    path: '',
    component: UserGroupComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.GROUP.LIST],
      title: 'model.user-group.title',
      breadcrum :[
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Quản lý phân quyền',
          path: `javascript:void(0)`
        },
      ]
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserGroupRoutingModule { }
