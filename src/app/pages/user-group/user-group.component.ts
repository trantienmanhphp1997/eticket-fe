import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { ToastService } from '@shared/service/helpers/toast.service';
import { UserGroupService } from '@shared/service/user-group.service';
import CommonUtil from '@shared/utils/common-utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { AUTH } from '@shared/constants/auth.constant';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { UserGroupDetailComponent } from './user-group-detail/user-group-detail.component';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { ACTION } from '@shared/constants/common.constant';
import { ModalComponent } from '@shared/components/modal/modal.component';

@Component({
  selector: 'app-user-group',
  templateUrl: './user-group.component.html',
  styleUrls: ['./user-group.component.css']
})
export class UserGroupComponent implements OnInit {

  // page data
  userGroupList: any = [];
  total: any = 0;
  pageIndex = PAGINATION.PAGE_DEFAULT;
  pageSize = PAGINATION.SIZE_DEFAULT;
  pageSizeOptions = PAGINATION.OPTIONS;
  isCallFirstRequest: boolean = true;
  auth: any = AUTH;

  //checked items
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly any[] = [];
  setOfCheckedId = new Set<number>();
  checked = false;
  selectedUserGroup: any = []

  //tree
  defaultCheckedNode: any = []
  defaultCheckedKeys = ['0-0-0'];
  defaultSelectedKeys = ['0-0-0'];
  defaultExpandedKeys = ['0-0', '0-0-0', '0-0-1'];
  roles: any;
  commonUtil: any = CommonUtil;
  constructor(
    private fb: FormBuilder,
    private userGroupService: UserGroupService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toast: ToastService,
  ) { }

  searchForm = this.fb.group({
    keyWord: ['']
  });

  UserGroupSearch = {
    orderBy: '',
    sortBy: '',
  }

  keyword: any = ''
  ngOnInit() {
    this.getAllRoles()
    this.loadData()
  }

  // load data
  loadData() {
    const params = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      search: this.searchForm.value.keyword,
      include: '',
      keyword: this.keyword,
      orderBy: this.UserGroupSearch.orderBy,
      sortBy: this.UserGroupSearch.sortBy,
    }
    this.userGroupService.getUserGroups(params, true).subscribe(
      (res) => {
        this.userGroupList = res.body?.data?.map((dept: any) => {
          return {
            ...dept,
            disabled: dept.check_can_edit ===0,
          }
        })
        this.total = res.body?.meta?.pagination?.total
      }
    );
  }

  getAllRoles() {
    this.userGroupService.getAllRoles(true).subscribe(
      (res) => {
        this.roles = res.body?.data;
      }
    );
  }

  // Events
  onSearch(event: any) {
    if (event) {
      this.keyword = event.target?.value;
      this.pageIndex = PAGINATION.PAGE_DEFAULT;
      this.loadData();
    }
  }

  nzEvent(event: NzFormatEmitEvent): void {
    this.defaultCheckedNode = event.keys
    this.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.loadData();
  }

  onRefresh(): void {
    this.getAllRoles()
    this.loadData()
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    this.UserGroupSearch.sortBy = params.sort.filter((e: any) => e.value !== null).map((e: any) => e.value?.slice(0, -3)).toString();
    this.UserGroupSearch.orderBy = params.sort.filter((e: any) => e.key !== null && e.key !== undefined && e.value !== null).map((e: any) => e.key).toString();
    this.loadData();
  }

  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.loadData();
  }

  onCreate = () => {
    const base = CommonUtil.modalBase(UserGroupDetailComponent, { action: ACTION.CREATE, roles: this.roles }, '65%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.getAllRoles()
        this.loadData()
      }
    });
    modal.componentInstance.emitService.subscribe(() => {
      this.loadData()
    });
  }

  onUpdate = (data: any) => {
    this.userGroupService.findUserGroup(data.id, true).subscribe(res => {
      this.selectedUserGroup = res.body?.data;

      const base = CommonUtil.modalBase(UserGroupDetailComponent, { action: ACTION.UPDATE, selectedUserGroup: this.selectedUserGroup, roles: this.roles, isUpdate: true }, '65%');
      const modal: NzModalRef = this.modalService.create(base);
      modal.afterClose.subscribe(result => {
        if (result && result?.success) {
          this.getAllRoles()
          this.loadData()
        }
      });
    });
  }

  onDetail(data: any) {
    this.userGroupService.findUserGroup(data.id, true).subscribe(res => {
      this.selectedUserGroup = res.body?.data;
      const base = CommonUtil.modalBase(UserGroupDetailComponent, { action: ACTION.VIEW, selectedUserGroup:  this.selectedUserGroup, roles: this.roles, isDetail: true }, '65%');
      const modal: NzModalRef = this.modalService.create(base);
      modal.afterClose.subscribe(result => {
        if (result && result?.success) {
          this.getAllRoles()
          this.loadData()
        }
      });
    });
  }

  onDelete = (data: any) => {
    if (data.check_can_edit==1){
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa nhóm người dùng <b>${this.commonUtil.limitWord(data.name, 20)}</b> không?`,
      okText: 'Xóa',
      isVisible: true,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    let usergroupIds = data instanceof Set ? Array.from(data) : [data.id];
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.userGroupService.deleteUserGroup(usergroupIds).subscribe((res: any) => {
          if (res) {
            this.toast.success('Xóa nhóm người dùng thành công');
            modal.close();
            this.getAllRoles()
            this.loadData()
          };
        })
      } else {
        modal.close();
      }
    });
  }
  }

  onMultiDelete(data: any) {
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa nhóm người dùng đã chọn không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let userGroupIds = data instanceof Set ? Array.from(data) : [data.id];

    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.userGroupService.deleteUserGroup(userGroupIds, true).subscribe(res => {
          this.toast.success(`Xóa nhóm người dùng thành công`);
          this.setOfCheckedId = new Set<number>();
          modal.close();
          this.loadData()
        })
      } else {
        modal.close();
      }
    });
  }

  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.userGroupList.filter((item: any) => !item.disabled);
    this.checked = listOfEnabledData.every((item: any) => this.setOfCheckedId.has(item.id));
    this.indeterminate = listOfEnabledData.some((item: any) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.userGroupList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }
  exportData() {
    this.userGroupService.export()
  }
}
