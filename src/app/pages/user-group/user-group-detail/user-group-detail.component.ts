
import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { STATUS } from '@shared/constants/status.constants';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { UserGroupService } from '@shared/service/user-group.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import CommonUtil from '@shared/utils/common-utils';
import { AUTH } from '@shared/constants/auth.constant';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { VALIDATORS } from '@shared/constants/validators.constant';
import { FormAddUsersComponent } from '../form-add-users/form-add-users.component';
import { ACTION } from '@shared/constants/common.constant';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';
import { CommonService } from '@shared/service/common.service';
import { UserService } from '@shared/service/user.service';

@Component({
  selector: 'app-user-group-detail',
  templateUrl: './user-group-detail.component.html',
  styleUrls: ['./user-group-detail.component.scss']
})
export class UserGroupDetailComponent implements OnInit {
  @Input() selectedUserGroup: any = []
  @Input() isUpdate: boolean = false
  @Input() isDetail: boolean = false
  @Input() roles: any
  @Input() action: any = ACTION.CREATE;
  @Output() emitService =new EventEmitter<string>()
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  VALIDATORS: any = VALIDATORS;
  isUpdating = false;
  count = 0;
  isLoading = false
  userGroup: any;
  users: any = []
  userIds: any = []
  departmentIds: any = []
  departments: any
  refs: any = [];
  auth: any = AUTH;
  total: any = 0;
  // title: any = 'Tạo mới đơn vị'
  form: FormGroup = new FormGroup({});
  actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
  viewEvent = ACTION.VIEW.event;
  actionEvent: any;
  commonUlti: any = CommonUtil;
  userQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    orderBy: '',
    sortBy: '',
  };

  constructor(
    private fb: FormBuilder,
    private userGroupService: UserGroupService,
    private modalRef: NzModalRef,
    private toast: ToastService,
    private modalService: NzModalService,
    private commonService: CommonService,
    private userService:UserService

  ) { }

  ngOnInit() {
    
    this.actionEvent = this.action.event
    // this.getUserAndDepartments()
    this.initForm();
    if (this.isUpdate || this.isDetail) {
      this.form.controls['code'].disable();
    }
  }

  initForm(): void {
    let roleIds = [];
    if (this.isUpdate || this.isDetail) {
      let roles = this.selectedUserGroup?.roles;
      for (let key in roles?.data) {
        roleIds.push(roles.data[key].id);
      }
    }

    if (this.isUpdate || this.isDetail) {
      // this.users = this.selectedUserGroup?.users?.data;
      this.loadUser();
    }

    this.form = this.fb.group({
      code: [{ value: this.actionToShowData.includes(this.actionEvent) ? this.selectedUserGroup?.code : null, disabled: this.isUpdate },
      { validators: [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.CODECUS_MAX_LENGTH.MAX), Validators.pattern('^[a-zA-Z0-9]*$')], asyncValidators: [CustomInputValidator.hasExited('usergroups', 'code', this.commonService)], updateOn: 'blur' }],
      name: [{ value: this.actionToShowData.includes(this.actionEvent) ? this.selectedUserGroup?.name : null, disabled: this.actionEvent === ACTION.VIEW.event },
      [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX), CustomInputValidator.isName]],
      roleIds: [this.actionToShowData.includes(this.actionEvent) ? roleIds : null, []],
      description: [{ value: this.actionToShowData.includes(this.actionEvent) ? this.selectedUserGroup?.description : null, disabled: this.actionEvent === ACTION.VIEW.event },
      [Validators.maxLength(LENGTH_VALIDATOR.TICKET_DESCRIPTION_LENGTH.MAX)]],
    });
  }
  loadUser(){
    var group_ids=[this.selectedUserGroup.id];
    this.userService.getUserOfGroup(this.userQuery, group_ids,true).subscribe(res=>{
      this.users = res?.body?.data;
      this.total = res?.body?.meta?.pagination.total;
    });
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.userQuery.pageIndex = pageIndex;
    this.userQuery.pageSize = pageSize;
    this.initForm();
  }

  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
      updating:this.isUpdating
    });
  }

  onSwitchUpdate(): void {
    this.actionEvent = 'update';
    this.initForm();
  }

  onSubmit(): void {
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
  
    if (this.users) {
      this.userIds = this.users?.map((element: any) => element.id)
    }
    this.users?.forEach((element: any) => {
      let department_ids = element.department_access?.map((element: any) => element.id)
      this.refs.push({
        userIds: [element.id],
        departmentIds: department_ids
      })
    })
    this.isLoading = true
    this.count += 1
    if(this.count == 1){
    const dataForm: any = {
      ...this.form.value,
      refs: this.refs,
    };

    const body = CommonUtil.trim(dataForm);
    this.isUpdating = true;

    let serviceEvent = this.isUpdate ? this.userGroupService.update(this.selectedUserGroup.id, body, true) : this.userGroupService.create(body, true)
    serviceEvent.subscribe(res => {
      if (res.body?.data) {
        this.isLoading = false
        this.toast.success(`model.user-group.success.${this.isUpdate ? 'update' : 'create'}`);
        if(this.isUpdate){
          this.isUpdating = false;
          this.modalRef.close({
          success: true,
          value: dataForm,
        });
        }else{
          this.isUpdate=true;
          this.action=ACTION.UPDATE;
          this.selectedUserGroup=res.body?.data;
          this.emitService.next();
        }
        
      }
    }, (e: any) => {
      this.isUpdating = false;
    },
      () => {
        this.isUpdating = false;
      });
    }
  }

  onAddUser = () => {
    var user_id = [];
    user_id = this.users?.map((i: any) => i.id);
    const base = CommonUtil.modalBase(FormAddUsersComponent, { isUpdate: false, userIds: user_id, groupids:[this.selectedUserGroup.id] }, '50%', false);

    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadUser()
        // if (this.users?.length > 0) {
        //   result.value[0].forEach((element: any) => {
        //     const userIds = this.users?.map((i: any) => i.id);
        //     if (userIds.includes(element.id)) {
        //       this.users.forEach((user: any) => {
        //         if (user.id == element.id) {
        //           const department_access = user.department_access?.map((d: any) => d.id);
        //           element.department_access?.forEach((dept: any) => {
        //             if (!department_access.includes(dept.id)) {
        //               user?.department_access.push(dept);
        //             }
        //           })
        //         }
        //       });
        //     } else {
        //       this.users.push(element);
        //     }


        //   });
        // } else {
        //   this.users = result.value[0];
        // }
        this.departmentIds = result.value[1];
      }
    });
  }

  onUpdate = (data: any) => {
    let arr: any = []
    Object.keys(data).map(function (key) {
      arr[key] = data[key]
      return arr;
    });

    let input: any = [];
    input.push(arr)
    const base = CommonUtil.modalBase(FormAddUsersComponent, { setUserChecked: input, isUpdate: true, action: "null",groupids:[this.selectedUserGroup.id] }, '50%', false);

    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadUser()
        // result.value[0].forEach((element: any) => {
        //   this.users.forEach((user: any) => {
        //     if (element.id == user.id) {
        //       user.department_access = element.department_access;
        //     }
        //   })
        // })
        this.departmentIds = result.value[1];
      }
    });
  }

  onDelete = (data: any) => {
    const param={
      group_ids:[this.selectedUserGroup.id],
      user_ids:[data?.user_id],
      department_ids:[],
      remove_user:1
    }
    this.userGroupService.getUserGroupEditOrDeleteUser(param).subscribe(()=>{
      this.loadUser();
    });
  }

  getUserAndDepartments() {
    this.userGroupService.getAllUserGroupRefs([this.selectedUserGroup?.id]).subscribe(
      (res) => {
        this.departments = res.body?.data;

        this.departments.forEach((element: any) => {
          this.users?.forEach((user: any) => {
            let department_access: any = [];
            if (user.id == element.user_id) {
              element.departments.forEach((department: any) => {
                department_access.push(department)
              })
              user.department_access = department_access;
            }
          })
        })
      }
    );
  }
}
