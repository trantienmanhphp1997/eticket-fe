
import { AfterViewChecked, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { UserGroupService } from '@shared/service/user-group.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import CommonUtil from '@shared/utils/common-utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { UserService } from '@shared/service/user.service';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd/dropdown';
import { DepartmentService } from '@shared/service/department.service';

@Component({
  selector: 'app-form-add-users',
  templateUrl: './form-add-users.component.html',
  styleUrls: ['./form-add-users.component.scss']
})
export class FormAddUsersComponent implements OnInit, AfterViewChecked {
  @Input() selectedUserGroup: any = []
  @Input() userIds: any = []
  @Input() isUpdate: boolean = false
  @Input() groupids:any =[];
  departments: any = []
  //checkItem
  //checked items
  usernameValid = false;
  departmentIds: any = [];
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly any[] = [];
  setOfCheckedId = new Set<number>();
  setUserChecked: any[] = [];
  checked = false;
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  isUpdating = false;
  userGroup: any;
  userList: any;
  nodes: any = [];
  isVisible = false;
  form: FormGroup = new FormGroup({});
  total: any = 0;
  pageIndex = PAGINATION.PAGE_DEFAULT;
  pageSize = PAGINATION.SIZE_DEFAULT;
  pageSizeOptions = PAGINATION.OPTIONS;
  userQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    departmentIds: '',
    orderBy: '',
    sortBy: '',
    keyword: '',
    excludeUser: '',
  };
  userWidth = '0px';
  keyword: any;
  menuConfig: NzDropdownMenuComponent | undefined;
  commonUtil: any = CommonUtil;
  constructor(
    private fb: FormBuilder,
    private userGroupService: UserGroupService,
    private userService: UserService,
    private modalRef: NzModalRef,
    private nzContextMenuService: NzContextMenuService,
    private departmentService: DepartmentService

  ) { }
  ngAfterViewChecked(): void {
    const el = document.getElementById('user_el');
    this.userWidth = `${el?.offsetWidth || 900}px`;
  }

  ngOnInit() {
    this.getAllUsers();
    this.getAllDepartments();
    this.initForm();

  }

  initForm(): void {
    if (this.isUpdate) {
      this.departmentIds = this.setUserChecked[0]?.departments?.map((element: any) => element.id)
    }
  }

  getAllUsers() {
    this.userService.getUser(this.userQuery, true).subscribe((res) => {
      this.userList = res.body?.data;
      this.total = res.body?.meta?.pagination?.total
    });
  }

  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }

  onSubmit(): void {
    const dataForm: any = {
      department_ids: this.departmentIds,
      user_ids:this.isUpdate?this.setUserChecked.map((item:any)=>item.user_id):this.setUserChecked.map((item:any)=>item.id),
      group_ids:this.groupids
    };
    if (this.setUserChecked.length == 0) {
      this.usernameValid = true;
      return;
    } else {
      this.usernameValid = false;
    }
    // this.setUserChecked.forEach((user: any) => {
    //   let department_access: any = [];
    //   if (this.departmentIds) {
    //     this.departments.forEach((element: any) => {
    //       dataForm.departmentIds.forEach((department_id: any) => {
    //         if (element.id == department_id) {
    //           department_access.push(element)
    //         }
    //       })
    //     })
    //   }
    //   user.department_access = department_access;
    // })
    const body = CommonUtil.trim(dataForm);
    this.isUpdating = true;
    const baseService= this.isUpdate?this.userGroupService.getUserGroupEditOrDeleteUser(body):this.userGroupService.getUserGroupAddUser(body);
    baseService.subscribe(()=>{
      this.modalRef.close({
      success: true,
      value: [this.setUserChecked, dataForm],
    });
    this.usernameValid = false;
    })
    
  }
  getAllDepartments() {
    const params = {
      limit: 10000,
      data_context: 'manage-department'
    }
    this.departmentService.getAllDepartments(params, true).subscribe(
      (res) => {
        this.nodes = res?.body;
      }
    );
  }
  // getAllDepartments() {
  //   debugger
  //   const param = {
  //     data_context: 'manage-user-group',
  //     limit: 10000,
  //   }
  //   this.userGroupService.getAllDepartments(param, true).subscribe(
  //     (res) => {
  //       this.nodes = this.createDataTree(res.body?.data)
  //       this.departments = res.body?.data
  //     }
  //   );

  // }
  // createDataTree = (dataset: any) => {
  //   debugger
  //   const hashTable = Object.create(null);
  //   dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.dept_name, key: aData.id, isLeaf: false });

  //   const dataTree: any = [];

  //   dataset.forEach((aData: any) => {
  //     if (aData.parent_id) {
  //       hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
  //     }
  //   });

  //   dataset.forEach((aData: any) => {
  //     if (hashTable[aData.id].children.length == 0) {
  //       hashTable[aData.id].isLeaf = true
  //     }
  //     else {
  //       hashTable[aData.id].expanded = false
  //     }
  //     if (!aData.parent_id) {

  //       dataTree.push(hashTable[aData.id]);
  //     }
  //   });

  //   return dataTree;
  // };

  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.userQuery.pageIndex = pageIndex;
    this.userQuery.pageSize = pageSize;
    this.getAllUsers();
  }
  onSearch() {

    this.userQuery.keyword = this.keyword;
    this.userQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.getAllUsers();
  }
  updateCheckedSet(item: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(item.id);
      this.setUserChecked.push(item)
    } else {
      this.setOfCheckedId.delete(item.id);
      this.setUserChecked = this.setUserChecked.filter(i => { return i.id !== item.id })
    }

    if (this.setUserChecked.length == 0) {
      this.usernameValid = true;
    } else {
      this.usernameValid = false;
    }
  }


  // onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
  //   this.listOfCurrentPageData = listOfCurrentPageData;
  //   this.refreshCheckedStatus();
  // }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.userList.filter((item: any) => !item.disabled);
    this.checked = listOfEnabledData.every((item: any) => this.setOfCheckedId.has(item.id));
    this.indeterminate = listOfEnabledData.some((item: any) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
    this.onChangeData('code', this.setUserChecked)
    if (this.keyword) {
      this.keyword = '';
      this.onSearch();
      setTimeout(() => {
        const el = document.getElementById('user_el');
        var elemRect = el?.getBoundingClientRect()
        const event = {
          x: elemRect?.left || 0,
          y: Math.floor(elemRect?.top || 0) + 42,
        }
        if (this.menuConfig) {
          this.nzContextMenuService.create(event, this.menuConfig);
        }
      }, 500)
    }
    
  }

  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.userList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item, checked)
        this.onChangeData('code', this.setUserChecked)
      }
    });
    this.refreshCheckedStatus();
    if (this.keyword) {
      this.keyword = '';
      this.onSearch();
      setTimeout(() => {
        const el = document.getElementById('user_el');
        var elemRect = el?.getBoundingClientRect()
        const event = {
          x: elemRect?.left || 0,
          y: Math.floor(elemRect?.top || 0) + 42,
        }
        if (this.menuConfig) {
          this.nzContextMenuService.create(event, this.menuConfig);
        }
      }, 500)
    }
    

  }

  onChangeData(type: string, content: any): void {
    this.form.get(type)?.setValue(Array.from(content).map((e: any) => e.username).toString());
  }

  onRemoveTagUser(item: any) {
    this.setOfCheckedId.delete(item.id);
    this.setUserChecked = this.setUserChecked.filter(i => { return i.id !== item.id })
    if (this.setUserChecked.length == 0) {
      this.usernameValid = true;
    } else {
      this.usernameValid = false;
    }
    this.refreshCheckedStatus();
  }
  setMenu(menu: NzDropdownMenuComponent) {
    this.menuConfig = menu;
  }
}
