import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../shared/service/product.service';
import { PAGINATION } from '@shared/constants/pagination.constants';
import CommonUtil from '@shared/utils/common-utils';
import { AUTH } from '@shared/constants/auth.constant';
import { ProductCreateComponent } from './product-create/product-create.component';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { StatusProductComponent } from './status-product/status-product.component';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';
import { environment } from '@env/environment';
import { ProductCategoryService } from '@shared/service/product-category.service';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { CreateCategoryComponent } from './create-category/create-category.component';
import { NzTreeView } from 'ng-zorro-antd/tree-view/tree';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ACTION } from '@shared/constants/common.constant';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { ModalConfirmComponent } from '@shared/components/modal-confirm/modal-confirm.component';
@Component({
  selector: 'app-service-product',
  templateUrl: './service-product.component.html',
  styleUrls: ['./service-product.component.scss']
})
export class ServiceProductComponent implements OnInit {
  productList: any = [];
  setOfCheckedId = new Set<number>();
  indeterminate = false;
  indeterminateC = false;
  checked = false;
  checkedAll = false;
  total: any = 0;
  is_product_sync: any;
  commonUtil: any = CommonUtil;
  auth: any = AUTH;
  productQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    departmentIds: '',
    orderBy: '',
    sortBy: '',
    keyword: '',
  };
  searchForm = this.fb.group({
    keyword: [null]
  });
  listOfCurrentPageData: readonly any[] = [];
  isCallFirstRequest: boolean = true;
  showDelete: boolean = false;
  node: any = [];
  nodes = [];
  nodeCategory = [];
  allKeyTree: any = [];
  defaultCheckedNode: any = [];
  defaultCheckedKeys: any = [];
  defaultSelectedKeys: any = [];
  defaultExpandedKeys: any = [];
  dataFile: any;
  isVisible = false;
  demCheckCategory: any = 0;
  demCategory: any = 0;
  @ViewChild('categoryTree')

  public categoryTree: any;
  constructor(
    private productService: ProductService,
    private modalService: NzModalService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private productCategoryService: ProductCategoryService,
    private toast: ToastService,
  ) { }

  ngOnInit(): void {
    this.getAllTreeCategory();
    this.getAllTree();
    this.loadData();
    this.is_product_sync = environment.is_product_sync;
  }
  loadData() {
    const params = {
      pageIndex: this.productQuery.pageIndex,
      pageSize: this.productQuery.pageSize,
      keyword: this.productQuery.keyword,
      categoryIds: this.defaultCheckedNode ? this.defaultCheckedNode.toString() : '',
      orderBy: this.productQuery.orderBy,
      sortBy: this.productQuery.sortBy,
    }
    // this.productService.search(params, true).subscribe(res => {
    //   this.productList = res.body?.data
    //   this.total = res.body?.meta?.pagination?.total;
    // });
    this.productService.search(params, true).subscribe(
      (res) => {
        this.productList = res.body?.data?.map((dept: any) => {
          return {
            ...dept, disabled: dept.check_can_edit ===0, 
          }
        })
        this.total = res.body?.meta?.pagination.total
      }
    );
  }
  getAllTree() {
    this.productService.getAll(true).subscribe(
      (res) => {
        this.nodes = this.createDataTree(res.body?.data);
      }
    );
  }
  getAllTreeCategory() {
    this.productCategoryService.getAll(true).subscribe(
      (res) => {
        this.nodeCategory = this.createDataTreeCatogory(res.body?.data);
      }
    );
  }
  createDataTreeCatogory = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.name, key: aData.id, isLeaf: false });
    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id].children?.push(hashTable[aData.id]);
      }
      this.allKeyTree.push(aData.id);
    });

    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children?.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {
        dataTree.push(hashTable[aData.id]);
      }
    });
    this.demCategory = dataTree.length
    return dataTree;
  };
  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset?.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.name, key: aData.id, isLeaf: false });
    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children?.push(hashTable[aData.id]);
      }
    });

    dataset?.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };
  onCreate = () => {;
    const base = CommonUtil.modalBase(ProductCreateComponent, { nodes: this.nodes, nodeCategory: this.nodeCategory }, '560px');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData();
        this.getAllTree();
        this.getAllTreeCategory();
      }
    });
  }
  createCategory = () => {
    const base = CommonUtil.modalBase(CreateCategoryComponent, { nodeCategory: this.nodeCategory }, '560px');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(res => {
      if (res && res?.success) {
        this.getAllTreeCategory();
      }
    })
  }
  updateCategory = (data: any) => {
    const base = CommonUtil.modalBase(CreateCategoryComponent, { selectCategory: data, nodeCategory: this.nodeCategory, isUpdate: true, action: ACTION.UPDATE }, '560px');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(res => {
      if (res && res?.success) {
        this.getAllTreeCategory();
      }
    })
  }
  onViewCate = ( data: any ) =>{
     const base = CommonUtil.modalBase ( CreateCategoryComponent ,{ selectCategory: data , nodeCategory : this.nodeCategory, isUpdate: true, action: ACTION.VIEW }, '560px');
      const modal:NzModalRef = this.modalService.create(base);
      modal.afterClose.subscribe( res =>{
        if( res && res?.success){
          this.getAllTreeCategory();
        
        } 
      })
  }
  onUpdate = (data: any) => {
    this.getAllTree();
    if(data.check_can_edit ==1){
      const base = CommonUtil.modalBase(ProductCreateComponent, { selectedProduct: data, nodes: this.nodes, nodeCategory: this.nodeCategory, isUpdate: true, action: ACTION.UPDATE }, '560px');
      const modal: NzModalRef = this.modalService.create(base);
      modal.afterClose.subscribe(result => {
        if (result && result?.success) {
          this.loadData();
          this.getAllTree();
        }
      });
    }
  }
  onView = (data: any) => {
    this.getAllTree();
    const base = CommonUtil.modalBase(ProductCreateComponent, { selectedProduct: data, nodes: this.nodes, nodeCategory: this.nodeCategory, isUpdate: true, action: ACTION.VIEW }, '560px');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData();
        this.getAllTree();
      }
    });
  }
  onDelete( data: any){
    if(data.check_can_edit ==1){
      const form = CommonUtil.modalBase(ModalComponent,{
        title:'Xác nhận xóa',
        content:`Bạn có chắc muốn xóa sản phẩm dịch vụ <b>${data?.name}</b> không?`,
        okText:'Xóa',
        callback: () => {
          return {
            success: true,
          };
        },
      },'450px',
       false,
      false,
      true,{ top: '20px' }, true
      );
      const modal: NzModalRef = this.modalService.create(form);
      modal.componentInstance.emitter.subscribe((result:any) => {
        if (result?.success) {
          this.productService.getDelete(data.id).subscribe((res: any) => {
            if (res) {
              modal.close();
              this.toast.success(`product.delete.success`);
              this.loadData();
            };
          })
        } else {
          modal.close();
        }
      });
    }
  }
  deleteCategory( data: any){
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc muốn xóa danh mục <b>${data?.name}</b> không?`,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'450px',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.productCategoryService.multiDelete(data.id).subscribe(res => {
          if (res) {
            this.toast.success(`product.delete.success`);
            this.getAllTreeCategory()
            modal.close();
          } else {
            this.toast.success(`Không thể xóa bản sản phẩm dịch vụ `);
          }
        })
      } else {
        modal.close();
      }
    });
  }
  deleteAllCategory( data: any ){
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc muốn xóa danh mục đã chọn không?`,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'450px',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.productCategoryService.multiDelete(this.defaultCheckedNode).subscribe((res: any) => {
          if (res) {
            modal.close();
            this.getAllTreeCategory();
            this.defaultCheckedNode=[];
            this.loadData();
            this.toast.success(`Xóa thành công !`);
          }
          else {
            this.toast.success(`Không thể xóa bản danh mục `);
          }
        })
      } else {
        modal.close();
      }
    });
  }
  onDeleteAll( data: any){
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc muốn xóa sản phẩm dịch vụ đã chọn không?`,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'450px',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    let productIds =  data instanceof Set ? Array.from(data) : [data.id] ; 
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.productService.multiDelete(productIds).subscribe((res: any) => {
          if (res) {
            modal.close();
            this.loadData();
            this.setOfCheckedId =new Set();
            this.toast.success(`Xóa thành công !`);
          }
        })
      } else {
        modal.close();
      }
    });
  }
  onSwitch(data: any) {
    if (data.status == 1) {
      data.status = false
    } else {
      data.status = true
    }
    const form = CommonUtil.modalBase(ModalConfirmComponent, {
      title: 'Xác nhận thay đổi trạng thái',
      content: `Bạn có chắc muốn thay đổi trạng thái <b>${data?.name}</b> không?`,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      false, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.productService.update(data.id, data ,true).subscribe(res => {
          if (res) {
            this.toast.success('Cập nhật thành công');
          } else {
            this.toast.success('Cập nhật thất bại');
          }
          modal.close();
        })
      } else {
        data.status = !data.status;
        modal.close();
      }
    });
  }
  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.productList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }
  checkAllCategory(checkedAll: boolean): void {
    if (checkedAll) {
      this.defaultCheckedKeys = this.allKeyTree;
      this.showDelete = true

    } else {
      this.defaultCheckedKeys = [];
      this.showDelete = false
    }
  }

  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  onSearch($event: any){
    if($event){
      this.productQuery.keyword = this.searchForm.value.keyword.trim();
      this.searchForm.patchValue({
        keyword:  this.productQuery.keyword
      })
      this.productQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
       this.loadData();
    }  
  }
  onRefresh(): void {
    this.productQuery = {
      pageIndex: PAGINATION.PAGE_DEFAULT,
      pageSize: PAGINATION.SIZE_DEFAULT,
      pageSizeOptions: PAGINATION.OPTIONS,
      departmentIds: '',
      orderBy: '',
      sortBy: '',
      keyword: '',
    };
    this.indeterminate = false;
    this.defaultCheckedNode=[];
    this.getAllTreeCategory();
    this.loadData()
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.productQuery.pageIndex = pageIndex;
    this.productQuery.pageSize = pageSize;
    this.loadData();
  }
  onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    this.productQuery.sortBy = params.sort.filter((e: any) => e.value !== null).map((e: any) => e.value?.slice(0, -3)).toString();
    this.productQuery.orderBy = params.sort.filter((e: any) => e.key !== null && e.key !== undefined && e.value !== null).map((e: any) => e.key).toString();
    this.loadData();
  }
  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter((item: any) => !item.disabled);
    this.checked = listOfEnabledData.every((item: any) => this.setOfCheckedId.has(item.id));
    this.indeterminate = listOfEnabledData.some((item: any) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }
  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }
  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }
  nzEvent(event: NzFormatEmitEvent): void {
    this.defaultCheckedNode = event?.keys;
    this.productQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.loadData();
    if (this.defaultCheckedNode.length > 0) {
      this.showDelete = true;
      this.demCheckCategory = this.defaultCheckedNode.length;
      if(this.demCheckCategory === this.demCategory){
          this.checkedAll = true
      }
      else{
        this.checkedAll = false
      }
    }
    else {
      this.showDelete = false
    }
  }
  showImport() {
    this.isVisible = true;
  }
  exportData() {
    this.productService.export()
  }
  handleLoadFile(e: any) {
    this.dataFile = e[0];

  }
  handleOk(): void {
    this.productService.importData(this.dataFile, true).subscribe(res => {
      if (res) {
        this.toast.success(`Import file thành công`);
        this.loadData();
      } else {
        this.toast.error(`Import file thất bại`);
      }
    });
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }
  exportTemplate() {
    this.productService.exportTemplate();
  }
}
