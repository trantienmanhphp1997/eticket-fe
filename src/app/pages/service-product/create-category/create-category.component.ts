import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import CommonUtil from '@shared/utils/common-utils';
import { LENGTH_VALIDATOR, VALIDATORS } from '@shared/constants/validators.constant';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ProductCategoryService } from '@shared/service/product-category.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';
import { ACTION } from '@shared/constants/common.constant';
@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent implements OnInit {
  @Input() selectCategory: any =[];
  @Input() nodeCategory: any =[];
  productCategory:any=[];
  isView : boolean = false ;
  isUpdate = false;
  form : FormGroup = new FormGroup({}) ;
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  @Input() action: any = ACTION.CREATE;
  actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
  existed: boolean = false ;
  description: any = [];
  isShowDescription : boolean = false;
  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private productCategoryService : ProductCategoryService,
    private toast: ToastService,
  ) { }

  ngOnInit(): void {
    this.productCategoryTree();
    this.initForm();
  }
  initForm(): void {
    this.form = this.fb.group({
      code:[{ value: this.isUpdate ? this.selectCategory?.code : null, disabled: this.action.event === ACTION.CREATE.event ? false : true },
        [Validators.required,Validators.maxLength(LENGTH_VALIDATOR.CODECUS_MAX_LENGTH.MAX), CustomInputValidator.isCode, CustomInputValidator.notBlankValidator]],
      name:[{ value: this.isUpdate ? this.selectCategory?.name : null, disabled: this.action.event === ACTION.VIEW.event ? true : false },
        [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.TITLE_MAX_LENGTH.MAX), CustomInputValidator.isName]],
        parent_id:[{ value: this.isUpdate ? this.selectCategory?.parent_id : null, disabled: this.action.event === ACTION.VIEW.event ? true : false },
        [ Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX)]],
      status:[{ value: this.isUpdate ? this.selectCategory?.status : 1, disabled: this.action.event === ACTION.VIEW.event ? true : false },],
      descriptions:[{ value: this.isUpdate ? this.selectCategory?.descriptions : null, disabled: this.action.event === ACTION.VIEW.event ? true : false }],
     })
     this.description = this.selectCategory?.description 
  }
  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  productCategoryTree(){
    this.productCategoryService.getAllServiceTree(this.selectCategory?.id,true).subscribe(
      (res) => {
        this.productCategory= this.createDataTreeCatogory(res.body?.data);
      }
    );
  }
  createDataTreeCatogory = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.name, key: aData.id, isLeaf: false });
    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id].children.push(hashTable[aData.id]);
      }
    });

    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {
        dataTree.push(hashTable[aData.id]);
      }
    });
    return dataTree;
  };
  onSwitch(){
    if(this.form.value.status == false ){
      this.form.patchValue({status: 0})
    }
    else{
      this.form.patchValue({status: 1})
    }
  }
  onExisted(){
    const code : any =  this.form.value.code

    if(code){
      this.productCategoryService.existed(code, true ).subscribe( res => {
        if (res.body == 1) {
          this.existed = true
        }
        else  if (res.body == 0){
          this.existed = false ;
        }
      })
    }
    else {
      this.existed = false ;
    }

  }
  onChangeData(type: string, content: string): void {
    this.form.get(type)?.setValue(content);
    if (CommonUtil.extractContent(this.form.value.descriptions)?.length > 500) {
      this.isShowDescription = true
    }
    else if (CommonUtil.extractContent(this.form.value.descriptions)?.length <= 500) {
      this.isShowDescription = false
    }
  }
  onSubmit(){
    if (this.action.event === ACTION.VIEW.event) {
      this.action = ACTION.UPDATE;
      this.initForm();
      return;
    }
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    if (CommonUtil.extractContent(this.form.value.descriptions)?.length > 500) {
      return;
    }
    const category: any ={
      ...this.form.value,
      parent_id: this.form.value.parent_id ?  this.form.value.parent_id: ''
    }
    const body = CommonUtil.trim(category);
    if(this.existed== false ){
      let serviceEvent = this.isUpdate ? this.productCategoryService.update( this.selectCategory.id, body, true) : this.productCategoryService.create(body, true)
      serviceEvent.subscribe(res => {
        if (res.body?.data) {
          if(this.isUpdate){
            this.toast.success(`category.successUpdate`);
          }
          else{
            this.toast.success(`category.success`);
          }
          this.modalRef.close({
            success: true,
            value: category,
          });
        }
      })
    }
  }
}
