import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ServiceProductRoutingModule }  from './service-product-routing.module';
import { ProductCreateComponent } from './product-create/product-create.component';
import { StatusProductComponent } from './status-product/status-product.component';
import { CreateCategoryComponent } from './create-category/create-category.component'

@NgModule({
  declarations: [
    ProductCreateComponent,
    StatusProductComponent,
    CreateCategoryComponent
  ],
  imports: [
    CommonModule,
    ServiceProductRoutingModule,
    SharedModule
  ]
})
export class ServiceProductModule { }
