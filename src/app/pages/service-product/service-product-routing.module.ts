import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { ServiceProductComponent } from './service-product.component'
const routes: Routes = [
    {
      path: '',
      component: ServiceProductComponent,
      canActivate: [AuthGuard],
      data: {
        authorities: [AUTH.PRODUCT.LIST],
        title: 'sidebar.service_products_management'
      }
    },
  ];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ServiceProductRoutingModule { }
