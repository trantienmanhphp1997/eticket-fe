import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import CommonUtil from '@shared/utils/common-utils';
import { LENGTH_VALIDATOR, VALIDATORS } from '@shared/constants/validators.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ProductService } from '@shared/service/product.service';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';
import { ACTION } from '@shared/constants/common.constant';
import { CommonService } from '@shared/service/common.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.scss']
})
export class ProductCreateComponent implements OnInit {
  @Input() selectedProduct: any = []
  @Input() isUpdate: boolean = false;
  @Input() nodes: any;
  @Input() nodeCategory: any = [];
  @Input() detail: any;
  @Input() action: any = ACTION.CREATE
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  statusList: any[] = [];
  isUpdating = false;
  products: any;
  form: FormGroup = new FormGroup({});
  existed: boolean = false;
  description: any = [];
  actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
  isShowDescription: boolean = false;
  isShowView: boolean = false;
  disabledCreate: boolean = true;
  isLoading: boolean = false;
  count = 0
  formInvalid = {
    code: false,
  }
  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toast: ToastService,
    private productService: ProductService,
    private commonService: CommonService,
  ) { }

  codeExist = new Subject<string>();
  ngOnInit() {
    this.codeExist.pipe(
      debounceTime(600),
      distinctUntilChanged())
      .subscribe(value => {
        this.checkExist('code', value)
      });
    if (this.action.event === ACTION.VIEW.event) {
      this.isShowView = true;
    }
    if (this.action.event == 'update' ) {
      this.disabledCreate = true;
    }
    this.action.event
    this.initForm();
  }
  initForm(): void {
    this.form = this.fb.group({
      code: [{ value: this.isUpdate ? this.selectedProduct?.code : null, disabled:  (this.action.event === 'update' && this.selectedProduct.check_can_edit ===1 && this.selectedProduct.sync_code) || this.action.event === 'view'? true  : false },
      [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.CODECUS_MAX_LENGTH.MAX), CustomInputValidator.isCode, CustomInputValidator.notBlankValidator]],
      name: [{ value: this.isUpdate ? this.selectedProduct?.name : null, disabled: this.action.event === ACTION.VIEW.event ? true : false },
      [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.VALUE_MAX_LENGTH.MAX), CustomInputValidator.isName]],
      parent_id: [{ value: this.isUpdate ? this.selectedProduct?.parent_id : null, disabled: this.action.event === ACTION.VIEW.event ? true : false }],
      category_id: [{ value: this.isUpdate ? this.selectedProduct?.category_id : null, disabled: this.action.event === ACTION.VIEW.event ? true : false },
      [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX)]],
      status: [{ value: this.isUpdate ? this.selectedProduct?.status : 1, disabled: this.action.event === ACTION.VIEW.event ? true : false },],
      description: [{ value: this.isUpdate ? this.selectedProduct?.description : null, disabled: this.action.event === ACTION.VIEW.event ? true : false }],
    });
    this.description = this.selectedProduct?.description
  }
  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  onSwitch() {
    if (this.form.value.status == false) {
      this.form.patchValue({ status: 0 })
    }
    else {
      this.form.patchValue({ status: 1 })
    }
  }
  onChangeData(type: string, content: string): void {
    this.form.get(type)?.setValue(content);
    if (CommonUtil.extractContent(this.form.value.description)?.length > 500) {
      this.isShowDescription = true
    }
    else if (CommonUtil.extractContent(this.form.value.description)?.length <= 500) {
      this.isShowDescription = false
    }
  }
  onSubmit(): void {
    let el = document.getElementById('myDiv');
    el?.click();
    setTimeout(() => {
      if (this.action.event === ACTION.VIEW.event) {
        this.action = ACTION.UPDATE;
        this.initForm();
        return;
      }
      if (this.form.controls['code'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.form.controls['name'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.form.controls['category_id'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.formInvalid.code) {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (CommonUtil.extractContent(this.form.value.description)?.length > 500) {
        return;
      }
      this.isLoading = true
      this.count +=1;
      if(this.count ==1){
      const products: any = {
        ...this.form.value,
        parent_id: this.form.value.parent_id ? this.form.value.parent_id : ''
      };
      const body = CommonUtil.trim(products);
      this.isUpdating = true;
      let serviceEvent = this.isUpdate ? this.productService.update(this.selectedProduct.id, body, true) : this.productService.create(body, true)
      serviceEvent.subscribe(res => {
        if (res.body?.data) {
          this.isLoading = false
          if (this.isUpdate) {
            this.toast.success(`product.successUpdate`);
          }
          else {
            this.toast.success(`product.success`);
          }
          this.modalRef.close({

            success: true,
            value: products,
          });
        }
      },
        (e: any) => {
          this.isUpdating = false;
        },
        () => {
          this.isUpdating = false;
        })
      }
    }, 300)
  }
  checkExist(type: any, value: any) {
    if (!value) {
      switch (type) {
        case 'code':
          this.formInvalid.code = false;
          break;
        default:
          break;
      }
    } else {
      switch (type) {
        case 'code':
          if (this.form.controls['code'].status == 'VALID') {
            this.commonService.checkExisted('products', { code: value }, false).then(
              (users: any) => {
                this.formInvalid.code = (users && users.body === 1) ? true : false;
              })
          }
          break;
        default:
          break;
      }
    }

  }
}

