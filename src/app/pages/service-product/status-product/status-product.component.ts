import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '@shared/service/product.service';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-status-product',
  templateUrl: './status-product.component.html',
  styleUrls: ['./status-product.component.scss']
})
export class StatusProductComponent implements OnInit {
  @Input() data: any =[];
  statusname = '';
  isClose = false;
  constructor(
    private productService: ProductService,
    private modalRef: NzModalRef,
  ) { }
  ngOnInit(): void {
    if( this.data.status == 1){
        this.statusname ='khóa';
        this.isClose= !this.isClose;
    }
    else{
      this.statusname ='mở '
    }
  }
  onLock(status: any){
    this.productService.updateStauts(status,this.data.id,true).subscribe( res =>{
      if (res.body?.data) {
        this.modalRef.close({
          success: true,
        });
      }
    })
  }
  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
}
