import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { LoginAuthComponent } from './login/login-auth/login-auth.component';
import {LoginComponent} from './login/login.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'login-sso',
    component: LoginAuthComponent
  }
  // {
  //   path: 'signup',
  //   component: SignupComponent
  // },
  // {
  //   path: 'forgot-password',
  //   component: ForgotPasswordComponent
  // },
  // {
  //   path: 'reset-password',
  //   component: ResetPasswordComponent,
  //   canActivate: [AuthGuard],
  //   data: {
  //     authorities: ['role:create'],
  //     title: 'login.reset-password.title'
  //   }
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule {
}
