import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { environment } from '@env/environment';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { AuthService } from '@shared/service/auth/auth.service';
import CommonUtil from '@shared/utils/common-utils';
import { LocalStorageService } from 'ngx-webstorage';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { NotificationService } from '@shared/service/notification.service';

@Component({
  selector: 'app-login-auth',
  templateUrl: './login-auth.component.html',
  styleUrls: ['./login-auth.component.scss']
})
export class LoginAuthComponent implements OnInit {

  constructor(
    // private route: ActivatedRoute,
    private authService: AuthService,
    private $localStorage: LocalStorageService,
    private angularFireMessaging: AngularFireMessaging,
    private noticationService: NotificationService,
  ) { }

  ngOnInit(): void {
    //Xử lý đăng nhập từ token của sso trả về
    // Hiện tại token đang lấy ở cookie của workspace nên cần xử lý đường dẫn
    const token = CommonUtil.getCookie('token')

    this.$localStorage.store(LOCAL_STORAGE.JWT_TOKEN, token);

    // Kiểm tra nếu có token trong cookie thì call api profile, ngược lại thì chuyển về trang base:
    if (token) {
      this.authService.storeProfile('/admin/dashboard');
      this.requestPermission()
      this.getNotification()
    }
    else {
      let url = `${window.location.protocol}//${window.location.host}/base/login?redirectURL=${window.location.protocol}//${window.location.host}/eticket/authentication/login-sso`;
      window.location.assign(url);
    }
  }

  requestPermission() {
    this.angularFireMessaging.requestPermission.subscribe((token) => {
    },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        this.authService.sendToken(token).subscribe(()=>{
        })
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }

  getNotification() {
    this.noticationService.getNotifi({ limit: PAGINATION.SIZE_DEFAULT }).subscribe((res: any) => {
      this.$localStorage.store('notification', { unread: res.body.meta.unread });
    })
  }
}
