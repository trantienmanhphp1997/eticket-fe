import { Component, OnInit } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LOCAL_STORAGE, SESSION_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { AuthService } from '@shared/service/auth/auth.service';
import { EventManagerService } from '@shared/service/helpers/event-manager.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { NotificationService } from '@shared/service/notification.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;
  passwordVisible = false;
  password?: string;
  LENGTH_VALIDATOR = LENGTH_VALIDATOR;
  isLoading = false;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService,
    private noticationService: NotificationService,
    private toast: ToastService,
    private router: Router,
    private eventManagerService: EventManagerService,
    private angularFireMessaging: AngularFireMessaging,
  ) {
    this.isTokenUnexpired();
  }

  ngOnInit(): void {
    if (environment.login_type == 'sso') {
      this.ssoLogin();
    }

    this.loginForm = this.fb.group({
      userName: ['', [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.USERNAME_MAX_LENGTH.MAX), Validators.minLength(1)]],
      password: ['', [Validators.required]],
      rememberMe: [true]
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  submitForm(): void {
    if (this.loginForm.valid) {
      this.isLoading = true
      this.authService.login(this.f.userName.value, this.f.password.value, this.f.rememberMe.value).subscribe(
        (token) => {
          this.requestPermission()
          if (token && this.authService.getToken()) {
            this.isLoading = false
            this.authService.storeProfile('/admin/dashboard');
            this.getNotification()
          }
        }, err => {
          this.isLoading = false;
        }
      );
    } else {
      Object.values(this.loginForm.controls).forEach(control => {
        this.isLoading = false
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  isTokenUnexpired(): void {
    this.eventManagerService.subscribe('reload', (res: any) => {
      this.router.navigate(['/']);
    });
    const jwt = this.localStorage.retrieve(LOCAL_STORAGE.JWT_TOKEN)
      || this.sessionStorage.retrieve(SESSION_STORAGE.JWT_TOKEN);
    if (jwt) {
      if (this.authService.getCurrentUser() === null) {
        this.authService.storeProfile('/', false);
      } else {
        this.router.navigate(['/']);
      }
    }
  }
  ssoLogin() {
    let url = `${window.location.host}/base/login?redirectURL=${window.location.host}/authentication/login-sso`;
    window.location.assign(url);
  }
  requestPermission() {
    this.angularFireMessaging.requestPermission.subscribe((token) => {
    },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        this.authService.sendToken(token).subscribe(()=>{
        })
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }
  getNotification() {
    this.noticationService.getNotifi({ limit: PAGINATION.SIZE_DEFAULT }).subscribe((res: any) => {
      this.localStorage.store('notification', { unread: res.body.meta.unread });
    })
  }
}
