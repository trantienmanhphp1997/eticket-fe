import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ACTION } from '@shared/constants/common.constant';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { CustomerService } from '@shared/service/customer.service';
import { environment } from '@env/environment';
import CommonUtil from '@shared/utils/common-utils';
import { AUTH } from '@shared/constants/auth.constant';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { ToastService } from '@shared/service/helpers/toast.service';
import { CustomerCreateComponent } from './customer-create/customer-create.component';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { ProductService } from '@shared/service/product.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  isCallFirstRequest: boolean = true;
  visible: boolean = false;
  product_id: any;
  productList: any = [];
  // page data
  customerList: any = [];
  total: any = 0;
  is_customer_sync: any;
  auth: any = AUTH;


  //checked items
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly any[] = [];
  setOfCheckedId = new Set<number>();
  checked = false;

  //form 
  searchForm = this.fb.group({
    keyword: [null]
  });
  customerSearch = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    orderBy: '',
    sortBy: '',
    search: this.searchForm.value.keyword,
    include: 'products,contacts',
    product_id: ''
  }

  commonUtil: any = CommonUtil
  constructor(
    private customerService: CustomerService,
    private fb: FormBuilder,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toastService: ToastService,
    private serviceProductService: ProductService
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.is_customer_sync = environment.is_customer_sync;
  }
  // load data
  loadData() {
    this.customerService.getCustomer(this.customerSearch, true).subscribe(
      (res) => {
        this.customerList = res.body?.data;
        this.total = res.body?.meta?.pagination.total;
      }
    );
  }

  getServiceProducts(products: any): any {
    var result = [];
    if (products.length > 0) {
      result = products.filter((item: any) => item.id != products[0].id)
    }
    return result;
  }
  getDeletgate(delegates: any): any {
    var result = [];
    if (delegates.length > 0) {
      result = delegates.filter((item: any) => item.id != delegates[0].id)
    }
    return result;
  }
  // Events
  onSearch($event: any) {
    this.customerSearch.search = this.searchForm.value.keyword.trim() || null;
    this.product_id = '';
    this.customerSearch.product_id = '';
      this.searchForm.patchValue({
        keyword: this.customerSearch.search
      })
      this.customerSearch.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.loadData();
  }
  onCreate = () => {
    const base = CommonUtil.modalBase(CustomerCreateComponent, { action: ACTION.CREATE }, '60%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }
  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }
  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData?.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }
  onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    this.customerSearch.sortBy = params.sort.filter((e: any) => e.value !== null).map((e: any) => e.value?.slice(0, -3)).toString();
    this.customerSearch.orderBy = params.sort.filter((e: any) => e.key !== null && e.key !== undefined && e.value !== null).map((e: any) => e.key).toString();
    this.loadData();
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.customerSearch.pageIndex = pageIndex;
    this.customerSearch.pageSize = pageSize;
    this.loadData();
  }
  onUpdate = (data: any) => {
    const base = CommonUtil.modalBase(CustomerCreateComponent, { action: ACTION.UPDATE, selectedCustomer: data, isUpdate: true }, '60%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.componentInstance.emitService.subscribe(() => {
      this.loadData()
    });
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }

  onDelete = (data: any) => {
    if(!data.check_can_edit){
      return
    }
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa khách hàng <b>${data?.name}</b> không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.customerService.delete(data?.id, true).subscribe((res: any) => {
          this.toastService.success(`customer.delete.success`);
          // this.getAllDepartments()
          modal.close();
          this.loadData()
        })
      } else {
        modal.close();
      }
    });
  }
  onMultiDelete(data: any) {
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa khách hàng đã chọn không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let customerIds = data instanceof Set ? Array.from(data) : [data.id];

    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.customerService.multiDelete(customerIds, true).subscribe(res => {
          this.toastService.success(`customer.delete.success`);
          this.setOfCheckedId = new Set<number>();
          modal.close();
          this.loadData()
        })
      } else {
        modal.close();
      }
    });
  }
  onAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.customerList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }
  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }
  onReset() {
    this.searchForm.patchValue({
      keyword: null,
    })
    this.loadData();
  }
  onDetail(data: any) {
    const base = CommonUtil.modalBase(CustomerCreateComponent, { action: ACTION.VIEW, selectedCustomer: data, isUpdate: true }, '45%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }
  getAllServiceProductData() {
    const params = {
      pageIndex: 0,
      pageSize: 0,
    }
    this.serviceProductService.getAllWithAlphabet(true).subscribe(res => {
      this.productList = this.createDataTree(res.body?.data?.filter(item => item.status == 1));
  });
  }
  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.name, key: aData.id, isLeaf: false });

    const dataTree: any = [];

    dataset.forEach((aData: any) => {
        if (aData.parent_id) {
            hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
        }
    });

    dataset.forEach((aData: any) => {
        if (hashTable[aData.id].children.length == 0) {
            hashTable[aData.id].isLeaf = true
        }
        else {
            hashTable[aData.id].expanded = false
        }
        if (!aData.parent_id) {

            dataTree.push(hashTable[aData.id]);
        }
    });

    return dataTree;
};
  openFillter() {
    this.visible = true;
    this.getAllServiceProductData();
  }
  onClose() {
    this.visible = false;
  }
  onChangeServiceProduct(event: any) {
    this.product_id = event;
    console.log(this.product_id);
    
  }
  onFilterData() {
    if (this.product_id) {
      this.customerSearch.search = null;
      this.searchForm.patchValue({
        keyword: null,
      })
      this.customerSearch.product_id = this.product_id;
      this.customerService.getCustomer(this.customerSearch, true).subscribe(res => {
        this.customerList = res.body?.data;
        this.total = res.body?.meta?.pagination?.total
      });
      this.visible = false;
    } else {
      this.visible = false;
      this.customerSearch.product_id = this.product_id;
      this.customerService.getCustomer(this.customerSearch, true).subscribe(res => {
        this.customerList = res.body?.data;
        this.total = res.body?.meta?.pagination?.total
      });
    }
  }
  onResetFilter() {
    this.product_id = '';
    this.customerSearch.product_id = '';
    this.loadData();
  }
  exportData() {
    const param = {
      product_id:this.product_id
    }
    this.customerService.export(param)
  }
}
