import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';
import { CustomerCreateComponent } from './customer-create/customer-create.component';




@NgModule({
  declarations: [CustomerComponent, CustomerCreateComponent],
  imports: [
    CommonModule,
    SharedModule,
    CustomerRoutingModule,
  ],
  exports: [CustomerComponent]
})
export class CustomerModule { }
