import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ACTION } from '@shared/constants/common.constant';
import { CUSTOMER_TYPE } from '@shared/constants/customer.constants';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { LENGTH_VALIDATOR, VALIDATORS } from '@shared/constants/validators.constant';
import { CommonService } from '@shared/service/common.service';
import { ContactService } from '@shared/service/contact.service';
import { CustomerService } from '@shared/service/customer.service';
import { DelegateService } from '@shared/service/delegate.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ProductService } from '@shared/service/product.service';
import { TicketService } from '@shared/service/ticket.service';
import CommonUtil from '@shared/utils/common-utils';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.scss']
})
export class CustomerCreateComponent implements OnInit {

  @Input() isUpdate: boolean = false;
  @Input() selectedCustomer: any = [];
  @Input() action: any = ACTION.CREATE
  @Input() source: any = 'customer';
  @Output() emitService = new EventEmitter();
  lengthValidator = LENGTH_VALIDATOR;
  customerType = CUSTOMER_TYPE;
  isLoading = false;
  count = 0;
  contact: any = [];
  delegate: any;
  listProduct: any[] = [];
  newCustomer: any;
  canAddContact = false;
  canEdit = false;
  viewEvent = ACTION.VIEW.event;
  actionNow = ACTION.CREATE;
  canEditDelegate = false;
  contactEditId = null;
  delegateId = null;
  serviceproduct: any[] = [];
  actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
  commonUntil: any = CommonUtil;
  expandKey = [''];
  listTicket: any = [];
  formInvalid = {
    phone: false,
    code: false,
    email: false,
    tax: false
  }
  ticketQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    departmentIds: '',
    orderBy: '',
    sortBy: '',
    keyword: '',
  };
  //form
  form: FormGroup = new FormGroup({});
  delegateForm = this.fb.group({
    name: ['', [Validators.maxLength(LENGTH_VALIDATOR.CODE_MAX_LENGTH.MAX), CustomInputValidator.isName]],
    email: ['', [Validators.maxLength(LENGTH_VALIDATOR.EMAIL_MAX_LENGTH.MAX), CustomInputValidator.isEmail]],
    phone: ['', [Validators.maxLength(LENGTH_VALIDATOR.PHONE_MAX_LENGTH.MAX), CustomInputValidator.isNumberPhone]],
    // address: ['', [Validators.maxLength(LENGTH_VALIDATOR.ADDRESS_MAX_LENGTH.MAX), Validators.pattern(VALIDATORS.ADDRESS)]],
  });
  contactForm = this.fb.group({
    name: ['', [Validators.maxLength(LENGTH_VALIDATOR.CODE_MAX_LENGTH.MAX), CustomInputValidator.isName]],
    email: ['', [Validators.maxLength(LENGTH_VALIDATOR.EMAIL_MAX_LENGTH.MAX), CustomInputValidator.isEmail]],
    phone: ['', [Validators.maxLength(LENGTH_VALIDATOR.PHONE_MAX_LENGTH.MAX), CustomInputValidator.isNumberPhone]],
    nameedit: [''],
    emailedit: [''],
    phoneedit: [''],
  });
  total: any = 0;
  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private customerService: CustomerService,
    private delegateService: DelegateService,
    private contactService: ContactService,
    private commonService: CommonService,
    private toastService: ToastService,
    private modalRef: NzModalRef,
    private ticketService: TicketService,
    private router: Router,
  ) {
  }
  codeExist = new Subject<string>();
  phoneExist = new Subject<string>();
  emailExist = new Subject<string>();
  taxExist = new Subject<string>();
  ngOnInit(): void {
    this.codeExist.pipe(
      debounceTime(600),
      distinctUntilChanged())
      .subscribe(value => {
        this.checkExist('code', value)
      });
    this.phoneExist.pipe(
      debounceTime(600),
      distinctUntilChanged())
      .subscribe(value => {
        this.checkExist('phone', value)
      });
    this.emailExist.pipe(
      debounceTime(600),
      distinctUntilChanged())
      .subscribe(value => {
        this.checkExist('email', value)
      });
    this.taxExist.pipe(
      debounceTime(600),
      distinctUntilChanged())
      .subscribe(value => {
        this.checkExist('tax', value)
      });
    if (this.actionNow.event === ACTION.CREATE.event) {
      this.canAddContact = true;
    }
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.loadData();
  }

  //loadData
  loadData() {
    this.actionNow = this.action;
    this.initForm();
    this.getListProduct();
    this.getListTicket();
  }
  initForm(): void {
    this.form = this.fb.group({
      name: [{ value: this.actionToShowData.includes(this.actionNow.event) ? this.selectedCustomer.name : '', disabled: this.actionNow.event === ACTION.VIEW.event ? true : false }, [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.VALUE_MAX_LENGTH.MAX), CustomInputValidator.isName]],
      code: [
        {
          value: this.actionToShowData.includes(this.actionNow.event) ? this.selectedCustomer.code : '',
          disabled: true
        },
        {
          validators: [Validators.maxLength(LENGTH_VALIDATOR.CODECUS_MAX_LENGTH.MAX), CustomInputValidator.notBlankValidator, CustomInputValidator.isCode]
        }
      ],
      serviceproduct: [, [Validators.required]],
      phone: [{ value: this.actionToShowData.includes(this.actionNow.event) ? this.selectedCustomer.phone : '', disabled: this.actionNow.event === ACTION.VIEW.event ? true : false },
      {
        validators: [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.PHONE_MAX_LENGTH.MAX), CustomInputValidator.isNumberPhone],
      }],
      email: [
        {
          value: this.actionToShowData.includes(this.actionNow.event) ? this.selectedCustomer.email : '', disabled: this.actionNow.event === ACTION.VIEW.event ? true : false
        },
        {
          validators: [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.EMAIL_MAX_LENGTH.MAX), CustomInputValidator.isEmail],
        }
      ],
      address: [{ value: this.actionToShowData.includes(this.actionNow.event) ? this.selectedCustomer.address : '', disabled: this.actionNow.event === ACTION.VIEW.event ? true : false }, [Validators.maxLength(LENGTH_VALIDATOR.ADDRESS_MAX_LENGTH.MAX), Validators.pattern(VALIDATORS.ADDRESS)]],
      customertype: [this.actionToShowData.includes(this.actionNow.event) ? this.selectedCustomer.type || '2' : '2'],
      tax: [
        {
          value: this.actionToShowData.includes(this.actionNow.event) ? this.selectedCustomer.tax : '', disabled: this.actionNow.event === ACTION.VIEW.event ? true : false
        },
        {
          validators: [Validators.maxLength(LENGTH_VALIDATOR.CODE_MAX_LENGTH.MAX), Validators.pattern(VALIDATORS.TAX)],
        }],
    });
    if (this.source == 'customer') {
      if (this.actionToShowData.includes(this.actionNow.event)) {
        this.delegateId = this.selectedCustomer.delegate_id;
        if (this.selectedCustomer.products?.data?.length > 0) {
          const productId = this.selectedCustomer.products?.data?.map((x: any, i: any) => x.id);
          this.serviceproduct = productId;
          this.form.patchValue({
            serviceproduct: productId,
          })
        }
        this.contact = this.selectedCustomer.contacts?.data;
        this.getDelegate();
      }
    }

  }
  getListTicket() {
    if (this.actionNow.event == ACTION.VIEW.event) {
      const param = {
        customer_id: this.selectedCustomer?.id
      }
      const options={
        limit:this.ticketQuery.pageSize,
        page:this.ticketQuery.pageIndex
      }
      this.ticketService.fillter(options, param, false).subscribe(res => {
        this.listTicket = res.body?.data;
        this.total = res.body?.meta?.pagination.total
      })
    }
  }
  getListProduct() {

    this.productService.getAll().subscribe(
      (res) => {
        this.listProduct = this.createDataTree(res.body?.data)
      }, err => {
        this.toastService.error(err);
      }
    );
  }
  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.name, key: aData.id, isLeaf: false });
    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children?.push(hashTable[aData.id]);
      }
    });
    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });
    return dataTree;

  };
  getDelegate() {
    if (this.delegateId) {
      this.delegateService.getDeledateByCustomerId(this.delegateId).subscribe(res => {
        this.delegate = res.body?.data;
      }, err => {
        this.toastService.error(err);
      }
      )
    }
  }
  getContact() {
    this.contactService.getContactByCustomerIdInCustomer(this.selectedCustomer.id, true).subscribe(res => {
      this.contact = res.body?.data;
    }, err => {
      this.toastService.error(err);
    })
  }
  //Event
  onSubmit() {
    let el = document.getElementById('myDiv');
    el?.click();
    setTimeout(() => {


      this.form.controls['serviceproduct'].setValue(this.serviceproduct);
      if (this.form.invalid) {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.form.controls['name'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }

      if (this.form.controls['code'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.formInvalid.code || this.formInvalid.phone || this.formInvalid.email || this.formInvalid.tax) {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }

      if (this.form.controls['phone'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.form.controls['email'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.form.controls['address'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.form.controls['tax'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.serviceproduct?.length == 0) {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.source == 'customer') {
        if (this.contactForm.invalid) {
          CommonUtil.markFormGroupTouched(this.contactForm);
          return;
        }
        if (this.delegateForm.invalid) {
          CommonUtil.markFormGroupTouched(this.delegateForm);
          return;
        }
      }
      this.isLoading = true
      this.count += 1
      if(this.count == 1){

      const customer: any = {
        name: this.form.value.name,
        code: this.form.value.code,
        phone: this.form.value.phone,
        email: this.form.value.email,
        address: this.form.value.address,
        type: this.form.value.customertype,
        tax: this.form.value.tax,
        productIds: this.serviceproduct
      };
      const body = CommonUtil.trim(customer);

      let serviceEvent = this.isUpdate ? this.customerService.update(this.selectedCustomer.id, body, true) : this.customerService.create(body, true)
      serviceEvent.subscribe((res) => {
        if (res.body?.data) {
          this.isLoading = false
          this.newCustomer = res.body.data;
          if (this.source == 'customer') {
            if (!this.isUpdate) {
              this.createContacts();
            }
            this.saveContactForm();
            this.createUpdateDelegate();
          }
          this.toastService.success(`model.customer.success.${this.isUpdate ? 'update' : 'create'}`);
          this.modalRef.close({
            success: true,
            value: customer,
          });
          this.emitService.next(null);
        }
      }, (e: any) => {
        this.isUpdate = false;
      },
        () => {
          this.isUpdate = false;
        });
      }
    }, 300)
  }
  createUpdateDelegate() {
    if (this.delegateForm.invalid) {
      CommonUtil.markFormGroupTouched(this.delegateForm);
      return;
    }
    const delegate = {
      ...this.delegateForm.value,
      customer_id: !this.isUpdate ? this.newCustomer?.id : this.selectedCustomer?.id,
    }
    const body = CommonUtil.trim(delegate);
    if (delegate.name?.trim() || delegate.phone?.trim() || delegate.email?.trim()) {
      let eventDelegate = !this.canEditDelegate ? this.delegateService.create(body, true) : this.delegateService.update(this.delegateId, body, true)
      eventDelegate.subscribe((res) => {
        if (this.isUpdate) {
          this.delegate = res?.body?.data;
          this.delegateId = this.delegate?.id;
          this.canEditDelegate = false;
          this.getDelegate();
        }
      }, (e: any) => {
        this.toastService.error(e);
      })
    }
  }

  createContacts() {
    this.contact.forEach((items: any, index: any) => {
      const params = {
        name: items.name,
        phone: items.phone,
        email: items.email,
        customer_id: this.newCustomer.id
      }
      const body = CommonUtil.trim(params);
      this.contactService.create(body, true).subscribe((res) => {
      }, (e: any) => {
        this.toastService.error(e);
      })
    })

  }
  saveContactForm() {
    if (this.contactForm.invalid) {
      CommonUtil.markFormGroupTouched(this.contactForm);
      return;
    }
    const params = {
      ...this.contactForm.value,
      customer_id: this.isUpdate ? this.selectedCustomer.id : this.newCustomer.id
    }
    if (this.contactForm.value.name?.trim() || this.contactForm.value.phone?.trim() || this.contactForm.value.email?.trim()) {
      const body = CommonUtil.trim(params);
      this.contactService.create(body, true).subscribe((res) => {
      }, (e: any) => {
        this.toastService.error(e);
      })
    }

  }
  onCancel() {
    if(this.actionNow!=ACTION.VIEW && this.isUpdate==true){
      this.emitService.next(null);
    }
    this.modalRef.close({
      success: false,
      value: null,
    });
  }

  onAddContact() {
    if (!this.canEdit) {
      this.canAddContact = true;
      if (this.contactForm.invalid) {
        CommonUtil.markFormGroupTouched(this.contactForm);
        return;
      }
      if (!this.contactForm.value.name?.trim() && !this.contactForm.value.phone?.trim() && !this.contactForm.value.email?.trim()) {
        return;
      }
      //
      if (this.isUpdate) {
        const params = {
          ...this.contactForm.value,
          customer_id: this.selectedCustomer?.id
        }
        const body = CommonUtil.trim(params);
        this.contactService.create(body, true).subscribe((res) => {
          this.getContact();
        }, (e: any) => {
          this.toastService.error(e);
        })
      } else {
        const arrRowAdd = {
          ...this.contactForm.value,
          customer_id: '',
        }
        const body = CommonUtil.trim(arrRowAdd);
        this.contact.push(body);
      }
      this.contactForm.patchValue({
        name: null,
        phone: null,
        email: null
      })
    }
  }
  onCancelDelegate() {
    this.canEditDelegate = false;
  }
  onDeleteDelegate(index: any) {
    this.delegateService.delete(index, true).subscribe(res => {
      this.delegate = [];
      this.delegateId = null;
      this.emitService.next(null);
    }, err => {
      this.toastService.error(err);
    })
  }
  onEditRowContact(data: any, index: any) {
    this.onEditStatus();
    this.contactEditId = index;
    this.contactForm.patchValue({
      nameedit: data.name,
      emailedit: data.email,
      phoneedit: data.phone,
    })
  }
  onUpdateRowContact(data: any, index: any) {
    if (this.contactForm.value.nameedit?.trim() || this.contactForm.value.emailedit?.trim() || this.contactForm.value.phoneedit?.trim()) {
      if (index != null) {
        const params = {
          name: this.contactForm.value.nameedit,
          email: this.contactForm.value.emailedit,
          phone: this.contactForm.value.phoneedit,
        }
        if (this.isUpdate) {
          this.contactService.update(index, params, true).subscribe(res => {
            this.getContact();
          }, err => {
            this.toastService.error(err);
          })
        } else {
          this.contact[index] = params;
        }
      } else {
        this.onAddContact();
      }
      this.onCancelStatus();
    }
  }
  onEditRowDelegate() {
    this.canEditDelegate = true;
    this.delegateForm.patchValue({
      name: this.delegate.name,
      email: this.delegate.email,
      phone: this.delegate.phone,
    })

  }
  onUpdateRowDelegate() {
    this.createUpdateDelegate();
  }
  onCancelRowContact() {
    this.onCancelStatus();
  }
  onDeleteRowContact(index: any) {
    if (!this.isUpdate) {
      this.contact = this.contact.filter((_: any, i: any) => i != index);
      if (this.contactEditId == index) {
        this.onCancelStatus();
      }
    } else {
      this.contactService.delete(index, true).subscribe(res => {
        this.getContact()
      }, err => {
        this.toastService.error(err);
      })
    }

  }

  onEditStatus() {
    this.canEdit = true;
    this.canAddContact = false;
    this.contactForm.controls['nameedit'].setValidators([Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX), CustomInputValidator.isName]);
    this.contactForm.controls['phoneedit'].setValidators([Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX), CustomInputValidator.isNumberPhone]);
    this.contactForm.controls['emailedit'].setValidators([Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX), CustomInputValidator.isEmail]);
    this.contactForm.updateValueAndValidity();
  }
  onCancelStatus() {
    this.canAddContact = false;
    this.canEdit = false;
    this.contactEditId = null;
    this.contactForm.patchValue({
      name: null,
      email: null,
      phone: null,
      nameedit: null,
      emailedit: null,
      phoneedit: null,
    })
    this.contactForm.controls['nameedit'].clearValidators();
    this.contactForm.controls['phoneedit'].clearValidators();
    this.contactForm.controls['emailedit'].clearValidators();
  }
  getClassInvalid(name: string) {
    if (this.form.get(name)?.errors) {
      return 'invalid';
    }
    return '';
  }
  onUpdate() {
    this.actionNow = ACTION.UPDATE;
    this.initForm();
  }
  checkExist(type: any, value: any) {
    if (!value) {
      switch (type) {
        case 'phone':
          this.formInvalid.phone = false;
          break;
        case 'code':
          this.formInvalid.code = false;
          break;
        case 'email':
          this.formInvalid.email = false
          break;
        case 'tax':
          this.formInvalid.tax = false
          break;

        default:
          break;

      }
    } else {
      switch (type) {
        case 'phone':
          if (this.form.controls['phone'].status == 'VALID') {
            this.commonService.checkExisted('customers', { phone: value }, false).then(
              (users: any) => {
                this.formInvalid.phone = (users && users.body === 1) ? true : false;
              })
          }
          break;
        case 'code':
          if (this.form.controls['code'].status == 'VALID') {
            this.commonService.checkExisted('customers', { code: value }, false).then(
              (users: any) => {
                this.formInvalid.code = (users && users.body === 1) ? true : false;
              })
          }
          break;
        case 'email':
          if (this.form.controls['email'].status == 'VALID') {
            this.commonService.checkExisted('customers', { email: value }, false).then(
              (users: any) => {
                this.formInvalid.email = (users && users.body === 1) ? true : false;
              })
          }
          break;

        case 'tax':
          if (this.form.controls['tax'].status == 'VALID') {
            this.commonService.checkExisted('customers', { tax: value }, false).then(
              (users: any) => {
                this.formInvalid.tax = (users && users.body === 1) ? true : false;
              })
          }
          break;

        default:
          break;
      }
    }

  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.ticketQuery.pageIndex = pageIndex;
    this.ticketQuery.pageSize = pageSize;
    this.getListTicket();
  }
  viewTicket(id: any) {
    this.modalRef.close({
      success: false,
      value: null,
    });
    this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}/${ROUTER_UTILS.ticket.view}/${id}`]);
  }
}
