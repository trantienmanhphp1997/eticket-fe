import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ProfileDetailRoutingModule } from './profile-detail-routing.module';
import { ProfileDetailComponent } from './profile-detail.component';
import { ChangePasswordProfileComponent } from './change-password/change-password.component';




@NgModule({
  declarations: [ProfileDetailComponent,ChangePasswordProfileComponent],
  imports: [
    CommonModule,
    SharedModule,
    ProfileDetailRoutingModule,
  ],
  exports: [ProfileDetailComponent]
})
export class ProfileDetailModule { }
