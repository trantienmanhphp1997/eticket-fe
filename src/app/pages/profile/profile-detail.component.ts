import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '@shared/service/user.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { CommonService } from '@shared/service/common.service';
import { UserGroupService } from '@shared/service/user-group.service';
import { ACTION } from '@shared/constants/common.constant';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { VALIDATORS } from '@shared/constants/validators.constant';
import { ActivatedRoute,Router  } from '@angular/router';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';
import { AuthService } from '@shared/service/auth/auth.service';
import { DepartmentService } from '@shared/service/department.service';
import CommonUtil from '@shared/utils/common-utils';
import { environment } from '@env/environment';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';

@Component({
    selector: 'profile',
    templateUrl: './profile-detail.component.html',
    styleUrls: ['./profile-detail.component.css']
})

export class ProfileDetailComponent implements OnInit {
    @Output() emitterError: any = new EventEmitter();
    @Output() emitter: any = new EventEmitter();
    @Output() emitterAction: any = new EventEmitter();

    actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
    form: FormGroup = new FormGroup({});
    LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
    profileId: any;
    isDetail: boolean = true;
    currentUser: any;
    VALIDATORS: any = VALIDATORS;
    nodes: any;
    dataProfile: any = [];
    groupUserList: any;
    nameUser: any;
    userGroups: any = [];
    idFile: any = [];
    checkErrorUpload: any = false;
    imgFile: any;
    urlgetway = environment.fileDomain
    is_user_sync = environment.is_user_sync
    profileFromLocalStorage: any;
    checkActionUpload = false;
    checkExistDept = false;

    constructor(
        private fb: FormBuilder,
        private userService: UserService,
        private toast: ToastService,
        private commonService: CommonService,
        private userGroup: UserGroupService,
        private route: ActivatedRoute,
        private authService: AuthService,
        private departmentService: DepartmentService,
        private localStorage: LocalStorageService,
        private router: Router,
    ) {
        this.route.paramMap.subscribe((res) => {
            this.profileId = res.get('id') || '';
        });
    }
    ngOnInit() {
        // console.log(profile);
        this.initForm();
        this.getcurrentUser();

        this.getAllDepartments();
        this.getDataGroupUser();

    }

    getDataGroupUser() {
        const params = {
            pageIndex: 0,
            pageSize: 0,
            is_default: 1
        }
        this.userGroup.getUserGroups(params, true).subscribe(res => {
            this.groupUserList = res.body?.data;
        });
    }
    getAllDepartments() {
        const params = {
            limit: 10000,
            data_context: 'user_self_update_dept'
        }
        this.departmentService.getAllDepartments(params, true).subscribe(
            (res) => {
                this.nodes = res.body
            }
        );
    }
    createDataTree = (dataset: any) => {
        const hashTable = Object.create(null);
        dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.dept_name, key: aData.id, isLeaf: false });

        const dataTree: any = [];

        dataset.forEach((aData: any) => {
            if (aData.parent_id) {
                hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
            }
        });

        dataset.forEach((aData: any) => {
            if (hashTable[aData.id].children.length == 0) {
                hashTable[aData.id].isLeaf = true
            }
            else {
                hashTable[aData.id].expanded = false
            }
            if (!aData.parent_id) {

                dataTree.push(hashTable[aData.id]);
            }
        });

        return dataTree;
    };

    getcurrentUser() {
        this.authService.myProfile().subscribe(res => {
            if (res.body?.data.department_id) {
                this.checkExistDept = true;
            }
            this.nameUser = res.body?.data.name;
            this.imgFile = res.body?.data?.avatarProfile?.data?.url;
            const usergroupId = res.body.data.userGroups?.data?.map((x: any) => x.id);
            this.userGroups = usergroupId;

            this.form.patchValue({
                userGroupIds: usergroupId,
                username: res.body.data.username,
                phone: res.body.data.phone,
                email: res.body.data.email,
                gender: res.body.data.gender,
                department_id: res.body.data.department_id
            })
        });
    }

    initForm(): void {
        this.form = this.fb.group({
            userGroupIds: [],
            username: [{ value: [], disabled: true }, []],
            phone: [{ value: [], disabled: true }, [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.PHONE_MAX_LENGTH.MAX)]],
            email: [{ value: [], disabled: true }, [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.EMAIL_MAX_LENGTH.MAX), CustomInputValidator.isEmail]],
            gender: [],
            department_id: []
        })
    }

    handleLoadFile(e: any) {
        this.idFile = e
    }
    addEvent(e: any) {
        let errArr = e.filter((f: any) => f !== undefined);
        if (errArr.length > 0) {
            this.checkErrorUpload = true;
        } else {
            this.checkErrorUpload = false;
        }
    }
    onSubmit() {
        if (this.form.invalid) {
            CommonUtil.markFormGroupTouched(this.form);
            return;
        }
        const user: any = {
            ...this.form.value,
            // gender: parseFloat(this.form.value.gender),
            avatarId: this.idFile.toString()
        };
        const body = CommonUtil.trim(user);
        this.userService.update(this.profileId, body, true).subscribe(res => {
            if (res.body?.data) {
                this.isDetail = true
                if (this.isDetail) {
                    this.form.controls['phone'].disable();
                    this.form.controls['email'].disable();
                }
                this.authService.myProfile().subscribe(res1 => {
                    this.toast.success(`Cập nhật thành công`);
                    this.localStorage.store(LOCAL_STORAGE.PROFILE, res1.body?.data);
                    location.reload();
                })

            }
        })
    }
    onChangeData(data: any) {

    }
    onClose() {
        history.back()
    }
    checkUpload(event: any) {
        this.checkActionUpload = event;

    }
}
