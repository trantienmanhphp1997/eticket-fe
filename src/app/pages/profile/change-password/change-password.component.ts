import { Component, Input, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '@shared/service/helpers/toast.service';
import { UserService } from '@shared/service/user.service';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import CommonUtil from '@shared/utils/common-utils';
import { NzInputModule } from 'ng-zorro-antd/input';
import { VALIDATORS } from '@shared/constants/validators.constant';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})

export class ChangePasswordProfileComponent implements OnInit {
    @Input() profileId: any;

    form: FormGroup = new FormGroup({});
    old_password: any;
    password: any;
    rep_password: any;
    LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
    checkRepPassword = '';
    passwordVisible = false;
    repPasswordVisible = false;
    oldPasswordVisible = false
    checkOldPassword = false;
    VALIDATORS: any = VALIDATORS;

    constructor(
        private modalRef: NzModalRef,
        private fb: FormBuilder,
        private toast: ToastService,
        private userService: UserService,
    ) { }
    ngOnInit() {
        this.initForm();
    }

    initForm(): void {
        this.form = this.fb.group({
            old_password: ['', [Validators.required,Validators.minLength(LENGTH_VALIDATOR.PASSWORD_MIN_LENGTH.MIN),Validators.pattern(VALIDATORS.PASSWORD)]],
            password: ['', [Validators.required,Validators.minLength(LENGTH_VALIDATOR.PASSWORD_MIN_LENGTH.MIN),Validators.pattern(VALIDATORS.PASSWORD)]],
            rep_password: ['', [Validators.required,Validators.minLength(LENGTH_VALIDATOR.PASSWORD_MIN_LENGTH.MIN),Validators.pattern(VALIDATORS.PASSWORD)]],

        });
    }
    onCancel(): void {
        this.modalRef.close({
            success: false,
            value: null,
        });
    }
    onSubmit() {
        if (this.form.invalid) {
            CommonUtil.markFormGroupTouched(this.form);
            return;
        }
        if (this.form.value.rep_password != this.form.value.password) {
            this.checkRepPassword = "Mật khẩu xác nhận chưa chính xác";
            return;
        }
        const param = {
            old_password: this.form.value.old_password,
            password: this.form.value.password
        };
        this.userService.update(this.profileId, param, true).subscribe(res => {
            if (res.body == 0) {
                this.checkOldPassword = true;
                return;
            }
            if (res.body?.data) {
                this.toast.success(`Thay đổi mật khẩu thành công`);
                this.modalRef.close({
                    success: true,
                });
            }
        })
    }

}