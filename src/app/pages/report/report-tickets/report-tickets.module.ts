import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportTicketsComponent } from './report-tickets.component';
import { SharedModule } from '@shared/shared.module';
import { ReportTicketsRoutingModule } from './report-tickets-routing.module';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';



@NgModule({
  declarations: [ReportTicketsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportTicketsRoutingModule,
    NgxDaterangepickerMd.forRoot()
  ],
  exports: [ReportTicketsComponent]
})
export class ReportTicketsModule { }
