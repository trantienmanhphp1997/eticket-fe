import { Component, OnInit, ViewChild } from '@angular/core';
import CommonUtil from '@shared/utils/common-utils';
import { TicketService } from '@shared/service/ticket.service';
import { DepartmentService } from '@shared/service/department.service';
import { FeatureUpgradeComponent } from './feature-upgrade/feature-upgrade.component';
import { InterfaceUpgradeComponent } from './interface-upgrade/interface-upgrade.component';
import { ReportRequestUpgradeService } from '@shared/service/report-request-upgrade.service';

@Component({
  selector: 'app-request-upgrade',
  templateUrl: './report-request-upgrade.component.html',
  styleUrls: ['./report-request-upgrade.component.scss']
})
export class ReportRequestUpgradeComponent implements OnInit {
  @ViewChild('feature') tab1!: FeatureUpgradeComponent;
  @ViewChild('interface') tab2!: InterfaceUpgradeComponent;

  visible: boolean = false;
  checked = false;
  indeterminate = false;
  today = new Date();
  listReportTicket: any = [];
  setOfCheckedId = new Set<number>();
  productList: any = [];
  ticketPriorityList: any = [];
  ticketSourceList: any = [];
  ticketCategoryList: any = [];
  ticketStateList: any = [];
  customerList: any = [];
  userList: any = [];
  listAssignee : any = [];
  deptList: any = [];
  total: any = 0;
  commonUtil: any = CommonUtil;
  queryFilter = {
    keyword: '',
    product_id: [],
    department_id: '',
    user_ids: [],
    category_ids: [],
    source_ids: [],
    state_ids: [],
    priority_ids: [],
    department_handle_id: '',
    user_create_ids: []
  };
  fillterDate: boolean = false;
  isCallFirstRequest: boolean = true;
  fillter: any = [];
  start_date: any;
  end_date: any;
  selectedIndex = 0;
  constructor(
    private ticketService: TicketService,
    private departmentService: DepartmentService,
    private reportRequestUpgradeService: ReportRequestUpgradeService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }
  reloadFillter() {
    this.visible = false;
    this.resetQuery();
    this.loadData();
  }
  resetQuery() {
    this.queryFilter = {
      keyword: '',
      product_id: [],
      department_id: '',
      user_ids: [],
      category_ids: [],
      source_ids: [],
      state_ids: [],
      priority_ids: [],
      department_handle_id: '',
      user_create_ids: []
    };
  }
  onChangeIndex($event: any) {
    this.resetQuery();
    // this.selectedIndex = $event;
    // switch (this.selectedIndex) {
    //   case 0:
    //     this.tab1.setDefaultDate()
    //     break;
    //   case 1:
    //     this.tab2.setDefaultDate()
    //     break;
    //   default:
    // }
    this.loadData();
  }
  loadData() {
    switch (this.selectedIndex) {
      case 0:
        this.tab1.queryFilter = this.queryFilter;
        this.tab1.loadData();
        break;
      case 1:
        this.tab2.queryFilter = this.queryFilter;
        this.tab2.loadData();
        break;
      default:
      // code block
    }
  }
  openFillter() {
    this.visible = true;
    this.user();
    this.department();
    this.ticketService.loadMultipleDataForTicketDetail().subscribe(res => {
      this.productList = res[0].body?.data;
      this.ticketPriorityList = res[1].body?.priority.data;
      this.ticketPriorityList.push({object: '"TicketPriority"',name:'Chưa xác định',id:'undefined_priority',status:1});
      this.ticketSourceList = res[1].body?.source.data;
      this.ticketCategoryList = res[1].body?.category.data;
      this.ticketStateList = res[1].body?.state.data;
    })
  }
  onSearch() {
    this.queryFilter.department_id = '';
    this.queryFilter.product_id = [];
    this.queryFilter.user_ids = [];
    this.queryFilter.category_ids = [];
    this.queryFilter.source_ids = [];
    this.queryFilter.state_ids = [];
    this.queryFilter.priority_ids = [];
    this.queryFilter.department_handle_id = '';
    this.queryFilter.user_create_ids = [];
    this.loadData();
  }
  exportReport() {
    switch (this.selectedIndex) {
      case 0:
        var param={
          from_date: this.tab1.start_date != '' ? this.tab1.start_date : '',
          to_date: this.tab1.end_date != '' ? this.tab1.end_date: '',
          keyword: this.queryFilter?.keyword,
          product_id: [this.queryFilter?.product_id],
          department_ids:this.queryFilter?.department_id,
          user_ids:[this.queryFilter?.user_ids],
          source_ids:[this.queryFilter?.source_ids],
          state_ids:[this.queryFilter?.state_ids],
          priority_ids: [this.queryFilter?.priority_ids],
          created_bys:[this.queryFilter?.user_create_ids],
          created_by_department_ids: this.queryFilter?.department_handle_id,
          is_feature: 1
        }
        this.reportRequestUpgradeService.exportReportData(param);
        break;
      case 1:
        var param1={
          from_date: this.tab2.start_date != '' ? this.tab2.start_date: '',
          to_date: this.tab2.end_date != '' ? this.tab2.end_date: '',
          keyword: this.queryFilter?.keyword,
          product_id: [this.queryFilter?.product_id],
          department_ids:this.queryFilter?.department_id,
          user_ids:[this.queryFilter?.user_ids],
          source_ids:[this.queryFilter?.source_ids],
          state_ids:[this.queryFilter?.state_ids],
          priority_ids: [this.queryFilter?.priority_ids],
          created_bys:[this.queryFilter?.user_create_ids],
          created_by_department_ids: this.queryFilter?.department_handle_id,
          is_feature: 0
        }
        this.reportRequestUpgradeService.exportReportData(param1);
        break;
      default:
      // code block
    }
  }
  close(): void {
    this.visible = false;
  }
  onFilterData() {
    this.queryFilter.keyword = '';
    this.loadData()
    this.visible = false;
  }
  department() {
    const params = {
      pageIndex: '',
      pageSize: 0,
      data_context: 'kpi_report'
    }
    this.departmentService.getAllDepartments(params, false).subscribe((res) => {
      this.deptList = res.body;
    });
  }
  user() {
    this.ticketService.getAllAssignee({ data_context: 'filter_assignee' }, true).subscribe(res => {
      this.listAssignee = res?.body?.data;
    })
  }
}
