import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { ReportRequestUpgradeComponent } from './report-request-upgrade.component';

const routes: Routes = [
  {
    path: '',
    component:ReportRequestUpgradeComponent ,
    canActivate: [AuthGuard],
    data: {
        authorities: [AUTH.REPORT.REQUEST_UPGRADE],
        title: 'Báo cáo yêu cầu nâng cấp tính năng, giao diện',
        breadcrum: [
          {
            level: 1,
            title: 'Báo cáo thống kê',
            path: 'javascript:void(0)'
          }
        ]
      }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRequestUpgradeRoutingModule { }