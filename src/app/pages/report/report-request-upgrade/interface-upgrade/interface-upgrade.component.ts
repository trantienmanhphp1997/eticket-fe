import { Component, Input, OnInit } from '@angular/core';
import { PAGINATION } from '@shared/constants/pagination.constants';
import CommonUtil from '@shared/utils/common-utils';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { ReportRequestUpgradeService } from '@shared/service/report-request-upgrade.service';
import { NzDatePickerComponent } from 'ng-zorro-antd/date-picker';
import { endOfMonth, endOfQuarter, endOfWeek, endOfYear, startOfMonth, startOfQuarter, startOfWeek, startOfYear, sub} from 'date-fns';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-interface-upgrade',
  templateUrl: './interface-upgrade.component.html',
  styleUrls: ['./interface-upgrade.component.scss']
})

export class InterfaceUpgradeComponent implements OnInit {
    @Input() queryFilter:any;
    data:any=[];
    total:any=0;
    common: any= CommonUtil;
    commonUtil: any = CommonUtil;
    checked = false;
    indeterminate = false;
    today =new Date();
    listReportTicket : any = [];
    setOfCheckedId = new Set<number>();
    listOfCurrentPageData: readonly any[] = [];
    isCallFirstRequest: boolean = true;
    fillter: any =[];
    isSearch: boolean = false;
    start_date = '';
    end_date = '';
    fillterDate: boolean = false;
    styleTooltip = {
        'min-width': 'max-content',
    };
    selected: any = { startDate: null, endDate: null };
    typeInput: boolean = false;
    calendarLocale = {
        format: 'DD/MM/YYYY',
        daysOfWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
        monthNames: ['Th1', 'Th2', 'Th3', 'Th4', 'Th5', 'Th6', 'Th7', 'Th7', 'Th9', 'Th10', 'Th11', 'Th12'],
        firstDay: 1
    };
    ranges = {
        'Hôm nay': [moment(), moment()],
        'Tuần này': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
        'Tháng này': [moment().startOf('month'), moment().endOf('month')],
        'Năm này':  [moment().startOf('year'), moment().endOf('year')],
        '1 tuần trước': [moment().subtract(6, 'days'), moment()],
        '1 tháng trước': [moment().subtract(29, 'days'), moment()],
        '1 năm trước': [moment().subtract(1, 'year'), moment()],
    };
    constructor(
        private reportRequestUpgradeService: ReportRequestUpgradeService,
        private router: Router,
    ) { 
    }
    reportQuery = {
        pageIndex: PAGINATION.PAGE_DEFAULT,
        pageSize: PAGINATION.SIZE_DEFAULT,
        pageSizeOptions: PAGINATION.OPTIONS,
        orderBy: '',
        sortBy: '',
    };
    
    ngOnInit(): void {
        this.setDefaultDate();
        this.loadData();
    }
    loadData(){
        let startDate = new Date(this.selected.startDate);
        let endDate = new Date(this.selected.endDate);
        
        this.fillter = {
            from_date: this.selected.startDate != null ? startDate.getFullYear() + '-' + String(startDate.getMonth() + 1).padStart(2, '0') + '-' + String(startDate.getDate()).padStart(2, '0'): '',
            to_date: this.selected.endDate != null ? endDate.getFullYear() + '-' + String(endDate.getMonth() + 1).padStart(2, '0') + '-' + String(endDate.getDate()).padStart(2, '0'): '',
        }

        const params = {
            pageIndex: this.reportQuery.pageIndex,
            pageSize: this.reportQuery.pageSize,
            keyword: this.queryFilter?.keyword,
            orderBy: this.reportQuery.orderBy,
            sortBy: this.reportQuery.sortBy,
            product_id: [this.queryFilter?.product_id],
            department_ids:this.queryFilter?.department_id,
            user_ids:[this.queryFilter?.user_ids],
            source_ids:[this.queryFilter?.source_ids],
            state_ids:[this.queryFilter?.state_ids],
            priority_ids: [this.queryFilter?.priority_ids],
            created_bys:[this.queryFilter?.user_create_ids],
            created_by_department_ids: this.queryFilter?.department_handle_id,
            is_feature: 0
        }
        this.reportRequestUpgradeService.getReportRequestUpgrade(params,this.fillter, true).subscribe(res =>{
            this.listReportTicket = res.body?.data;
            this.total = res.body?.meta?.pagination?.total;
        })
    }
    onQuerySearch(params: any): void {
        const { pageIndex, pageSize } = params;
        this.reportQuery.pageIndex = pageIndex;
        this.reportQuery.pageSize = pageSize;
        this.loadData();
    }
    onChangeDate(){
        this.fillterDate = true;
        this.loadData()
    }
    onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
        this.listOfCurrentPageData = listOfCurrentPageData;
        this.refreshCheckedStatus();
    }
    refreshCheckedStatus(): void {
        const listOfEnabledData = this.listOfCurrentPageData.filter((item: any) => !item.disabled);
        this.checked = listOfEnabledData.every((item: any) => this.setOfCheckedId.has(item.id));
        this.indeterminate = listOfEnabledData.some((item: any) => this.setOfCheckedId.has(item.id)) && !this.checked;
    }
    onQueryParamsChange(params: NzTableQueryParams): void {
        if (this.isCallFirstRequest) {
            this.isCallFirstRequest = false;
            return;
        }
        this.reportQuery.sortBy = params.sort.filter((e: any) => e.value !== null).map((e: any) => e.value?.slice(0, -3)).toString();
        this.reportQuery.orderBy = params.sort.filter((e: any) => e.key !== null && e.key !== undefined && e.value !== null).map((e: any) => e.key).toString();
        this.loadData();
    }
    onSearchDate($event: any) {
        this.loadData();
    }
    clearDate(event: any){
        event.stopPropagation();
        this.start_date= '';
        this.end_date =''
        this.loadData();
    }
    setDefaultDate(){
        const date =new Date();
        this.selected.startDate =new Date(date.getFullYear(), date.getMonth()-1, this.today.getDate());
        this.selected.endDate = new Date();
        let startDate = new Date(this.selected.startDate);
        let endDate = new Date(this.selected.endDate);
        this.start_date = startDate.getFullYear() + '-' + String(startDate.getMonth() + 1).padStart(2, '0') + '-' + String(startDate.getDate()).padStart(2, '0');
        this.end_date = endDate.getFullYear() + '-' + String(endDate.getMonth() + 1).padStart(2, '0') + '-' + String(endDate.getDate()).padStart(2, '0');
    }
    openDetail(id : any){
        let url_detail = "/eticket/admin/ticket-management/view-ticket/" + id;
        const url = this.router.serializeUrl(
            this.router.createUrlTree([`${url_detail}`])
        );
        window.open(url);
    }
    ngModelChange(event:any): void {
        if(event.type=='change'){
            if(event.target.value){
                this.typeInput = true;
                const date = event.target.value;
                const vitri: number = date.search("-");
                const SD =  date.slice(0,Number(vitri-1));
                const pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
                this.selected.startDate = new Date(SD.replace(pattern,'$3-$2-$1'));
                const ED =  date.slice(Number(vitri+2),date.length);
                this.selected.endDate = new Date(ED.replace(pattern,'$3-$2-$1'));
                if(!this.selected.startDate.toISOString() || !this.selected.endDate.toISOString()){
                    return;
                }
            }else{
                this.selected.startDate = null;
                this.selected.endDate = null;
                this.start_date = '';
                this.end_date = '';
            }
        }else{
            let startDate = new Date(this.selected.startDate);
            let endDate = new Date(this.selected.endDate);
            this.start_date = startDate.getFullYear() + '-' + String(startDate.getMonth() + 1).padStart(2, '0') + '-' + String(startDate.getDate()).padStart(2, '0');
            this.end_date = endDate.getFullYear() + '-' + String(endDate.getMonth() + 1).padStart(2, '0') + '-' + String(endDate.getDate()).padStart(2, '0');
        }
        this.loadData();
    }
}
