import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { ReportRequestUpgradeComponent } from './report-request-upgrade.component';
import { ReportRequestUpgradeRoutingModule } from './report-request-upgrade-routing.module';
import { FeatureUpgradeComponent } from './feature-upgrade/feature-upgrade.component';
import { InterfaceUpgradeComponent } from './interface-upgrade/interface-upgrade.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';

@NgModule({
  declarations: [FeatureUpgradeComponent, InterfaceUpgradeComponent, ReportRequestUpgradeComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportRequestUpgradeRoutingModule,
    NgxDaterangepickerMd.forRoot()
  ],
  exports: [ReportRequestUpgradeComponent]
})
export class ReportRequestUpgradeModule { }