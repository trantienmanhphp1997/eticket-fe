import { Component, OnInit, ViewChild } from '@angular/core';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { DepartmentService } from '@shared/service/department.service';
import { ProductService } from '@shared/service/product.service';
import { ReportKpiService } from '@shared/service/report-kpi.service';
import { LocalStorageService } from 'ngx-webstorage';
import { StatisticalKpiComponent } from './statistical-kpi/statistical-kpi.component';
import { StatisticalMonthComponent } from './statistical-month/statistical-month.component';
import { StatisticalYearComponent } from './statistical-year/statistical-year.component';

@Component({
  selector: 'app-report-kpi',
  templateUrl: './report-kpi.component.html',
  styleUrls: ['./report-kpi.component.scss'],
})
export class ReportKpiComponent implements OnInit {
  @ViewChild('kpi') tab3!: StatisticalKpiComponent;
  @ViewChild('year') tab1!: StatisticalYearComponent;
  @ViewChild('month') tab2!: StatisticalMonthComponent;
  visible: boolean = false;
  checked = false;
  indeterminate = false;
  productList: any = [];
  deptList: any = [];
  selectedIndex = 0;
  queryFilter = {
    product_id: null,
    department_id: null,
    keyword: null,
  }
  profile: any;
  constructor(
    private departmentService: DepartmentService,
    private serviceProductService: ProductService,
    private reportKPIService: ReportKpiService,
    private localStorage: LocalStorageService
  ) { }

  ngOnInit(): void {
     this.profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
  }
  openFillter() {
    this.visible = true;
    this.getAllDepartmentData();
    this.getAllServiceProductData();
  }
  close(): void {
    this.visible = false;
  }
  getAllDepartmentData() {
    const params = {
      pageIndex: '',
      pageSize: 0,
      data_context: 'kpi_report'
    }
    this.departmentService.getAllDepartments(params, false).subscribe((res) => {
      this.deptList = res.body;
    });
  }
  getAllServiceProductData() {
    const params = {
      pageIndex: 0,
      pageSize: 0,
    }
    this.serviceProductService.getAll(false).subscribe((res) => {
      this.productList = res.body?.data;
    });
  }
  onFilterData() {
    if (this.queryFilter.department_id || this.queryFilter.product_id) {
      this.queryFilter.keyword = null;
      this.loadData()
      this.visible = false;
    } else {
      this.visible = false;
      this.loadData()
    }
  }
  onSearch() {
    this.queryFilter.department_id = null;
    this.queryFilter.product_id = null;
    this.loadData()
  }
  onChangeIndex($event: any) {
    this.queryFilter = {
      department_id: null,
      product_id: null,
      keyword: null
    }
    this.selectedIndex = $event;
    switch (this.selectedIndex) {
      case 0:
        this.tab1.setDefaultDate()
        break;
      case 1:
        this.tab2.setDefaultDate()

        break;
      case 2:
        // this.tab3.setDefaultDate()
        break;
      default:
    }
    this.loadData();
  }
  loadData() {
    switch (this.selectedIndex) {
      case 0:
        this.tab1.queryFilter = this.queryFilter;
        this.tab1.loadData();
        break;
      case 1:
        this.tab2.queryFilter = this.queryFilter;
        this.tab2.loadData();
        break;
      case 2:
        this.tab3.queryFilter = this.queryFilter;
        this.tab3.loadData();
        break;
      default:
      // code block
    }
  }
  exportData(){
    switch (this.selectedIndex) {
      case 0:
        var param={
          date:(new Date(this.tab1.date)).getFullYear(),
          department_ids:this.queryFilter.department_id,
          product_ids:this.queryFilter.product_id,
          keyword:this.queryFilter.keyword
        }
        this.reportKPIService.exportKPIYear(param);
        break;
      case 1:
        var param1={
          date:this.tab2.date? (new Date(this.tab2.date)).toISOString():'',
          department_ids:this.queryFilter.department_id,
          product_ids:this.queryFilter.product_id,
          keyword:this.queryFilter.keyword
        }
        this.reportKPIService.exportKPIMonth(param1);
        break;
      case 2:
        const param3={
          from_date: this.tab3.start_date ? this.tab3.start_date : '',
          to_date: this.tab3.end_date ? this.tab3.end_date : '',
          department_ids:this.queryFilter.department_id,
          product_ids:this.queryFilter.product_id,
          keyword:this.queryFilter.keyword
        }
        this.reportKPIService.exportKPI(param3);
        break;
      default:
      // code block
    }
  }
}
