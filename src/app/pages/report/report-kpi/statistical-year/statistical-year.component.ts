import { Component, Input, OnInit } from '@angular/core';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { ReportKpiService } from '@shared/service/report-kpi.service';
import CommonUtil from '@shared/utils/common-utils';

@Component({
  selector: 'app-statistical-year',
  templateUrl: './statistical-year.component.html',
  styleUrls: ['./statistical-year.component.scss']
})
export class StatisticalYearComponent implements OnInit {
  @Input() queryFilter:any;
  date :any;
  data:any=[];
  total:any=0;
  common: any= CommonUtil;
  styleTooltip = {
    'min-width': 'max-content',
  };
  constructor(
    private reportKPIService: ReportKpiService
  ) { 
  }
  reportQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    parentId: '',
    orderBy: '',
    sortBy: '',
    keyword: '',
    department_id: '',
    product_id: ''
  };
  
  ngOnInit(): void {
    this.setDefaultDate();
    this.loadData();
  }
  setDefaultDate(){
    const date =new Date();
    this.date=new Date().setFullYear(date.getFullYear(), 0, 1);
  }
  loadData(){
    const params = {
      pageIndex: this.reportQuery.pageIndex,
      pageSize: this.reportQuery.pageSize,
      keyword: this.queryFilter?.keyword,
      department_ids: this.queryFilter?.department_id,
      product_ids: this.queryFilter?.product_id,
      date: this.date? this.getYear():'',
    }
    this.reportKPIService.getReportKpiYear(params, true).subscribe(
      (res:any) => {
        this.data = res.body?.data
        this.total = res.body?.meta?.pagination?.total
      }
    );
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.reportQuery.pageIndex = pageIndex;
    this.reportQuery.pageSize = pageSize;
    this.loadData();
  }
  onChangeDate(){
    this.loadData()
  }
  getYear(){
    return new Date(this.date).getFullYear();
  }
}
