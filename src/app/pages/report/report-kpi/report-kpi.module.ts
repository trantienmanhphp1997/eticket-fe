import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { ReportKpiComponent } from './report-kpi.component';
import {ReportTicketsRoutingModule} from './report-kpi-routing.module';
import { StatisticalYearComponent } from './statistical-year/statistical-year.component';
import { StatisticalMonthComponent } from './statistical-month/statistical-month.component';
import { StatisticalKpiComponent } from './statistical-kpi/statistical-kpi.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';


@NgModule({
  declarations: [ReportKpiComponent,StatisticalYearComponent, StatisticalMonthComponent,StatisticalKpiComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportTicketsRoutingModule,
    NgxDaterangepickerMd.forRoot()
  ],
  exports: [ReportKpiComponent]
})
export class ReportKpiModule { }
