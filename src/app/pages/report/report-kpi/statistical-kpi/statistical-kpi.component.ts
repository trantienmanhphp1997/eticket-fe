import { Component, Input, OnInit } from '@angular/core';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { ReportKpiService } from '@shared/service/report-kpi.service';
import CommonUtil from '@shared/utils/common-utils';
import * as moment from 'moment';

@Component({
  selector: 'app-statistical-kpi',
  templateUrl: './statistical-kpi.component.html',
  styleUrls: ['./statistical-kpi.component.scss']
})
export class StatisticalKpiComponent implements OnInit {
  @Input() queryFilter:any;
  selected: any = { startDate: null, endDate: null };
  visible: boolean = false;
  date:any=[];
  total:any=0;
  common: any= CommonUtil;
  typeInput: boolean = false;
  reportQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    parentId: '',
    orderBy: '',
    sortBy: '',
    keyword: '',
    department_id: '',
    product_id: '',
  };
  data: any =[];
  start_date: any;
  end_date: any;
  styleTooltip = {
    'min-width': 'max-content',
  };
  calendarLocale = {
    format: 'DD/MM/YYYY',
    daysOfWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    monthNames: ['Th1', 'Th2', 'Th3', 'Th4', 'Th5', 'Th6', 'Th7', 'Th7', 'Th9', 'Th10', 'Th11', 'Th12'],
    firstDay: 1
  };
  ranges = {
    'Hôm nay': [moment(), moment()],
    'Tuần này': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
    'Năm này':  [moment().startOf('year'), moment().endOf('year')],
    '1 tuần trước': [moment().subtract(6, 'days'), moment()],
    '1 tháng trước': [moment().subtract(29, 'days'), moment()],
    '1 năm trước': [moment().subtract(1, 'year'), moment()],
  };
  constructor(private reportKPI: ReportKpiService) { }

  ngOnInit(): void {
    this.setDefaultDate();
    this.loadData();
  }
  setDefaultDate(){
    const date =new Date();
    this.selected.startDate =new Date(date.getFullYear(), date.getMonth()-1, date.getDate());
    this.selected.endDate = new Date();
  }
  openFillter() {
    this.visible = true;
  }
  onSearchDate($event: any) {
    this.loadData();
  }
  ngModelChange(event:any): void {
    if(event.type=='change'){
      this.typeInput = true;
      const date = event.target.value ;
      const vitri: number = date.search("-");
      const SD =  date.slice(0,Number(vitri-1));
      const pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
      this.selected.startDate = new Date(SD.replace(pattern,'$3-$2-$1'));
      const ED =  date.slice(Number(vitri+2),date.length);
      this.selected.endDate = new Date(ED.replace(pattern,'$3-$2-$1'));
    }
    this.loadData();
  }
  clearDate(event: any){
    event.stopPropagation();
    this.start_date= '';
    this.end_date =''
    this.loadData();
  }
  loadData() {
    const params = {
      pageIndex: this.reportQuery.pageIndex,
      pageSize: this.reportQuery.pageSize,
      keyword: this.queryFilter?.keyword,
      department_ids: this.queryFilter?.department_id,
      product_ids: this.queryFilter?.product_id,
      from_date: this.selected.startDate.toISOString(),
      to_date:   this.selected.endDate.toISOString(),
    }
    this.reportKPI.getReportKpi(params, true).subscribe(
      (res) => {
        this.data = res.body?.data
        this.total = res.body?.meta?.pagination?.total
      }
    );
    this.start_date = this.selected.startDate.toISOString()
    this.end_date = this.selected.endDate.toISOString()
    this.date = [
      this.start_date,
      this.end_date
    ]
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.reportQuery.pageIndex = pageIndex;
    this.reportQuery.pageSize = pageSize;
    this.loadData();
  }
  onChangeDate(){
    this.loadData()
  }
}
