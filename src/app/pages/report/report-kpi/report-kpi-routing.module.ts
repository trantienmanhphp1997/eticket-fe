import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { ReportKpiComponent } from './report-kpi.component';

const routes: Routes = [
  {
    path: '',
    component: ReportKpiComponent,
    canActivate: [AuthGuard],
    data: {
        authorities: [AUTH.REPORT.KPI],
        title: 'Báo cáo thống kê KPI',
        breadcrum: [
          {
            level: 1,
            title: 'Báo cáo thống kê',
            path: 'javascript:void(0)'
          }
        ]
      }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportTicketsRoutingModule { }