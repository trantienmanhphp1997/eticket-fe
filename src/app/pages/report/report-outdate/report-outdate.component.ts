import { Component, OnInit } from '@angular/core';
import { ReportTicketService } from '@shared/service/report-ticket.service';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { FormBuilder } from '@angular/forms';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import CommonUtil from '@shared/utils/common-utils';
import { TicketService } from '@shared/service/ticket.service';
import { DepartmentService } from '@shared/service/department.service';
import { environment } from '@env/environment';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-report-outdate',
  templateUrl: './report-outdate.component.html',
  styleUrls: ['./report-outdate.component.scss']
})
export class ReportOutdateComponent implements OnInit {
  selected: any = { startDate: null, endDate: null };
  visible: boolean = false;
  checked = false;
  indeterminate = false;
  today =new Date();
  listReportTicket : any = [];
  setOfCheckedId = new Set<number>();
  listOfCurrentPageData: readonly any[] = [];
  productList: any = [];
  ticketPriorityList: any = [];
  ticketSourceList: any = [];
  ticketCategoryList: any = [];
  ticketStateList: any =[];
  customerList: any = [];
  userList: any = [];
  listAssignee : any = [];
  deptList: any = [];
  total: any = 0;
  commonUtil: any = CommonUtil;
  date: any = [];
  timeKpiRemain: any;
  typeInput: boolean = false;
  reportQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    departmentIds: '',
    orderBy: '',
    sortBy: '',
    keyword: '',
    product_id:[],
    department_id:'',
    created_by_department_ids: '',
    user_ids:[],
    category_ids:[],
    source_ids:[],
    state_ids: [],
    priority_ids: [],
  };
  fillterDate: boolean = false;
  searchForm = this.fb.group({
    keyword: [null]
  });
  isCallFirstRequest: boolean = true;
  fillter: any =[];
  isSearch: boolean = false;
  start_date: any;
  end_date: any;
  styleTooltip = {
    'min-width': 'max-content',
  };
  calendarLocale = {
    format: 'DD/MM/YYYY',
    daysOfWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    monthNames: ['Th1', 'Th2', 'Th3', 'Th4', 'Th5', 'Th6', 'Th7', 'Th7', 'Th9', 'Th10', 'Th11', 'Th12'],
    firstDay: 1
  };
  ranges = {
    'Hôm nay': [moment(), moment()],
    'Tuần này': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
    'Năm này':  [moment().startOf('year'), moment().endOf('year')],
    '1 tuần trước': [moment().subtract(6, 'days'), moment()],
    '1 tháng trước': [moment().subtract(31, 'days'), moment()],
    '1 năm trước': [moment().subtract(1, 'year'), moment()],
  };
  constructor(
    private reportTicketService: ReportTicketService,
    private fb: FormBuilder,
    private ticketService: TicketService,
    private departmentService: DepartmentService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.setDefaultDate();
    this.loadData();
  }
  reloadFillter(){
    this.visible = false;
    this.reportQuery = {
      pageIndex: PAGINATION.PAGE_DEFAULT,
      pageSize: PAGINATION.SIZE_DEFAULT,
      pageSizeOptions: PAGINATION.OPTIONS,
      departmentIds: '',
      orderBy: '',
      sortBy: '',
      keyword: '',
      product_id:[],
      department_id:'',
      created_by_department_ids:'',
      user_ids:[],
      category_ids:[],
      source_ids:[],
      state_ids: [],
      priority_ids: [],
    };
    this.loadData();
  }
  loadData(){
    this.fillter = {
      from_date: this.selected.startDate.toISOString(),
      to_date: this.selected.endDate.toISOString(),
    }
    const params = {
      pageIndex: this.reportQuery.pageIndex,
      pageSize: this.reportQuery.pageSize,
      keyword: this.reportQuery.keyword,
      orderBy: this.reportQuery.orderBy,
      sortBy: this.reportQuery.sortBy,
      product_id: [this.reportQuery?.product_id],
      department_id:this.reportQuery?.department_id,
      created_by_department_ids: this.reportQuery?.created_by_department_ids,
      user_ids:[this.reportQuery?.user_ids],
      category_ids:[this.reportQuery?.category_ids],
      source_ids:[this.reportQuery?.source_ids],
      state_ids:[this.reportQuery?.state_ids] ,
      priority_ids: [this.reportQuery?.priority_ids],
      }
      this.reportTicketService.getAllOutdate(params,this.fillter, true).subscribe(res =>{
          this.listReportTicket = res.body?.data;
          this.total = res.body?.meta?.pagination?.total;
      })
  }
  clearDate(event: any){
    event.stopPropagation();
    this.start_date= '';
    this.end_date =''
    this.loadData();
  }
  onSearchDate($event: any) {
    this.loadData();
  }
  onSearch($event: any){
    if($event){
      this.reportQuery.keyword = this.searchForm.value.keyword.trim();
      this.searchForm.patchValue({
        keyword:  this.reportQuery.keyword
      })
      if(this.isSearch){
        this.reportQuery.pageIndex= PAGINATION.PAGE_DEFAULT;
        this.reportQuery.pageSize= PAGINATION.SIZE_DEFAULT;
        this.reportQuery.pageSizeOptions= PAGINATION.OPTIONS;
        this.reportQuery.departmentIds= '';
        this.reportQuery.orderBy= '';
        this.reportQuery.sortBy='';
        this.reportQuery.product_id=[];
        this.reportQuery.department_id='';
        this.reportQuery.created_by_department_ids = '';
        this.reportQuery.user_ids=[];
        this.reportQuery.category_ids=[];
        this.reportQuery.source_ids=[];
        this.reportQuery.state_ids= [];
        this.reportQuery.priority_ids=[];;
    }
       this.loadData();
    }
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.reportQuery.pageIndex = pageIndex;
    this.reportQuery.pageSize = pageSize;
    this.loadData();
  }
  onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    this.reportQuery.sortBy = params.sort.filter((e: any) => e.value !== null).map((e: any) => e.value?.slice(0, -3)).toString();
    this.reportQuery.orderBy = params.sort.filter((e: any) => e.key !== null && e.key !== undefined && e.value !== null).map((e: any) => e.key).toString();
    this.loadData();
  }
  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }
  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter((item: any) => !item.disabled);
    this.checked = listOfEnabledData.every((item: any) => this.setOfCheckedId.has(item.id));
    this.indeterminate = listOfEnabledData.some((item: any) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }
  setDefaultDate(){
    const date =new Date();
    this.selected.startDate = new Date(date.getFullYear(), date.getMonth()-1, date.getDate());
    this.selected.endDate = new Date();
  }
  onChangeDate(): void {
    this.fillterDate = true
    this.loadData();
  }
  ngModelChange(event:any): void {
    if(event.type=='change'){
      this.typeInput = true;
      const date = event.target.value ;
      const vitri: number = date.search("-");
      const SD =  date.slice(0,Number(vitri-1));
      const pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
      this.selected.startDate = new Date(SD.replace(pattern,'$3-$2-$1'));
      const ED =  date.slice(Number(vitri+2),date.length);
      this.selected.endDate = new Date(ED.replace(pattern,'$3-$2-$1'));
    }
    this.loadData();
  }
  openFillter() {
    this.visible = true;
    this.isSearch= true;
    this.user();
    this.department();
    this.ticketService.loadMultipleDataForTicketDetail().subscribe(res => {
      this.productList = res[0].body?.data;
      this.ticketPriorityList = res[1].body?.priority.data;
      this.ticketPriorityList.push({object: '"TicketPriority"',name:'Chưa xác định',id:'undefined_priority',status:1});
      this.ticketSourceList = res[1].body?.source.data;
      this.ticketCategoryList = res[1].body?.category.data;
      this.ticketStateList = res[1].body?.state.data;
    })
  }
  department(){
    const params = {
      pageIndex: '',
      pageSize: 0,
      data_context: 'kpi_report'
    }
    this.departmentService.getAllDepartments(params, false).subscribe((res) => {
      this.deptList = res.body;
    });
  }
  user(){
    this.ticketService.getAllAssignee({ data_context: 'filter_assignee' }, true).subscribe(res => {
      this.listAssignee = res?.body?.data;
    })
  }
  onFilterData() {
    this.searchForm.patchValue({
      keyword: null,
    })
    this.reportQuery.keyword= '';
    this.loadData()
    this.visible = false;
  }
  close(): void {
    this.visible = false;
  }
  exportTemplate() {
      this.fillter = {
      from_date: this.selected.startDate.toISOString(),
      to_date:   this.selected.endDate.toISOString(),
      keyword: this.reportQuery.keyword,
      product_id: [this.reportQuery?.product_id],
      department_id:this.reportQuery?.department_id,
      created_by_department_ids: this.reportQuery?.created_by_department_ids,
      user_ids:[this.reportQuery?.user_ids],
      category_ids:[this.reportQuery?.category_ids],
      source_ids:[this.reportQuery?.source_ids],
      state_ids:[this.reportQuery?.state_ids] ,
      priority_ids: [this.reportQuery?.priority_ids],
      }
    this.reportTicketService.exportTemplate2(this.fillter);
  }
  openDetail(id : any){
    let url_detail = "/eticket/admin/ticket-management/view-ticket/" + id;
    const url = this.router.serializeUrl(
        this.router.createUrlTree([`${url_detail}`])
    );
    window.open(url);
  }
}
