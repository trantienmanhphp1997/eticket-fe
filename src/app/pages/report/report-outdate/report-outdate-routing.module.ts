import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { ReportOutdateComponent } from './report-outdate.component';

const routes: Routes = [
  {
    path: '',
    component:ReportOutdateComponent ,
    canActivate: [AuthGuard],
    data: {
        authorities: [AUTH.REPORT.OUTDATE_TICKET],
        title: 'Báo cáo chi tiết yêu cầu quá hạn',
        breadcrum: [
          {
            level: 1,
            title: 'Báo cáo thống kê',
            path: 'javascript:void(0)'
          }
        ]
      }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportTicketsRoutingModule { }