import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { ReportOutdateComponent } from './report-outdate.component';
import { ReportTicketsRoutingModule } from './report-outdate-routing.module';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';

@NgModule({
  declarations: [ReportOutdateComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportTicketsRoutingModule,
    NgxDaterangepickerMd.forRoot()
  ],
  exports: [ReportOutdateComponent]
})
export class ReportOutdateModule { }