import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ContactCreateComponent } from './contact-create/contact-create.component';
import { TicketCreateComponent } from './ticket-create/ticket-create.component';
import { TicketDetailComponent } from './ticket-detail/ticket-detail.component';
import { TicketRoutingModule } from './ticket-routing.module';
import { TicketComponent } from './ticket.component';
import { TaskCreateComponent } from './task-create/task-create.component';
import { DetailHistoryComponent } from './ticket-detail/detail-history/detail-history.component';
import { LevelChangeComponent } from './ticket-detail/level-change/level-change.component';
import { TicketFlowComponent } from './ticket-detail/ticket-flow/ticket-flow.component';
import { MailHistoryComponent } from './ticket-detail/mail-history/mail-history.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [TicketComponent, TicketCreateComponent, TicketDetailComponent, ContactCreateComponent, TaskCreateComponent, DetailHistoryComponent, LevelChangeComponent, TicketFlowComponent, MailHistoryComponent],
  imports: [
    CommonModule,
    SharedModule,
    TicketRoutingModule,
    NgxDaterangepickerMd.forRoot(),
    NgSelectModule
  ],
  exports: [TicketComponent]
})
export class TicketModule { }
