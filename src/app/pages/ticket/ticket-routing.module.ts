import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { TicketCreateComponent } from './ticket-create/ticket-create.component';
import { TicketDetailComponent } from './ticket-detail/ticket-detail.component';
import { TicketComponent } from './ticket.component';

const routes: Routes = [
  {
    path: '',
    component: TicketComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.TICKET.LIST],
      title: 'model.ticket.title',
      breadcrum: []   
    }
  },
  {
    path: ROUTER_UTILS.ticket.create,
    component: TicketCreateComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.TICKET.CREATE],
      title: 'model.ticket.create',
      breadcrum: [
        {
          level: 1,
          title: 'model.ticket.title',
          path: `admin/${ROUTER_UTILS.ticket.root}`
        }
      ]
    }
  },
  {
    path: `${ROUTER_UTILS.ticket.view}/:id`,
    component: TicketDetailComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.TICKET.VIEW],
      title: 'model.ticket.detailT', 
      breadcrum: [
        {
          level: 1,
          title: 'model.ticket.title',
          path: `admin/${ROUTER_UTILS.ticket.root}`
        }
      ]
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketRoutingModule { }
