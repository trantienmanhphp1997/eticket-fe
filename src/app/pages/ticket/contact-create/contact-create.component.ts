
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ACTION } from '@shared/constants/common.constant';
import { STATUS } from '@shared/constants/status.constants';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { VALIDATORS } from '@shared/constants/validators.constant';
import { ContactService } from '@shared/service/contact.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import CommonUtil from '@shared/utils/common-utils';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';
@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.scss']
})
export class ContactCreateComponent implements OnInit {
  @Input() isUpdate: boolean = false;
  @Input() customerId: any = [];
  @Input() action: any = ACTION.CREATE
  @Output() emitService = new EventEmitter();
  nodes = [];
  lengthValidator = LENGTH_VALIDATOR;
  VALIDATORS : any = VALIDATORS;
  contact: any = [];
  delegate: any;
  listProduct: any;
  newCustomer: any;
  canAddContact = false;
  canEdit = false;
  viewEvent = ACTION.VIEW.event;
  canEditDelegate = false;
  contactEditId = null;
  delegateId = null;
  serviceproduct: any[] = [];
  actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
  //form
  contactForm: FormGroup = new FormGroup({});
 
  constructor(
    private fb: FormBuilder,
    private contactService: ContactService,
    private toastService: ToastService,
    private modalRef: NzModalRef
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  //loadData
  initForm(): void {
    this.contactForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.VALUE_MAX_LENGTH.MAX),CustomInputValidator.isName]],
      email: ['', [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.EMAIL_MAX_LENGTH.MAX), Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$'), CustomInputValidator.isEmail]],
      phone: ['',{
        validators: [Validators.required,Validators.maxLength(LENGTH_VALIDATOR.PHONE_MAX_LENGTH.MAX), CustomInputValidator.isNumberPhone], 
        asyncValidators: '', updateOn: 'blur'
      } ],
    });
  }

  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.name, key: aData.id, isLeaf: false });
    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children?.push(hashTable[aData.id]);
      }
    });
    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = true
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };

  //Event
  onSubmit() {
    if (this.contactForm.invalid) {
      CommonUtil.markFormGroupTouched(this.contactForm);
      return;
    }

    const contact: any = {
      ...this.contactForm.value,
      customer_id: this.customerId
    };
    const body = CommonUtil.trim(contact);

    this.contactService.create(body, true).subscribe((res) => {
      if (res.body?.data) {
        // this.toast.success(`model.department.success.${this.action.event}`);
        this.modalRef.close({
          success: true,
          value: res.body.data,
        });
      }
    }, (e: any) => {
      this.toastService.error(e);
    })
  }

  onCancel() {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
}
