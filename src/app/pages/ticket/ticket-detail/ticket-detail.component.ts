
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { ClassicEditor } from '@shared/lib/ckeditor5/build/ckeditor';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TicketService } from '@shared/service/ticket.service';
import CommonUtil from '@shared/utils/common-utils';
import { AUTH } from '@shared/constants/auth.constant';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { LocalStorageService } from 'ngx-webstorage';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '@shared/service/auth/auth.service';
import { TaskCreateComponent } from '../task-create/task-create.component';
import { DetailHistoryComponent } from './detail-history/detail-history.component';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { LevelChangeComponent } from './level-change/level-change.component';
import { TicketFlowComponent } from './ticket-flow/ticket-flow.component';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { environment } from '@env/environment';
import { HttpClient } from '@angular/common/http';
import { DetailContactComponent } from './detail-contact/detail-contact.component';
import { ModalCustomComponent } from '@shared/components/modal-custom/modal-custom.component';
import { MailHistoryComponent } from './mail-history/mail-history.component';
import { Subject } from 'rxjs';
import { UploadComponent } from '@shared/components/upload/upload.component';
import { UploadCommentComponent } from '@shared/components/upload-comment/upload-comment.component';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { FILE_SIZE, LIMIT_FILE } from '@shared/constants/file.constant';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss']
})
export class TicketDetailComponent implements OnInit {
  public Editor = ClassicEditor;
  @Input() selectedticket: any = []
  @Input() isUpdate: boolean = false;
  @ViewChild('upload') uploadFileComment!: UploadComponent;
  @ViewChild('uploadComment') uploadFileComment1!: UploadComponent
  @ViewChild('uploadTicket') uploadFileTicket!: UploadCommentComponent;
  @ViewChild('uploadReply') uploadReply!: UploadCommentComponent;

  searchChangeCus$ = "";
  searchChangeUser$ = "";
  commonUlti: any = CommonUtil;
  urlgetway = environment.fileDomain;
  url_domain = environment.fe_domain
  timeKpi: any;
  idFile: any = [];
  idFileComment: any = [];
  idFileComment1: any = [];
  idFileComment2: any = [];
  idFileMail: any = [];
  timeKpiRemain: any;
  ticketId: any
  infoUser: any;
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  ticketPriorityList: any = [];
  ticketCategoryList: any = [];
  ticketSourceList: any = [];
  productList: any = [];
  ticketList: any = [];
  isUpdating = false;
  commonUtil: any = CommonUtil;
  auth: any = AUTH;
  checkComment: boolean = true;
  checkReplyComment: boolean = true;
  ticket: any;
  title: any = ''
  form: FormGroup = new FormGroup({});
  formComment: FormGroup = new FormGroup({});
  replyComment: FormGroup = new FormGroup({});
  formCommentTicket: FormGroup = new FormGroup({});
  replyCommentTicket: FormGroup = new FormGroup({});
  formMail: FormGroup = new FormGroup({});
  checked = false;
  indeterminate = false;
  userGroup: any;
  userList: any;
  tagUserComment:any=[];
  tagUserReplyComment:any=[];
  tagUserCommentTicket:any=[];
  tagUserReplyCommentTicket:any=[];
  timeSlaRemain: any;
  contactList: any = [];
  ticketStateList: any = [];
  listCustomer: any = [];
  total: any = 0;
  ticketTitleMaxLenght = LENGTH_VALIDATOR.TICKET_TITLE_LENGTH.MAX
  ticketDescreiptionMaxLenght = LENGTH_VALIDATOR.TICKET_DESCRIPTION_LENGTH.MAX
  profile: any = null;
  isShowTransaction = false;
  listLevel: any = [];
  listTask: any = [];
  listComment: any = [];
  listCommentTicket: any = [];
  isShoweditor: any = [];
  showEditcomment: any = [];
  showEditReplycomment: any = [];
  showEditTicketcomment: any = [];
  showEditTicketReplycomment: any = [];
  listLog: any = [];
  childId: any = [];
  currentUser: any = [];
  isShowDescription: boolean = false;
  isShowDescriptionComment : boolean = false ;
  isShowDescriptionTicket : boolean = false ;
  description: any = [];
  pageIndex = PAGINATION.PAGE_DEFAULT;
  pageSize = PAGINATION.SIZE_DEFAULT;
  titleWorkflow: any = [];
  dataFile: any = [];
  timeSla: any;
  colorStatus: any = [];
  nzSelectedIndex: number = 0;
  colorPriority: any;
  isShowDescriptions: boolean = false;
  checkErrorUpload: any = false;
  checkErrorUploadComment: any = false;
  historyEmail: any = [];
  mailList: any = [];
  mail_to: any = [];
  mail_cc: any =[];
  replies: any = [];
  permissions: any = [];
  imgFile: any;
  currentUserId: any;
  listCustomerSelect: any = []
  isLoadingUser = false;
  isLoadingCus = false;
  isShowcommmetTK: boolean = true;
  isShowcommmetTKT: boolean = true;
  descriptionMail = '';
  isShowButtonMail: boolean = false;
  disabledMail: boolean = true;
  setTimeSLAInterval: any;
  setTimeKPIInterval: any;
  involvedUserList:any = []
  isLoading = false;
  ccMail: any = [];
  content: any = [];
  contentTicket: any = [];
  active: any ;
  validatorContent = [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.IDS_MAX_LENGTH.MAX)]
  customerQuery = {
    pageSize: PAGINATION.SIZE_DEFAULT,
    total: 0
  }
  userQuery = {
    pageSize: PAGINATION.SIZE_DEFAULT,
    total: 0
  }
  user_id: any;
  isLoadDataSelect: boolean = false;
  isLoadingComment = false;
  isLoadingReplyComment = false;
  isLoadingCommentTicket = false;
  isLoadingReplyCommentTicket = false;
  isDisable: boolean = false;
  isDisableReply: boolean = false;
  isDisableTicket: boolean = false;
  isDisableTicketReply: boolean = false;
  isDisableMailReply: boolean = false;
  styleTooltip = {
    'min-width': 'max-content',
  };
  checkErrorAmoutUpload: any = false;
  checkErrorFileUpload: any = false;
  isUploading: boolean = false;
  constructor(
    private fb: FormBuilder,
    private ticketService: TicketService,
    private router: ActivatedRoute,
    private route: Router,
    private modalService: NzModalService,
    private localStorage: LocalStorageService,
    private translateService: TranslateService,
    private authService: AuthService,
    private toast: ToastService,
    private router_reopen: Router,
    protected http: HttpClient,) {
    this.router.paramMap.subscribe((res: any) => {
      this.ticketId = res.get('id') || '';
    });
  }
  userQuestionUpdate = new Subject<string>();
  cusQuestionUpdate = new Subject<string>();
  reqQuestionUpdate = new Subject<string>();

  ngOnInit() {
    this.active = 1;
    this.userQuestionUpdate.pipe(debounceTime(800), distinctUntilChanged()).subscribe(value => {
        if (this.searchChangeUser$ != value) {
          this.onSearchData('user', value)
        }
      });
    this.cusQuestionUpdate.pipe(debounceTime(800),distinctUntilChanged()).subscribe(value => {
        if (this.searchChangeCus$ != value) {
          this.onSearchData('customer', value)
        }
      });
    this.reqQuestionUpdate.pipe( debounceTime(800), distinctUntilChanged()).subscribe(value => {
        this.onSearchData('require', value)
      });
    this.initForm();
    this.getcurrentUser();
    this.loadData();
    this.getListLog();
    this.formMail = this.fb.group({
      content: ['', [Validators.required]],
      cc_to: '',
      mail_to:'',
    });
    this.formComment = this.fb.group({
      contentComment: ['', this.validatorContent],
      tagged_users:[]

    });
    this.replyComment = this.fb.group({
      contentReply: ['', this.validatorContent],
      tagged_users:[]
    });
    this.formCommentTicket = this.fb.group({
      contentTicket: ['',this.validatorContent],
      tagged_users:[]

    });
    this.replyCommentTicket = this.fb.group({
      contentReplyTicket: ['', this.validatorContent],
      tagged_users:[]
    })
    this.getIntervalKPITime();
    this.getIntervalSLATime();
    this.loadDataInvoledUserTicket();

  }
  getIntervalSLATime() {
    this.setTimeSLAInterval = setInterval(() => {
      this.ticketList.resolution_remaining_time--;
      this.ticketList.timeSlaRemain = this.getTimeRemain(this.ticketList.resolution_remaining_time)
    }, 1000);
  }
  getIntervalKPITime() {
    this.setTimeKPIInterval = setInterval(() => {
      var seconds = this.ticketList.current_sla_internal_resolution_remaining_time--;
      this.ticketList.timeKpiRemain = this.getTimeRemain(seconds)
    }, 1000);
  }
  getTimeRemain(time: any) {
    var label = ''
    if (time == 0) {
      label = 'Hết hạn ';
      return label;
    } else if (time < 0) {
      label = 'Quá hạn ';
    }
    var seconds = Math.abs(Math.floor(time))
    return label + CommonUtil.convertTime(seconds)
  }
  getTime(time: any) {
    let convertSeconds = time * 60;
    return CommonUtil.convertTime(convertSeconds)
  }
  getTimeSecond(time: any) {
    return CommonUtil.convertTime(time)
  }
  static convertTime(seconds: any) {
    let day = Math.floor(seconds / (3600 * 24));
    let hours = Math.floor(seconds % (3600 * 24) / 3600);
    let dayTime = day + ' ' + 'ngày ';
    if (dayTime == 0 + ' ' + 'ngày ') {
      dayTime = ' ';
    } else {
      dayTime = day + ' ' + 'ngày ';
    }
    let hoursTime = hours + ' ' + 'giờ ';
    if (hoursTime == 0 + ' ' + 'giờ ') {
      hoursTime = ' ';
    } else {
      hoursTime = hours + ' ' + 'giờ ';
    }
    let minutesTime = Math.floor((seconds % 3600) / 60) + ' ' + 'phút ';
    if (minutesTime == 0 + ' ' + 'phút ') {
      minutesTime = ' ';
    }
    let secondTime = Math.floor(seconds % 60) + ' ' + 'giây';
    return `${dayTime} ${hoursTime} ${minutesTime} ${secondTime}`;
  }

  loadData(idTicket?: any) {
    this.isShowDescription = false;
    if (this.ticketId) {
      this.ticketService.detail(idTicket ? idTicket : this.ticketId, true).subscribe((res: any) => {
        if (res) {
          this.ticketList = res?.body?.data;
          this.ticketList.timeKpi = this.getTime(this.ticketList?.current_sla_internal_resolution_time_require),
            this.ticketList.timeSla = this.getTime(this.ticketList?.resolution_time_require),
            this.ticketList.timeSla = this.getTime(this.ticketList?.resolution_time_require),
            this.ticketList.actual_finish_ticket_time = this.getTimeSecond(this.ticketList?.actual_finish_ticket_time),
            this.listComment = res?.body?.data?.internalComments?.data
          this.listCommentTicket = res?.body?.data?.commentsWithCustomer?.data
          if (this.ticketList.source_icon == 'mail') {
            this.nzSelectedIndex = 2;
            this.disabledMail = false;
            this.getHistoryEmail();
            this.loadCustomer();
          }
          else {
            this.nzSelectedIndex = 0;
            this.disabledMail = true
          }
          if (this.ticketList.transactions.data.length != 0) {
            this.isShowTransaction = true;
            this.listLevel = this.ticketList.transactions.data
            this.titleWorkflow = this.ticketList.workflow.data.name
          }
          else {
            this.isShowTransaction = false;
          }
          if (this.permissions || this.ticketList.execute_ticket_able == 1) {
            if (!this.isLoadDataSelect) {
              this.loadDataForCreateTicket();
            }
          }

          this.initForm();
          this.dataFile = this.ticketList.files.data.map((dept: any) => {
            return {
              ...dept, size2: this.converMB(dept.size),
            }
          });
        }
      })
    }
  }
  size: any;
  converMB(dept: any) {
    return this.size = Math.round((Number(dept) * 0.00000095367431640625) * 100) / 100
  }
  switchTab(type: any){
    this.active = type
  }
  assignToMe() {
    const params = { user_id: this.currentUserId }
    this.ticketService.updateTickket(this.ticketList.id, params, true).subscribe(res => {
      if (res) {
        this.loadData();
        this.getListLog();
        this.toast.success(`Gán thành công`);
      }
    })
  }
  replyMail() {
    let trim = this.formMail.value.content.trim();
    if (this.checkErrorUploadComment || this.checkErrorAmoutUpload || this.isUploading || this.checkErrorFileUpload) {
      return;
    }
    else {
      if (this.idFileMail.length > 0) {
      }
      else {
        if (this.formMail.invalid) {
          CommonUtil.markFormGroupTouched(this.formMail);
          return;
        }
        if (trim == '') {
          this.toast.error(`Không được nhập toàn khoảng trắng`);
          return
        }
      }
      let  mail_to : any = '';
      let  mail_cc : any = '';
     if (  this.mail_to.length){
        if(this.formMail.value.mail_to){
           mail_to = this.formMail.value.mail_to +','+ this.mail_to.toString()
        }
        else{
           mail_to = this.mail_to.toString()
        }
     }
     else if (this.formMail.value.mail_to ){
        mail_to = this.formMail.value.mail_to
     }
     else{
       this.toast.error("Bạn cần nhập mail đến");
     }

     if (this.mail_cc.length){
      if(this.formMail.value.cc_to){
         mail_cc = this.formMail.value.cc_to+','+ this.mail_cc.toString()
      }
      else{
         mail_cc = this.mail_cc.toString()
      }
    }
      else if (this.formMail.value.cc_to ){
          mail_cc = this.formMail.value.cc_to
      }

        const mail: any = {
        ...this.formMail.value,
        mail_to: mail_to,
        cc_to:mail_cc,
        fileIds: this.idFileMail,
        ticket_id: this.ticketList.id
      };
      const body = CommonUtil.trim(mail);
      this.uploadFileTicket.deleAllFile();
     if (this.mail_to.length || this.formMail.value.mail_to){
      this.ticketService.sendMail(body, true).subscribe(res => {
        if (res) {
          this.isShowButtonMail = false
          this.loadData();
          this.formMail.reset();
          this.toast.success(`Gửi mail thành công`);
        }
      })
     }
    }
  }
  getcurrentUser() {
    const profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
    if (profile) {
      this.currentUser = profile?.name;
      this.currentUserId = profile?.id;
      this.infoUser = profile;
      this.permissions = this.infoUser.permissions.some((item: any, i: number) => {
        return item.name == 'help-desk'
      });
    };
  }
  showDiscription() {
    if (this.ticketList?.execute_ticket_able || this.permissions) {
      this.isShowDescription = true;
    }
  }
  getHistoryEmail() {
    this.ticketService.historyEmail(this.ticketId, true).subscribe(res => {
      this.historyEmail = res?.body?.data;
      this.mailList = res?.body?.meta;
      this.mail_to = this.mailList?.mail_to;
      this.mail_cc = this.mailList?.mail_cc;
    })
  }
  getTask() {
    this.ticketService.task(this.ticketId, true).subscribe(res => {
      this.listTask = res.body?.data;
      this.total = this.listTask.length;
    })
  }
  getComment() {
    this.ticketService.comment(this.ticketId, true).subscribe(res => {
      this.listComment = res.body?.data;
    })
  }
  getCommentTicket() {
    this.ticketService.commentTicket(this.ticketId, true).subscribe(res => {
      this.listCommentTicket = res.body?.data;

    })
  }
  getListLog() {
    const params = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
    }
    this.ticketService.log(this.ticketId, params, true).subscribe(res => {
      this.listLog = res.body?.data;
    })
  }
  openTask() {
    const base = CommonUtil.modalBase(TaskCreateComponent, '30%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) { }
    });
  }
  replySubmit(id: any) {
    debugger
    this.isLoadDataSelect = true;
    this.isLoadingReplyComment = true;
    let trim = this.replyComment.value.contentReply.trim();
    if (this.checkErrorUploadComment || this.checkErrorAmoutUpload || this.isUploading) {
        this.isLoadingReplyComment = false;
        return;
    } else {
        if (this.idFileComment1.length > 0) {
        }
        else {
          if (this.replyComment.invalid) {
            CommonUtil.markFormGroupTouched(this.replyComment);
            this.toast.error(`Ghi chú nội bộ không được để trống`);
            this.isLoadingReplyComment = false;
            return;
          }
          if (trim == '') {
            this.toast.error(`Không được nhập toàn khoảng trắng`);
            this.isLoadingReplyComment = false;
            return
          }
        }
        const commmentReply: any = {
          model_id: this.ticketId,
          reply_id: id,
          content: this.replyComment.value.contentReply,
          fileIds: this.idFileComment1,
          tagged_users:this.replyComment.value.tagged_users
        };
        const body = CommonUtil.trim(commmentReply);
        let serviceEvent = this.ticketService.createComment(body, false)
        serviceEvent.subscribe(res => {
          if (res) {
            this.isShoweditor[id] = false;
            this.loadData();
            this.toast.success(`Thành công`);
            this.isLoadingReplyComment = false;
            this.replyComment.reset();
          }
          else {
            this.toast.error(`Lỗi không bình luận được `);
          }
        })
    }
  }
  replySubmitTicket(id: any) {
    debugger
    this.isLoadDataSelect = true;
    this.isLoadingReplyCommentTicket = true;
    let trim = this.replyCommentTicket.value.contentReplyTicket.trim();
    if (this.checkErrorUploadComment || this.checkErrorAmoutUpload || this.isUploading) {
        this.isLoadingReplyCommentTicket = false;
        return;
    }

    else {
      if (this.idFileComment2.length > 0) {
      }
      else {
        if (this.replyCommentTicket.invalid) {
          CommonUtil.markFormGroupTouched(this.replyCommentTicket);
          this.toast.error(`Ghi chú nội bộ không được để trống`);
          this.isLoadingReplyCommentTicket = false;
          return;
        }
        if (trim == '') {
          this.toast.error(`Không được nhập toàn khoảng trắng`);
          this.isLoadingReplyCommentTicket = false;
          return
        }
      }
      const commmentReply: any = {
        model_id: this.ticketId,
        reply_id: id,
        content: this.replyCommentTicket.value.contentReplyTicket,
        fileIds: this.idFileComment2,
        scope: 'with_customer',
        tagged_users:this.replyCommentTicket.value.tagged_users
      };
      const body = CommonUtil.trim(commmentReply);

      this.ticketService.createComment(body, false).subscribe(res => {
        if (res) {
          this.isShoweditor[id] = false;
          this.loadData()
          this.replyCommentTicket.reset();
          this.toast.success(`Thành công`);
          // this.isLoadingReplyCommentTicket = false;
        }
        else {
          this.toast.error(`Lỗi không bình luận được `);
        }
      })
        this.uploadFileComment.deleAllFile();
    }
  }

  commentSubmit(id?: any) {
    this.isLoadDataSelect = true;
    this.isLoadingComment = true;
    let trim = this.formComment.value.contentComment.trim();
    if (this.checkErrorUploadComment || this.checkErrorAmoutUpload || this.isUploading) {
        this.isLoadingComment = false;
        return;
    }

    else {
      if (this.idFileComment.length > 0) {

      }
      else {
        if (this.formComment.invalid) {
          CommonUtil.markFormGroupTouched(this.formComment);
          this.toast.error(`Ghi chú nội bộ không được để trống`);
          this.isLoadingComment = false;
          return;
        }
        if (trim == '') {
          this.toast.error(`Không được nhập toàn khoảng trắng`);
          this.isLoadingComment = false;
          return
        }
      }
      const commment: any = {
        model_id: this.ticketId,
        ...this.formComment.value,
        content: this.formComment.value.contentComment,
        fileIds: this.idFileComment
      };
      const body = CommonUtil.trim(commment);
      this.isShowcommmetTK= false
      let serviceEvent = this.ticketService.createComment(body, false)
      this.uploadFileComment.deleAllFile();
      serviceEvent.subscribe(res => {
        if (res) {
          this.toast.success(`Thành công`);
          this.loadData();
          this.formComment.reset();
          
          this.isLoadingComment = false;
          this.isShowcommmetTK= true
        }
        else if (this.idFileComment.length == 0) {
          this.toast.error(`Lỗi không bình luận được `);
        }
        else {
          this.toast.error(`Lỗi không bình luận được `);
        }
      })
    }

  }
  commentSubmitTicket(id?: any) {
    this.isLoadDataSelect = true;
    this.isLoadingCommentTicket = true;
    let trim = this.formCommentTicket.value.contentTicket.trim();
    if (this.checkErrorUploadComment || this.checkErrorAmoutUpload || this.isUploading) {
        this.isLoadingCommentTicket = false;
        return;
    }

    else {
      if (this.idFileComment.length > 0) {
      }
      else {
        if (this.formCommentTicket.invalid) {
          CommonUtil.markFormGroupTouched(this.formCommentTicket);
          this.toast.error(`Trao đổi Ticket không được để trống`);
          this.isLoadingCommentTicket = false;
          return;
        }
        if (trim == '') {
          this.toast.error(`Không được nhập toàn khoảng trắng`);
          this.isLoadingCommentTicket = false;
          return
        }
      }
      const commment: any = {
        model_id: this.ticketId,
        ...this.formCommentTicket.value,
        content: this.formCommentTicket.value.contentTicket,
        fileIds: this.idFileComment,
        scope: 'with_customer'

      };
      const body = CommonUtil.trim(commment);
      this.isShowcommmetTKT = false;
      let serviceEvent = this.ticketService.createComment(body, false);
      this.uploadFileTicket.deleAllFile();
      serviceEvent.subscribe(res => {
        if (res) {
          this.toast.success(`Thành công`);
          this.loadData();
          this.isLoadingCommentTicket = false;
          this.isShowcommmetTKT = true;
          this.formCommentTicket.reset();
        }
        else if (this.idFileComment.length == 0) {
          this.toast.error(`Lỗi không bình luận được `);
        }
        else {
          this.toast.error(`Lỗi không bình luận được `);
        }
      })
    }
  }
  showEditor(id: any) {
    this.childId = id;
    this.isShoweditor[id] = true;

  }
  changeColor(event: any) {
    const id = event;
    const item = this.ticketPriorityList.find((e: { id: any; }) => e.id === id)
    if (item.color) {
      this.colorPriority = item.color
    }

  }
  onExcute(data: any) {
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    if (this.ticketList.priority_name) {
      const base = CommonUtil.modalBase(LevelChangeComponent, { data, ticket: this.ticketId }, '30%');
      const modal: NzModalRef = this.modalService.create(base);
      modal.afterClose.subscribe(result => {
        if (result && result?.success) {
          this.getListLog();
          this.getComment();
          this.loadData();
          location.reload();
          this.ticketService.detail(this.ticketId, true).subscribe((res: any) => {
            this.ticketList = res?.body?.data;
          })
        }
      });
    } else {
      this.toast.error('Chưa cập nhật độ ưu tiên');
      location.reload();
    }
  }
  loadDataForCreateTicket() {
    this.ticketService.loadMultipleDataForTicketDetail().subscribe(res => {
      this.productList = res[0].body?.data;
      this.ticketPriorityList = res[1].body?.priority.data;
      this.ticketSourceList = res[1].body?.source.data;
      this.ticketCategoryList = res[1].body?.category.data;
      this.ticketStateList = res[1].body?.state.data;
      if (this.ticketList.wf_current_execute_department_name) {
        this.loadUser();
      }
      // this.loadCustomer();
      this.form.patchValue({
        user_id: this.ticketList?.user_id,
        require_id: this.ticketList?.require_id
      })

    })
  }
  initForm() {
    this.profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
    this.form = this.fb.group({
      title: [{ value: this.ticketId ? this.ticketList?.title : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.execute_ticket_able == 0 || this.ticketList?.ticket_status_type == 3 ? true : false }, [Validators.required, Validators.maxLength(this.ticketTitleMaxLenght)]],
      product_id: [{ value: this.ticketId ? this.ticketList?.product_id : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.execute_ticket_able == 0 || this.ticketList?.ticket_status_type == 3 ? true : false }, [Validators.required]],
      state_id: [{ value: this.ticketId ? this.ticketList?.state_id : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.execute_ticket_able == 0 ? true : false }, [Validators.required]],
      priority_id: [{ value: this.ticketId ? this.ticketList?.priority_id : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.update_ticket_able == 0 ? true : false }, [Validators.required]],
      category_id: [{ value: this.ticketId ? this.ticketList?.category_id : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.execute_ticket_able == 0 || this.ticketList.ticket_status_type == 3 ? true : false }, [Validators.required]],
      customer_id: [{ value: this.ticketId ? this.ticketList?.customer_id : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.execute_ticket_able == 0 || this.ticketList.ticket_status_type == 3 ? true : false }],
      user_id: [{ value: this.ticketId ? this.ticketList?.user_id : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.execute_ticket_able == 0 || this.ticketList.ticket_status_type == 3 ? true : false }, this.permissions ? '' : [Validators.required]],
      require_id: [{ value: this.ticketId ? this.ticketList?.require_id : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.execute_ticket_able == 0 || this.ticketList.ticket_status_type == 3 ? true : false }, []],
      department_id: [],
      description: [{ value: this.ticketId ? this.ticketList?.description : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.execute_ticket_able == 0 || this.ticketList.ticket_status_type == 3 ? true : false },],
      source_id: [{ value: this.ticketId ? this.ticketList?.source_id : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.execute_ticket_able == 0 || this.ticketList.ticket_status_type == 3 ? true : false }, [Validators.required]],
      deputy: [{ value: this.ticketId ? this.ticketList?.deputy : null, disabled: this.permissions ? this.ticketList?.ticket_status_type == 3 || this.ticketList?.ticket_status_type == 4 : this.ticketList?.execute_ticket_able == 0 || this.ticketList.ticket_status_type == 3 ? true : false }]
    });
    let des = this.description;
    des = this.ticketList?.description;
    des = des?.replaceAll('\\', '')
    this.description = des;
  }
  processEmailContent(str: string) {
    var arr = (!str.includes('ÓM')) ? str : str.split('ÓM')[0]
    return arr;
  }
  onChangeData(type: string, content: any): void {
    this.form.get(type)?.setValue(content);
    if (CommonUtil.extractContent(this.form.value.description)?.length > 5000) {
      this.isShowDescriptions = true
    }
    else if (CommonUtil.extractContent(this.form.value.description)?.length <= 5000) {
      this.isShowDescriptions = false
    }
  }
  openHistory(data: any) {
    const base = CommonUtil.modalBase(DetailHistoryComponent, { data, currentUser: this.currentUser }, '980px');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => { });
  }

  openContact() {
    this.loadCustomer();
    const base = CommonUtil.modalBase(DetailContactComponent, { data: this.ticketList }, '500px');
    const modal: NzModalRef = this.modalService.create(base);

    modal.afterClose.subscribe(result => {
      if (result?.success) {
        this.loadData();
        this.getListLog();
        modal.close();
      }
      else {
        modal.close();
      }
    });
  }
  onSubmit(uploadfile?: boolean): void {
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    if (CommonUtil.extractContent(this.form.value.description)?.length > 5000) {
      this.isShowDescription = true
      return;

    }
    if (this.checkErrorUpload) {
      return;
    }
    const ticket: any = {
      ...this.form.value,
      fileIds: this.idFile
    };
    const body = CommonUtil.trim(ticket);
    this.isUpdating = true;
    let serviceEvent = this.ticketService.updateTickket(this.ticketId, body, false)
    serviceEvent.subscribe(res => {
      if (res) {
        location.reload();
        this.toast.success(`model.ticket.success.update`);
      }
    }, (e: any) => {
      this.isUpdating = false;
    },
      () => {
        this.isUpdating = false;
      });
  }

  showButton() {
    this.isShowButtonMail = true;
  }
  getDetail(data: any, i: number) {
    if (data?.excute_info?.process_type != 'todo') {
      this.ticketService.detail(this.ticketId, false).subscribe((res: any) => {
        if (res) {
          const excuting_level = res?.body?.data.excuting_level;
          excuting_level.forEach((item: any, index: number) => {
            if (i == index) {
              const base = CommonUtil.modalBase(TicketFlowComponent, { data: item, statusTicket: this.ticketList?.ticket_status_type }, '40%');
              const modal: NzModalRef = this.modalService.create(base);
              modal.afterClose.subscribe(result => {
                this.ticketService.detail(this.ticketId, false).subscribe(res => {
                })
              });
            }
          })
        }
      })
    }
  }
  openDetailMail(data: any, i: any) {
    const base = CommonUtil.modalBase(MailHistoryComponent, { data: data, index: i, content: this.description }, '1120px');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => { });

  }
  getStringLenght(value: any) {
    var str = value;
    if (!value) {
      str = 'Chưa có người nhận';
    }
    return str.length;
  }
  handleLoadFile(e: any) {
    this.idFile = e;
  }
  checkFileUploading() {
    this.isUploading = true;
  }
  handleLoadFileComment(e: any) {
    this.idFileComment = e
    if(!this.idFileComment.includes(undefined)) {
        this.isUploading = false;
    }
    if (this.idFileComment.length > 0) {
      this.checkComment = false;
    }
  }
  handleLoadFileComment1(e: any) {
    this.idFileComment1 = e
    if(!this.idFileComment1.includes(undefined)) {
        this.isUploading = false;
    }
    if (this.idFileComment1.length > 0) {
      this.checkComment = false;
    }
  }
  handleLoadFileComment2(e: any) {
    this.idFileComment2 = e
    if(!this.idFileComment2.includes(undefined)) {
        this.isUploading = false;
    }
    if (this.idFileComment2.length > 0) {
      this.checkComment = false;
    }
  }
  handleLoadFileMail(e: any) {
    this.idFileMail = e
    if(!this.idFileComment.includes(undefined)) {
        this.isUploading = false;
    }
    if (this.idFileMail.length > 0) {
      this.checkComment = false;
    }
  }
  addEventMail(e: any) {
    let errArr = e.filter((f: any) => f !== undefined);
    if (errArr.length > 0) {
      this.checkErrorUpload = true;
    } else {
      this.checkErrorUpload = false;
    }
  }
  addEvent(e: any) {
    let errArr = e.filter((f: any) => f !== undefined);
    if (errArr.length > 0) {
      this.checkErrorUpload = true;
    } else {
      this.checkErrorUpload = false;
    }
  }
  addEvent2(e: any){
    if(e){
        this.checkErrorAmoutUpload = true;
    }else{
        this.checkErrorAmoutUpload = false;
    }
  }
  checkFileSize(e: any) {
    if(e){
        this.checkErrorFileUpload = true;
      } else {
        this.checkErrorFileUpload = false;
    }
  }
  addEventComment(e: any, type?: number) {
    if (e.length > 0 && type == 0) {
      this.isDisable = true;
    }
    if (e.length > 0 && type == 1) {
      this.isDisableTicket = true
    }
    if (e.length > 0 && type == 2) {
      this.isDisableReply = true
    }
    if (e.length > 0 && type == 3) {
      this.isDisableTicketReply = true
    }
    if (e.length > 0 && type == 4) {
      this.isDisableMailReply = true
    }
    let errArr = e.filter((f: any) => f !== undefined);
    if (errArr.length > 0) {
      this.checkErrorUploadComment = true;
    } else {
      this.checkErrorUploadComment = false;
    }
  }

  changeComment(event: any) {
    if (event.length > 0) {
      this.checkComment = false;
    } else {
      this.checkComment = true;
    }
  }
  changeReplyComment(event: any) {
    if (event.length > 0) {
      this.checkReplyComment = false;
    } else {
      this.checkReplyComment = true;
    }
  }
  closeTickets() {
    const form = CommonUtil.modalBase(ModalCustomComponent, {
      title: 'XÁC NHẬN HỦY TICKET',
      content: `Bạn có chắc muốn hủy ticket <b>${this.commonUtil.limitWord(this.ticketList.title, 20)}</b> không?`,
      okText: 'close',
      requirer: `${this.ticketList.require_name}`,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '470px',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        const params = {
          ticket_status_type: 4,
          cancellation_reason: result.cancellation_reason
        }
        this.ticketService.updateTickket(this.ticketList.id, params, true).subscribe((res: any) => {
          this.toast.success(`Hủy ticket thành công`);
          this.loadData();
          this.getListLog();
        })
      } else {
        modal.close();
      }
    });
  }
  openTickets(event: any) {
    const form = CommonUtil.modalBase(ModalCustomComponent, {
      title: event == 0 ? 'Xác nhận mở Ticket' : 'Xác nhận chuyển cho Helpdesk',
      content: event == 0 ? `Bạn có chắc muốn mở ticket <b>${this.commonUtil.limitWord(this.ticketList.title, 20)}</b> không?` : `Bạn có chắc muốn chuyển ticket <b>${this.commonUtil.limitWord(this.ticketList.title, 20)}</b> cho Helpdesk không?`,
      okText: event == 0 ? 'open' : 'assign',
      requirer: `${this.infoUser.name}`,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '470px',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        if (event == 0) {
          const params = {
            reopen_reason: result.cancellation_reason
          }
          this.ticketService.reOpenTicket(this.ticketList.id, params, true).subscribe((res: any) => {
            this.toast.success(`Mở ticket thành công`);
            this.router_reopen.navigate([`admin/${ROUTER_UTILS.ticket.root}/${ROUTER_UTILS.ticket.view}`, res.body.data.id]);
            this.loadData(res.body.data.id);
          })
        }
        else if (event == 1) {
          const params = {
            message: result.cancellation_reason
          }
          this.ticketService.assignTicket(this.ticketList.id, params, true).subscribe((res: any) => {
            this.toast.success(`Chuyển thành công`);
            this.loadData();
          })
        }

      } else {
        modal.close();
      }
    });
  }
  onStatus() {
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    const form = CommonUtil.modalConfirm(
      this.translateService,
      'Xác nhận hoàn thành',
      'Bạn có chắc muốn hoàn thành Ticket không?',
      {},
      () => {
        return {
          success: true,
        };
      },
      () => {
        return {
          success: false,
        };
      },
      'Hoàn thành',
    );
    const modal: NzModalRef = this.modalService.confirm(form);
    modal.afterClose.subscribe(result => {
      if (result?.success) {
        const param = {
          priority_id: this.form.value.priority_id
        }
        if (this.ticketList.priority_name) {
          this.ticketService.status(this.ticketId, param, true).subscribe(res => {
            if (res) {
              modal.close();
              this.toast.success(`Đã xử lý ticket`);
              this.loadData();
              this.getListLog();
            };
          })
        }
        else {
          this.toast.error(`Chưa cập nhật độ ưu tiên`);
          this.loadData();
          this.getListLog();
        }
      } else {
        modal.close();
      }
    });
  }
  onSearchData(type: any, $event: any) {
    if (type == 'customer') {
      this.searchChangeCus$ = $event;
      this.customerQuery.pageSize = PAGINATION.SIZE_DEFAULT
      this.loadCustomer();
    } else if (type == 'user') {
      this.searchChangeUser$ = $event;
      this.userQuery.pageSize = PAGINATION.SIZE_DEFAULT
      this.loadUser();
    }
  }
  loadCustomer() {
    const param = {
      name: this.searchChangeCus$,
      limit: this.customerQuery.pageSize
    }
    this.ticketService.getAllCustomer(param, false).subscribe(res => {
      this.listCustomerSelect = res?.body?.data;
      this.customerQuery.total = res.body?.meta?.pagination?.total || 0
      if (this.ticketList?.customer_id && this.ticketList?.customer_name && !this.searchChangeCus$) {
        const exist = this.listCustomerSelect?.some((item: any) => item?.id == this.ticketList?.customer_id);
        if (!exist) {
          this.listCustomerSelect.push({ id: this.ticketList?.customer_id, name: this.ticketList?.customer_name })
          this.customerQuery.total++;
        }
        this.listCustomerSelect.sort((a: any, b: any) => a.name.localeCompare(b.name));
      }
      this.isLoadingCus = false;
    })
  }
  loadUser() {
    const param = {
      data_context: 'filter_assignee',
      name: this.searchChangeUser$,
      limit: this.userQuery.pageSize,
      ticket_id: this.ticketList?.id
    }
    this.ticketService.getAllAssignee(param, false).subscribe(res => {
      this.userList = res?.body?.data;
      this.userQuery.total = res.body?.meta?.pagination?.total || 0
      if (this.ticketList?.user_id && this.ticketList?.user_name && !this.searchChangeUser$) {
        const exist = this.userList?.some((item: any) => item?.id == this.ticketList?.user_id);
        if (!exist) {
          this.userList.push({ id: this.ticketList?.user_id, name: this.ticketList?.user_name })
          this.customerQuery.total++;
        }
        this.userList.sort((a: any, b: any) => a.name.localeCompare(b.name));
        this.isLoadingUser = false;
      }
    })
  }
  loadDataInvoledUserTicket(){
    const param = {
        data_context: 'filter_assignee',
        ticket_id: this.ticketId,
        relevant_users : 1,
    } 
    this.ticketService.getAllAssignee(param,true).subscribe(res=>{
        this.involvedUserList = res.body?.data;
        
    })
}
  loadMoreCus(): void {
    if (this.customerQuery.pageSize > this.customerQuery.total) {
      return
    }
    this.customerQuery.pageSize += 10;
    this.isLoadingCus = true;
    this.loadCustomer()
  }
  loadMoreUser(): void {
    if (this.userQuery.pageSize > this.userQuery.total) {
      return
    }
    this.userQuery.pageSize += 10;
    this.isLoadingUser = true;
    setTimeout(() => this.loadUser(), 1000)

  }
  dropdownBottom(type: any) {
    if (type == 'user') {
      if (this.userQuery.pageSize > this.userQuery.total) {
        this.isLoadingUser = false
      } else {
        this.isLoadingUser = true;
      }
    } if (type == 'customer') {
      if (this.customerQuery.pageSize > this.customerQuery.total) {
        this.isLoadingCus = false
      } else {
        this.isLoadingCus = true;
      }
    }
  }
  onOpenData(type: any) {
    if (type == 'user') {
      if (this.userQuery.total < 10) {
        this.isLoadingUser = false;
      } else {
        if (this.userList[this.userQuery.pageSize - 1]?.id == this.form.value.user_id || this.userList[this.userQuery.pageSize]?.id == this.form.value.user_id) {
          this.isLoadingUser = true;
        } else {
          this.isLoadingUser = false;
        }
      }
    } if (type == 'customer') {
      if (this.customerQuery.total < 10) {
        this.isLoadingCus = false;
      } else {
        if (this.listCustomerSelect[this.customerQuery.pageSize - 1]?.id == this.form.value.customer_id || this.listCustomerSelect[this.customerQuery.pageSize]?.id == this.form.value.customer_id) {
          this.isLoadingCus = true;
        } else {
          this.isLoadingCus = false;
        }
      }
    }
  }
  onChangeDataMail(type: string, content: any): void {
    this.formMail.get(type)?.setValue(content);
  }
  onChangeCommentTicket(type: string, content: any): void {
    this.formCommentTicket.get(type)?.setValue(content);
    if (CommonUtil.extractContent(this.formCommentTicket.value.contentTicket)?.length > 500) {
      this.isShowDescriptionTicket = true
    }
    else if (CommonUtil.extractContent(this.formCommentTicket.value.contentTicket)?.length <= 500) {
      this.isShowDescriptionTicket = false
    }
  }
  onChangeComment(type: string, content: any): void {
    this.formComment.get(type)?.setValue(content);
    if (CommonUtil.extractContent(this.formComment.value.contentComment)?.length > 500) {
      this.isShowDescriptionComment = true
    }
    else if (CommonUtil.extractContent(this.formComment.value.contentComment)?.length <= 500) {
      this.isShowDescriptionComment = false
    }
  }
  onChangeCommentEdit(type: string, content: any): void {
    this.content = content
  }
  onChangeCommentTicketEdit(type: string, content: any): void {
    this.contentTicket = content
  }
  onChangeCommenReply(type: string, content: any): void {
    this.replyComment.get(type)?.setValue(content);
  }
  onChangeCommenReplyTicket(type: string, content: any): void {
    this.replyCommentTicket.get(type)?.setValue(content);
    
  }
  onChangeCommenReplyEidt(type: string, content: any): void {
    this.content = content
  }
  editComment(id: any) {
    const editComment: any = {
      content: this.content,
      tagged_users:this.tagUserComment
    };
    const body = CommonUtil.trim(editComment);
    if (this.content.trim() != '') {
      let serviceEvent = this.ticketService.editComment(id, body, false)
      serviceEvent.subscribe(res => {
        if (res) {
          this.isShoweditor[id] = false;
          this.loadData();
          this.showEditcomment[id] = false;
          this.toast.success(`Thành công`);
        }
        else {
          this.toast.error(`Lỗi không bình luận được `);
        }
      })
    }
    else {
      this.toast.error(`Ghi chú nội bộ không không được bỏ trống  `);
    }

  }
  editTicketComment(id: any) {
    const editComment: any = {
      content: this.contentTicket,
      tagged_users:this.tagUserCommentTicket
    };
    const body = CommonUtil.trim(editComment);
    if (this.contentTicket.trim() != '') {
      let serviceEvent = this.ticketService.editComment(id, body, false)
      serviceEvent.subscribe(res => {
        if (res) {
          this.loadData();
          this.showEditTicketcomment[id] = false;
          this.toast.success(`Thành công`);
        }
        else {
          this.toast.error(`Lỗi không bình luận được `);
        }
      })
    }
    else {
      this.toast.error(`Ghi chú nội bộ không không được bỏ trống  `);
    }

  }
  editReplyComment(id: any, content: any) {

    const editComment: any = {
      content: this.content,
      tagged_users:this.tagUserReplyComment
    };
    const body = CommonUtil.trim(editComment);
    if (content.trim() != '') {
      let serviceEvent = this.ticketService.editComment(id, body, false)
      serviceEvent.subscribe(res => {
        if (res) {
          this.isShoweditor[id] = false;
          this.loadData();
          this.showEditReplycomment[id] = false;
          this.toast.success(`Thành công`);
        }
        else {
          this.toast.error(`Lỗi không bình luận được `);
        }
      })
    }
    else {
      this.toast.error(`Ghi chú nội bộ không không được bỏ trống  `);
    }

  }
  editTicketReplyComment(data: any) {
    debugger
    const editComment: any = {
      content: this.content,
      tagged_users:this.tagUserReplyCommentTicket
    };
    const body = CommonUtil.trim(editComment);
    if (data.content.trim() != '') {
      let serviceEvent = this.ticketService.editComment(data.id, body, false)
      serviceEvent.subscribe(res => {
        if (res) {
          this.isShoweditor[data.id] = false;
          this.loadData();
          this.showEditTicketReplycomment[data.id] = false;
          this.toast.success(`Thành công`);
        }
        else {
          this.toast.error(`Lỗi không bình luận được `);
        }
      })
    }
    else {
      this.toast.error(`Ghi chú nội bộ không không được bỏ trống  `);
    }

  }
  showEditReplyComment(data: any) {
    this.showEditReplycomment[data.id] = true;
    this.tagUserReplyComment = data.tagged_user_ids;
  }
  showEditticketReplyComment(data: any) {
    this.showEditTicketcomment[data.id] = true;
    this.contentTicket = data.content
    this.tagUserReplyCommentTicket = data.tagged_user_ids
  }
  showEditComment(data: any) {
    this.showEditcomment[data.id] = true;
    this.content = data.content
    this.tagUserComment = data.tagged_user_ids;

  }
  showEditTReplyComment(data: any) {
    this.showEditTicketReplycomment[data.id] = true;
    this.content = data.content;
    this.tagUserReplyCommentTicket = data.tagged_user_ids;
  }
  deleteReplyComment(id: any) {
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa bình luận không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '450px',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.ticketService.deleteComment(id, true).subscribe((res: any) => {
          if (res) {
            modal.close();
            this.loadData();
            this.toast.success(`Xóa thành công`);
          };
        })
      } else {
        modal.close();
      }
    });
  }
  onDeleteMail(mail: any, type: any) {
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa Mail không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '450px',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        if(type === 1){
          this.mail_to= this.mail_to.filter((item:any) => item !== mail)
        }
        else{
          this.mail_cc= this.mail_cc.filter((item:any) => item !== mail)
        }
      }  
      else {
        modal.close();
      }
    });
  }
  deleteComment(id: any) {
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa bình luận không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '450px',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.ticketService.deleteComment(id, true).subscribe((res: any) => {
          if (res) {
            modal.close();
            this.loadData();
            this.toast.success(`Xóa thành công`);
          };
        })
      } else {
        modal.close();
      }
    });
  }
  closeEditComment(data: any) {
    this.showEditcomment[data.id] = false;
  }
  closeEditComment1(data: any) {
    this.showEditTicketcomment[data.id] = false;
  }
  closeEditReplyComment(data: any) {
    this.showEditReplycomment[data.id] = false;
    this.loadData();
  }
  closeComment(id: any) {
    this.isShoweditor[id] = false;
    this.replyComment.patchValue({
      contentReply: ' '
    })
  }
  closeComment1(id: any) {
    this.isShoweditor[id] = false;
    this.replyCommentTicket.patchValue({
      contentReplyTicket: ' '
    })
  }
  closeEditTReplyComment(id: any) {
    this.showEditTicketReplycomment[id] = false;
    this.loadData();
  }
  cancelReply() {
    this.formMail.get('content')?.setValue('');
    this.formMail.get('cc_to')?.setValue('');
    this.uploadReply.deleAllFile();
    this.isShowButtonMail = false
  }
  download(url: any, original: any, fileId: any) {
    this.ticketService.downloadFile(fileId).subscribe(res => {
      let tmp: any = res.body;
      let objectURL = URL.createObjectURL(tmp);
      let link = document.createElement("a");
      link.href = objectURL;
      link.download = original;
      link.click();
    }, error => console.trace(error));
  }
  changeTab() {
    this.uploadFileTicket.deleAllFile();
    this.uploadFileComment.deleAllFile();
    this.checkErrorUploadComment = false;
    this.checkErrorAmoutUpload = false;
    this.isUploading = false;
  }
}


