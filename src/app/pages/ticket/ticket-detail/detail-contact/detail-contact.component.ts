import { Component, Input, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import CommonUtil from '@shared/utils/common-utils';
import { TicketService } from '@shared/service/ticket.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ContactService } from '@shared/service/contact.service';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-detail-contact',
  templateUrl: './detail-contact.component.html',
  styleUrls: ['./detail-contact.component.scss']
})
export class DetailContactComponent implements OnInit {
  @Input() data: any = [];
  contactList: any = [];
  contact_id: any = [];
  delegateList: any = [];
  delegate_id: any;
  delegate_name: any = [];
  isLoading = false;
  changecustomer = false;
  customerQuery = {
    pageSize: PAGINATION.SIZE_DEFAULT,
    total: 0
  };
  searchChangeCus$="";
  customListSearch: any;
  constructor(
    private modalRef: NzModalRef,
    private fb: FormBuilder,
    private ticketService: TicketService,
    private toast: ToastService,
    private contacterService: ContactService,
  ) { }
  form: FormGroup = new FormGroup({});
  cusQuestionUpdate = new Subject<string>();
  ngOnInit(): void {
    // this.cusQuestionUpdate.pipe(
    //   debounceTime(800),
    //   distinctUntilChanged())
    //   .subscribe(value => {
    //     if (this.searchChangeCus$ != value) {
    //       this.onSearchData(value)
    //     }

    //   });
    this.delegate_name = this.data.delegate_name;
    this.loadCustomer();
    this.initForm();
    this.getContact();
  }
  getContact() {
    if (this.data?.customer_id) {
      this.contacterService.getContactByCustomerIdTK(this.data.customer_id, true).subscribe(res => {
        this.contactList = res.body?.data;
      });
    }

  }
  initForm(): void {
    this.form = this.fb.group({
      customer_id: [{ value: this.data ? this.data?.customer_id : null, disabled: false }],
      contact_id: [{ value: this.data ? this.data?.contact_id : null, disabled: false }],
    });
  }
  changeCustomer(id: any) {
    this.changecustomer = true
    this.form.patchValue({
      contact_id: null,
    })
    this.delegate_name = '';
    this.delegate_id = ''
    this.customListSearch.forEach((item: any) => {
      if (item.id == id) {
        this.delegate_name = item.delegate_name;
        this.delegate_id = item.delegate_id
      }
    })
    this.contacterService.getContactByCustomerIdTK(id, true).subscribe(res => {
      this.contactList = res.body?.data;
    });

  }
  onSubmit(): void {
    const ticket: any = {
      customer_id: this.form.value.customer_id ? this.form.value.customer_id : '',
      contact_id: this.form.value.contact_id ? this.form.value.contact_id : '',
      delegate_id: this.changecustomer ? this.delegate_id : this.data.delegate_id
    };
    const body = CommonUtil.trim(ticket);
    this.ticketService.updateTickket(this.data.id, body, true).subscribe(res => {
      if (res.body?.data) {
        this.modalRef.close({
          success: true,
          value: ticket,
        });
      }
    })
  }
  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  onSearchData($event: any) {
    this.searchChangeCus$ = $event;
    this.customerQuery.pageSize = PAGINATION.SIZE_DEFAULT
    this.loadCustomer();
  }
  loadCustomer() {
    const param = {
      name: this.searchChangeCus$,
      limit: this.customerQuery.pageSize
    }
      this.ticketService.getAllCustomerDetail(param, false).subscribe(res => {
        this.customListSearch = res?.body?.data;
        this.customerQuery.total = res.body?.meta?.pagination?.total || 0
        if (this.data?.customer_id && this.data?.customer_name && !this.searchChangeCus$) {
          const exist = this.customListSearch?.some((item: any) => item?.id == this.data?.customer_id);
          if (!exist) {
            this.customListSearch.push({ id: this.data?.customer_id, name: this.data?.customer_name })
            this.customerQuery.total++;
          }
          this.customListSearch.sort((a: any, b: any) => (a.phone)?.toString().localeCompare(b.phone?.toString()));
        }
  
        this.isLoading = false;
      })

  }
  loadMoreCus(): void {
    if (this.customerQuery.pageSize > this.customerQuery.total) {
      this.isLoading = false
      return
    }
    this.customerQuery.pageSize += 10;
    this.isLoading = true;
    this.loadCustomer()
  }
}
