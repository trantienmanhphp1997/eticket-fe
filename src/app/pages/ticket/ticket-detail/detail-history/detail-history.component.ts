import { Component, OnInit, Output } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import CommonUtil from '@shared/utils/common-utils';
import { environment } from '@env/environment';
@Component({
  selector: 'app-detail-history',
  templateUrl: './detail-history.component.html',
  styleUrls: ['./detail-history.component.scss']
})
export class DetailHistoryComponent implements OnInit {
  @Output() data : any = [];
  @Output() currentUser: any=[];
  commonUtil: any = CommonUtil;
  description: any = [];
  descriptionnew: any = [];
  urlgetway = environment.fileDomain

  constructor(
    private modalRef: NzModalRef,
  ) { }

  ngOnInit(): void {
    let des = this.data.properties.attributes.description;
    let des2 = this.data.properties.old.description;
    des = des?.replace('\\','')
    des = des?.replace('<html><body>','')
    des = des?.replace('</body></html>','')
    this.description = des;

    des2 = des2?.replace('\\','')
    des2 = des2?.replace('<html><body>','')
    des2 = des2?.replace('</body></html>','')
    this.descriptionnew = des2;
  }
  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
}
