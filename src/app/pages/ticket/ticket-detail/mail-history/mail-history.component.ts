import { Component, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { environment } from '@env/environment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { TicketService } from '@shared/service/ticket.service';
import CommonUtil from '@shared/utils/common-utils';

@Component({
  selector: 'app-mail-history',
  templateUrl: './mail-history.component.html',
  styleUrls: ['./mail-history.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MailHistoryComponent implements OnInit {
  @Output() data : any = [];
  @Output() content : any = '';
  @Output() index : any = '';
  commonUtil: any = CommonUtil;
  description : any = ''
  urlgetway = environment.fileDomain
  constructor(
    private modalRef: NzModalRef,
    private ticketService: TicketService,
  ) { }

  ngOnInit(): void {
    this.description =this.content;
    this.description=this.description?.replaceAll('\\','')
  }
  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  download(original: any, fileId: any) {
    this.ticketService.downloadFile(fileId).subscribe(res => {
            let tmp: any = res.body;
            let objectURL = URL.createObjectURL(tmp);
            let link = document.createElement("a");
            link.href = objectURL;
            link.download = original;
            link.click();
    }, error => console.trace(error));
}
}
