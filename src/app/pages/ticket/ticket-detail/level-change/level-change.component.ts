import { Component, OnInit ,Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TicketService } from '@shared/service/ticket.service';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-level-change',
  templateUrl: './level-change.component.html',
  styleUrls: ['./level-change.component.scss']
})
export class LevelChangeComponent implements OnInit {
  @Input() data: any =[];
  @Input() ticket: any ;
  form: FormGroup = new FormGroup({});
  constructor(
    private modalRef: NzModalRef,
    private ticketService : TicketService,
    private fb: FormBuilder,
    private toast: ToastService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      description: '',
    });
  }
  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  onSubmit(){
    const body: any = {
      transaction_id:this.data.id,
      message: this.form.value.description
    };

    this.ticketService.excuteTransaction( body, this.ticket, true).subscribe( res =>{
      if (res) {
        this.toast.success(`Chuyển thành công`);
        this.modalRef.close({
          success: true,
          value: null,
        });
      }
    })
  }
}
