import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TicketService } from '@shared/service/ticket.service';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-ticket-flow',
  templateUrl: './ticket-flow.component.html',
  styleUrls: ['./ticket-flow.component.scss']
})
export class TicketFlowComponent implements OnInit {
  @Input() data: any;
  @Input() statusTicket: any;
  constructor(
    private modalRef: NzModalRef,
    private ticketService: TicketService,
    private fb: FormBuilder,
    private toast: ToastService
  ) { }
  kpi: any =[]
  ngOnInit(): void {
    this.convertTime1();
    if(this.data?.excute_info?.process_type == 'processing'){
      this.convertTime();
    }

  }
  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  convertTime() {
    let seconds: number = (this.data?.excute_info?.duration)-1;
    let textSec: any = '0';
    let statSec: number = 60;

    // let dayTimeKpi = Math.floor(convertSeconds / (3600 * 24)) + ' ' + 'ngày';
    const convertTime = setInterval(() => {
      seconds++;
      if (statSec != 0) statSec--;
      else statSec = 59;

      if (statSec < 10) {
        textSec = '0' + statSec;
      } else textSec = statSec;


      let convertSeconds = Math.floor(seconds);
      let hoursTimeKpi = Math.floor(convertSeconds / 3600) + ' ' + ' giờ ';
      if (hoursTimeKpi == 0 + ' giờ ') {
        hoursTimeKpi = '';
      }
      let minutesTimeKpi = Math.floor(convertSeconds % 3600 / 60) + ' ' + ' phút ';
      if (minutesTimeKpi == 0 + ' phút ') {
        minutesTimeKpi = '';
      }
      let secondTimeKpi = Math.floor((convertSeconds) % 60) + ' ' + ' giây ';
      if (secondTimeKpi == 0 + ' giây ') {
        secondTimeKpi = '';
      } else {
        if (!minutesTimeKpi) {
          minutesTimeKpi = 0 + ' phút '
        }
      }
      this.kpi = hoursTimeKpi + minutesTimeKpi + secondTimeKpi;
    }, 1000);
  }
  convertTime1() {
    let convertSeconds: number = this.data?.excute_info?.duration;
      let hoursTimeKpi = Math.floor(convertSeconds / 3600) + ' ' + ' giờ ';
      if (hoursTimeKpi == 0 + ' giờ ') {
        hoursTimeKpi = '';
      }
      let minutesTimeKpi = Math.floor(convertSeconds % 3600 / 60) + ' ' + ' phút ';
      if (minutesTimeKpi == 0 + ' phút ') {
        minutesTimeKpi = '';
      }
      let secondTimeKpi = Math.floor((convertSeconds) % 60) + ' ' + ' giây ';
      if (secondTimeKpi == 0 + ' giây ') {
        secondTimeKpi = '';
      } else {
        if (!minutesTimeKpi) {
          minutesTimeKpi = 0 + ' phút '
        }
      }
      this.kpi = hoursTimeKpi + minutesTimeKpi + secondTimeKpi;
  }
  convertTimeSLA(time: any) {
    let days = '';
    let hours = '';
    let minutes = '';
    if (time?.internel_require_d == 0 && time?.internel_require_h == 0 && time?.internel_require_m == 0) {
      minutes = time?.internel_require_m + ' ' + 'phút';
    } else {
      if (time?.internel_require_d != 0) {
        days = time?.internel_require_d + ' ' + 'ngày';
      } else {
        days = '';
      }

      if (time?.internel_require_h != 0) {
        hours = time?.internel_require_h + ' ' + 'giờ';
      } else {
        hours = '';
      }

      if (time?.internel_require_m != 0) {
        minutes = time?.internel_require_m + ' ' + 'phút';
      } else {
        minutes = '';
      }
    }

    return `${days} ${hours} ${minutes}`;
  }
}
