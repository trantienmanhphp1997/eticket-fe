import { Component, OnInit, ViewChild } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzUploadChangeParam } from 'ng-zorro-antd/upload';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TicketService } from '@shared/service/ticket.service';
import { TicketPriorityService } from '@shared/service/ticket-priority.service';
import { UserService } from '@shared/service/user.service';
import CommonUtil from '@shared/utils/common-utils';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TaskService } from '@shared/service/task.service';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { NzDatePickerComponent } from 'ng-zorro-antd/date-picker';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.scss']
})
export class TaskCreateComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  dateFormat = 'dd/MM/yyyy';
  ticketList: any = [];
  ticketPriorityList: any = [];
  userList: any = [];
  profile: any = [];
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  constructor(
      private msg: NzMessageService,
      private router: Router,
      private fb: FormBuilder,
      private ticketService: TicketService,
      private ticketPriorityService: TicketPriorityService,
      private userService: UserService,
      private toast: ToastService,
      private taskService: TaskService,
      private modalRef: NzModalRef,
      private localStorage: LocalStorageService,
  ) { }
  startValue: Date | null = null;
  endValue: Date | null = null;
  @ViewChild('endDatePicker') endDatePicker!: NzDatePickerComponent;
  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.endValue) {
      return false;
    }
    return startValue.getTime() > this.endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.startValue) {
      return false;
    }
    return endValue.getTime() <= this.startValue.getTime();
  };
  ngOnInit() {
      this.initForm();
      this.getTicketData();
      this.getTicketPriorityData();
      this.getUserData();
  }

  initForm(): void {
      this.profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
      this.form = this.fb.group({
          name: ['', [Validators.required, Validators.maxLength( LENGTH_VALIDATOR.CODECUS_MAX_LENGTH.MAX)]],
        //   type:  ['', [Validators.required]],
          priority: ['', [Validators.required]],
          start_time: ['', [Validators.required]],
          end_time: ['', [Validators.required]],
          description: ['',[Validators.maxLength(LENGTH_VALIDATOR.IDS_MAX_LENGTH.MAX)]],
          ticket_id: ['', [Validators.required]],
          reporter_id:  [{value: this.profile.id, disabled: false}, [Validators.required]],
          assignee_id:['', [Validators.required]],
          progress: ['',[Validators.required]]
      });
  }

  handleChange({ file, fileList }: NzUploadChangeParam): void {
      const status = file.status;
      if (status !== 'uploading') {
      }
      if (status === 'done') {
          this.msg.success(`${file.name} file uploaded successfully.`);
      } else if (status === 'error') {
          this.msg.error(`${file.name} file upload failed.`);
      }
  }

  onCancel() {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  getTicketData() {
      const params = {
          pageIndex: '',
          pageSize: 0
      }
      this.ticketService.getAll(params, true).subscribe((res) => {
          this.ticketList = res.body?.data;
      });
  }
  getTicketPriorityData() {
      const params = {
          pageIndex: '',
          pageSize: 0
      }
      this.ticketPriorityService.getAllTicketPriority(params, true).subscribe((res) => {
          this.ticketPriorityList = res.body?.data;
      });
  }
  getUserData() {
      const params = {
          pageIndex: '',
          pageSize: 0
      }
      this.userService.getUser(params, true).subscribe((res) => {
          this.userList = res.body?.data;
      });
  }

  onSubmit() {
      // if (this.form.invalid) {
      //     CommonUtil.markFormGroupTouched(this.form);
      //     return;
      // }
      const params = {
          ...this.form.value,
          start_time: moment(this.form.value.start_time).toISOString(),
          end_time: moment(this.form.value.end_time).toISOString()
      }
      const body = CommonUtil.trim(params);
      this.taskService.create(body, true).subscribe(res => {
          if (res.body?.data) {
              this.toast.success(`Tạo công việc thành công`);
              this.modalRef.close({
                success: false,
                value: null,
              });
          }
      });
  }
  onOk(result: Date | Date[] | null): void {
  }

  onChangeData(type: string, content: string): void {
      this.form.get(type)?.setValue(content);
  }
}
