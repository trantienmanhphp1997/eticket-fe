import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup,FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { DepartmentService } from '@shared/service/department.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TicketService } from '@shared/service/ticket.service';
import CommonUtil from '@shared/utils/common-utils';
import { AUTH } from '@shared/constants/auth.constant';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { NzDatePickerComponent } from 'ng-zorro-antd/date-picker';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { TicketDetailComponent } from './ticket-detail/ticket-detail.component';
import { AuthService } from '@shared/service/auth/auth.service';
import { TICKET_ORDERBY, TICKET_SORTBY } from '@shared/constants/ticket.constant';
import { HttpClient } from '@angular/common/http';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { ImportModalComponent } from '../../shared/components/import-modal/import-modal.component';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { interval, Subject } from 'rxjs';
import { environment } from '@env/environment';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import * as moment from 'moment';
import { debugOutputAstAsTypeScript } from '@angular/compiler';
import { slice } from 'lodash';
const DATE_FORMATS = {
  SHORT: 'd.MMM yyyy',
};
@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {
  selected: any = { startDate: null, endDate: null };
  date = null;
  isVisible = false;
  visible1 = false;
  start:any;
  end: any;
  productList: any = [];
  ticketPriorityList: any = [];
  ticketSourceList: any = [];
  ticketCategoryList: any = [];
  customerList: any = [];
  userList: any = [];
  infoUser: any;
  ticketStateList: any = [];
  commonUtil: any = CommonUtil
  auth: any = AUTH;
  url_domain = environment.fe_domain
  total: any = 0;
  pageIndex = PAGINATION.PAGE_DEFAULT;
  pageSize = PAGINATION.SIZE_DEFAULT;
  pageSizeOptions = PAGINATION.OPTIONS;
  orderBy: any = 'created_at';
  sortBy: any = 'desc';
  dayTimeKpi: any;
  hoursTimeKpi: any;
  minutesTimeKpi: any;
  visible = false
  listSortBy = TICKET_SORTBY;
  listOrderBy = TICKET_ORDERBY;
  //checked items
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly any[] = [];
  setOfCheckedId = new Set<number>();
  checked = false;
  //tree
  defaultCheckedNode: any
  defaultCheckedKeys = ['0-0-0'];
  defaultSelectedKeys = ['0-0-0'];
  defaultExpandedKeys = ['0-0', '0-0-0', '0-0-1'];
  nodes = [];
  nodeDapartment: any = [];
  listFillter: any = [];
  //filter
  statusFilter: any;
  priorityFilter: any;
  isShowFillter: boolean = false;
  ticketList: any = [];
  form: FormGroup = new FormGroup({});
  form1: FormGroup = new FormGroup({});
  today = new Date();
  start_date =  '';
  end_date ='';
  typeInput: boolean = false;
  // start_date: any;
  // end_date: any;
  startDateValue: any;
  endDateValue : any;
  calendarLocale = {
    format: 'DD/MM/YYYY',
    daysOfWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    monthNames: ['Th1', 'Th2', 'Th3', 'Th4', 'Th5', 'Th6', 'Th7', 'Th7', 'Th9', 'Th10', 'Th11', 'Th12'],
    firstDay: 1
  };
  ranges = {
    'Hôm nay': [moment(), moment()],
    'Tuần này': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
    'Năm này':  [moment().startOf('year'), moment().endOf('year')],
    '1 tuần trước': [moment().subtract(6, 'days'), moment()],
    '1 tháng trước': [moment().subtract(31, 'days'), moment()],
    '1 năm trước': [moment().subtract(1, 'year'), moment()],
  };
  listStatus: any = [];
  currentUser: any;
  params: any = [];
  quick_filter_type: any  =[]
  state_id: any = [];
  state: any; // dùng cho thanh trạng thái
  key: any = [];
  allStatus: any = [];
  isShow: any = [];
  idFile: any = [];
  dataFile: any;
  isFillterDate: boolean = true;
  listAssignee: any = [];
  status: any = [];
  border: any = [];
  cleandate: boolean = false;
  statusFillter: boolean = false;
  isLoading = false;
  kpicound: any;
  slacound: any
  stated: any;
  typeTicket: any;
  isInput: boolean = false;
  priority_id: any;
  customer_id: any;
  department_id: any;
  require_id: any;
  category_id: any;
  user_id: any;
  source_id: any;
  ticket_type: any;
  product_id: any;
  creator_department_id:any
  checkResetAdvancedFilter: boolean = false;
  customerQuery = {
    pageSize: 20,
    total: 0
  }
  userQuery = {
    pageSize: 20,
    total: 0
  }
  userReqQuery = {
    pageSize: 20,
    total: 0
  }

  filterUser = "";
  filterUserReq = "";
  filterCus = "";
  isLoadingCus = false;
  isLoadingUser = false;
  isLoadingReq = false
  timeSla: any;
  setTimeSLAInterval: any;
  setTimeKPIInterval: any;
  urlDetail: any;
  checkdefault: boolean = true;
  date1: any = [this.today.getFullYear() + '-' + String(this.today.getMonth()).padStart(2, '0') + '-' + String(this.today.getDate()).padStart(2, '0'), this.today.getFullYear() + '-' + String(this.today.getMonth() + 1).padStart(2, '0') + '-' + String(this.today.getDate()).padStart(2, '0')];
  styleTooltip = {
    'min-width': 'max-content',
  };
  constructor(
    private fb: FormBuilder,
    private ticketService: TicketService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private router: Router,
    private toast: ToastService,
    private departmentService: DepartmentService,
    private authService: AuthService,
    protected http: HttpClient,
    private localStorage: LocalStorageService,
  ) { }

  searchForm = this.fb.group({
    keyword: [null]
  });

  keyword: any = '';
  urlSearch: any = []
  userQuestionUpdate = new Subject<string>();
  cusQuestionUpdate = new Subject<string>();
  reqQuestionUpdate = new Subject<string>();
  private readonly _stop = new Subject<void>();
  private readonly _start = new Subject<void>();
  ngOnInit() {
    const date =new Date();
    this.selected.startDate =new Date(date.getFullYear(), date.getMonth()-1, date.getDate());
    this.selected.endDate = new Date();

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.userQuestionUpdate.pipe(
      debounceTime(800),
      distinctUntilChanged())
      .subscribe(value => {
        if (this.filterUser != value) {
          this.onSearchData('user', value)
        }
      });
    this.cusQuestionUpdate.pipe(
      debounceTime(800),
      distinctUntilChanged())
      .subscribe(value => {
        if (this.filterCus != value) {
          this.onSearchData('customer', value)
        }
      });
    this.reqQuestionUpdate.pipe(
      debounceTime(800),
      distinctUntilChanged())
      .subscribe(value => {
        if (this.filterUserReq != value) {
          this.onSearchData('require', value)
        }
      });
    const profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
    if (profile) {
      this.initForm();
      // this.getcurrentUser();
      this.loadData();
      this.ticketStatus();
      this.getIntervalSLATime()
      this.getIntervalKPITime();

    }
  }

  ngModelChange(event:any): void {
    if(event.type=='change'){
      this.typeInput = true;
      const date = event.target.value ;
      const vitri: number = date.search("-");
      const SD =  date.slice(0,Number(vitri-1));
      const pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
      this.selected.startDate = new Date(SD.replace(pattern,'$3-$2-$1'));
      const ED =  date.slice(Number(vitri+2),date.length);
      this.selected.endDate = new Date(ED.replace(pattern,'$3-$2-$1'));
    }
    if(this.selected.startDate){
      this.selected;
      this.paramAll();
    }
  }

  loadData() {  
    const queryParams =   this.localStorage.retrieve(LOCAL_STORAGE.URL);
    this.selected.startDate = queryParams?.params?.type ? queryParams?.params?.start_time : new Date(queryParams?.params?.start_time ? queryParams?.params?.start_time : this.selected.startDate)
    this.selected.endDate = queryParams?.params?.type ? queryParams?.params?.end_time : new Date(queryParams?.params?.end_time ? queryParams?.params?.end_time : this.selected.endDate)
    this.keyword = queryParams?.params?.search
    this.params = {
      sortBy: this.sortBy,
      orderBy: this.orderBy,
      start_time: this.selected.startDate.toISOString(),
      end_time:  this.selected.endDate.toISOString(),
    }
    // ưu tiên border lọc theo thanh trạng thái
    if(queryParams?.params?.state){
      this.border = queryParams?.params?.state;
      this.state_id = ['total_tickets', 'expire', 'outdate'].includes(queryParams?.params?.state) == false && queryParams?.params?.state_id?.length == 0 ? [queryParams?.params?.state] : queryParams?.params?.state_id;
    } else {
      if(queryParams?.params?.state_id?.length == 1){
        this.border = queryParams?.params?.state_id[0];
      }
      if(queryParams?.params?.state_id?.length >= 2){
        this.border = 'total_tickets';
      }
      this.state_id = queryParams?.params?.state_id  ? queryParams?.params?.state_id : '';
    }

    this.pageSize = queryParams?.params?.limit ? queryParams?.params?.limit :  this.pageSize;
    this.pageIndex = queryParams?.params?.page ? queryParams?.params?.page :  this.pageIndex,
    this.quick_filter_type = this.status =  queryParams ? queryParams?.params?.quick_filter_type : '';
    this.searchForm.controls['keyword'].setValue( this.keyword  ? this.keyword  : '');
    this.priority_id = queryParams?.params?.priority_id ? queryParams?.params?.priority_id :  null;
    this.customer_id = queryParams?.params?.customer_id ? queryParams?.params?.customer_id :  null;
    this.department_id =queryParams?.params?.department_id ? queryParams?.params?.department_id :  null;
    this.require_id =queryParams?.params?.require_id ? queryParams?.params?.require_id :  null;
    this.category_id =queryParams?.params?.category_id ? queryParams?.params?.category_id :  null;
    this.user_id =queryParams?.params?.user_id ? queryParams?.params?.user_id :  null;
    this.source_id =queryParams?.params?.source_id ? queryParams?.params?.source_id :  null;
    this.ticket_type =queryParams?.params?.ticket_type ? queryParams?.params?.ticket_type :  null;
    this.product_id =queryParams?.params?.product_id ? queryParams?.params?.product_id :  null;
    this.creator_department_id =queryParams?.params?.creator_department_id ? queryParams?.params?.creator_department_id :  null;
    this.state =queryParams?.params?.state ? queryParams?.params?.state :  null;
    const url = {
      state_id: queryParams?.params?.state_id ? queryParams?.params?.state_id : null,
      search: this.keyword? this.keyword : null ,
      quick_filter_type:  queryParams?.params?.quick_filter_type ? queryParams?.params?.quick_filter_type : null,
      page: queryParams?.params?.page ? queryParams?.params?.page :  this.pageIndex,
      priority_id: queryParams?.params?.priority_id ? queryParams?.params?.priority_id :  null,
      customer_id: queryParams?.params?.customer_id ? queryParams?.params?.customer_id :  null,
      department_id :queryParams?.params?.department_id ? queryParams?.params?.department_id :  null,
      require_id:queryParams?.params?.require_id ? queryParams?.params?.require_id :  null,
      category_id:queryParams?.params?.category_id ? queryParams?.params?.category_id :  null,
      user_id:queryParams?.params?.user_id ? queryParams?.params?.user_id :  null,
      source_id:queryParams?.params?.source_id ? queryParams?.params?.source_id :  null,
      ticket_type:queryParams?.params?.ticket_type ? queryParams?.params?.ticket_type :  null,
      product_id:queryParams?.params?.product_id ? queryParams?.params?.product_id :  null,
      creator_department_id:queryParams?.params?.creator_department_id ? queryParams?.params?.creator_department_id :  null,
      limit: queryParams?.params?.limit ? queryParams?.params?.limit :  this.pageSize,
      start_time: (this.selected.startDate).toISOString(),
      end_time: (this.selected.endDate).toISOString(),
      state: queryParams?.params?.state ? queryParams?.params?.state :  null,
    }
    this.urlSearch = CommonUtil.trim(url);
    this.router.navigate([`admin/ticket-management`], { queryParams: this.urlSearch  });
    this.ticketService.fillter(this.urlSearch,  this.params, true).subscribe(
      (res) => {
        this.ticketList = res.body?.data?.map((dept: any) => {
          return {
            ...dept,
            disabled: dept.ticket_status_type != 4 && dept.ticket_status_type != 1,
            timeKpi: this.getTime(dept?.current_sla_internal_resolution_time_require),
            slaTime: this.getTime(dept?.resolution_time_require),
          }
        })
        this.nodes = this.createDataTree(res.body?.data)
        this.total = res.body?.meta?.pagination?.total;
      }
    );
    if (this.border == '') this.border = 'total_tickets'
  }
  getIntervalSLATime() {
    this.setTimeSLAInterval = setInterval(() => {
      this.ticketList.forEach((item: any, key: number) => {
        item.resolution_remaining_time--;
        this.ticketList[key].timeSlaRemain = this.getTimeRemain(item.resolution_remaining_time)
      })
    }, 1000);
  }
  getIntervalKPITime() {
    this.setTimeKPIInterval = setInterval(() => {
      this.ticketList.forEach((item: any, key: number) => {
        var seconds = item.current_sla_internal_resolution_remaining_time--;
        this.ticketList[key].timeKpiRemain = this.getTimeRemain(seconds)
      })
    }, 1000);
  }
  loadDataAll() {
    this.state_id = null;
    this.status = '';
    this.loadData();
    this.ticketStatus();
  }
  refeshSearch() {
    this.searchForm.reset()
    this.orderBy = 'created_at';
    this.sortBy = 'desc';
    this.loadData();
    this.ticketStatus();
  }
  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.dept_name, key: aData.dept_code, isLeaf: false });
    const dataTree: any = [];
    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
      }
    });
    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });
    return dataTree;
  };
  // Events
  onSearch(event: any) {
    this.cleandate = true;
    this.border = '';
    if (event) {
      this.keyword = event.target?.value.trim() || null;
      this.searchForm.patchValue({
        keyword: this.keyword
      })

      this.pageIndex = PAGINATION.PAGE_DEFAULT;
      this.form.reset();
      const fillter: any = {
        ...this.form.value,
        quick_filter_type: this.quick_filter_type,
        page: this.pageIndex ? this.pageIndex : null,
      };
      this.listFillter = CommonUtil.trim(fillter);
     this.paramAll();
    }
  }
  resetAdvancedFilter() {
    this.form.reset();
    this.checkResetAdvancedFilter = true;
  }
  onSubmit() {
    this.statusFillter = true
    this.isFillterDate = false;
    if( this.form.value.state_id?.length >= 0){
      this.state = '';
    }
     this.pageIndex = PAGINATION.PAGE_DEFAULT;
    const fillter: any = {
      ...this.form.value,
      quick_filter_type: this.quick_filter_type,
      page: this.pageIndex ? this.pageIndex : null,

    };
    this.listFillter = CommonUtil.trim(fillter);

    this.isShowFillter = false;
    this.paramAll();
  }

  nzEvent(event: NzFormatEmitEvent): void {
    this.defaultCheckedNode = event.node?.origin.id
    this.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.loadData();
    this.ticketStatus();
  }

  onQuerySearch(params: any): void {
    this.checked = false
    const { pageIndex, pageSize } = params;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
    this.paramAll();
  }

  onCreate = () => {
    var role_create_ticket = [this.auth.TICKET.CREATE];
    if (this.authService.hasAnyAuthority(role_create_ticket)) {
      this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}/${ROUTER_UTILS.ticket.create}`]);
    } else {
      this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}`]);
    }
  }

  onUpdate = (data: any) => {
    const base = CommonUtil.modalBase(TicketDetailComponent, { selectedticket: data, isUpdate: true }, '30%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result) {
        this.loadData()
        this.ticketStatus();
      }
    });
  }

  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.ticketList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }

  public goToTicketDetail(titketId?: string): void {
    if (titketId) {
      this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}/${ROUTER_UTILS.ticket.view}`, titketId]);
    }
  }
  openFillter() {
    const queryParams =   this.localStorage.retrieve(LOCAL_STORAGE.URL);
    let idList = {
      user_id: queryParams?.params?.user_id ? queryParams?.params?.user_id : null,
      require_id: queryParams?.params?.require_id ? queryParams?.params?.require_id : null,
      customer_id: queryParams?.params?.customer_id ? queryParams?.params?.customer_id : null
    }
    this.isShowFillter = true;
    this.loadDataFillterTicket(idList);
    this.loadCustomer(idList.customer_id);
    this.loadUser(idList.user_id);
    this.loadDataRequester(idList.require_id);
    this.loadDataDapartment();

  }
  closeFillter() {
    this.isShowFillter = false;
  }
  loadDataFillterTicket(params: any = null) {
    this.ticketService.loadMultipleDataForTicketDetail(params).subscribe(res => {
      this.productList = res[0].body?.data;
      this.ticketPriorityList = res[1].body?.priority.data;
      this.ticketSourceList = res[1].body?.source.data;
      this.ticketCategoryList = res[1].body?.category.data;
      this.ticketStateList = res[1].body?.state.data;
      this.ticketStateList.push({ object: 'TicketState', name: 'Đã hủy', id: 'cancelled_ticket', status: 1 })
    })
  }
  loadDataRequester(id: any = null) {
    const param = {
      data_context: 'filter_requester',
      name: this.filterUserReq,
      limit: this.userReqQuery.pageSize,
      pinnedRecord: id ? id.join(',') : id
    }
    this.ticketService.getAllAssignee(param, false).subscribe(res => {
      this.userList = res?.body?.data;
      this.userReqQuery.total = res.body?.meta?.pagination?.total || 0
      this.isLoadingReq = false;
    })
  }
  loadDataDapartment() {
    this.departmentService.getAllDepartments({ data_context: 'manage-ticket' }, true).subscribe(res => {
      this.nodeDapartment = res?.body;
    })
  }
  initForm(): void {
    const  queryParams =   this.localStorage.retrieve(LOCAL_STORAGE.URL);
    this.form = this.fb.group({
      product_id:  [{value: queryParams?.params?.product_id ? queryParams?.params?.product_id :  null,  disabled: false}],
      state_id: [{value: queryParams?.params?.state_id ? queryParams?.params?.state_id :   null,  disabled: false}],
      priority_id: [{value: queryParams?.params?.priority_id ? queryParams?.params?.priority_id :   null,  disabled: false}],
      category_id: [{value: queryParams?.params?.category_id ? queryParams?.params?.category_id :   null,  disabled: false}],
      require_id:  [{value: queryParams?.params?.require_id ? queryParams?.params?.require_id :   null,  disabled: false}],
      user_id:  [{value: queryParams?.params?.user_id ? queryParams?.params?.user_id :  null,  disabled: false}],
      source_id:  [{value: queryParams?.params?.source_id ? queryParams?.params?.source_id :   null,  disabled: false}],
      department_id: [{value: queryParams?.params?.department_id ? queryParams?.params?.department_id :   null,  disabled: false}],
      customer_id:  [{value: queryParams?.params?.customer_id ? queryParams?.params?.customer_id :   null,  disabled: false}],
      ticket_type:  [{value: queryParams?.params?.ticket_type ? queryParams?.params?.ticket_type :  null,  disabled: false}],
      creator_department_id: [{value: queryParams?.params?.creator_department_id ? queryParams?.params?.creator_department_id :  null,  disabled: false}],
      state: [{value: queryParams?.params?.state ? queryParams?.params?.state :  null,  disabled: false}],
    });
  }
  multiDelete(data: any) {
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa ticket đã chọn không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let ticketIds = data instanceof Set ? Array.from(data) : [data.id];
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.ticketService.multiDelete(ticketIds).subscribe(res => {
          if (res) {
            this.toast.success(`Xóa thành công !`);
            this.setOfCheckedId = new Set()
            modal.close();
            this.checked = false;
            this.indeterminate = false;
            this.loadData()
            this.ticketStatus();
          };
        })
      } else {
        modal.close();
      }
    });
  }
  ticketStatus() {
    const queryParams =   this.localStorage.retrieve(LOCAL_STORAGE.URL);
    // this.selected.startDate = this.start_date= queryParams?.params?.start_time ? queryParams?.params?.start_time   :  ''
    // this.selected.endDate = this.end_date= queryParams?.params?.end_time ? queryParams?.params?.end_time   : ''
    // this.selected.startDate =  queryParams?.params?.start_time ? queryParams?.params?.start_time   : this.selected.startDate
    // this.selected.endDate = queryParams?.params?.end_time ? queryParams?.params?.end_time   : this.selected.endDate;
    const params = {
      // ...this.form.value,
      state_id: queryParams?.params?.state_id ? queryParams?.params?.state_id : null,
      search: this.keyword? this.keyword : null ,
      quick_filter_type:  queryParams?.params?.quick_filter_type ? queryParams?.params?.quick_filter_type : null,
      page: queryParams?.params?.page ? queryParams?.params?.page :  this.pageIndex,
      priority_id: queryParams?.params?.priority_id ? queryParams?.params?.priority_id :  null,
      customer_id: queryParams?.params?.customer_id ? queryParams?.params?.customer_id :  null,
      department_id :queryParams?.params?.department_id ? queryParams?.params?.department_id :  null,
      require_id:queryParams?.params?.require_id ? queryParams?.params?.require_id :  null,
      category_id:queryParams?.params?.category_id ? queryParams?.params?.category_id :  null,
      user_id:queryParams?.params?.user_id ? queryParams?.params?.user_id :  null,
      source_id:queryParams?.params?.source_id ? queryParams?.params?.source_id :  null,
      ticket_type:queryParams?.params?.ticket_type ? queryParams?.params?.ticket_type :  null,
      product_id:queryParams?.params?.product_id ? queryParams?.params?.product_id :  null,
      creator_department_id: queryParams?.params?.creator_department_id ? queryParams?.params?.creator_department_id :  null,
      limit: queryParams?.params?.limit ? queryParams?.params?.limit :  this.pageSize,
      start_time: this.selected.startDate.toISOString(),
      end_time: this.selected.endDate.toISOString(),
    }
    this.ticketService.ticketStatus(params).subscribe(res => {
      this.listStatus = res.body?.data
    })
  }
  paramAll(resetStateId = false){
    const params = {
      type: this.typeInput,
      search: this.keyword ? this.keyword : null,
      page: this.pageIndex? this.pageIndex: null,
      limit: this.pageSize ? this.pageSize:null,
      start_time: this.typeInput ? this.selected.startDate : (this.selected.startDate ? this.selected.startDate : this.selected.startDate).toISOString(),
      end_time: this.typeInput ? this.selected.endDate : (this.selected.endDate ? this.selected.endDate : this.selected.endDate).toISOString(),
      state: this.state?  this.state: null,
      // param bộ lọc nâng cao
      state_id: this.checkResetAdvancedFilter || resetStateId ? null : (this.listFillter.state_id ? this.listFillter.state_id : this.state_id),
      quick_filter_type: this.checkResetAdvancedFilter ? null : (this.quick_filter_type ? this.quick_filter_type : null),
      priority_id: this.checkResetAdvancedFilter ? null : (this.listFillter.priority_id ? this.listFillter.priority_id : this.priority_id),
      product_id: this.checkResetAdvancedFilter ? null : (this.listFillter.product_id ? this.listFillter.product_id : this.product_id),
      category_id: this.checkResetAdvancedFilter ? null : (this.listFillter.category_id ? this.listFillter.category_id : this.category_id),
      require_id: this.checkResetAdvancedFilter ? null : (this.listFillter.require_id ? this.listFillter.require_id : this.require_id),
      user_id: this.checkResetAdvancedFilter ? null : (this.listFillter.user_id ? this.listFillter.user_id : this.user_id),
      source_id: this.checkResetAdvancedFilter ? null : (this.listFillter.source_id ? this.listFillter.source_id : this.source_id),
      department_id: this.checkResetAdvancedFilter ? null : (this.listFillter.department_id ? this.listFillter.department_id : this.department_id),
      customer_id: this.checkResetAdvancedFilter ? null : (this.listFillter.customer_id ? this.listFillter.customer_id : this.customer_id),
      ticket_type: this.checkResetAdvancedFilter ? null : (this.listFillter.ticket_type ? this.listFillter.ticket_type : this.ticket_type),
      creator_department_id: this.checkResetAdvancedFilter ? null : (this.listFillter.creator_department_id ? this.listFillter.creator_department_id : this.creator_department_id),
    }
    this.urlSearch = CommonUtil.trim(params);
    this.router.navigate([`admin/ticket-management`], { queryParams: this.urlSearch});
    this.localStorage.store('url', { params: this.urlSearch });
    this.loadData();
    this.ticketStatus();
  }
  fillterStatus(state_id?: any) {
    this.isFillterDate = false;
    this.border = state_id;
    this.state = state_id;
    this.pageIndex = PAGINATION.PAGE_DEFAULT;
    // nếu this.state_id chỉ có 1 giá trị hoặc ít hơn thì luôn luôn được làm mới
    // nếu this.state_id có nhiều hơn 1 giá trị thì không làm mới trong mọi trường hợp
    let new_state_id = ['total_tickets', 'expire', 'outdate'].includes(state_id) == false && (this.state_id?.length < 2 || this.state_id == null)  ? [state_id] : null;
    let resetStateId = this.state_id?.length >= 2 ? false : new_state_id == null
    const fillter: any = {
      ...this.form.value,
      state: this.state,
      quick_filter_type: this.quick_filter_type,
      page: this.pageIndex,
      state_id: new_state_id
    };
    this.listFillter = CommonUtil.trim(fillter);
    this.paramAll(resetStateId)

  }

  onFillterTickets(type: any) {
    this.status = type;
    this.pageIndex = PAGINATION.PAGE_DEFAULT;
    const fillter: any = {
      ...this.form.value,
      state_id: this.state_id,
      quick_filter_type: this.quick_filter_type,
      page: this.pageIndex ,
    };
    this.quick_filter_type = type
    this.listFillter = CommonUtil.trim(fillter);
    this.paramAll();
  }
  refeshFillter(){
    this.status ='';
    this.border ='';
    this.form.reset();
    this.searchForm.reset();
    this.keyword ='';
    this.state_id ='';
    this.key ='';
    this.quick_filter_type = '';
    this.pageIndex = 1;
    const date = new Date();
    this.selected.startDate =new Date(date.getFullYear(), date.getMonth()-1, this.today.getDate());
    this.selected.endDate = new Date();
    const fillter: any = {
      ...this.form.value,
      state_id: this.form.value.state_id,
      quick_filter_type: this.quick_filter_type,
    };
    this.listFillter = CommonUtil.trim(fillter);
    this.router.navigate([`admin/ticket-management`]);
    this.localStorage.clear(LOCAL_STORAGE.URL)
    this.loadData();
    this.ticketStatus();
  }
  exportData() {
    const queryParams =   this.localStorage.retrieve(LOCAL_STORAGE.URL);
    const url = {
      state_id: queryParams?.params?.state_id ? queryParams?.params?.state_id : null,
      search: this.keyword? this.keyword : null ,
      quick_filter_type:  queryParams?.params?.quick_filter_type ? queryParams?.params?.quick_filter_type : null,
      priority_id: queryParams?.params?.priority_id ? queryParams?.params?.priority_id :  null,
      customer_id: queryParams?.params?.customer_id ? queryParams?.params?.customer_id :  null,
      department_id :queryParams?.params?.department_id ? queryParams?.params?.department_id :  null,
      require_id:queryParams?.params?.require_id ? queryParams?.params?.require_id :  null,
      category_id:queryParams?.params?.category_id ? queryParams?.params?.category_id :  null,
      user_id:queryParams?.params?.user_id ? queryParams?.params?.user_id :  null,
      source_id:queryParams?.params?.source_id ? queryParams?.params?.source_id :  null,
      ticket_type:queryParams?.params?.ticket_type ? queryParams?.params?.ticket_type :  null,
      product_id:queryParams?.params?.product_id ? queryParams?.params?.product_id :  null,
      creator_department_id:queryParams?.params?.creator_department_id ? queryParams?.params?.creator_department_id :  null,
      start_time: this.selected.startDate.toISOString(),
      end_time:  this.selected.endDate.toISOString(),
    }
    this.ticketService.exportTicketData(url, true)
  }
  exportTemplate() {
    this.ticketService.exportTemplate();
  }
  showImport() {
    // this.isVisible = true;
    const base = CommonUtil.modalBase(ImportModalComponent, {}, '40%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        // this.getAllDepartments()
        this.loadData();
        this.ticketStatus();
      }
    });
  }
  handleLoadFile(e: any) {
    this.dataFile = e[0];

  }
  handleOk(): void {
    this.ticketService.importTicketData(this.dataFile, true).subscribe(res => {
      if (res) {
        this.toast.success(`Import file thành công`);
        this.loadData();
        this.ticketStatus();
      } else {
        this.toast.error(`Import file thất bại`);
      }
    });
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }
  // getcurrentUser() {
  //   this.authService.myProfile().subscribe(res => {
  //     this.currentUser = res?.body?.data.id;
  //     this.infoUser = res?.body?.data;
  //   });
  // }
  assignToMe(id: any) {
    const profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
    const params = { user_id: profile.id }
    this.ticketService.updateTickket(id, params, true).subscribe(res => {
      if (res) {
        this.loadData();
        this.ticketStatus();
        this.toast.success(`Gán thành công`);
      }
    })
  }
  onChangeSortBy(value: any): void {
    this.sortBy = value;
    if (this.sortBy && this.orderBy) {
      this.loadData();
    }

  }
  onChangeOrderBy(value: any): void {
    this.orderBy = value;
    if (this.sortBy && this.orderBy) {
      this.loadData();
    }
  }
  getTimeRemain(time: any) {
    var label = ''
    if (time == 0) {
      label = 'Hết hạn ';
      return label;
    } else if (time < 0) {
      label = 'Quá hạn ';
    }
    var seconds = Math.abs(Math.floor(time))
    return label + CommonUtil.convertTime(seconds)
  }
  getTime(time: any) {
    let convertSeconds = time * 60;
    return CommonUtil.convertTime(convertSeconds)
  }
  filterSatus(value: any) {
    this.statusFilter = value
  }
  filterPriority(value: any) {
    this.priorityFilter = value
  }
  filterTicketType(value: any) {
    this.typeTicket = value;
  }
  onSearchData(type: any, $event: any) {
    const limit = PAGINATION.SIZE_DEFAULT
    if (type == 'customer') {
      this.customerQuery.pageSize = limit
      this.filterCus = $event;
      this.loadCustomer()
    } else if (type == 'user') {
      this.userQuery.pageSize = limit
      this.filterUser = $event;
      this.loadUser();
    } else if (type == 'require') {
      this.filterUserReq = $event;
      this.userReqQuery.pageSize = limit
      this.loadDataRequester();
    }
  }
  loadCustomer(id: any = null) {
    const param = {
      name: this.filterCus || null,
      limit: this.customerQuery.pageSize,
      pinnedRecord: id ? id.join(',') : id,
      skipSort: true
    }
    this.ticketService.getAllCustomerDetail(param, false).subscribe(res => {
      this.customerList = res?.body?.data;
      this.customerQuery.total = res.body?.meta?.pagination?.total || 0
      this.isLoadingCus = false;
    })
  }
  loadUser(id: any = null) {
    const param = {
      data_context: 'filter_assignee',
      name: this.filterUser,
      limit: this.userQuery.pageSize,
      pinnedRecord: id ? id.join(',') : id
    }
    this.ticketService.getAllAssignee(param, false).subscribe(res => {
      this.listAssignee = res?.body?.data;
      this.userQuery.total = res.body?.meta?.pagination?.total || 0
      this.isLoadingUser = false;
    })
  }
  loadMoreCus(): void {
    if (this.customerQuery.pageSize > this.customerQuery.total) {
      return
    }
    this.customerQuery.pageSize += 10;
    this.isLoadingCus = true;
    this.loadCustomer()
  }
  loadMoreUser(): void {
    if (this.userQuery.pageSize > this.userQuery.total) {
      return
    }
    this.userQuery.pageSize += 10;
    this.isLoadingUser = true;
    this.loadUser();
  }
  loadMoreReq(): void {
    if (this.userReqQuery.pageSize > this.userReqQuery.total) {
      return
    }
    this.userReqQuery.pageSize += 10;
    this.isLoadingReq = true;
    this.loadDataRequester();
  }
  viewTicket(id: any) {
    this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}/${ROUTER_UTILS.ticket.view}/${id}`]);
  }

  // dropdownBottom(type: any) {
  //   if (type == 'user') {
  //     if (this.userQuery.pageSize > this.userQuery.total) {
  //       this.isLoadingUser = false
  //     } else {
  //       this.isLoadingUser = true;
  //     }
  //     this.loadMoreUser();
  //   } if (type == 'customer') {
  //     if (this.customerQuery.pageSize > this.customerQuery.total) {
  //       this.isLoadingCus = false
  //     } else {
  //       this.isLoadingCus = true;
  //     }
  //     this.loadMoreCus();
  //   }
  //   if (type == 'require') {
  //     if (this.userReqQuery.pageSize > this.userReqQuery.total) {
  //       this.isLoadingReq = false
  //     } else {
  //       this.isLoadingReq = true;
  //     }
  //     this.loadMoreReq();
  //   }
  // }
}
function dayjs() {
  throw new Error('Function not implemented.');
}

