
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { STATUS } from '@shared/constants/status.constants';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { ClassicEditor } from '@shared/lib/ckeditor5/build/ckeditor';
import { ContactService } from '@shared/service/contact.service';
import { CustomerService } from '@shared/service/customer.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ProductService } from '@shared/service/product.service';
import { TicketCategoryService } from '@shared/service/ticket-category.service';
import { TicketPriorityService } from '@shared/service/ticket-priority.service';
import { TicketSourceService } from '@shared/service/ticket-source.service';
import { TicketStateService } from '@shared/service/ticket-state.service';
import { TicketService } from '@shared/service/ticket.service';
import { UserService } from '@shared/service/user.service';
import CommonUtil from '@shared/utils/common-utils';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { LocalStorageService } from 'ngx-webstorage';
import { ContactCreateComponent } from '../contact-create/contact-create.component';
import { AuthService } from '@shared/service/auth/auth.service';
import { DepartmentService } from '@shared/service/department.service';
import { CustomerCreateComponent } from '../../customer/customer-create/customer-create.component';
import { ACTION } from '@shared/constants/common.constant';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-ticket-create',
  templateUrl: './ticket-create.component.html',
  styleUrls: ['./ticket-create.component.scss']
})
export class TicketCreateComponent implements OnInit {
  public Editor = ClassicEditor;

  @Output() emitterError: any = new EventEmitter();
  @Output() emitter: any = new EventEmitter();
  @Output() emitterAmountError: any = new EventEmitter();
  @Input() selectedticket: any = []
  @Input() isUpdate: boolean = false
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  commonUtil: any = CommonUtil
  statusList: any[] = [];
  nodes: any = [];
  productList: any = [];
  customerList: any = [];
  ticketPriorityList: any = [];
  ticketCategoryList: any = [];
  ticketSourceList: any = [];
  isUpdating = false;
  ticket: any;
  title: any = '';
  searchForm = this.fb.group({
    keyword: ''
  });
  keyword = '';
  form: FormGroup = new FormGroup({});
  selectedDelegate: any = null
  setOfCheckedId = new Set<number>();
  setUserChecked: any[] = [];
  setCustomerChecked: any[] = [];
  listOfCurrentPageData: any
  checked = false;
  indeterminate = false;
  checkErrorAmoutUpload:any = false;
  userGroup: any;
  checkErrorUpload: any = false;
  userList: any;
  contactList: any = [];
  ticketStateList: any = [];
  userQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    departmentIds: '',
    orderBy: '',
    sortBy: '',
    keyword: ''
  };
  searchUser = {
    pageSize: PAGINATION.SIZE_DEFAULT,
    total: 0,
  }
  total: any = 0;
  ticketTitleMaxLenght = LENGTH_VALIDATOR.TICKET_TITLE_LENGTH.MAX
  ticketDescreiptionMaxLenght = LENGTH_VALIDATOR.TICKET_DESCRIPTION_LENGTH2.MAX
  enableContactSelectBox: any = false;
  profile: any = null;
  showDepartment: boolean = false;
  nodeDapartment: any;
  idFile: any = [];
  checkedItem: boolean = true;
  statusCT: any = [];
  statusCT1: any = [];
  customer_id: any = "";
  contact_id: any = "";
  isShowDescription: boolean = false;
  showSearch: boolean = true;
  errorTitle: boolean = false;
  searchChangeUser$ = "";
  isLoadingUser = false;
  limit: number = 40;
  cusHasCreatedName: any;
  cusHasCreatedId: any;
  showCustomer: boolean = false;
  idProduct: any = [];
  isUploading: boolean = false;
  constructor(
    private fb: FormBuilder,
    private ticketService: TicketService,
    private productService: ProductService,
    private ticketSourceService: TicketSourceService,
    private ticketCategoryService: TicketCategoryService,
    private ticketPriorityService: TicketPriorityService,
    private customerService: CustomerService,
    private userService: UserService,
    private contacterService: ContactService,
    private ticketStateService: TicketStateService,
    private router: Router,
    private modalService: NzModalService,
    private localStorage: LocalStorageService,
    private authService: AuthService,
    private departmentService: DepartmentService,
    private toast: ToastService) { }

  userQuestionUpdate = new Subject<string>();
  ngOnInit() {
    this.userQuestionUpdate.pipe(
      debounceTime(800),
      distinctUntilChanged())
      .subscribe(value => {
        if (this.searchChangeUser$ != value) {
          this.onSearchData(value)
        }

      });
    this.loadDataForCreateTicket();
    this.initForm();
    this.getcurrentUser();
    this.loadDataCustomer();
    // this.loadDataDapartment();
    // this.loadTicketCategory();
    // this.loadTicketPriority();
  }
  loadDataCustomer() {
    this.ticketService.customer(this.limit, this.keyword, false).subscribe(res => {
      this.customerList = res.body?.data;
    })
  }
  onSearch() {
    this.keyword = this.searchForm.value.keyword
    this.loadDataCustomer();

  }
  checkTitle() {
    if (this.form.value.title.trim() == '') {
      this.errorTitle = true;
    }
    else {
      this.errorTitle = false;
    }

  }
  onExisted() {
    this.searchForm.patchValue({ keyword: '' })
    this.keyword = '';
    this.loadDataCustomer()
  }
  loadDataForCreateTicket() {
    this.ticketService.loadMultipleDataForTicketDetail().subscribe(res => {
      this.nodes = this.createDataTree(res[0].body?.data);
      this.ticketPriorityList = res[1].body?.priority.data;
      this.ticketSourceList = res[1].body?.source.data;
      this.ticketCategoryList = res[1].body?.category.data;
      this.ticketStateList = res[1].body?.state.data;
      this.loadUser();
    })
  }
  loadUser() {
    const param = {
      name: this.searchChangeUser$,
      limit: this.searchUser.pageSize,
    }
    this.ticketService.getAllAssignee(param, false).subscribe(res => {
      this.userList = res?.body?.data;
      this.searchUser.total = res.body?.meta?.pagination?.total || 0;
      this.isLoadingUser = false;
      if (this.profile?.id && this.profile?.name && !this.searchChangeUser$) {
        const exist = this.userList?.some((item: any) => item?.id == this.profile?.id);
        if (!exist) {
          this.userList.push({ id: this.profile?.id, username: this.profile?.username, name: this.profile?.name })
          this.searchUser.total++;
        }
        this.userList.sort((a: any, b: any) => a.name.localeCompare(b.name));
      }

    })
  }
  initForm(): void {
    this.profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);

    this.form = this.fb.group({
      title: ['', [Validators.required, Validators.maxLength(this.ticketTitleMaxLenght), Validators.pattern('^([a-zA-Z0-9._(),?:;\'"&!ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ/-\\s]+[" "]*)$')]],
      product_id: ['', [Validators.required]],
      // state_id: ['', [Validators.required]],
      priority_id: ['', [Validators.required]],
      category_id: ['', [Validators.required]],
      // sla_id: ['', [Validators.required]],
      customer_id: ['', []],
      // require_id: ['', [Validators.required]],
      delegate_id: ['', []],
      require_id: [{ value: this.profile.id, disabled: false }, [Validators.required]],
      department_id: [],
      description: [''],
      // source_id: ['', [Validators.required]],
      contact_id: ['', []],
      deputy: [{ value: '', disabled: true }],
      fileIds: this.idFile
    });
  }
  getcurrentUser() {
    const profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
    if (profile) {
      if (profile?.is_super_admin == 1) {
        this.showDepartment = true
      }
      else {
        this.showDepartment = false
      }
    }
  }
  loadDataDapartment() {
    this.departmentService.getAllDepartments({ data_context: 'manage-ticket' }, true).subscribe(res => {
      this.nodeDapartment = res?.body;
    })
  }
  loadTicketCategory() {
    const params = {
      pageIndex: '',
      pageSize: '',
    }
    this.ticketCategoryService.search(params, true).subscribe(res => {
      this.ticketCategoryList = res.body?.data?.filter(item => item.status == 1);
    });
  }

  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.name, key: aData.id, isLeaf: true });

    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
      }
    });

    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };

  loadCustomer() {
    const params = {
      pageIndex: '',
      pageSize: '',
    }
    this.customerService.getCustomer(params, true).subscribe(
      (res) => {
        this.customerList = res.body?.data;
      }
    );
  }

  loadTicketSource() {
    const params = {
      pageIndex: '',
      pageSize: '',
      keyword: ''
    }
    this.ticketSourceService.search(params, true).subscribe(res => {
      this.ticketSourceList = res.body?.data
    });
  }

  loadTicketPriority() {
    const params = {
      pageIndex: '',
      pageSize: '',
      keyword: ''
    }
    this.ticketPriorityService.search(params, true).subscribe(res => {
      this.ticketPriorityList = res.body?.data?.filter(item => item.status == 1);
    });
  }

  loadContacterByCustomerId(customerId: any) {
    const params = {
      pageIndex: '',
      pageSize: '',
      keyword: ''
    }
    this.contacterService.getContactByCustomerIdTK(customerId, true).subscribe(res => {
      this.contactList = res.body?.data
      this.total = res.body?.meta?.pagination?.total;
    });
  }


  loadTicketState() {
    const params = {
      pageSize: 0
    }
    this.ticketStateService.search(params, true).subscribe((res: any) => {
      this.ticketStateList = res.body?.data
    });
  }
  addEvent(e: any) {
    let errArr = e.filter((f: any) => f !== undefined);
    if (errArr.length > 0) {
      this.checkErrorUpload = true;
    } else {
      this.checkErrorUpload = false;
    }
  }
  addEvent2(e: any){
    if(e){
        this.checkErrorAmoutUpload = true;
    }else{
        this.checkErrorAmoutUpload = false; 
    }
}

  onSubmit(): void {
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    if (this.checkErrorUpload) {
      return
    }
    if (this.checkErrorAmoutUpload) {
      return
    }
    if (this.isUploading) {
        return;
    }
    if (CommonUtil.extractContent(this.form.value.description)?.length > 5000) {
      return;
    }
    const ticket: any = {
      ...this.form.value,
      customer_id: this.customer_id ? this.customer_id : '',
      contact_id: this.form.value.contact_id ? this.form.value.contact_id : '',
      fileIds: this.idFile
    };

    delete ticket.deputy
    const body = CommonUtil.trim(ticket);
    this.isUpdating = true;
    let serviceEvent = this.ticketService.create(body, true)
    if (!this.errorTitle) {
      serviceEvent.subscribe(res => {
        if (res) {
          this.toast.success(`model.ticket.success.${this.isUpdate ? 'update' : 'create'}`);
          this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}`]);
        }
      }, (e: any) => {
        this.isUpdating = false;
      },
        () => {
          this.isUpdating = false;
        });
    }
  }
  getClassInvalid(name: string) {
    if (this.form.get(name)?.errors) {
      return 'invalid';
    }
    return '';
  }
  onChangeData(type: string, content: any): void {
    this.form.get(type)?.setValue(content)
  }

  onChangeData1(type: string, content: any): void {
    this.form.get(type)?.setValue(content);
    if (CommonUtil.extractContent(this.form.value.description)?.length > 5000) {
      this.isShowDescription = true
    }
    else if (CommonUtil.extractContent(this.form.value.description)?.length <= 5000) {
      this.isShowDescription = false
    }
  }


  onCancel(): void {
    this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}`]);
  }

  changeCustomer(es: any): void {
    if (es) {
      let a = this.customerList.find((e: any) => e.id == es);
      this.selectedDelegate = a.delegate_name
      this.onChangeData('deputy', a.delegate_name)
      this.onChangeData('delegate_id', a.delegate_id)
      this.enableContactSelectBox = true
      this.loadContacterByCustomerId(es)
    }
    else {
      this.enableContactSelectBox = false
      this.onChangeData('deputy', '')
      this.onChangeData('delegate_id', '')
    }
  }

  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.userQuery.pageIndex = pageIndex;
    this.userQuery.pageSize = pageSize;
    // this.getAllUsers();
  }
  updateCheckedSet(item: any, checked: boolean): void {
    this.setUserChecked = []
    this.setOfCheckedId.clear();
    if (checked) {
      this.setOfCheckedId.add(item.id);
      this.setUserChecked.push(item)
    } else {
      this.setOfCheckedId.delete(item.id);
      this.setUserChecked = this.setUserChecked.filter(i => { return i.id !== item.id })
    }
  }
  updateCheckedSetCustomer(item: any, checked: boolean): void {
    this.setCustomerChecked = []
    this.enableContactSelectBox = true;
    this.loadContacterByCustomerId(item.id)
    this.setOfCheckedId.clear();
    if (checked) {
      this.setOfCheckedId.add(item.id);
      this.setCustomerChecked.push(item)
    } else {
      this.setOfCheckedId.delete(item.id);
      this.setCustomerChecked = this.setCustomerChecked.filter(i => { return i.id !== item.id })
    }
  }


  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.contactList.filter((item: any) => !item.disabled);
    this.checked = listOfEnabledData.every((item: any) => this.setOfCheckedId.has(item.id));
    this.indeterminate = listOfEnabledData.some((item: any) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  onItemChecked(id: any, checked: boolean): void {
    this.statusCT = id.id;
    this.contact_id = id.id;
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
    this.onChangeData('contact_id', this.setUserChecked.map(e => e.id).toString())
  }

  onItemCheckedCustomer(id: any, checked: boolean): void {
    this.showSearch = false;
    this.statusCT1 = id.id;
    this.customer_id = id.id;
    this.selectedDelegate = id.delegate_name;
    this.onChangeData('deputy', id.delegate_name);
    this.onChangeData('delegate_id', id.delegate_id);
    this.loadContacterByCustomerId(this.customer_id);
    this.updateCheckedSetCustomer(id, checked);
    this.refreshCheckedStatus();
    this.enableContactSelectBox = true;

  }

  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.contactList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item, checked)
        this.onChangeData('code', this.setUserChecked)
      }
    });
    this.refreshCheckedStatus();
  }

  onRemoveTagUser(item: any) {
    this.statusCT = '';
    this.contact_id = '';
    this.setOfCheckedId.delete(item.id);
    this.setUserChecked = this.setUserChecked.filter(i => { return i.id !== item.id })
    this.refreshCheckedStatus();
  }
  onRemoveTagCustomer(item: any) {
    this.showSearch = true;
    this.customer_id = '';
    this.enableContactSelectBox = false;
    this.statusCT1 = '';
    this.setOfCheckedId.delete(item.id);
    this.onChangeData('deputy', '')
    this.onChangeData('delegate_id', '')
    this.setCustomerChecked = this.setCustomerChecked.filter(i => { return i.id !== item.id });
    this.setUserChecked = [];
    this.refreshCheckedStatus();
  }

  onCreateContact = () => {
    const base = CommonUtil.modalBase(ContactCreateComponent, { customerId: this.customer_id }, '30%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadContacterByCustomerId(result.value.customer_id)
        this.updateCheckedSet(result.value, true);
        this.refreshCheckedStatus();
        this.onChangeData('contact_id', result.value.id);
        this.statusCT = result.value.id;
      }
    });
  }
  checkFileUploading() {
    this.isUploading = true;
  }
  handleLoadFile(e: any) {
    this.idFile = e
    if(!this.idFile.includes(undefined)) {
        this.isUploading = false;
    }
  }
  onCreateCustomer() {
    const base = CommonUtil.modalBase(CustomerCreateComponent, { action: ACTION.CREATE }, '45%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        const params =  {
          pageIndex: '',
          pageSize: '',
          orderBy:'created_at',
          sortBy:'desc'
      }
      this.customerService.getCustomer(params, false).subscribe(res => {
          this.customerList = res?.body?.data;
          this.cusHasCreatedName = this.customerList[0].name; 
          this.cusHasCreatedId = this.customerList[0].id; 
          this.onItemCheckedCustomer(this.customerList[0],this.checkedItem);     
      })
        this.loadDataForCreateTicket()
      }
    });
  }
  onSearchData($event: any) {
    this.searchChangeUser$ = $event;
    this.searchUser.pageSize = PAGINATION.SIZE_DEFAULT
    this.loadUser();
  }
  loadMoreUser(): void {
    if (this.searchUser.pageSize > this.searchUser.total) {
      return
    }
    this.searchUser.pageSize += 10;
    this.isLoadingUser = true;
    this.loadUser();
  }
  onScroll(event:any){
    if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight - 10) {
      this.limit += 10
      this.loadDataCustomer();
    }
  }
  // changeProduct(event: any) {
  //   if (event) {
  //     this.showCustomer = true;
  //     this.loadDataCustomer();
  //     this.setCustomerChecked = [];
  //     this.idProduct = event;
  //   }
  //   else {
  //     this.showCustomer = false;
  //   }
  // }
}
