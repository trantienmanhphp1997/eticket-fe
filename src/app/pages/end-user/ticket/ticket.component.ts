import { Component, OnInit } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TicketService } from '@shared/service/ticket.service';
import { Router } from '@angular/router';
import CommonUtil from '@shared/utils/common-utils';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { SlaService } from '@shared/service/sla.service';
import { TicketPriorityService } from '@shared/service/ticket-priority.service';
import { TicketStateService } from '@shared/service/ticket-state.service';
import { ProductService } from '@shared/service/product.service';
import { TicketSourceService } from '@shared/service/ticket-source.service';
import { TicketCategoryService } from '@shared/service/ticket-category.service';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { LocalStorageService } from 'ngx-webstorage';
import { FormBuilder } from '@angular/forms';
import { environment } from '@env/environment';
@Component({
    selector: 'app-enduser-ticket',
    templateUrl: './ticket.component.html',
    styleUrls: ['./tickket.component.css']
})

export class TicketEndUserComponent implements OnInit {
    dateFormat = 'dd/MM/yyyy';
    date = null;
    searchForm = this.fb.group({
        keyword: [null]
    });
    // page data
    ticketList: any = [];
    product_id: any;
    sla_id: any;
    fromDate: any;
    productList: any = [];
    toDate: any;
    priorityList: any = [];
    priority_id: any;
    category_id: any;
    categoryList: any = [];
    state_id: any;
    stateList: any = [];
    search = '';
    total: any = 0;
    source_id: any;
    sourceList: any = [];
    commonUtil: any = CommonUtil;
    url_domain = environment.fe_domain
    ticketQuery = {
        pageIndex: PAGINATION.PAGE_DEFAULT,
        pageSize: PAGINATION.SIZE_DEFAULT,
        pageSizeOptions: PAGINATION.OPTIONS,
        keyword: '',
        priority_id: '',
        sla_id: '',
        state_id: '',
        orderBy: '',
        sortBy: '',
        fromDate: '',
        toDate: '',
        product_id: '',
        source_id: '',
        category_id: ''
    }
    defaultCheckedNode: any;
    keyword = '';
    visible = false;
    statusList: any = [];
    setTimeSLAInterval:any;
    constructor(
        private ticketService: TicketService,
        private modalService: NzModalService,
        private router: Router,
        private fb: FormBuilder,
        private slaService: SlaService,
        private ticketPriority: TicketPriorityService,
        private ticketState: TicketStateService,
        private serviceProductService: ProductService,
        private sourceService: TicketSourceService,
        private categoryService: TicketCategoryService,
        private localStorage: LocalStorageService,
    ) { }

    ngOnInit() {
        this.dateFormat = 'dd/MM/yyyy';
        const profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
        if (profile) {
            this.loadData();
            this.getAllDataTicketConfig()
            this.getIntervalSLATime()
        }

    }

    loadData() {
        this.ticketService.getListTicketEndUser(this.ticketQuery, true).subscribe((res) => {
         
            this.ticketList = res.body?.data?.map((dept: any) => {
                return {
                    ...dept, disabled: dept.dept_code === 'TCT', slaTime: this.convertTimeSla(dept)
                }
            })
            this.total = res.body?.meta?.pagination?.total;
        });
    }
    onChangeFromDate(event: any) {
        this.fromDate = event;
        console.log(this.fromDate);
        
    }
    onChangeToDate(event: any) {
        this.toDate = event;
    }
    onQuerySearch(params: any): void {
        const { pageIndex, pageSize } = params;
        this.ticketQuery.pageIndex = pageIndex;
        this.ticketQuery.pageSize = pageSize;
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
      
        this.loadData();
    }
    onDetail(ticketId: any) {
        this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}/${ROUTER_UTILS.endUserTicket.detail}/${ticketId}`]);
    }
    afterClose() {

    }
    onSearch(event: any) {
        if (event) {
            this.ticketQuery.keyword = event.target?.value.trim() || null;
            this.sla_id = '';
            this.priority_id = '';
            this.state_id = '';
            this.toDate = '';
            this.fromDate = '';
            this.product_id = '';
            this.source_id = '';
            this.category_id = '';
            this.ticketQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
            this.loadData();
        }
    }
    onSearchButton() {

    }
    onCreateTicket() {
        this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}/${ROUTER_UTILS.endUserTicket.create}`]);
    }

    // getAllServiceProductData() {
    //     this.serviceProductService.getAllWithAlphabet(true).subscribe((res) => {
    //         this.productList = res.body?.data?.filter((item: any) => item.status == 1);
    //     });
    // }
    getAllDataTicketConfig() {
        this.ticketService.loadMultipleDataForTicketDetail().subscribe(res => {
            this.productList = res[0].body?.data;
            this.sourceList = res[1].body?.source.data;
            this.categoryList = res[1].body?.category.data?.filter((item:any) => item.status == 1);
            this.stateList = res[1].body?.state.data?.filter((item:any) => item.status == 1);
            this.priorityList = res[1].body?.priority.data?.filter((item:any) => item.status == 1);
            this.stateList.push({ object: 'TicketState', name: 'Đã hủy', id: 'cancelled_ticket', status: 1 })
        });
    }
    onChangeSla(event: any) {
        this.sla_id = event;
    }
    onChangePriority(event: any) {
        this.priority_id = event;
    }
    onChangeState(event: any) {
        this.state_id = event;
    }
    onChangeServiceProduct(event: any) {
        this.product_id = event;
    }
    onChangeSource(event: any) {
        this.source_id = event;
    }
    onChangeCategory(event: any) {
        this.category_id = event;
    }
    onFilterData() {
        this.ticketQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
        if (this.sla_id || this.priority_id || this.state_id || this.fromDate || this.toDate || this.product_id || this.source_id || this.category_id) {
            this.ticketQuery.keyword = '';
            this.searchForm.patchValue({
                keyword: null,
            })
            this.ticketQuery.sla_id = this.sla_id;
            this.ticketQuery.priority_id = this.priority_id;
            this.ticketQuery.state_id = this.state_id;
            this.ticketQuery.product_id = this.product_id;
            this.ticketQuery.source_id = this.source_id;
            this.ticketQuery.category_id = this.category_id;
            this.ticketQuery.fromDate = this.fromDate ? this.fromDate.getFullYear() + '-' + String(this.fromDate.getMonth() + 1).padStart(2, '0') + '-' + String(this.fromDate.getDate()).padStart(2, '0') : '';
            this.ticketQuery.toDate = this.toDate ? this.toDate.getFullYear() + '-' + String(this.toDate.getMonth() + 1).padStart(2, '0') + '-' + String(this.toDate.getDate()).padStart(2, '0') : '',
                this.ticketService.getListTicketEndUser(this.ticketQuery, true).subscribe((res) => {
                    this.ticketList = res.body?.data;
                    this.total = res.body?.meta?.pagination?.total;
                });
        } else {
            this.ticketQuery.sla_id = this.sla_id;
            this.ticketQuery.priority_id = this.priority_id;
            this.ticketQuery.state_id = this.state_id;
            this.ticketQuery.product_id = this.product_id;
            this.ticketQuery.source_id = this.source_id;
            this.ticketQuery.category_id = this.category_id;
            this.ticketQuery.fromDate = this.fromDate ? this.fromDate.getFullYear() + '-' + String(this.fromDate.getMonth() + 1).padStart(2, '0') + '-' + String(this.fromDate.getDate()).padStart(2, '0') : '';
            this.ticketQuery.toDate = this.toDate ? this.toDate.getFullYear() + '-' + String(this.toDate.getMonth() + 1).padStart(2, '0') + '-' + String(this.toDate.getDate()).padStart(2, '0') : '',
                this.ticketService.getListTicketEndUser(this.ticketQuery, true).subscribe((res) => {
                    this.ticketList = res.body?.data;
                    this.total = res.body?.meta?.pagination?.total
                });
        }
    }
    onResetFilter() {
        this.sla_id = '';
        this.priority_id = '';
        this.state_id = '';
        this.toDate = '';
        this.fromDate = '';
        this.product_id = '';
        this.source_id = '';
        this.category_id = '';
        this.onFilterData();
    }
    convertTimeSla(dept: any) {
        let timeSla = dept.resolution_time_require;
        let convertSeconds = timeSla * 60;
        return CommonUtil.convertTime(convertSeconds);
    }
    
    open(): void {
        this.visible = true;
    }

    close(): void {
        this.visible = false;
    }
    getTimeRemain(time: any) {
        var label = ''
        if (time == 0) {
          label = 'Hết hạn ';
          return label;
        } else if (time < 0) {
          label = 'Quá hạn ';
        }
        var seconds = Math.abs(Math.floor(time))
        return label + CommonUtil.convertTime(seconds)
      }
      getIntervalSLATime() {
        this.setTimeSLAInterval=setInterval(()=>{
          this.ticketList.forEach((item: any, key: number) => {
            item.resolution_remaining_time--;
            this.ticketList[key].timeRemainSla=this.getTimeRemain(item.resolution_remaining_time)
          })
        }, 1000);
      }
}