import { Component, Input, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TicketService } from '@shared/service/ticket.service';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { Router } from '@angular/router';
import CommonUtil from '@shared/utils/common-utils';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { FILE_DOMAIN } from '@shared/constants/base.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TicketFlowComponent } from 'src/app/pages/ticket/ticket-detail/ticket-flow/ticket-flow.component';
import { ModalCustomComponent } from '@shared/components/modal-custom/modal-custom.component';
import { environment } from '@env/environment';
import { saveAs } from 'file-saver';
import { AuthService } from '@shared/service/auth/auth.service';
import { UploadComponent } from '@shared/components/upload/upload.component';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { FileService } from '@shared/service/file.service';
import { MAX_FILE_SIZE, FILE_SIZE, LIMIT_FILE } from '@shared/constants/file.constant';

@Component({
    selector: 'app-ticket-detail',
    templateUrl: './ticket-detail.component.html',
    styleUrls: ['./ticket-detail.component.css']
})

export class TicketEndUserDetailComponent implements OnInit {
    @Output() emitterError: any = new EventEmitter();
    @Output() emitter: any = new EventEmitter();
    @Output() emitterErrorComment: any = new EventEmitter();
    @Output() emitterComment: any = new EventEmitter();
    @ViewChild('upload') uploadFileComp!: UploadComponent;
    @ViewChild('uploadComment') uploadFileComment!: UploadComponent
    domainFile = environment.fileDomain
    ticketId: any = [];
    ticketData: any = [];
    involvedUserList : any = [];
    tagUserComment:any=[];
    tagUserReplyComment:any=[];
    titleWorkflow: any;
    delFile: any = false;
    isShowDescription: boolean = false;
    isShowDescription1: boolean = false;
    isDescription: boolean = false;
    dataFile: any;
    description: any = [];
    listComment: any = [];
    timeSlaRemain: any;
    slaTime: any;
    childId: any = [];
    isShoweditor: any = [];
    commonUtil: any = CommonUtil;
    form: FormGroup = new FormGroup({});
    formComment: FormGroup = new FormGroup({});
    replyComment: FormGroup = new FormGroup({});
    checkComment: boolean = true;
    checkReplyComment: boolean = true;
    title: any;
    LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
    idFile: any = [];
    idFileComment: any = [];
    idFileComment2: any = [];
    checkErrorUpload: any = false;
    checkErrorUploadComment: any = false;
    toastService: any;
    cancellation_reason: any;
    infoUser: any = [];
    setTimeSLAInterval: any;
    file: number = 0;
    commentId: any;
    commentIdReply: any;
    content: any;
    contentReply: any;
    checkErrorAmoutUpload: any = false;
    isLoadingComment = false;
    isDisable: boolean = false;
    isDisableReply: boolean = false;
    isUploading: boolean = false;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private ticketService: TicketService,
        private fb: FormBuilder,
        private toast: ToastService,
        private modalService: NzModalService,
        private authService: AuthService,
        private localStorage: LocalStorageService,
        private fileService: FileService,
    ) {
        this.route.paramMap.subscribe((res) => {
            this.ticketId = res.get('id') || '';
        });

    }

    ngOnInit() {
        this.loadDataById();
        this.initForm();
        this.getComment();
        this.formComment = this.fb.group({
            content: ['', [Validators.required, , Validators.maxLength(LENGTH_VALIDATOR.IDS_MAX_LENGTH.MAX)]],
            fileIds: this.idFile,
            tagged_users:[]
        });
        this.replyComment = this.fb.group({
            contentReply: ['', [Validators.required, , Validators.maxLength(LENGTH_VALIDATOR.IDS_MAX_LENGTH.MAX)]],
            fileIds: this.idFile,
            tagged_users:[]
        })
        this.infoUser = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
        this.loadDataInvoledUserTicket();
    }
    loadDataById() {
        this.isShowDescription = false;
        const param = {
            include: 'files,workflow'
        }
        this.ticketService.getTicketEndUserById(param, this.ticketId, true).subscribe(res => {
            if (res) {
                this.ticketData = res.body?.data;
                this.dataFile = this.ticketData.files.data.map((dept: any) => {
                    return {
                        ...dept, size2: this.converMB(dept.size),
                    }

                })

                this.titleWorkflow = this.ticketData?.workflow?.data?.name;
                this.convertTimeSla(this.ticketData.resolution_time_require);
                this.getIntervalSLATime()
                this.initForm();
            }

        });
    }
    loadDataInvoledUserTicket(){
        const param = {
            data_context: 'filter_assignee',
            ticket_id: this.ticketId,
            relevant_users : 1,
        }
        this.ticketService.getAllAssignee(param,true).subscribe(res=>{
            this.involvedUserList = res.body?.data;

        })
    }
    getTimeRemain(time: any) {
        var label = ''
        if (time == 0) {
            label = 'Hết hạn ';
            return label;
        } else if (time < 0) {
            label = 'Quá hạn ';
        }
        var seconds = Math.abs(Math.floor(time))
        return label + CommonUtil.convertTime(seconds)
    }
    getIntervalSLATime() {
        this.setTimeSLAInterval = setInterval(() => {
            this.ticketData.resolution_remaining_time--;
            this.timeSlaRemain = this.getTimeRemain(this.ticketData.resolution_remaining_time)
        }, 1000);
    }
    size: any;
    converMB(dept: any) {
        return this.size = Math.round(Number(dept) * 0.0009765625 * 100) / 100
    }
    initForm(): void {

        this.form = this.fb.group({
            title: [{ value: this.ticketId ? this.ticketData?.title : null, disabled: this.ticketData?.ticket_status_type != 1 ? true : false }, [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.TITLE_MAX_LENGTH.MAX)]],
            description: [{ value: this.ticketId ? this.ticketData?.description : null, disabled: this.ticketData?.ticket_status_type != 1 ? true : false }],
            fileIds: this.idFile,
        });
    }
    showDiscription() {
        if (this.ticketData.ticket_status_type == 1) {
            this.isShowDescription = true;
        } else {
            this.isShowDescription = false;
        }
    }
    showDiscription1() {
        if (this.ticketData.ticket_status_type == 1) {
            this.isShowDescription1 = true;
        } else {
            this.isShowDescription1 = false;
        }
    }

    onCancel() {
        this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}`]);
    }
    onChangeData(type: string, content: string): void {
        this.form.get(type)?.setValue(content);
        if (CommonUtil.extractContent(this.form.value.description)?.length > 5000) {
            this.isDescription = true
        }
        else if (CommonUtil.extractContent(this.form.value.description)?.length <= 5000) {
            this.isDescription = false
        }
    }
    addEvent(e: any) {
        let errArr = e.filter((f: any) => f !== undefined);
        if (errArr.length > 0) {
            this.checkErrorUpload = true;
        } else {
            this.checkErrorUpload = false;
        }
    }
    addEvent2(e: any){
        if (e) {
            this.checkErrorAmoutUpload = true;
        } else {
            this.checkErrorAmoutUpload = false;
        }
    }
    addEventComment(e: any, type?: number) {
        if (e.length > 0 && type == 0) {
            this.isDisable = true;
        }
        if (e.length > 0 && type == 1) {
            this.isDisableReply = true;
        }
        let errArr = e.filter((f: any) => f !== undefined);
        if (errArr.length > 0) {
            this.checkErrorUploadComment = true;
        } else {
            this.checkErrorUploadComment = false;
        }
    }
    checkFileUploading() {
        this.isUploading = true;
    }
    handleLoadFileComment(e: any) {
        this.idFileComment = e
        if (!this.idFileComment.includes(undefined)) {
            this.isUploading = false;
        }
        if (this.idFileComment.length > 0) {
            this.checkComment = false;
        }
    }
    handleLoadFileComment2(e: any) {
        this.idFileComment2 = e
        if (!this.idFileComment2.includes(undefined)) {
            this.isUploading = false;
        }
        if (this.idFileComment2.length > 0) {
            this.checkComment = false;
        }
    }
    handleLoadFile(e: any) {
        this.idFile = e
        if (!this.idFile.includes(undefined)) {
            this.isUploading = false;
        }
    }
    onSubmit() {
        // this.uploadFileComp.deleAllFile();
        if (this.form.invalid) {
            CommonUtil.markFormGroupTouched(this.form);
            return
        }
        if (this.isDescription) {
            return;
        }
        if (this.isUploading) {
            return;
        }
        this.isShowDescription = false
        if (this.checkErrorUpload) {
            return
        }
        const params = {
            title: this.form.value.title,
            description: this.form.value.description,
            fileIds: this.idFile,
        }
        this.ticketService.updateTicetEndUser(this.ticketId, params, true).subscribe(res => {
            if (res) {
                // this.loadDataById();
                location.reload();
                this.toast.success(`Cập nhật thành công`);
            }
        })
    }
    getStringLenght(value: any) {
        var str = value;
        if (!value) {
            str = 'Chưa có người nhận';
        }
        return str.length;
    }
    getDetail(data: any) {
        const base = CommonUtil.modalBase(TicketFlowComponent, { data: data, statusTicket: this.ticketData.ticket_status_type }, '40%');
        if (data?.excute_info?.process_type != 'todo') {
            const modal: NzModalRef = this.modalService.create(base);
            modal.afterClose.subscribe(result => { });
        }
    }
    onDelete(data: any) {
        const form = CommonUtil.modalBase(ModalCustomComponent, {
            title: 'XÁC NHẬN HỦY TICKET',
            content: `Bạn có chắc muốn hủy ticket <b>${this.commonUtil.limitWord(this.ticketData.title, 20)}</b> không?`,
            okText: 'close',
            requirer: `${this.ticketData.require_name}`,
            callback: () => {
                return {
                    success: true,
                };
            },
        }, '25%',
            false,
            false,
            true, { top: '20px' }, true
        );
        const modal: NzModalRef = this.modalService.create(form);

        modal.componentInstance.emitter.subscribe((result: any) => {
            if (result?.success) {
                const params = {
                    ticket_status_type: 4,
                    cancellation_reason: result.cancellation_reason
                }
                this.ticketService.updateTicetEndUser(data, params, true).subscribe((res: any) => {
                    this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}`]);
                    this.toast.success(`Hủy ticket thành công`);
                })
            } else {
                modal.close();
            }
        });
    }
    onCreateTicket() {
        this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}/${ROUTER_UTILS.endUserTicket.create}`]);
    }
    getComment() {
        this.ticketService.commentTicket(this.ticketId, true).subscribe(res => {
            this.listComment = res.body?.data;
        })
    }
    replySubmit(id: any) {
        this.uploadFileComment.deleAllFile();
        let trim = this.replyComment.value.contentReply.trim();
        if (this.checkErrorUploadComment || this.checkErrorAmoutUpload || this.isUploading) {
            return;
        } else {
            if (this.idFileComment2.length > 0) {
            }
            else {
                if (this.replyComment.invalid) {
                    CommonUtil.markFormGroupTouched(this.replyComment);
                    return;
                }
                if (trim == '') {
                    this.toast.error(`Không được nhập toàn khoảng trắng`);
                    return
                }
            }
            const commmentReply: any = {
                model_id: this.ticketId,
                reply_id: id,
                content: this.replyComment.value.contentReply,
                fileIds: this.idFileComment2,
                tagged_users: this.replyComment.value.tagged_users,
                scope: 'with_customer'
            };
            const body = CommonUtil.trim(commmentReply);
            let serviceEvent = this.ticketService.createComment(body, true)
            serviceEvent.subscribe(res => {
                if (res) {
                    this.getComment();
                    this.isShoweditor[id] = false;
                    this.replyComment.patchValue({
                        contentReply: "",
                        tagged_users : []
                    })
                    this.toast.success(`Thành công`);
                }
                else {
                    this.toast.error(`Lỗi không bình luận được `);
                }
            });
        }

    }
    changeComment(event: any) {
        if (event.length > 0) {
            this.checkComment = false;
        } else {
            this.checkComment = true;
        }
    }
    changeReplyComment(event: any) {
        if (event.length > 0) {
            this.checkReplyComment = false;
        } else {
            this.checkReplyComment = true;
        }
    }
    commentSubmit(id?: any) {
        let trim = this.formComment.value.content.trim();
        if (this.checkErrorUploadComment || this.checkErrorAmoutUpload || this.isUploading) {
            return;
        }
        else {
            if (this.idFileComment.length > 0) {
            }
            else {
                if (this.formComment.invalid) {
                    CommonUtil.markFormGroupTouched(this.formComment);
                    this.toast.error(`Trao đổi Ticket không được để trống`);
                    return;
                }
                if (trim == '') {
                    this.toast.error(`Không được nhập toàn khoảng trắng`);
                    return
                }
            }
            const commment: any = {
                model_id: this.ticketId,
                ...this.formComment.value,
                fileIds: this.idFileComment,
                scope: 'with_customer',
            };
            const body = CommonUtil.trim(commment);
            this.isLoadingComment = true;
            let serviceEvent = this.ticketService.createComment(body, true);
            this.uploadFileComment.deleAllFile();
            serviceEvent.subscribe(res => {
                if (res) {
                    this.toast.success(`Thành công`);
                    this.getComment();
                    this.isLoadingComment = false;
                    this.formComment.patchValue({
                        content: ' ',
                        tagged_users : []
                    })
                }
                else {
                    this.toast.error(`Lỗi không bình luận được `);
                }
            })
        }
    }
    showEditor(id: any) {
        this.childId = id;
        this.isShoweditor[id] = true;
        this.commentId = null;
    }
    closeComment(id: any) {
        this.isShoweditor[id] = false;
        this.commentId = null;
        this.replyComment.patchValue({
            contentReply: ''
        })
    }
    closeReplyComment(id: any) {
        this.isShoweditor[id] = false;
        this.commentIdReply = null;
        this.replyComment.patchValue({
            contentReply: ''
        })
    }
    convertTimeSla(dept: any) {
        let data = dept;
        let convertSeconds = data * 60;
        let dayTimeSla = Math.floor(convertSeconds / (3600 * 24)) + ' ' + 'ngày';
        if (dayTimeSla == 0 + ' ' + 'ngày') {
            dayTimeSla = '';
        } else {
            dayTimeSla = Math.floor(convertSeconds / (3600 * 24)) + ' ' + 'ngày';
        }
        let hoursTimeSla = Math.floor(convertSeconds % (3600 * 24) / 3600) + ' ' + 'giờ';
        if (hoursTimeSla == 0 + ' ' + 'giờ') {
            hoursTimeSla = '';
        } else {
            hoursTimeSla = Math.floor(convertSeconds % (3600 * 24) / 3600) + ' ' + 'giờ';
        }
        let minutesTimeSla = Math.floor(convertSeconds % 3600 / 60) + ' ' + 'phút';

        this.slaTime = dayTimeSla + ' ' + hoursTimeSla + ' ' + minutesTimeSla;
    }

    download(url: any, original: any, fileId: any) {
        this.ticketService.downloadFile(fileId).subscribe(res => {
                let tmp: any = res.body;
                let objectURL = URL.createObjectURL(tmp);
                let link = document.createElement("a");
                link.href = objectURL;
                link.download = original;
                link.click();
        }, error => console.trace(error));
    }
    editComment(comment: any) {
        this.commentId = comment.id;
        this.content = comment.content;
        this.tagUserComment = comment.tagged_user_ids
        for (let item in this.isShoweditor) {
            this.isShoweditor[item] = false;
        }
    }
    editCommentReply(comment: any) {
        this.commentIdReply = comment.id;
        this.contentReply = comment.content;
        this.tagUserReplyComment = comment.tagged_user_ids
        for (let item in this.isShoweditor) {
            this.isShoweditor[item] = false;
        }
    }
    updateComment(id: any) {
        if (!this.content) {
            this.deleteComment(id)
        } else {
            const param = {
                content: this.content,
                tagged_users:this.tagUserComment
            }
            const body = CommonUtil.trim(param)
            this.ticketService.updateComment(id, body).subscribe((res => {
                this.getComment();
                this.closeComment(id);
            }));
        }

    }
    updateReplyComment(id: any) {
        if (!this.contentReply) {
            this.deleteComment(id)
        } else {
            const param = {
                content: this.contentReply,
                tagged_users:this.tagUserReplyComment
            }
            const body = CommonUtil.trim(param)
            this.ticketService.updateComment(id, body).subscribe((res => {
                this.getComment();
                this.closeReplyComment(id);
            }));
        }

    }
    deleteComment(id: any) {
        const form = CommonUtil.modalBase(ModalComponent, {
            title: 'Xác nhận xóa',
            content: `Bạn có chắc muốn xóa bình luận không?`,
            okText: 'Xóa',
            callback: () => {
                return {
                    success: true,
                };
            },
        }, '25%',
            false,
            false,
            true, { top: '20px' }, true
        );
        const modal: NzModalRef = this.modalService.create(form);

        modal.componentInstance.emitter.subscribe((result: any) => {
            if (result?.success) {
                this.ticketService.deleteComment(id).subscribe((res => {
                    this.getComment();
                    this.closeComment(id);
                }));
            } else {
                modal.close();
            }
        });
    }

}
