
import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TicketCategoryService } from '@shared/service/ticket-category.service';
import CommonUtil from '@shared/utils/common-utils';
import { LENGTH_VALIDATOR, VALIDATORS } from '@shared/constants/validators.constant';
import { TicketService } from '@shared/service/ticket.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { Router } from '@angular/router';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { ProductService } from '@shared/service/product.service';
import { CustomerService } from '@shared/service/customer.service';
import { ContactService } from '@shared/service/contact.service';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { UserService } from '@shared/service/user.service';
import { HttpClient } from '@angular/common/http';
import { fromEvent, Subject } from 'rxjs';
import { CustomerCreateComponent } from 'src/app/pages/customer/customer-create/customer-create.component';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ACTION } from '@shared/constants/common.constant';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { ContactCreateComponent } from 'src/app/pages/ticket/contact-create/contact-create.component';
import {
    debounceTime,
    tap,
    distinctUntilChanged,
    filter
} from "rxjs/operators";
import { collapseTextChangeRangesAcrossMultipleVersions } from 'typescript';
import { orderBy } from 'lodash';

@Component({
    selector: 'app-ticket-create',
    templateUrl: './ticket-create.component.html',
    styleUrls: ['./ticket-create.component.css']
})

export class TicketEndUserCreateComponent implements OnInit {
    @Output() emitterError: any = new EventEmitter();
    @Output() emitter: any = new EventEmitter();
    @Output() emitterAmountError :any = new EventEmitter();
    @ViewChild('select', { static: true }) searchSelect: ElementRef | undefined;
    searchChange$ = "";
    checkAlert = false;
    commonUtil: any = CommonUtil
    title: any = '';
    statusCT: any = [];
    statusCT1: any = [];
    checkedItem: boolean = true;
    category_id: any = [];
    description = [];
    productList: any = [];
    customer_id: any;
    customerList: any = [];
    keyword = '';
    ticketCategoryList: any = [];
    searchForm = this.fb.group({
        keyword: [null]
    });
    showSearch: boolean = true;
    isShowDescription: boolean = false;
    selectedDelegate: any = null;
    VALIDATORS: any = VALIDATORS;
    contactList: any = [];
    checked = false;
    idFile: any = [];
    nodes: any = [];
    total: any = 0;
    totalCus: any = 0;
    pageSize: any = PAGINATION.SIZE_DEFAULT;
    userList: any = [];
    setUserChecked: any[] = [];
    contact_id: any = "";
    enableContactSelectBox: any = false;
    indeterminate = false;
    setCustomerChecked: any[] = [];
    setOfCheckedId = new Set<number>();
    form: FormGroup = new FormGroup({});
    checkErrorUpload: any = false;
    checkErrorAmoutUpload :any = false;
    LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
    fileId: any;
    isLoading = false;
    errorTitle: boolean = false;
    cusHasCreated: any;
    idProduct: any = [];
    isShowCustomer: boolean = true;
    limit: number = 20;
    cusHasCreatedName: any;
    cusHasCreatedId: any;
    profile: any = null;
    userQuestionUpdate = new Subject<string>();
    isLoadingUser = false;
    searchChangeUser$ = "";
    isUploading: boolean = false;
    userQuery = {
        pageIndex: PAGINATION.PAGE_DEFAULT,
        pageSize: PAGINATION.SIZE_DEFAULT,
        pageSizeOptions: PAGINATION.OPTIONS,
        departmentIds: '',
        orderBy: '',
        sortBy: '',
        keyword: ''
    };
    searchUser = {
        pageSize: PAGINATION.SIZE_DEFAULT,
        total: 0,
      }
    // getCustomer = (name: string): Observable<any> =>{
    //     if(this.searchChange$.value){
    //         this.pageSize=PAGINATION.SIZE_DEFAULT;
    //     } 
    //     let searchName=this.searchChange$.value ? `&searchName=${this.searchChange$.value}`:''
    //     return this.http
    //         .get(`${environment.gateway}customers?limit=${this.pageSize}${searchName}`)
    //         .pipe(
    //             catchError(() => of({ data: [] })),
    //             map((res: any) => {
    //                 this.totalCus=res.meta?.pagination?.total
    //                 return res.data
    //             })
    //         );
    // }

    constructor(
        private fb: FormBuilder,
        private ticketCategoryService: TicketCategoryService,
        private ticketService: TicketService,
        private toast: ToastService,
        private router: Router,
        private userService: UserService,
        private productService: ProductService,
        private customerService: CustomerService,
        private contacterService: ContactService,
        protected http: HttpClient,
        private modalService: NzModalService,
        private localStorage: LocalStorageService,
    ) {
    }
    // loadMore(): void {
    //     if(this.pageSize>this.totalCus){
    //         return
    //     }
    //     this.pageSize+=10;
    //     this.isLoading = true;
    //     const optionList$: Observable<string[]> = this.searchChange$
    //         .asObservable()
    //         .pipe(debounceTime(800))
    //         .pipe(switchMap(this.getCustomer));
    //     optionList$.subscribe(data => {
    //         this.isLoading = false;
    //         this.customerList = data;
    //     });
    // }
    cusQuestionUpdate = new Subject<string>();
    ngOnInit() {
        this.cusQuestionUpdate.pipe(
            debounceTime(800),
            distinctUntilChanged())
            .subscribe(value => {
                if(this.searchChange$!=value){
                   this.onSearchData(value) 
                }
            });
        this.loadUser();
        this.initForm();
        this.loadTicketCategory();
        this.loadServiceProduct();

        this.loadCustomer()
    }
    onSearchData($event: any) {
        this.searchChangeUser$ = $event;
        this.searchUser.pageSize = PAGINATION.SIZE_DEFAULT
        this.loadUser();
      }
    checkTitle() {
        if (this.form.value.title.trim() == '') {
            this.errorTitle = true;
        }
        else {
            this.errorTitle = false;
        }

    }
    loadMoreUser(): void {
        if (this.searchUser.pageSize > this.searchUser.total) {
          return
        }
        this.searchUser.pageSize += 10;
        this.isLoadingUser = true;
        this.loadUser();
      }
      loadUser() {
        const param = {
          name: this.searchChangeUser$,
          limit: this.searchUser.pageSize,
        }
        this.ticketService.getAllAssignee(param, false).subscribe(res => {
          this.userList = res?.body?.data;
          this.searchUser.total = res.body?.meta?.pagination?.total || 0;
          this.isLoadingUser = false;
          if (this.profile?.id && this.profile?.name && !this.searchChangeUser$) {
            const exist = this.userList?.some((item: any) => item?.id == this.profile?.id);
            if (!exist) {
              this.userList.push({ id: this.profile?.id, username: this.profile?.username, name: this.profile?.name })
              this.searchUser.total++;
            }
            this.userList.sort((a: any, b: any) => a.name?.localeCompare(b?.name));
          }
    
        })
      }
    initForm(): void {
        this.profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
        this.form = this.fb.group({
            title: ['', [Validators.required, Validators.maxLength(this.LENGTH_VALIDATOR.TICKET_TITLE_LENGTH.MAX), Validators.pattern('^([a-zA-Z0-9._(),?:;\'"&!ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ/-\\s]+[" "]*)$')]],
            category_id: [, [Validators.required]],
            description: [],
            product_id: [, [Validators.required]],
            customer_id: [],
            deputy: [{ value: '', disabled: true }],
            contact_id: [],
            delegate_id: ['', []],
            fileIds: this.idFile,
            require_id: [{ value: this.profile.id, disabled: false }, [Validators.required]],
        });
    }
    onExisted() {
        this.searchForm.patchValue({ keyword: '' })
        this.loadCustomer()
    }
    onSearch() {
        this.keyword = this.searchForm.value.keyword
        this.loadCustomer();

    }
    onItemCheckedCustomer(id: any, checked: boolean): void {
        this.showSearch = false;
        this.statusCT1 = id.id;
        this.customer_id = id.id;
        this.selectedDelegate = id.delegate_name;
        this.onChangeData('deputy', id.delegate_name);
        this.onChangeData('delegate_id', id.delegate_id);
        this.loadContacterByCustomerId(this.customer_id);
        this.updateCheckedSetCustomer(id, checked);
        this.refreshCheckedStatus();
        this.enableContactSelectBox = true;
    }

    updateCheckedSetCustomer(item: any, checked: boolean): void {
        this.setCustomerChecked = []
        this.enableContactSelectBox = true;
        this.loadContacterByCustomerId(item.id)
        this.setOfCheckedId.clear();
        if (checked) {
            this.setOfCheckedId.add(item.id);
            this.setCustomerChecked.push(item)
        } else {
            this.setOfCheckedId.delete(item.id);
            this.setCustomerChecked = this.setCustomerChecked.filter(i => { return i.id !== item.id })
        }
    }
    onRemoveTagCustomer(item: any) {
        this.showSearch = true;
        this.customer_id = '';
        this.enableContactSelectBox = false;
        this.statusCT1 = '';
        this.setOfCheckedId.delete(item.id);
        this.onChangeData('deputy', '')
        this.onChangeData('delegate_id', '')
        this.setCustomerChecked = this.setCustomerChecked.filter(i => { return i.id !== item.id });
        this.setUserChecked = [];
        this.refreshCheckedStatus();
    }
    onChangeData(type: string, content: string): void {
        this.form.get(type)?.setValue(content);
        if (CommonUtil.extractContent(this.form.value.description)?.length > 5000) {
            this.isShowDescription = true
        }
        else if (CommonUtil.extractContent(this.form.value.description)?.length <= 5000) {
            this.isShowDescription = false
        }
    }
    loadUsers() {
        this.userService.getUser(this.userQuery, true).subscribe((res) => {
            this.userList = res.body?.data;
            // this.total = res.body?.meta?.pagination?.total
        });
    }

    changeCustomer(es: any): void {
        if (es) {
            let a = this.customerList.find((e: any) => e.id == es);
            this.selectedDelegate = a.delegate_name
            this.onChangeData('deputy', a.delegate_name)
            this.onChangeData('delegate_id', a.delegate_id)
            this.enableContactSelectBox = true
            this.loadContacterByCustomerId(es)
        }
        else {
            this.enableContactSelectBox = false
            this.onChangeData('deputy', '')
            this.onChangeData('delegate_id', '')
        }
    }
    updateCheckedSet(item: any, checked: boolean): void {
        this.setUserChecked = []
        this.setOfCheckedId.clear();
        if (checked) {
            this.setOfCheckedId.add(item.id);
            this.setUserChecked.push(item)
        } else {
            this.setOfCheckedId.delete(item.id);
            this.setUserChecked = this.setUserChecked.filter(i => { return i.id !== item.id })
        }
    }
    onCreateCustomer() {
        const base = CommonUtil.modalBase(CustomerCreateComponent, { action: ACTION.CREATE, product_id: this.idProduct }, '45%');
        const modal: NzModalRef = this.modalService.create(base);
        modal.afterClose.subscribe(result => {
            if (result) {
                const params = {
                    pageIndex: '',
                    pageSize: '',
                    orderBy:'created_at',
                    sortBy:'desc'
                }
                this.customerService.getCustomer(params, false).subscribe(res => {
                    this.customerList = res?.body?.data;
                    this.cusHasCreatedName = this.customerList[0].name; 
                    this.cusHasCreatedId = this.customerList[0].id; 
                    this.onItemCheckedCustomer(this.customerList[0],this.checkedItem);     
                })
                this.onItemCheckedCustomer(this.customerList[0],this.checkedItem); 
            }
        });

    }

    onItemChecked(id: any, checked: boolean): void {
        this.statusCT = id.id;
        this.contact_id = id.id;
        this.updateCheckedSet(id, checked);
        this.refreshCheckedStatus();
        this.onChangeData('contact_id', this.setUserChecked.map(e => e.id).toString())
    }

    updateAllChecked(checked: boolean): void {
        this.indeterminate = false;
        this.contactList.forEach((item: any) => {
            if (!item.disabled) {
                this.updateCheckedSet(item, checked)
                // this.onChangeData('code', this.setUserChecked)
            }
        });
        this.refreshCheckedStatus();
    }

    onQuerySearch(params: any): void {
        const { pageIndex, pageSize } = params;
        this.userQuery.pageIndex = pageIndex;
        this.userQuery.pageSize = pageSize;
        // this.getAllUsers();
    }

    onRemoveTagUser(item: any) {
        this.setOfCheckedId.delete(item.id);
        this.setUserChecked = this.setUserChecked.filter(i => { return i.id !== item.id })
        this.refreshCheckedStatus();
    }

    refreshCheckedStatus(): void {
        const listOfEnabledData = this.contactList.filter((item: any) => !item.disabled);
        this.checked = listOfEnabledData.every((item: any) => this.setOfCheckedId.has(item.id));
        this.indeterminate = listOfEnabledData.some((item: any) => this.setOfCheckedId.has(item.id)) && !this.checked;
    }

    loadContacterByCustomerId(customerId: any) {
        const params = {
            pageIndex: '',
            pageSize: '',
            keyword: ''
        }
        this.contacterService.getContactByCustomerIdTK(customerId, true).subscribe(res => {
            this.contactList = res.body?.data
            this.total = res.body?.meta?.pagination?.total
        });
    }
    loadTicketCategory() {
        const params = {
            pageIndex: '',
            pageSize: 0,
        }
        this.ticketCategoryService.search(params, true).subscribe(res => {
            this.ticketCategoryList = res.body?.data?.filter(item => item.status == 1)
        });
    }
    loadServiceProduct() {
        this.productService.getAllWithAlphabet(true).subscribe(res => {
            this.nodes = this.createDataTree(res.body?.data?.filter(item => item.status == 1));
        });
    }

    createDataTree = (dataset: any) => {
        const hashTable = Object.create(null);
        dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.name, key: aData.id, isLeaf: false });

        const dataTree: any = [];

        dataset.forEach((aData: any) => {
            if (aData.parent_id) {
                hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
            }
        });

        dataset.forEach((aData: any) => {
            if (hashTable[aData.id].children.length == 0) {
                hashTable[aData.id].isLeaf = true
            }
            else {
                hashTable[aData.id].expanded = false
            }
            if (!aData.parent_id) {

                dataTree.push(hashTable[aData.id]);
            }
        });

        return dataTree;
    };


    loadCustomer() {
        this.ticketService.customer(this.limit, this.keyword, false).subscribe(res => {
            this.customerList = res.body?.data;
          })
    }
    onCancel() {
        this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}`]);
    }
    addEvent(e: any) {
        let errArr = e.filter((f: any) => f !== undefined);
        if (errArr.length > 0) {
            this.checkErrorUpload = true;
        } else {
            this.checkErrorUpload = false;
        }
    }
    addEvent2(e: any){
        if(e){
            this.checkErrorAmoutUpload = true;
        }else{
            this.checkErrorAmoutUpload = false; 
        }
    }

    onSubmit() {
        if (this.form.invalid) {
            CommonUtil.markFormGroupTouched(this.form);
            return
        }
        if (CommonUtil.extractContent(this.form.value.description)?.length > 5000) {
            return;
        }
        if (this.checkErrorUpload) {
            return
        }
        if (this.isUploading) {
            return
        }
        if (this.checkErrorAmoutUpload) {
            return
        }
        if (this.isShowDescription) {
            return;
        }
        const tickets = {
            ...this.form.value,
            fileIds: this.idFile,
            customer_id: this.customer_id ? this.customer_id : '',
        }
        const body = CommonUtil.trim(tickets);
        if (!this.errorTitle) {
            this.ticketService.createTicketEndUser(body, true).subscribe(res => {
                if (res.body?.data) {
                    this.toast.success(`Tạo ticket thành công`);
                    this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}`]);
                }
            });
        }
    }
    checkFileUploading() {
        this.isUploading = true;
    }
    handleLoadFile(e: any) {
        this.idFile = e
        if(!this.idFile.includes(undefined)) {
            this.isUploading = false;
        }
    }
    onScroll(event:any){
        if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight - 10) {
          this.limit += 10
          this.loadCustomer();
        }
      }
      onCreateContact = () => {
        const base = CommonUtil.modalBase(ContactCreateComponent, { customerId: this.customer_id }, '30%');
        const modal: NzModalRef = this.modalService.create(base);
        modal.afterClose.subscribe(result => {
          if (result && result?.success) {
            this.loadContacterByCustomerId(result.value.customer_id)
            this.updateCheckedSet(result.value, true);
            this.refreshCheckedStatus();
            this.onChangeData('contact_id', result.value.id);
            this.statusCT = result.value.id;
          }
        });
      }
    // onSearchData($event: any) {
    //     this.pageSize = PAGINATION.SIZE_DEFAULT
    //     this.searchChange$ = $event
    //     this.loadCustomer()
    // }
    // loadMore(): void {
    //     if (this.pageSize > this.totalCus) {
    //         return
    //     }
    //     this.pageSize += 10;
    //     this.isLoading = true;
    //     this.loadCustomer()
    // }
    // changeProduct(event: any) {
    //     if (event) {
    //         this.isShowCustomer = false;
    //         this.idProduct = event;
    //         this.setCustomerChecked = [];
    //         this.form.patchValue({
    //             customer_id: ''
    //         })
    //         this.loadCustomer(event)
    //     }
    //     else {
    //         this.isShowCustomer = true;
    //         this.form.patchValue({
    //             customer_id: ''
    //         })
    //     }
    // }
}
