import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { WelcomeRoutingModule } from './ticket-routing.module';
import { TicketEndUserComponent } from './ticket.component';
import { TicketEndUserCreateComponent } from './ticket-create/ticket-create.component';
import { TicketEndUserDetailComponent } from './ticket-detail/ticket-detail.component';



@NgModule({
    declarations: [
        TicketEndUserComponent,
        TicketEndUserCreateComponent,
        TicketEndUserDetailComponent,
    ],
    imports: [
      CommonModule,
      SharedModule,
      WelcomeRoutingModule,
    ],
    exports: [
        TicketEndUserComponent,
    ]
  })
  export class EndUserTicketModule { }
  