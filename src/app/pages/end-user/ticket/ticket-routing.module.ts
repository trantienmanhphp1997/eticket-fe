import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { TicketEndUserComponent } from './ticket.component';
import { TicketEndUserCreateComponent } from './ticket-create/ticket-create.component';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { TicketEndUserDetailComponent } from './ticket-detail/ticket-detail.component';
import { AUTH } from '@shared/constants/auth.constant';

const routes: Routes = [
  {
    path: '',
    component: TicketEndUserComponent,
    // canActivate: [AuthGuard],
    data: {
      // authorities: [AUTH.TICKET.LIST],
      title: 'Danh sách Ticket'
    }
  },
  {
    path: ROUTER_UTILS.endUserTicket.create,
    component: TicketEndUserCreateComponent,
    // canActivate: [AuthGuard],
    data: {
      // authorities: [AUTH.TICKET.CREATE],
      title: 'Tạo mới ticket'
    }
  },
    {
    path: `${ROUTER_UTILS.endUserTicket.detail}/:id`,
    component: TicketEndUserDetailComponent,
    // canActivate: [AuthGuard],
    data: {
      // authorities: [AUTH.TICKET.UPDATE],
      title: 'Chi tiết ticket'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }