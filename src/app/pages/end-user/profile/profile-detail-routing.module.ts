import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { ProfileDetailEuComponent} from './profile-detail.component';

const routes: Routes = [
  {
    path:`${ROUTER_UTILS.endUserProfile.detail}/:id`,
    component: ProfileDetailEuComponent,
    // canActivate: [AuthGuard],
    data: {
      // authorities: [],
      title: 'Thông tin cá nhân',
      // path:ROUTER_UTILS.profile.root
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileDetailEuRoutingModule { }
