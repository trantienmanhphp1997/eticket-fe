import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ProfileDetailEuRoutingModule } from './profile-detail-routing.module';
import { ProfileDetailEuComponent } from './profile-detail.component';




@NgModule({
  declarations: [ProfileDetailEuComponent],
  imports: [
    CommonModule,
    SharedModule,
    ProfileDetailEuRoutingModule,
  ],
  exports: [ProfileDetailEuComponent]
})
export class EndUserProfileDetailModule { }
