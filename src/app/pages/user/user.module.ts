import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { WelcomeRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { UserDetailComponent } from './user-create/user-detail.component';
import { GroupUserComponent } from './group-user/group-user.component';
import { GroupUserDetailComponent } from './group-user/group-user-detail/group-user-detail.component';
import { StatusUserComponent } from './status-user/status-user.component';
import { ChangePasswordComponent } from './change-password/change-password.component';


@NgModule({
    declarations: [
      UserComponent,
      UserDetailComponent,
      GroupUserComponent,
      GroupUserDetailComponent,
      StatusUserComponent,
      ChangePasswordComponent,
    ],
    imports: [
      CommonModule,
      SharedModule,
      WelcomeRoutingModule,
    ],
    exports: [
      UserComponent,
    ]
  })
  export class UserModule { }
  