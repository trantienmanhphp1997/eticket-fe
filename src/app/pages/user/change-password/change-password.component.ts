import { Component, Input, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '@shared/service/helpers/toast.service';
import { UserService } from '@shared/service/user.service';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import CommonUtil from '@shared/utils/common-utils';
import { NzInputModule } from 'ng-zorro-antd/input';
import { VALIDATORS } from '@shared/constants/validators.constant';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})

export class ChangePasswordComponent implements OnInit {
    @Input() selectedUser: any = []

    form: FormGroup = new FormGroup({});
    password :any;
    rep_password :any;
    LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
    checkRepPassword = '';
    passwordVisible = false;
    repPasswordVisible = false;
    VALIDATORS: any = VALIDATORS;

    constructor(
        private modalRef: NzModalRef,
        private fb: FormBuilder,
        private toast: ToastService,
        private userService: UserService,
    ) { }
    ngOnInit() {
        this.initForm();
    }

    initForm(): void {
        this.form = this.fb.group({
            password: ['',[Validators.required,Validators.minLength(LENGTH_VALIDATOR.PASSWORD_MIN_LENGTH.MIN),Validators.pattern(VALIDATORS.PASSWORD)]],
            rep_password : ['',[Validators.required,Validators.minLength(LENGTH_VALIDATOR.PASSWORD_MIN_LENGTH.MIN),Validators.pattern(VALIDATORS.PASSWORD)]],

        });
    }
    onCancel(): void {
        this.modalRef.close({
            success: false,
            value: null,
        });
    }
    onSubmit() {
        if (this.form.invalid) {
            CommonUtil.markFormGroupTouched(this.form);
            return;
          }
        if(this.form.value.rep_password != this.form.value.password){
            this.checkRepPassword = "Mật khẩu xác nhận chưa chính xác";
            return;
        }
        const param = {
            password: this.form.value.password
        };
        this.userService.update(this.selectedUser.id, param, true).subscribe(res => {
            this.toast.success(`Thay đổi mật khẩu thành công`);
            this.modalRef.close({
                success: true,
            });
        })
    }

}