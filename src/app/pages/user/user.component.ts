import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { UserService } from '@shared/service/user.service';
import { ACTION } from '@shared/constants/common.constant';
import CommonUtil from '@shared/utils/common-utils';
import { AUTH } from '@shared/constants/auth.constant';
import { environment } from '@env/environment';
import { TranslateService } from '@ngx-translate/core';
import { UserDetailComponent } from './user-create/user-detail.component';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DepartmentService } from '@shared/service/department.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { GroupUserComponent } from './group-user/group-user.component';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { StatusUserComponent } from './status-user/status-user.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UserGroupService } from '@shared/service/user-group.service';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { GroupUserDetailComponent } from './group-user/group-user-detail/group-user-detail.component';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { ModalConfirmComponent } from '@shared/components/modal-confirm/modal-confirm.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Output() emitter = new EventEmitter();

  // page data
  total: any = 0;
  formModal = {};

  searchForm = this.fb.group({
    keyword: [null]
  });
  //searchQuery
  userQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    departmentIds: '',
    orderBy: 'created_at',
    sortBy: 'desc',
    keyword: this.searchForm.value.keyword,
    include: 'userGroups',
    departmentId: '',
    userGroupId: '',
    status: ''

  };

  defaultCheckedKeys: any = [];
  defaultSelectedKeys: any = [];
  defaultExpandedKeys: any = [];
  listOfCurrentPageData: readonly any[] = [];
  username: any;
  name: any;
  email: any;
  phone: any;
  department_id: any = [];
  userGroupId: any = [];
  status: any;
  is_user_sync: any;
  commonUtil: any = CommonUtil;
  auth: any = AUTH;
  //checkItem
  indeterminate = false;
  setOfCheckedId = new Set<number>();
  checked = false;
  countClick = 0;

  //tree
  defaultCheckedNode: any = []
  nodes: any=[];


  totalRecord = 0;
  isCallFirstRequest: boolean = true;
  visible = false;
  form: FormGroup = new FormGroup({});
  dataList: any = [];
  departmentList: any = [];
  groupUserList: any = [];
  userList: any = [];
  currentUser='';
  styleTooltip = {
    'min-width': 'max-content',
  };
  ngOnInit() {
    this.loadData();
    this.getAllDepartments();
    this.is_user_sync = environment.is_user_sync;
  }

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private modalService: NzModalService,
    private departmentService: DepartmentService,
    private translateService: TranslateService,
    private toast: ToastService,
    private groupUserService: UserGroupService,
    private localStorage: LocalStorageService,
  ) { }

  loadData() {
    const profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
    if (profile) {
      this.currentUser = profile?.id;
    };
    this.userService.getUser(this.userQuery, true).subscribe((res) => {
      this.userList = res.body?.data?.map((dept: any) => {
          return {
            ...dept, 
            disabled: dept.id == this.currentUser 
          }
      })
      this.total = res.body?.meta?.pagination?.total
    });
  }

  onUpdate = (data: any) => {
    const base = CommonUtil.modalBase(UserDetailComponent, { action: ACTION.UPDATE, selectedUser: data, isUpdate: true }, '40%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }

  onDetail = (data: any) => {
    const base = CommonUtil.modalBase(UserDetailComponent, { action: ACTION.VIEW, selectedUser: data, isDetail: true }, '40%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }

  onDelete = (data: any) => {
    if(data.disabled){
      return
    }
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa người dùng <b>${data?.name}</b> không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let userId = data instanceof Set ? Array.from(data) : [data.id];
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.userService.delete(userId).subscribe((res: any) => {
          if (res) {
            this.toast.success(`Xóa người dùng thành công`);
            modal.close();
            this.loadData()
          };
        })
      } else {
        modal.close();
      }
    });
  }
  onMultiDelete(data: any) {
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa người dùng đã chọn không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let userIds = data instanceof Set ? Array.from(data) : [data.id];

    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.userService.delete(userIds, true).subscribe(res => {
          this.toast.success(`Xóa người dùng thành công`);
          this.setOfCheckedId = new Set<number>();
          modal.close();
          this.loadData()
        })
      } else {
        modal.close();
      }
    });
  }

  onSearch($event: any) {
    if ($event) {
      this.userQuery.keyword = this.searchForm.value.keyword.trim() || null;
      // this.department_id = '';
      // this.userGroupId = '';
      // this.status = '';
      this.searchForm.patchValue({
        keyword: this.userQuery.keyword
      })
      this.userQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
      this.loadData();
    }
  }

  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.userQuery.pageIndex = pageIndex;
    this.userQuery.pageSize = pageSize;
    this.loadData();
  }
  onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    this.userQuery.sortBy = params.sort.filter((e: any) => e.value !== null).map((e: any) => e.value?.slice(0, -3)).toString();
    this.userQuery.orderBy = params.sort.filter((e: any) => e.key !== null && e.key !== undefined && e.value !== null).map((e: any) => e.key).toString();
    this.loadData();
  }

  onRefresh(): void {
    this.userQuery = {
      pageIndex: PAGINATION.PAGE_DEFAULT,
      pageSize: PAGINATION.SIZE_DEFAULT,
      pageSizeOptions: PAGINATION.OPTIONS,
      departmentIds: '',
      orderBy: 'created_at',
      sortBy: 'desc',
      keyword: '',
      include: 'userGroups',
      departmentId: '',
      userGroupId: '',
      status: ''
    };
    this.getAllDepartments()
    this.loadData()
  }

  getAllDepartments() {
    const params = {
      limit: 10000,
      data_context: 'manage-user'
    }
    this.departmentService.getAllDepartments(params, true).subscribe(
      (res) => {
        this.nodes = res.body
      }
    );
  }
  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.dept_name, key: aData.id, isLeaf: false });

    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
      }
    });

    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };
  onCreate() {
    const base = CommonUtil.modalBase(UserDetailComponent, { isCreate: true }, '40%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        // this.getAllDepartments()
        this.loadData()
      }
    });
  }

  nzEvent(event: NzFormatEmitEvent): void {
    this.defaultCheckedNode = event.keys;
    this.userQuery.departmentIds = this.defaultCheckedNode ? this.defaultCheckedNode.toString() : '';
    this.userQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.loadData();
  }


  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.userList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }
  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }
  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
  }
  onUpdateStatus(data: any) {

  }
  onGroupUser(data: any) {
    const base = CommonUtil.modalBase(GroupUserComponent, { nodes: this.nodes, selectedUser: data }, '50%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        // this.getAllDepartments()
        this.loadData()
      }
    });
    modal.componentInstance.emitService.subscribe((emmitedValue: any) => {
      this.loadData()
    });
  }
  onGroupUserList(idUserArray: any) {
    const base = CommonUtil.modalBase(GroupUserDetailComponent, { nodes: this.nodes, idUserArray: idUserArray }, '40%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }
  getDepartName(departments: any) {
    var result = [];
    if (departments.length > 0) {
      result = departments.filter((item: any) => item.id != departments[0].id)
    }
    return result;
  }

  onSwitch(data: any) {
    if(data.disabled){
      return
    }
    if (data.status == 1) {
      data.status = false
    } else {
      data.status = true
    }
    const form = CommonUtil.modalBase(ModalConfirmComponent, {
      title: 'Xác nhận thay đổi trạng thái',
      content: `Bạn có chắc muốn thay đổi trạng thái <b>${data?.name}</b> không?`,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      false, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.userService.update(data.id, data ,true).subscribe(res => {
          if (res) {
            this.toast.success('Cập nhật thành công');
          } else {
            this.toast.success('Cập nhật thất bại');
          }
          modal.close();
        })
      } else {
        data.status = !data.status;
        modal.close();
      }
    });
  }

  onUpdatePassword(data: any) {
    const base = CommonUtil.modalBase(ChangePasswordComponent, { selectedUser: data }, '35%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }
  onFilter() {
    this.visible = true;
    this.countClick +=1;
    if(this.countClick ==1){
      this.getAllDepartmentData();
      this.getAllGroupUserData();
    }
    // this.getAllDepartments();
  }
  onClose() {
    this.visible = false;
  }
  getAllDepartmentData() {
    const params = {
      pageIndex: 0,
      pageSize: 0,
      limit: 0
    }
    this.departmentService.getDepartments(params, true).subscribe((res) => {
      this.departmentList = res.body?.data;
    });
  }
  getAllGroupUserData() {
    const params = {
      pageIndex: '',
      pageSize: 0,
    }
    this.groupUserService.getUserGroups(params, true).subscribe((res) => {
      this.groupUserList = res.body?.data;
    });
  }
  onChangeDepartment(event: any) {
    this.department_id = event;
  }
  onChangeGroupUser(event: any) {
    this.userGroupId = event;
  }
  onChangeStatus(event: any) {
    this.status = event;
  }
  onFilterData() {
    if (this.department_id || this.userGroupId || this.status) {
      this.userQuery.keyword = null;
      this.searchForm.patchValue({
        keyword: null,
      })
      this.userQuery.departmentIds =  '';
      this.userQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
      this.userQuery.departmentId = this.department_id;
      this.userQuery.userGroupId = this.userGroupId;
      this.userQuery.status = this.status;
      this.userService.getUser(this.userQuery, true).subscribe((res) => {
        this.userList = res.body?.data;
        this.total = res.body?.meta?.pagination?.total
      });
      this.visible = false;
    } else {
      this.visible = false;
      this.userQuery.departmentId = this.department_id;
      this.userQuery.userGroupId = this.userGroupId;
      this.userQuery.status = this.status;
      this.userService.getUser(this.userQuery, true).subscribe((res) => {
        this.userList = res.body?.data;
        this.total = res.body?.meta?.pagination?.total
      });
    }

  }
  onResetFilter() {
    this.department_id = '';
    this.userGroupId = '';
    this.status = '';
    this.onFilterData();
  }
  exportData() {
    const params= {
      departmentId :this.department_id,
      userGroupId :this.userGroupId,
      status :this.status,
    }
    this.userService.export(params)
  }


}