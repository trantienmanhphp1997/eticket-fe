import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { UserService } from '@shared/service/user.service';
import { NzModalRef } from 'ng-zorro-antd/modal';
import CommonUtil from '@shared/utils/common-utils';
import { ToastService } from '@shared/service/helpers/toast.service';

@Component({
    selector: 'app-status-user',
    templateUrl: './status-user.component.html',
    styleUrls: ['./status-user.component.css']
})

export class StatusUserComponent implements OnInit {
    @Input() data: any =[];

    statusname = '';
    isClose = false;

    constructor(
        private userService: UserService,
        private modalRef: NzModalRef,
        private toast: ToastService
      ) { }

    ngOnInit(): void {
        if( this.data.status == true){
            this.statusname ='khóa';
            this.isClose= !this.isClose;
        }
        else{
          this.statusname ='mở '
        }
    }
    onLock(status: any){
        const statusUser = {
            status:status
        }
        this.userService.update(this.data.id,statusUser,true).subscribe( res =>{
            this.toast.success(`Thay đổi trạng thái thành công`);
              this.modalRef.close({
                success: true,
              });
          })
    }
    onCancel(){
      this.modalRef.close({
        success: false,
        value: null,
      });
    }
}