import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ACTION } from '@shared/constants/common.constant';
import { UserService } from '@shared/service/user.service';
import CommonUtil from '@shared/utils/common-utils';
import { ToastService } from '@shared/service/helpers/toast.service';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { VALIDATORS } from '@shared/constants/validators.constant';
import { differenceInCalendarDays } from 'date-fns';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';
import { CommonService } from '@shared/service/common.service';
import { UserGroupService } from '@shared/service/user-group.service';
import { RoleService } from '@shared/service/role.service';
import { AuthService } from '@shared/service/auth/auth.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DepartmentService } from '@shared/service/department.service';
import { AUTH } from '@shared/constants/auth.constant';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})

export class UserDetailComponent implements OnInit {
  @Input() selectedUser: any = []
  @Input() isUpdate: boolean = false
  @Input() action: any = ACTION.CREATE
  @Input() isDetail: boolean = false
  @Input() isCreate: any;

  auth: any = AUTH;
  actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
  count = 0;
  date = null;
  username: any;
  viewEvent = ACTION.VIEW.event;
  name: any;
  email: any;
  numberPhone: any;
  checked: any = false;
  birth: any;
  gender: any;
  address: any;
  department_id = [];
  userGroupIds = [];
  groupUserList: any = [];
  roleList: any;
  departmentNode: any = [];
  status: any;
  // nodes = [];
  roleHasUserGroupId: any;
  groupUserDefaultId: any;
  isUpdating = false;
  form: FormGroup = new FormGroup({});
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  VALIDATORS: any = VALIDATORS;
  dateFormat = 'dd/MM/yyyy';
  today = new Date();
  userRole: any;
  disabledDate: any;
  departmentList : any = [];
  isLoading = false
  formInvalid = {
    phone: false,
    username: false,
    email: false,
  }
  constructor(
    private modalRef: NzModalRef,
    private fb: FormBuilder,
    private userService: UserService,
    private toast: ToastService,
    private commonService: CommonService,
    private userGroup: UserGroupService,
    private roleService: RoleService,
    private authService: AuthService,
    private departmentService : DepartmentService
  ) { }
  usernameExist = new Subject<string>();
  phoneExist = new Subject<string>();
  emailExist = new Subject<string>();

  ngOnInit() {
    console.log(this.selectedUser);

    this.usernameExist.pipe(
      debounceTime(600),
      distinctUntilChanged())
      .subscribe(value => {
        this.checkExist('username', value)
      });
    this.phoneExist.pipe(
      debounceTime(600),
      distinctUntilChanged())
      .subscribe(value => {
        this.checkExist('phone', value)
      });
    this.emailExist.pipe(
      debounceTime(600),
      distinctUntilChanged())
      .subscribe(value => {
        this.checkExist('email', value)
      });
    this.initForm()
    if(this.isUpdate){
      this.getDepartmentNode();
    }
    else{
      this.getAllDepartments();
    }
    this.getDataGroupUserDefault();
    this.getDataGroupUser();
    this.getListRoleData();
    this.getListRoleHasUserGroup();
    this.disabledDate = (current: Date): boolean => differenceInCalendarDays(current, this.today) > 0;
  }

  initForm(): void {
    this.form = this.fb.group({
      username: [
        {
          value: this.actionToShowData.includes(this.action.event) ? this.selectedUser.username : '',
          disabled: this.action.event === ACTION.VIEW.event || this.action.event === ACTION.UPDATE.event ? true : false
        },
        {
          validators: [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX), Validators.pattern('^[a-zA-Z0-9_.\s\+]*$')]
        }
      ],
      // usernameExist: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedUser.username : '', disabled: false }, { asyncValidators: [CustomInputValidator.hasExited('users', 'username', this.commonService, this.selectedUser?.username)], updateOn: 'blur' },
      // { asyncValidators: [CustomInputValidator.hasExited('users', 'username', this.commonService, this.selectedUser?.username)], updateOn: 'blur' }
      // ],
      name: [
        {
          value: this.actionToShowData.includes(this.action.event) ? this.selectedUser.name : '',
          disabled: this.isDetail||this.selectedUser.sync_code ? true : false
        },
        {
          validators: [Validators.required, CustomInputValidator.isName, Validators.maxLength(LENGTH_VALIDATOR.TITLE_MAX_LENGTH.MAX)]
        }
      ],
      email: [
        {
          value: this.actionToShowData.includes(this.action.event) ? this.selectedUser.email : '',
          disabled: this.isDetail ||this.selectedUser.sync_code ? true : false
        },
        {
          validators: [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.EMAIL_MAX_LENGTH.MAX), CustomInputValidator.isEmail]
        }
      ],
      // emailExist: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedUser.email : '', disabled: false },
      // { asyncValidators: [CustomInputValidator.hasExited('users', 'email', this.commonService, this.selectedUser?.email)], updateOn: 'blur' }
      // ],
      phone: [
        {
          value: this.actionToShowData.includes(this.action.event) ? this.selectedUser.phone : '',
          disabled: this.isDetail ||this.selectedUser.sync_code ? true : false
        },
        {
          validators: [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.PHONE_MAX_LENGTH.MAX), CustomInputValidator.isNumberPhone]
        }
      ],
      // phoneExist: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedUser.phone : '', disabled: false },
      // { asyncValidators: [CustomInputValidator.hasExited('users', 'phone', this.commonService, this.selectedUser?.phone)], updateOn: 'blur' }
      // ],

      birth: [this.actionToShowData.includes(this.action.event) ? this.selectedUser?.birth : null, []],
      gender: [this.actionToShowData.includes(this.action.event) ? this.selectedUser?.gender : "1", []],
      address: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedUser?.address : null, disabled: this.isDetail ? true : false }, [Validators.maxLength(LENGTH_VALIDATOR.ADDRESS_MAX_LENGTH.MAX), Validators.pattern('^[a-zA-Z0-9-_./ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ\s\+," "]*$')]],
      department_id: [this.actionToShowData.includes(this.action.event) ? this.selectedUser?.department_id : null, []],
      userGroupIds: [, []],
      status: [this.actionToShowData.includes(this.action.event) ? this.selectedUser?.status : 1, []],
      is_super_admin: [this.actionToShowData.includes(this.action.event) ? this.selectedUser?.is_super_admin == 1 : true ? false : null, []],
      role_ids: [{ value: [], disabled: true }, []]
    });

    if (this.action.event === ACTION.VIEW.event || this.action.event === ACTION.UPDATE.event) {
      let dataGroupId = this.selectedUser?.userGroups.data.map((item: any) => item.id)
      this.form.patchValue({ userGroupIds: dataGroupId ? dataGroupId : this.groupUserDefaultId })
    }
  }

  onChange(result: Date): void {

  }

  getDataGroupUser() {
    const params = {
      pageIndex: 0,
      pageSize: 0,
      is_default: 1
    }
    this.userGroup.getUserGroups(params, true).subscribe(res => {
      this.groupUserList = res.body?.data;
    });
  }
  getListRoleData() {
    const params = {
      pageIndex: 0,
      pageSize: 0,
      limit: 0
    }
    this.roleService.getRole(params, true).subscribe(res => {
      this.roleList = res.body?.data;
    });
  }

  getListRoleHasUserGroup() {
    const param = {
      userGroup: this.form.value.userGroupIds
    }
    if (this.form.value.userGroupIds?.length > 0) {
      this.roleService.getRoleHasUserGroup(param, true).subscribe(res => {
        this.roleHasUserGroupId = res.body?.data?.map((item: any) => item.id);
        this.form.get('role_ids')?.setValue(this.roleHasUserGroupId);
      });
    }
  }

  getDataGroupUserDefault() {
    const params = {
      pageIndex: 0,
      pageSize: 0,
      is_default: 1
    }
    this.userGroup.getDefaultUserGroup(params, true).subscribe(res => {
      this.groupUserDefaultId = res.body?.data?.map((item: any) => item.id);
      if (!this.actionToShowData.includes(this.action.event)) {
        this.form.get('userGroupIds')?.setValue(this.groupUserDefaultId);
      }
    });
  }

  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }

  onSubmit() {

    let el = document.getElementById('myDiv');
    el?.click();
    setTimeout(() => {
      if (this.form.invalid) {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.form.controls['phone'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.form.controls['name'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.form.controls['username'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.form.controls['email'].status == 'INVALID') {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      if (this.formInvalid.username||this.formInvalid.phone||this.formInvalid.email) {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      this.isLoading = true
      this.count += 1
      if(this.count == 1){
      const user: any = {
        ...this.form.value,
        gender: parseFloat(this.form.value.gender),
        // status : this.form.value.status ? this.form.value.status : 0
        department_id :this.form.value.department_id ? this.form.value.department_id : ''
      };
      const body = CommonUtil.trim(user);
      this.isUpdating = true;
      let serviceEvent = this.action.event === ACTION.UPDATE.event ? this.userService.update(this.selectedUser.id, body, true) : this.userService.create(body, true)
      serviceEvent.subscribe(res => {
        if (res.body?.data) {
          this.isLoading = false
          this.toast.success(`model.user.success.${this.isUpdate ? 'update' : 'create'}`);
          this.modalRef.close({
            success: true,
            value: user,
          });
        }
      }, (e: any) => {
        this.isUpdating = false;
      },
        () => {
          this.isUpdating = false;
        });
      }
    }, 300)

  }
  onUpdate() {
    this.action = ACTION.UPDATE;
    this.isDetail = false;
    this.initForm();
    this.getListRoleHasUserGroup();
  }
  getcurrentUser() {
    this.authService.myProfile().subscribe(res => {
      this.userRole = res?.body?.data.is_super_admin;
    });
  }
  checkExist(type: any, value: any) {
    if (!value) {
      switch (type) {
        case 'phone':
          this.formInvalid.phone=false;
          break;
        case 'email':
          this.formInvalid.email=false
          break;
        case 'username':
          this.formInvalid.username=false
          break;

        default:
          break;
      }
    }else{
      switch (type) {
        case 'phone':
          if(this.form.controls['phone'].status == 'VALID'){
            this.commonService.checkExisted('users', { phone: value }, false).then(
            (users: any) => {
              this.formInvalid.phone = (users && users.body === 1) ? true : false;
            })
          }
          break;
        case 'username':
          if(this.form.controls['username'].status == 'VALID'){
          this.commonService.checkExisted('users', { username: value }, false).then(
            (users: any) => {
              this.formInvalid.username = (users && users.body === 1) ? true : false;
            })
          }
          break;
        case 'email':
          if(this.form.controls['email'].status == 'VALID'){
          this.commonService.checkExisted('users', { email: value }, false).then(
            (users: any) => {
              this.formInvalid.email = (users && users.body === 1) ? true : false;
            })
          }
          break;
        default:
          break;
      }
    }

  }

  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.dept_name, key: aData.id, isLeaf: false });

    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
      }
    });

    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };
  getAllDepartments() {
    const params = {
      limit: 10000,
      data_context: 'manage-user'
    }
    this.departmentService.getAllDepartments(params, true).subscribe(
      (res) => {
        this.departmentList = res.body
      }
    );
  }

  getDepartmentNode() {
    this.departmentService.getAllDepartments({ selectedId: this.selectedUser.department_id, data_context: 'config-workflow-node' }, false).subscribe(
      (res: any) => {
        this.departmentList = res.body
      }
    );
  }
  scrollElementNode() {
    this.getDepartmentNode();
    if (this.form?.value.department_id) {
      setTimeout(() => {
        const el = document.getElementsByClassName('deptNode')[0]?.getElementsByClassName('ant-select-tree-node-selected')[0];
        el?.scrollIntoView()
      }, 500)
    }
    return true;
  }
}
