import { Component, Input, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserGroupService } from '@shared/service/user-group.service';
import { PAGINATION } from '@shared/constants/pagination.constants';
import CommonUtil from '@shared/utils/common-utils';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { ToastService } from '@shared/service/helpers/toast.service';

@Component({
    selector: 'app-group-user-detail',
    templateUrl: './group-user-detail-component.html',
    styleUrls: ['./group-user-detail.component.css']
})

export class GroupUserDetailComponent implements OnInit {

    @Input() selectedUser: any = [];
    @Input() idUserArray: any = [];
    @Input() data: any = [];
    @Input() isUpdate: boolean = false;

    form: FormGroup = new FormGroup({});
    userGroupList: any = [];
    group_ids: any = [];
    department_ids: any = [];
    user_ids: any = [];

    total: any = 0;
    pageIndex = PAGINATION.PAGE_DEFAULT;
    pageSize = PAGINATION.SIZE_DEFAULT;
    pageSizeOptions = PAGINATION.OPTIONS;
    nodes = [];

    constructor(
        private modalRef: NzModalRef,
        private userGroupService: UserGroupService,
        private fb: FormBuilder,
        private toast: ToastService

    ) { }

    ngOnInit(): void {
        this.getAllUserGroup();   
        this.initForm();
    }
    initForm(): void {
        this.form = this.fb.group({
            group_ids: [{value: this.isUpdate ? [this.data?.group_id]: [], disabled:this.isUpdate ? true: false}, [Validators.required]],
            department_ids: [{value: this.isUpdate ? this.data?.departments.map((element: any) => element.id) : [], disabled: false}]
        });
    }
    onCancel() {
        this.modalRef.close({
            success: false,
            value: null,
        });
    }

    getAllUserGroup() {
        const param = {
            data_context:'edit_user_to_group',
            group_ref_user_id : this.selectedUser.id,
            group_ref_group_id: this.data?.group_id
        }
        this.userGroupService.getAllUserGroup(param, true).subscribe((res) => {
            this.userGroupList = res.body?.data;
        });
    }

    onSubmit() {
        let arrUserId = Array.from(this.idUserArray);
        if (this.form.invalid) {
            CommonUtil.markFormGroupTouched(this.form);
            return;
        }
        if(this.isUpdate){
            const userGrop: any = {
                department_ids: this.form.value.department_ids,
                group_ids: this.data.group_id,
                user_ids : arrUserId?.length >0 ? arrUserId : this.selectedUser.id
              };
              this.userGroupService.getUserGroupUpdate(userGrop).subscribe( res =>{
                this.toast.success(`Cập nhật thành công`);
                this.modalRef.close({
                    success: true
                });
              })
        }
        else{
            let serviceEvent = this.userGroupService.addUserToGroup(this.group_ids = this.form.value.group_ids,
                this.department_ids = this.form.value.department_ids, this.user_ids = arrUserId?.length >0 ? arrUserId : this.selectedUser.id, true);
            serviceEvent.subscribe(res => {
                this.toast.success(`Gán nhóm thành công`);
                this.modalRef.close({
                    success: true
                });
            });
        }
        

    }

}
