import { Component, Input, OnInit, Output ,EventEmitter} from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import CommonUtil from '@shared/utils/common-utils';
import { GroupUserDetailComponent } from './group-user-detail/group-user-detail.component';
import { UserGroupService } from '@shared/service/user-group.service';
import { forEach } from 'lodash';
import { isNamedExportBindings } from 'typescript';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TranslateService } from '@ngx-translate/core';
import { ModalComponent } from '@shared/components/modal/modal.component';

@Component({
  selector: 'app-group-user',
  templateUrl: './group-user.component.html',
  styleUrls: ['./group-user.component.css']
})

export class GroupUserComponent implements OnInit {

  @Input() selectedUser: any = [];
  @Output() emitService :any = new EventEmitter();
  @Input()  nodes:any = [];
  groupByUserList: any = [];
  user_ids = [];
  departmentName: any;

  constructor(
    private modalRef: NzModalRef,
    private modalService: NzModalService,
    private userGroupService: UserGroupService,
    private toast: ToastService,
    private translateService: TranslateService,

  ) { }
  ngOnInit(): void {
    this.getAllGroupByUser();
  }

  getAllGroupByUser() {
    this.userGroupService.getUserGroupRefs(this.selectedUser.id.split(','), true).subscribe((res) => {
      this.groupByUserList = res.body?.data;
      console.log(this.groupByUserList);
    });
  }
  onCancel() {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  onDelete(data: any) {
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xóa nhóm người dùng',
      content:`Bạn có chắc chắn muốn xóa <b>${data.group_name}</b> không?`,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'25%',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    let groupUserId = data instanceof Set ? Array.from(data) : [data.group_id];
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        const params={
          group_ids:groupUserId,
          user_ids:this.selectedUser.id.split(','),
          remove_user: 1
        }
        this.userGroupService.removeGroupUser( params , true).subscribe((res: any) => {
          if (res) {
            this.getAllGroupByUser()
            this.toast.success(`Xóa nhóm thành công`);
            this.emitService.next(null);
          };
        })
      }
    });
  }
  onAddGroup() {
    const base = CommonUtil.modalBase(GroupUserDetailComponent, { nodes: this.nodes, selectedUser: this.selectedUser }, '40%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.getAllGroupByUser()
        this.emitService.next(null);
      }
    });
  }
  onUpdate(data: any) {
    const base = CommonUtil.modalBase(GroupUserDetailComponent, { nodes: this.nodes, selectedUser: this.selectedUser, data:data ,isUpdate: true}, '40%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.getAllGroupByUser()
        this.emitService.next(null);
      }
    });
  }
  getDepartName(departments: any) {
    var result = [];
    if (departments.length > 0) {
      result = departments.filter((item: any) => item.id != departments[0].id)
    }
    return result;
  }
}