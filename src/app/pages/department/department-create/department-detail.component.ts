
import { AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ACTION } from '@shared/constants/common.constant';
import { STATUS } from '@shared/constants/status.constants';
import { AUTH } from '@shared/constants/auth.constant';
import { LENGTH_VALIDATOR, VALIDATORS } from '@shared/constants/validators.constant';
import { CommonService } from '@shared/service/common.service';
import { DepartmentService } from '@shared/service/department.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import CommonUtil from '@shared/utils/common-utils';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-department-detail',
  templateUrl: './department-detail.component.html',
  styleUrls: ['./department-detail.component.scss']
})
export class DepartmentDetailComponent implements OnInit, AfterContentChecked {
  @Input() selectedDepartment: any = []
  @Input() isUpdate: boolean = false
  @Input() action: any = ACTION.CREATE
  // @Input() nodes: any
  @Input() provices: any
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  statusList: any[] = [];
  nodes: any = [];
  auth: any = AUTH;
  isUpdating = false;
  department: any;
  districts: any = [];
  wards: any = [];
  isShowView: boolean = false;
  message = '';
  showSearch: boolean = true;
  nameDeparment: any = [];
  formInvalid = {
    dept_code: false,

  }
  deptcodeExist = new Subject<string>();
  // title: any = 'Tạo mới đơn vị'
  form: FormGroup = new FormGroup({});
  actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
  userQuestionUpdate = new Subject<string>();
  searchValue = '';
  constructor(
    private fb: FormBuilder,
    private departmentService: DepartmentService,
    private commonService: CommonService,
    private modalRef: NzModalRef,
    private cdr: ChangeDetectorRef,
    private toast: ToastService,) {
    this.userQuestionUpdate
      .pipe(debounceTime(800), distinctUntilChanged())
      .subscribe((value) => {
        this.searchD()
      });

  }

  ngOnInit() {
    
    this.deptcodeExist.pipe(
      debounceTime(600),
      distinctUntilChanged())
      .subscribe(value => {
        this.checkExist('dept_code', value)
      });
    if (this.action.event === ACTION.VIEW.event) {
      this.isShowView = true
    }
    if (this.selectedDepartment.province_id) {
      this.getDistrictByProvice(this.selectedDepartment.province_id);
      this.getWardByDistrict(this.selectedDepartment.district_id)
    }

    this.initForm()
    this.getAllDepartments();
    this.getProVices();
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }
  getProVices (): void{
    const params = {
      keyword: ''
    }
    this.commonService.getProvinces(params, true).subscribe(
      (res: any) => {
        this.provices = res.body?.data
      }
    );
  }
  searchD() {
    const params = {
      keyword: this.form.value.search
    }
    if (this.form.value.search) {
      this.departmentService.getDepartments(params, true).subscribe(
        (res) => {
          this.nodes = this.createDataTree1(res.body?.data);
        }
      );
    }

  }


  createDataTree1 = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.dept_name, key: aData.id, isLeaf: false });

    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
      }
    });

    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = true
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };
  nzEvent(event: any): void {
    this.showSearch = false
    this.nameDeparment = event.nodes[0].origin.dept_name
  }
  close() {
    this.nameDeparment = '';
    this.showSearch = true;
    this.form.patchValue({
      search: null
    })
  }
  getAllDepartments() {
    const params = {
      excludeChild: this.selectedDepartment.id,
      data_context: 'manage-department'
    }
    this.departmentService.getAllDepartments(params, true).subscribe(
      (res) => {
        this.nodes = res?.body;
      }
    );
  }
  // createDataTree = (dataset: any) => {
  //   const hashTable = Object.create(null);
  //   dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.dept_name, key: aData.id, isLeaf: false });

  //   const dataTree: any = [];

  //   dataset.forEach((aData: any) => {
  //     if (aData.parent_id) {
  //       console.log('here');
  //       hashTable[aData.parent_id].children.push(hashTable[aData.id]);
  //     }
  //   });

  //   dataset.forEach((aData: any) => {
  //     if (hashTable[aData.id].children.length == 0) {
  //       hashTable[aData.id].isLeaf = false
  //     }
  //     else {
  //       hashTable[aData.id].expanded = false
  //     }
  //     if (!aData.parent_id) {

  //       dataTree.push(hashTable[aData.id]);
  //     }
  //   });

  //   return dataTree;
  // };

  getDistrictByProvice(provice?: any): void {
    const params = {
      keyword: '',
      province: provice || this.form.value.province_id
    }
    this.commonService.getDistricts(params, true).subscribe(
      (res: any) => {
        this.districts = res.body?.data
        this.wards = []
      }
    );
  }

  getWardByDistrict(district?: any): void {
    const params = {
      keyword: '',
      district: district || this.form.value.district_id
    }
    this.commonService.getWards(params, true).subscribe(
      (res: any) => {
        this.wards = res.body?.data
      }
    );
  }
  onChangeProvice() {
    this.getDistrictByProvice();
  }

  onChangeDistrict() {
    this.getWardByDistrict();
  }

  initForm(): void {
    this.form = this.fb.group({
      dept_code: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedDepartment?.dept_code : null, disabled: this.action.event === ACTION.CREATE.event ? false : true },
      { validators: [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.CODECUS_MAX_LENGTH.MAX), CustomInputValidator.notBlankValidator, CustomInputValidator.isCode]}],
      dept_name: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedDepartment?.dept_name : null, disabled: this.action.event === ACTION.VIEW.event || this.selectedDepartment.sync_code ? true : false },
      [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX)]],
      address: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedDepartment?.address : null, disabled: this.action.event === ACTION.VIEW.event ? true : false }, [Validators.maxLength(LENGTH_VALIDATOR.ADDRESS_MAX_LENGTH.MAX)]],
      province_id: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedDepartment?.province_id : null, disabled: this.action.event === ACTION.VIEW.event ? true : false }, []],
      district_id: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedDepartment?.district_id : null, disabled: this.action.event === ACTION.VIEW.event ? true : false }, []],
      ward_id: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedDepartment?.ward_id : null, disabled: this.action.event === ACTION.VIEW.event ? true : false }, []],
      parent_id: [this.actionToShowData.includes(this.action.event) ? this.selectedDepartment?.parent_id : null, []],
      search: null

    });
  }

  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }

  onSubmit(): void {
    let el = document.getElementById('myDiv');
    el?.click();
    setTimeout(() => {
      if (this.action.event === ACTION.VIEW.event) {
        this.isShowView = false;
        this.action = ACTION.UPDATE;
        this.initForm();
        return;
      }

      if (this.form.invalid) {
        CommonUtil.markFormGroupTouched(this.form);
        return;
      }
      const deparment: any = {
        ...this.form.value,
        parent_id: this.form.value.parent_id || 'NxOpZowo9GmjKqdR',
        province_id: this.form.value.province_id ? this.form.value.province_id : ''
      };

      const body = CommonUtil.trim(deparment);

      this.isUpdating = true;

      let serviceEvent = this.action.event === ACTION.UPDATE.event ? this.departmentService.update(this.selectedDepartment.id, body, true) : this.departmentService.create(body, true)
      serviceEvent.subscribe(res => {
        if (res.body?.data) {
          this.toast.success(`model.department.success.${this.action.event}`);
          this.modalRef.close({
            success: true,
            value: deparment,
          });
        }
      }, (e: any) => {
        this.isUpdating = false;
      },
        () => {
          this.isUpdating = false;
        });
    }, 300)
  }
  checkExist(type: any, value: any) {
    if (!value) {
      switch (type) {
        case 'dept_code':
          this.formInvalid.dept_code = false;
          break;
        default:
          break;
      }
    } else {
      switch (type) {
        case 'dept_code':
          if (this.form.controls['dept_code'].status == 'VALID') {
            this.commonService.checkExisted('departments', { dept_code: value }, false).then(
              (users: any) => {
                this.formInvalid.dept_code = (users && users.body === 1) ? true : false;
              })
          }
          break;
        default:
          break;
      }
    }

  }
}
