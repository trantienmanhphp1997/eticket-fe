import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ACTION } from '@shared/constants/common.constant';
import { environment } from '@env/environment'; 
import { PAGINATION } from '@shared/constants/pagination.constants';
import { CommonService } from '@shared/service/common.service';
import { DepartmentService } from '@shared/service/department.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import CommonUtil from '@shared/utils/common-utils';
import { AUTH } from '@shared/constants/auth.constant';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { DepartmentDetailComponent } from './department-create/department-detail.component';
import { ModalComponent } from '@shared/components/modal/modal.component';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {
  isCallFirstRequest: boolean = true;
  // page data
  departmentList: any = [];
  provices: any = []
  total: any = 0;
  auth:any = AUTH;
  is_department_sync:any;
  departmentQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize :PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    parentId: '',
    orderBy: '',
    sortBy: '',
    keyword: '',
    data_context: 'manage-department',
  };
  

  //checked items
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly any[] = [];
  setOfCheckedId = new Set<number>();
  checked = false;

  //tree
  defaultCheckedNode: any = []
  defaultCheckedKeys :any = [];
  defaultSelectedKeys:any = [];
  defaultExpandedKeys :any = [];
  nodes :any= [];
  
  commonUtil: any = CommonUtil
  constructor(
    private fb: FormBuilder,
    private departmentService: DepartmentService,
    private commonService: CommonService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toast: ToastService
  ) { }

  searchForm = this.fb.group({
    keyword: ['']
  });

  keyword: any = ''
  ngOnInit() {
    this.getAllDepartments()
    this.loadData()
    this.is_department_sync = environment.is_department_sync;
  }
  
  // load data
  loadData() {
    // const params = {
    //   pageIndex: this.pageIndex,
    //   pageSize: this.pageSize,
    //   parentId: this.defaultCheckedNode ? this.defaultCheckedNode.toString() : '',
    //   keyword: this.keyword
    // }
    this.departmentList = [];
    this.departmentService.getDepartments(this.departmentQuery, true).subscribe(
      (res) => {
        this.departmentList = res.body?.data?.map((dept: any) => {
          return {
            ...dept, disabled: dept.dept_code === 'TCT'
          }
        })
        this.total = res.body?.meta?.pagination?.total
      }
    );
    
  }


 
  getAllDepartments() {
    const param = {
      limit : 10000,
      data_context:'manage-department',
      search:''
    }
    this.departmentService.getAllDepartments(param,true).subscribe(
      (res) => {
        this.nodes = res.body;
      }
    );
  }
  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.dept_name, key: aData.id, isLeaf: false });

    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
      }
    });

    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };

  // Events
  onSearch(event: any) {
    if (event) {
      this.departmentQuery.keyword = this.searchForm.value.keyword;
      this.departmentQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
      this.loadData();
    }
  }

  nzEvent(event: NzFormatEmitEvent): void {
    this.defaultCheckedNode = event.keys
    this.departmentQuery.parentId =  this.defaultCheckedNode ? this.defaultCheckedNode.toString() : '',
    this.departmentQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.loadData();
  }
   onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    this.departmentQuery.sortBy = params.sort.filter(e=>e.value !== null).map(e=>e.value?.slice(0,-3)).toString();
    this.departmentQuery.orderBy = params.sort.filter(e=>e.key !== null && e.key !== undefined && e.value !== null ).map(e=>e.key).toString();
    this.loadData();
  }
  
  onRefresh(): void {
    this.departmentQuery = {
      pageIndex: PAGINATION.PAGE_DEFAULT,
      pageSize :PAGINATION.SIZE_DEFAULT,
      pageSizeOptions: PAGINATION.OPTIONS,
      parentId: '',
      orderBy: '',
      sortBy: '',
      keyword: '',
      data_context: 'manage-department'
    };
    this.getAllDepartments()
    this.loadData()
  }

  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.departmentQuery.pageIndex = pageIndex;
    this.departmentQuery.pageSize = pageSize;
    this.loadData();
  }

  onCreate = () => {
    const base = CommonUtil.modalBase(DepartmentDetailComponent, { action: ACTION.CREATE, nodes: this.nodes, provices: this.provices }, '40%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        console.log('done');
        this.getAllDepartments()
        this.loadData()
      }
    });
  }

  onUpdate = (data: any) => {
    const base = CommonUtil.modalBase(DepartmentDetailComponent, { action: ACTION.UPDATE, provices: this.provices, selectedDepartment: data, isUpdate: true }, '40%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        // this.getAllDepartments()
        this.loadData()
      }
    });
  }

  onView = (data: any) => {
    const base = CommonUtil.modalBase(DepartmentDetailComponent, { action: ACTION.VIEW, provices: this.provices, selectedDepartment: data, nodes: this.nodes }, '40%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.getAllDepartments()
        this.loadData()
      }
    });
  }

  onDeleteAll = (data: any) => {
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc muốn xóa đơn vị được chọn không?`,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'450px',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let departmentIds =  data instanceof Set ? Array.from(data) : [data.id] ; 
    
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.departmentService.delete(departmentIds).subscribe((res: any) => {
          if (res) {
            this.toast.success(`model.department.success.delete`);
            this.setOfCheckedId = new Set();
            this.getAllDepartments()
            this.loadData()
          };
        })
      }
    });
  }
  onDelete = (data: any) => {
    debugger
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc muốn xóa đơn vị <b>${data?.dept_name}</b> không?`,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'450px',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let departmentIds =  data instanceof Set ? Array.from(data) : [data.id] ; 
    
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.departmentService.delete(departmentIds).subscribe((res: any) => {
          if (res) {
            this.toast.success(`model.department.success.delete`);
            this.setOfCheckedId = new Set();
            this.getAllDepartments()
            this.loadData()
          };
        })
      }
    });
  }

  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.departmentList.filter((item: any) => !item.disabled);
    this.checked = listOfEnabledData.every((item: any) => this.setOfCheckedId.has(item.id));
    this.indeterminate = listOfEnabledData.some((item: any) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.departmentList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }
  exportData(){
    const params= {
      parentIds: this.departmentQuery.parentId
    }
    this.departmentService.export(params)
  }
}
