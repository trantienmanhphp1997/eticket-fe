import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { DepartmentDetailComponent } from './department-create/department-detail.component';
import { WelcomeRoutingModule } from './department-routing.module';
import { DepartmentComponent } from './department.component';




@NgModule({
  declarations: [DepartmentComponent, DepartmentDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    WelcomeRoutingModule,
  ],
  exports: [DepartmentComponent]
})
export class DeparmentModule { }
