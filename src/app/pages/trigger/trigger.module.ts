import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { TriggerCreateComponent } from './trigger-create/trigger-create.component';
import { TriggerRoutingModule } from './trigger-routing.module';
import { TriggerComponent } from './trigger.component';




@NgModule({
  declarations: [TriggerComponent, TriggerCreateComponent],
  imports: [
    CommonModule,
    SharedModule,
    TriggerRoutingModule,
  ],
  exports: [TriggerComponent]
})
export class TriggerModule { }
