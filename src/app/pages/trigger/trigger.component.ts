import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PAGINATION } from '@shared/constants/pagination.constants';
import CommonUtil from '@shared/utils/common-utils';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { AUTH } from '@shared/constants/auth.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TriggerService } from '@shared/service/trigger.service';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { TRIGGER_ORDERBY, TRIGGER_SORTBY } from '@shared/constants/trigger.constant';
import { ACTION } from '@shared/constants/common.constant';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { ModalConfirmComponent } from '@shared/components/modal-confirm/modal-confirm.component';
@Component({
  selector: 'app-trigger',
  templateUrl: './trigger.component.html',
  styleUrls: ['./trigger.component.scss']
})
export class TriggerComponent implements OnInit {
  isCallFirstRequest: boolean = true;

  triggerList: any = [];
  total: any = 0;
  pageIndex = PAGINATION.PAGE_DEFAULT;
  pageSize = PAGINATION.SIZE_DEFAULT;
  pageSizeOptions = PAGINATION.OPTIONS;
  orderBy: any = 'created_at';
  sortBy: any = 'desc';
  auth:any = AUTH;

  //checked items
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly any[] = [];
  setOfCheckedId = new Set<number>();
  checked = false;
  visible = false;
  newOder: any;
  //form 
  searchForm = this.fb.group({
    keyword: [null]
  });
  listSortBy = TRIGGER_SORTBY;
  listOrderBy = TRIGGER_ORDERBY;
  commonUtil:any = CommonUtil;
  constructor(
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toast: ToastService,
    private fb: FormBuilder,
    private triggerService: TriggerService,
    private router: Router,
  ) {

  }

  ngOnInit(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.loadData();
  }


  //load data
  loadData() {
    const params = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      search: this.searchForm.value.keyword,
      include: 'triggers',
      orderBy: this.orderBy,
      sortBy: this.sortBy,

    }
    this.triggerService.getTrigger(params, true).subscribe(
      (res) => {
        this.triggerList = res.body?.data;
        this.total = res.body?.meta?.pagination.total;
      }
    );
  }
  // Events
  onSearch($event: any) {
    if ($event) {
      this.loadData();
    }
  }
  onCreate = () => {
    this.router.navigate([`admin/${ROUTER_UTILS.trigger.root}/${ROUTER_UTILS.trigger.create}`]);
  }

  onDeleteAll = (data: any) => {
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc muốn xóa trigger đã chọn không?`,
      okText:'Xóa',
      isVisible:true,
      callback: () => {
        return {
          success: true,
        };
      },
    },'25%',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    let triggerIds = data instanceof Set ? Array.from(data) : [data.id];
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.triggerService.multiDeleteTrigger(triggerIds).subscribe((res: any) => {
          if (res) {
            this.toast.success(`Xóa Trigger thành công`);
            // this.getAllDepartments()
            modal.close();
            this.setOfCheckedId =new Set();
            this.loadData()
          };
        })
      }else{
        modal.close();
      }
    });
  }


  onDelete(data: any) {
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc muốn xóa trigger <b>${data.title}</b> không?`,
      okText:'Xóa',
      isVisible:true,
      callback: () => {
        return {
          success: true,
        };
      },
    },'25%',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.triggerService.deleteTrigger(data?.id).subscribe((res: any) => {
          if (res) {
            this.toast.success(`Xóa Trigger thành công`);
            modal.close();
            this.loadData()
          };
        })
      }else{
        modal.close();
      }
    });
  }

  onAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.triggerList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }

  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    this.sortBy = params.sort.filter((e: any) => e.value !== null).map((e: any) => e.value?.slice(0, -3)).toString();
    this.orderBy = params.sort.filter((e: any) => e.key !== null && e.key !== undefined && e.value !== null).map((e: any) => e.key).toString();
    this.loadData();
  }

  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.loadData();
  }

  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  onUpdateStatus(data: any) {
    if (data.status == 1) {
      data.status = true
    } else {
      data.status = false
    }
    const form = CommonUtil.modalBase(ModalConfirmComponent, {
      title: 'Xác nhận thay đổi trạng thái',
      content: `Bạn có chắc muốn thay đổi trạng thái <b>${data?.title}</b> không?`,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      false, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.triggerService.updateTrigger(data.id, data ,true).subscribe(res => {
          if (res) {
            this.toast.success('Cập nhật thành công');
          } else {
            this.toast.success('Cập nhật thất bại');
          }
          modal.close();
        })
      } else {
        data.status = !data.status;
        modal.close();
      }
    });
  }
  onChangeSortBy(value: any): void {
    this.sortBy = value;
    if (this.sortBy && this.orderBy) {
      this.loadData();
    }

  }
  onChangeOrderBy(value: any): void {
    this.orderBy = value;
    if (this.sortBy && this.orderBy) {
      this.loadData();
    }
  }
  onResetSearch() {
    this.sortBy = 'desc';
    this.orderBy = 'created_at';
    this.loadData();
  }
  onUpdate(data: any) {
    if (data.id) {
      this.router.navigate([`admin/${ROUTER_UTILS.trigger.root}`, data?.id]);
    }
  }
  onDetail(data:any){
    if (data.id) {
      this.router.navigate([`admin/${ROUTER_UTILS.trigger.root}/${ROUTER_UTILS.trigger.detail}`, data.id]);
    }
  }
  drop($event: any){
    const triggerDrag = this.triggerList?.find((x:any, k:any)=>k==$event.previousIndex);
    let getFirstValue = this.triggerList.at();
    if (getFirstValue.order !== 1) {
      let getSecondValue = this.triggerList.at(1);
      this.newOder = getSecondValue.order;
    }
    const params = {
      id: triggerDrag?.id,
      new_order: this.newOder ? this.newOder : $event.currentIndex + 1
    }
    this.triggerService.updateOrderTrigger(params).subscribe(res=>{
      this.loadData()
    })
  }
}
