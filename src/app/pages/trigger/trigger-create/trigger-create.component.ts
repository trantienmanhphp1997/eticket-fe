
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TriggerService } from '@shared/service/trigger.service';
import CommonUtil from '@shared/utils/common-utils';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { TRIGGER_ACTION, TRIGGER_EDIT_TYPE, TRIGGER_KEY } from '@shared/constants/trigger.constant';
import { ACTION } from '@shared/constants/common.constant';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-trigger-create',
  templateUrl: './trigger-create.component.html',
  styleUrls: ['./trigger-create.component.scss']
})
export class TriggerCreateComponent implements OnInit {
  detailTrigger: boolean = false;
  isUpdate: boolean = false;
  selectedTrigger: any = [];
  lengthValidator = LENGTH_VALIDATOR;
  triggerEditType = TRIGGER_EDIT_TYPE;
  triggerKey = TRIGGER_KEY;
  pageIndex = PAGINATION.PAGE_DEFAULT;
  pageSize = PAGINATION.SIZE_DEFAULT;
  pageSizeOptions = PAGINATION.OPTIONS;
  orderBy: any = '';
  sortBy: any = '';
  empForm: FormGroup = new FormGroup({});
  isAllCondition: boolean = true;
  isAnyCondition: boolean = true;
  errorTitle: boolean = false;
  isUpdating = false;
  conditionList: any = [];
  operatorList: any = [[]];
  optionList: any = [[]];
  querySearch: any = [[]];
  isLoadingCondition: any = [[]];
  selectSearchUpdate:any =[[]];
  conditionArray: any = [];
  actionArray: any = [];
  actionList: any = [];
  selectedCondition: string = '';
  isShowCondition: boolean = true;
  edit_type: any = [[]];
  all = true;
  actionCondition: any[] = [];
  sub: any;
  id: any;
  inputtext: any[] = [];
  action: any;
  eventView = 'detail';
  edittype: any;
  actionType: any = [];
  noteAction = [TRIGGER_KEY.SET_TICKET_NOTE, TRIGGER_KEY.SET_TICKET_STATUS, TRIGGER_KEY.SET_TICKET_PRIORITY, TRIGGER_KEY.SET_TICKET_CATEGORY]
  constructor(
    private fb: FormBuilder,
    private triggerService: TriggerService,
    private router: Router,
    private toast: ToastService,
    private activatedRoute: ActivatedRoute,
  ) {

  }

  ngOnInit(): void {
    this.activatedRoute.url.subscribe( res =>{
      if(res[0].path == 'detail'){
        this.detailTrigger = true;
      }
  })
    this.initForm()
    this.loadCondition()
  }
  initForm() {
    this.empForm = this.fb.group({
      title: this.fb.control({ value: this.selectedTrigger?.title, disabled: ( this.detailTrigger ? true : false) }, [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.TITLE_MAX_LENGTH.MAX)]),
      descriptions: new FormControl(this.selectedTrigger?.descriptions, [Validators.maxLength(LENGTH_VALIDATOR.TICKET_DESCRIPTION_LENGTH.MAX)]),
      conditions: this.isUpdate ? this.initCondition(this.selectedTrigger?.conditions) : this.fb.array([this.newCondition()]),
      actions: this.isUpdate ? this.initAction(this.selectedTrigger?.actions) : this.fb.array([this.newAction()]),
    });
  }
  onSubmit(): void {
    if (this.empForm.invalid) {
      CommonUtil.markFormGroupTouched(this.empForm);
      return;
    }

    const trigger: any = {
      ...this.empForm.value,
      operator: this.all ? 'all' : 'any',
      status: 1
    };
    const body = CommonUtil.trim(trigger);
    this.isUpdating = true;
    let triggerEvent = this.isUpdate ? this.triggerService.updateTrigger(this?.id, body, true) : this.triggerService.createTrigger(body, true)
    triggerEvent.subscribe(res => {
      if (res) {
        this.toast.success(`model.trigger.success.${this.isUpdate ? 'update' : 'create'}`);
        this.router.navigate([`admin/${ROUTER_UTILS.trigger.root}`]);
      }
    }, (e: any) => {
      this.isUpdating = false;
    },
      () => {
        this.isUpdating = false;
      });
  }

  onCancel(): void {
    this.router.navigate([`admin/${ROUTER_UTILS.trigger.root}`]);
  }

  onChangeData(type: string, content: string, index: any): void {
    if (type == 'value') {
      this.actions().at(index).get(type)?.setValue(content);
    } else {
      this.empForm.get(type)?.setValue(content);
    }

  }

  loadCondition() {
    const params = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      include: 'appconfigs/triggers',
      orderBy: this.orderBy,
      sortBy: this.sortBy,

    }
    this.triggerService.getAllConfigTrigger(params, true).subscribe(
      (res: any) => {
        this.conditionList = res.body?.ticket_fields?.filter((item:any)=>item.key=='title');
        this.actionList = res.body?.actions.filter((item:any)=>TRIGGER_ACTION.includes(item.key));
        this.sub = this.activatedRoute.paramMap.subscribe(params => {
          this.id = params.get('id') || 0;
          this.activatedRoute.queryParams.subscribe(params => {
            if (params.view == 'detail') {
              this.action = params.view || null;
            }
          })
          if (this.id) {
            this.isUpdate = true;
            this.triggerService.findTriggerId(this.id).subscribe(res => {
              if (res.body?.data) {
                this.selectedTrigger = res.body.data;
                this.all = this.selectedTrigger.operator == 'all' ? true : false;
                this.initForm();
                // this.empForm.setControl('title', new FormControl({ value: this.selectedTrigger.title, disabled: (this.action == ACTION.VIEW.event ? true : false) },[Validators.required, Validators.maxLength(LENGTH_VALIDATOR.TITLE_MAX_LENGTH.MAX)]))
                this.empForm.get('descriptions')?.setValue(this.selectedTrigger?.descriptions);
                // this.empForm.removeControl('conditions');
                // this.empForm.addControl('conditions', this.initCondition(this.selectedTrigger?.conditions))
                // this.empForm.setControl('actions', this.initAction(this.selectedTrigger?.actions))

              }
            })
          }
        })
      }
    );
  }


  chooseConditions(condition: any, empIndex: number, skillIndex: number) {
    const controls = this.conditions().at(empIndex).get('properties') as FormArray;
    controls.at(skillIndex).get('operator')?.setValue(null);
    controls.at(skillIndex).get('value')?.setValue(null);
    this.getListCondition(condition, empIndex, skillIndex);
    if (this.edittype == TRIGGER_EDIT_TYPE.INPUT_TEXT) {
      controls.at(skillIndex).get('value')?.setValidators([Validators.required, Validators.maxLength(LENGTH_VALIDATOR.TITLE_MAX_LENGTH.MAX)]);
    }
  }

  getListCondition(condition: any, empIndex: number, skillIndex: number) {
    const conditions = this.conditionList.find((cdn: any) => cdn.key == condition)
    const options: { label: any; value: any; }[] | null = [];
    if (conditions?.operators?.length) {
      this.operatorList[empIndex][skillIndex] = conditions?.operators;
    }
    if (conditions?.edit_type == TRIGGER_EDIT_TYPE.INPUT_TEXT) {
      this.edittype = conditions?.edit_type;
    } else {
      this.edittype = null;
    }
    this.edit_type[empIndex][skillIndex] = {
      value: conditions?.edit_type,
      option_type: conditions?.option_display_type,
      options_source:conditions?.options_source
    }
    if (conditions?.options) {
      switch (conditions?.options_source) {
        case "offline":
          conditions?.options.forEach((item: any) => {
            options.push({ label: item?.name, value: item?.id });
          });
          this.optionList[empIndex][skillIndex] = options;
          break;
        case "online":
          const param={
            searchName: null,
            urlApi:conditions?.options,
            limit:PAGINATION.SIZE_DEFAULT,
          }
          this.triggerService.getOptionsTriggerConfig(param).subscribe(res => {
            if (res.body?.data) {
              if (conditions?.option_display_type == 'flat') {
                res.body?.data?.forEach((item: any) => {
                  options.push({ label: item?.name, value: item?.id });
                });
                this.optionList[empIndex][skillIndex] = options;
              } else if (conditions?.option_display_type == 'tree') {
                this.optionList[empIndex][skillIndex] = this.createDataTree(res.body?.data)
              }

            }
          }, err => {
            this.toast.error(err);
          })
          break;
        default:
          break;
      }
    }
  }


  conditions(): FormArray {
    return this.empForm.get('conditions') as FormArray;
  }
  checkTitle() {
    if (this.empForm.value.title.trim() == '') {
      this.errorTitle = true;
    }
    else {
      this.errorTitle = false;
    }

  }
  newCondition(): FormGroup {
    return this.fb.group({
      match_type: new FormControl('all'),
      properties: this.fb.array([this.newSubCondition()])
    });
  }
  initCondition(conditions: any): FormArray {
    const group = new FormArray([]);
    conditions.forEach((_condition: any, index: number) => {
      group.push(this.fb.group({
        match_type: new FormControl({ value: _condition.match_type, disabled: (this.detailTrigger ? true : false) }),
        properties: this.initSubCondition(_condition.properties, index)
      }))
    })
    return group;
  }
  addConditionForm() {
    if (this.conditions().invalid) {
      CommonUtil.markFormGroupTouched(this.empForm);
      return;
    }
    this.operatorList.push([])
    this.optionList.push([]);
    this.edit_type.push([])
    this.conditions().push(this.newCondition());
  }

  removeConditionForm(empIndex: number) {
    this.conditions().removeAt(empIndex);
  }

  subConditions(empIndex: number): FormArray {
    return this.conditions()
      .at(empIndex)
      .get('properties') as FormArray;
  }

  getRelationshipCondition(empIndex: number) {
    return this.conditions()
      .at(empIndex).value.match_type;
  }
  newSubCondition(): FormGroup {
    return this.fb.group({
      field_name: new FormControl('', [Validators.required]),
      operator: new FormControl('', [Validators.required]),
      value: new FormControl([], [Validators.required]),
    });
  }
  initSubCondition(properties: any, empIndex: any): FormArray {
    const group = new FormArray([]);
    console.log(this.operatorList[empIndex])
    if(this.operatorList[empIndex]==undefined){
      this.operatorList.push([])
    }
    if(this.edit_type[empIndex]==undefined){
      this.edit_type.push([])
    }if(this.optionList[empIndex]==undefined){
      this.optionList.push([])
    }
    if(this.selectSearchUpdate[empIndex]==undefined){
      this.selectSearchUpdate.push([])
    }
    
    properties.forEach((property: any, skillIndex: any) => {
      this.getListCondition(property.field_name, empIndex, skillIndex);
      
      const valueValidator = [Validators.required];
      if (this.edittype == TRIGGER_EDIT_TYPE.INPUT_TEXT) {
        valueValidator.push(Validators.maxLength(LENGTH_VALIDATOR.TITLE_MAX_LENGTH.MAX))
      }
      group.push(this.fb.group({
        field_name: new FormControl({ value: property.field_name, disabled: (this.detailTrigger ? true : false) }, [Validators.required]),
        operator: new FormControl({ value: property.operator, disabled: (this.detailTrigger ? true : false) }, [Validators.required]),
        value: new FormControl({ value: property.value, disabled: (this.detailTrigger ? true : false) }, valueValidator),
      }))
      this.initSearch(empIndex,skillIndex )
    })
    return group;
  }

  addSubCondition(empIndex: number) {
    const control = this.subConditions(empIndex)
    if (control.invalid) {
      CommonUtil.markFormGroupTouched(this.empForm);
      return;

    }
    this.subConditions(empIndex).push(this.newSubCondition());
    this.initSearch(empIndex,null )
    
  }

  removeSubCondition(empIndex: number, skillIndex: number) {
    if(empIndex==0){
      this.subConditions(empIndex).removeAt(skillIndex);
    }else{
      if(this.subConditions(empIndex).controls?.length==1){
        this.removeConditionForm(empIndex)
      }else{
        this.subConditions(empIndex).removeAt(skillIndex);
      }
    }
    
    this.operatorList[empIndex]=this.operatorList[empIndex]?.filter((value:any, key:any)=>key!=skillIndex)
    this.optionList[empIndex]=this.optionList[empIndex]?.filter((value:any, key:any)=>key!=skillIndex)
  }


  actions(): FormArray {
    return this.empForm.get('actions') as FormArray;
  }

  newAction(): FormGroup {
    return this.fb.group({
      action: new FormControl('', [Validators.required]),
      value: '',
    });
  }
  initAction(actions: any): FormArray {

    const group = new FormArray([]);
    actions.forEach((action: any, empIndex: any) => {
      this.actionType[empIndex]=action?.action;
      this.actionList.find((i: any) => {
        if (i?.key == action.action) {
          if (typeof i?.options == 'string') {
            this.actionCondition[empIndex] = {
              ...i,
              options: []
            };
          } else {
            this.actionCondition[empIndex] = i;
          }


          this.setActions(i, empIndex)
        }
      })
      group.push(this.fb.group({
        action: new FormControl({ value: action.action, disabled: (this.detailTrigger ? true : false) }, [Validators.required]),
        value: this.addActionsToInitForm(action)
      }))

    })
    return group;
  }
  addActionForm() {
    if (this.actions().invalid) {
      CommonUtil.markFormGroupTouched(this.empForm);
      return;
    }
    this.actions().push(this.newAction());
  }

  removeAction(empIndex: number) {
    this.actions().removeAt(empIndex);
    this.actionCondition=this.actionCondition.filter((value:any, key:any)=>key!=empIndex)
  }
  onChangeValueAction(empIndex: any, $event: any) {
    return
  }
  onChangeAction($event: any, value = null, empIndex: number) {
    const control = this.actions().at(empIndex) as FormGroup;
        control.get('value')?.setValue(null);
    this.actionList.find((i: any) => {
      if (i?.key == $event) {
        
        this.actionCondition[empIndex] = { ...i, options: [] };
        this.setActions(i, empIndex)
        switch (i?.key) {
          case this.triggerKey.SET_TICKET_NOTE:
            if (!this.actionType || !this.noteAction.includes(this.actionType[empIndex])) {
              this.setTicketNote(value, empIndex);
            }
            break;
          case this.triggerKey.SET_TICKET_CATEGORY:
            if (!this.actionType || !this.noteAction.includes(this.actionType[empIndex])) {
              this.setTicketNote(value, empIndex);
            }
            break;
          case this.triggerKey.SET_TICKET_PRIORITY:
            if (!this.actionType || !this.noteAction.includes(this.actionType[empIndex]) ) {
              this.setTicketNote(value, empIndex);
            }
            break;
          case this.triggerKey.SET_TICKET_STATUS:
            if (!this.actionType || !this.noteAction.includes(this.actionType[empIndex]) ) {
              this.setTicketNote(value, empIndex);
            }
            break;
          case this.triggerKey.MAIL_STAFF:
            this.setMailStaffAction(value, empIndex);
            break;
          case this.triggerKey.SMS_STAFF:
            this.setSmsStaffAction(value, empIndex);
            break;
          case this.triggerKey.SET_PRODUCT_ID:
            this.setTickeChangeProduct(value, empIndex);
            break;
          default:
            break;
        }
        this.actionType[empIndex] = i?.key;
      }
    })
  }
  setTickeChangeProduct(value: any, empIndex: number) {
    const control = this.actions().at(empIndex) as FormGroup;
    control.removeControl('value')
    control.addControl('value', new FormControl(this.isUpdate ? value : [], [Validators.required]));
  }
  setTicketNote(value: any, empIndex: number) {
    const control = this.actions().at(empIndex) as FormGroup;
    control.setControl('value', new FormControl(this.isUpdate ? value : null, [Validators.required]));
  }
  setMailStaffAction(value: any, empIndex: number) {
    const control = this.actions().at(empIndex) as FormGroup;
    control.setControl('value', new FormGroup({
      fwd_to: new FormControl(this.isUpdate ? value.fwd_to : null, [Validators.required]),
      fwd_note_body: new FormControl(this.isUpdate ? value.fwd_note_body : null, [Validators.required]),
      fwd_title: new FormControl(this.isUpdate ? value.fwd_title : null, [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.TICKET_TITLE_LENGTH.MAX)])
    }))
  }
  setSmsStaffAction(value: any, empIndex: number) {
    const control = this.actions().at(empIndex) as FormGroup;
    control.setControl('value', new FormGroup({
      fwd_to: new FormControl('', [Validators.required]),
      fwd_note_body: new FormControl('', [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.SMS_CONTENT.MAX)]),
    }))
    control.patchValue({
      fwd_to: this.isUpdate ? value.fwd_to : null,
      fwd_note_body: this.isUpdate ? value.fwd_note_body : null,
    });
  }
  setActions(value: any, empIndex: number) {
    const options: { label: any; value: any; }[] = [];
    switch (value?.options_source) {

      case "offline":
        value?.options?.forEach((o: any) => {
          options.push({ label: o?.name || o?.label, value: o?.id || o?.value });
          this.actionCondition[empIndex].options = options
        })
        break;
      case "online":
        if (typeof value?.options == 'string') {
          const param={
            searchName: null,
            urlApi:value?.options,
            limit:0,
          }
          this.triggerService.getOptionsTriggerConfig(param).subscribe(res => {
            if (res.body?.data) {
              if (value?.option_display_type == 'tree') {
                this.actionCondition[empIndex].options = this.createDataTree(res.body?.data)
              } else if (value?.option_display_type == 'flat') {
                this.actionCondition[empIndex].options = res.body?.data?.map((i: any) => {
                  return { value: i.id, label: i.name }
                })
              }

            }
          }, err => {
            this.toast.error(err);
          })
        }
        break;
      default:
        break;
    }
  }
  setTicketSatus(empIndex: number) {
    const control = this.actions().at(empIndex) as FormGroup;
    control.removeControl('value')
    control.addControl('value', new FormControl(null, [Validators.required]));
  }
  addActionsToInitForm(action: any) {
    var control: any;
    switch (action.action) {
      case this.triggerKey.SET_TICKET_NOTE:
        control = new FormControl({ value: action.value, disabled: (this.detailTrigger ? true : false) }, [Validators.required])
        break;
      case this.triggerKey.SET_TICKET_CATEGORY:
        control = new FormControl({ value: action.value, disabled: (this.detailTrigger ? true : false) }, [Validators.required])
        break;
      case this.triggerKey.SET_TICKET_PRIORITY:
        control = new FormControl({ value: action.value, disabled: (this.detailTrigger ? true : false) }, [Validators.required])
        break;
      case this.triggerKey.SET_TICKET_STATUS:
        control = new FormControl({ value: action.value, disabled: (this.detailTrigger ? true : false) }, [Validators.required])
        break;
      case this.triggerKey.MAIL_STAFF:
        control = new FormGroup({
          fwd_to: new FormControl({ value: action.fwd_to, disabled: (this.detailTrigger ? true : false) }, [Validators.required]),
          fwd_note_body: new FormControl({ value: action.fwd_note_body, disabled: (this.detailTrigger ? true : false) }, [Validators.required]),
          fwd_title: new FormControl({ value: action.fwd_title, disabled: (this.detailTrigger ? true : false) }, [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.TICKET_TITLE_LENGTH.MAX)])
        })
        break;
      case this.triggerKey.SMS_STAFF:
        control = new FormGroup({
          fwd_to: new FormControl({ value: action.fwd_to, disabled: (this.detailTrigger ? true : false) }, [Validators.required]),
          fwd_note_body: new FormControl({ value: action.fwd_note_body, disabled: (this.detailTrigger ? true : false) }, [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.SMS_CONTENT.MAX)]),
        })
        break;
      case this.triggerKey.SET_PRODUCT_ID:
        control = new FormControl({ value: action.value, disabled: (this.detailTrigger ? true : false) }, [Validators.required])
        break;
      default:
        break;
    }
    return control;
  }
  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.name, key: aData.id, isLeaf: false });
    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children?.push(hashTable[aData.id]);
      }
    });
    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };

  //select search server large data
  loadMore(empIndex:any,skillIndex:any,option:any){
    var query =this.querySearch[empIndex][skillIndex];
    if (query?.pageSize > query.total) {
      return
    }
    this.querySearch[empIndex][skillIndex].pageSize += 10;
    this.isLoadingCondition[empIndex][skillIndex] = true;
    this.loadDataOnlineCondition(empIndex,skillIndex,option)
  }
  loadDataOnlineCondition(empIndex:any,skillIndex:any,option:any){
    const conditions = this.conditionList.find((cdn: any) => cdn.key == option)
    const options: { label: any; value: any; }[] | null = [];
    const param={
      searchName: this.querySearch[empIndex][skillIndex]?.searchName,
      urlApi:conditions?.options,
      limit:this.querySearch[empIndex][skillIndex]?.pageSize,
    }
    this.triggerService.getOptionsTriggerConfig(param).subscribe(res => {
      if (res.body?.data) {
        if (conditions?.option_display_type == 'flat') {
          res.body?.data?.forEach((item: any) => {
            options.push({ label: item?.name, value: item?.id });
          });
          this.optionList[empIndex][skillIndex] = options;
        } else if (conditions?.option_display_type == 'tree') {
          this.optionList[empIndex][skillIndex] = this.createDataTree(res.body?.data)
        }
        this.isLoadingCondition[empIndex][skillIndex] = false;
        this.querySearch[empIndex][skillIndex].total = res.body?.meta?.pagination?.total || 0
      }
    }, err => {
      this.toast.error(err);
    })
  } 
  initSearch(empIndex:any,skillIndex:any ){
    if(skillIndex==null){
       skillIndex = this.selectSearchUpdate[empIndex]?.length ||0
    }
       this.selectSearchUpdate[empIndex][skillIndex] = new Subject();
    this.selectSearchUpdate[empIndex][skillIndex].pipe(
      debounceTime(800),
      distinctUntilChanged())
      .subscribe((value:any) => {
        this.onSearchData(empIndex,skillIndex, value.value, value.option)
      });
  }
  onSearchData(empIndex:any,skillIndex:any, $event:any,option:any){
    // const limit = $event ? 0 : 10
    this.querySearch[empIndex][skillIndex]={
      pageSize: 10,
      searchName : $event,
      total:0
    };
    this.loadDataOnlineCondition(empIndex,skillIndex,option)
  }
}
