import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { TriggerCreateComponent } from './trigger-create/trigger-create.component';
import { TriggerComponent } from './trigger.component';

const routes: Routes = [
  {
    path: '',
    component: TriggerComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.TRIGGER.LIST],
      title: 'Danh sách Trigger',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: `javascript:void(0)`
        },
      ]
    }
  },
  {
    path: ROUTER_UTILS.trigger.create,
    component: TriggerCreateComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.TRIGGER.CREATE],
      title: 'Tạo mới Trigger',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: `javascript:void(0)`
        },
        {
          level: 3,
          title: 'Danh sách Trigger',
          path: `admin/${ROUTER_UTILS.trigger.root}`
        }
      ]
    }
  },
  {
    path: ROUTER_UTILS.trigger.edit,
    component: TriggerCreateComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.TRIGGER.UPDATE],
      title: 'Cập nhật Trigger',
      titleOther: 'Chi tiết Trigger',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: `javascript:void(0)`
        },
        {
          level: 3,
          title: 'Danh sách Trigger',
          path: `admin/${ROUTER_UTILS.trigger.root}`
        }
      ]
    }
  },
  {
    path: `${ROUTER_UTILS.trigger.detail}/:id`,
    component: TriggerCreateComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.TRIGGER.UPDATE],
      title: 'Chi tiết Trigger',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: `javascript:void(0)`
        },
        {
          level: 3,
          title: 'Danh sách Trigger',
          path: `admin/${ROUTER_UTILS.trigger.root}`
        }
      ]
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TriggerRoutingModule { }
