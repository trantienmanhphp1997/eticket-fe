import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DepartmentService } from '@shared/service/department.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ProductService } from '@shared/service/product.service';
import { TicketCategoryService } from '@shared/service/ticket-category.service';
import { TicketPriorityService } from '@shared/service/ticket-priority.service';
import { WorkFlowService } from '@shared/service/work-flow.service';
import CommonUtil from '@shared/utils/common-utils';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-workflow-clone',
  templateUrl: './workflow-clone.component.html',
  styleUrls: ['./workflow-clone.component.scss']
})
export class WorkFlowCloneComponent implements OnInit {
  workFlowInfoForm: FormGroup = new FormGroup({});

  conditionInforList: any[] = [];
  listCondition: any = [
    {
      value: "products", name: "Sản phẩm dịch vụ",
      info: []
    },
    {
      value: "ticket_priority", name: "Độ ưu tiên", info: []
    },
    {
      value: "ticket_category", name: "Phân loại ticket", info: []
    }
  ];
  checked = false;
  departmentTree: any = [];
  application_to: any;
  application_from: any;
  dateInvalid = false;
  listConditionNoProduct = [];
  serviceproduct = [];
  changeProduct = false;
  ticket_category: any;
  ticket_priority: any;
  workFLowId: any;
  data: any;
  checkedProduct=false;
  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private ticketPriorityService: TicketPriorityService,
    private ticketCategoryService: TicketCategoryService,
    private departmentService: DepartmentService,
    private translateService: TranslateService,
    private modalService: NzModalService,
    private route: Router,
    private router: ActivatedRoute,
    private workFlowService: WorkFlowService,
    private toast: ToastService,
  ) {
    this.router.paramMap.subscribe((res: any) => {
      this.workFLowId = res.get('id') || '';
    });
  }
  ngOnInit(): void {
    this.conditionInforList[0] = [];

    this.initForm()
    this.getAllDepartments(null)
    if (this.workFLowId) {
      this.loadWorkFlows();
    }
  }
  loadWorkFlows() {
    this.workFlowService.getWorkFlowsDetail(this.workFLowId, true).subscribe(
      (res: any) => {
        this.data = res.body.data;
        this.checked = res.body.data?.is_global ? true : false
        this.workFlowInfoForm = this.fb.group({
          name: [res.body.data.name, []],
          status: [0, []],
          descriptions: [res.body.data.descriptions, []],
          department_id: [res.body.data.department_id, [Validators.required]],
          is_global: [this.checked, []]
        });
        this.application_from = res.body.data?.application_from;
        this.application_to = res.body.data?.application_to;
        // this.workflowName = res.body.data?.name;
        if (res?.body?.data?.applicable_to) {
          this.workFlowInfoForm.addControl('conditions', this.loadCondition(res?.body?.data?.applicable_to))
          if(res?.body?.data?.applicable_to?.product_all){
            this.checkedProduct=true;
            this.conditions().at(0).get('conditionInformation')?.disable();
          }else{
            this.checkedProduct=false;
            this.conditions().at(0).get('conditionInformation')?.enable();
          }
        } else {
          this.workFlowInfoForm.addControl('conditions', this.fb.array([this.newCondition()]))
        }
        if (!this.checked) {
          this.workFlowInfoForm.get('department_id')?.setValidators([Validators.required]);
          this.workFlowInfoForm.get('department_id')?.updateValueAndValidity();
        } else {
          this.workFlowInfoForm.get('department_id')?.setValue('');
          this.workFlowInfoForm.get('department_id')?.setValidators([]);
          this.workFlowInfoForm.get('department_id')?.updateValueAndValidity();
        }
      }
    );
  }
  loadCondition(applicable_to: any): FormArray {
    const conditions = new FormArray([]);
    var i = 0
    for (var item in applicable_to) {
      this.getConditionInforList(item, i);
      if (item == 'products') {
        this.serviceproduct = applicable_to[item];
      }
      conditions.push(this.fb.group({
        conditionName: new FormControl(item=='product_all'?'products':item, [Validators.required]),
        conditionInformation: new FormControl(item=='product_all'?[]:applicable_to[item], [Validators.required]),
        product_all:[item=='product_all'?true:false],
      }))
      i++;
    }
    return conditions;
  }
  onChangeTitle() {
    this.workFlowInfoForm.get('name')?.setValue(this.workFlowInfoForm.value.name?.trim())
  }
  initForm() {
    this.workFlowInfoForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      department_id: ['', [Validators.required]],
      conditions: this.fb.array([this.newCondition()]),
      status: [1, []],
      descriptions: ['', []],
      is_global: [false]
    });
  }
  newCondition(): FormGroup {
    var conditionName = '';
    if (!this.conditions()) {
      this.getConditionInforList('products', 0);
      conditionName = 'products';
    }
    return this.fb.group({
      conditionName: new FormControl(conditionName, [Validators.required]),
      conditionInformation: new FormControl([], [Validators.required]),
      product_all:[false],
    });
  }
  conditions(): FormArray {
    return this.workFlowInfoForm.get('conditions') as FormArray;
  }
  getConditionInforList(conditionType: string, index: number) {
    switch (conditionType) {
      case "product_all":
        this.productService.getAll().subscribe(res => {
          this.conditionInforList[index] = this.createDataTree(res.body?.data, 'product');
          this.listCondition[index].info = res.body?.data;
        })
        break;
      case "products":
        this.productService.getAll().subscribe(res => {
          this.conditionInforList[index] = this.createDataTree(res.body?.data, 'product');
          this.listCondition[index].info = res.body?.data;
        })
        break;
      case "ticket_priority":
        this.ticketPriorityService.getAll(true).subscribe(res => {
          this.conditionInforList[index] = res.body?.data;
          this.listCondition[1].info = res.body?.data;
        })
        break;
      case "ticket_category":
        this.ticketCategoryService.getAll().subscribe(res => {
          this.conditionInforList[index] = res.body?.data;
          this.listCondition[2].info = res.body?.data;
        })
        break;
    }
  }
  createDataTree = (dataset: any, type: any) => {

    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => {
      var name = '';
      if (type == 'department') {
        name = aData.dept_name
      } else if (type == 'product') {
        name = aData.name
      }
      hashTable[aData.id] = { ...aData, children: [], title: name, key: aData.id, isLeaf: false }
    });
    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children?.push(hashTable[aData.id]);
      }
    });
    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };
  onChangeChecked($event: any) {
    this.checked = $event.target.checked;
    let department_id =this.workFlowInfoForm.get('department_id');
    if (!this.checked) {
      department_id?.setValidators([Validators.required]);
      department_id?.updateValueAndValidity();
    } else {
      department_id?.setValue('');
      department_id?.setValidators([]);
      department_id?.updateValueAndValidity();
    }
  }
  scrollElementDept() {
    this.getAllDepartments(this.workFlowInfoForm?.value.department_id || null);
    if (this.workFlowInfoForm?.value.department_id) {
      setTimeout(() => {
        const el = document.getElementsByClassName('dept')[0]?.getElementsByClassName('ant-select-tree-node-selected')[0];
        el?.scrollIntoView()
      }, 500)
    }
  }
  getAllDepartments(deptId: any) {
    this.departmentService.getAllDepartments({ selectedId: deptId, data_context: 'config-workflow' }, false).subscribe(
      (res: any) => {
        this.departmentTree = res?.body
      }
    );
  }
  onChangeTimeline() {
    if (this.application_from && this.application_to) {
      const fromdate = Math.floor((new Date(this.application_from)).getTime() / 1000);
      const todate = Math.floor((new Date(this.application_to)).getTime() / 1000);
      if (fromdate > todate) {
        this.dateInvalid = true;
        return;
      } else {
        this.dateInvalid = false;
      }
    } else {
      this.dateInvalid = false;
    }
  }
  changeCodition(event: any, index: number) {

    this.conditions().at(index).get('conditionInformation')?.setValue([]);
    this.getConditionInforList(event, index);
  }
  removeCondition(empIndex: any) {
    this.conditions().removeAt(empIndex);
  }
  onChangeProduct(empIndex: any) {
    this.changeProduct = true;
    this.conditions().at(empIndex).get('conditionInformation')?.setValue(this.serviceproduct)
  }
  filter($event: any, type: any) {
    if (type == 'ticket_priority') {
      this.ticket_priority = $event;
    } else if (type == 'ticket_category') {
      this.ticket_category = $event;
    }
  }
  addConditionForm() {
    if (this.conditions().invalid) {
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      return;
    }
    this.conditions().push(this.newCondition());
  }
  onChangeData(type: string, content: any): void {
    this.workFlowInfoForm.get(type)?.setValue(content);
  }
  onCancel() {
    const base = CommonUtil.modalConfirm(
      this.translateService,
      'Xác nhận thoát',
      `Bạn có thực sự muốn thoát không?`,
      {},
      () => {
        return {
          success: true,
        };
      },
      () => {
        return {
          success: false,
        };
      },
      'action.exit'
    );
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result) {
        if (result?.success) {
          this.route.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}`]);
        }
      }
    });
  }
  onClone() {
    const fromdate = Math.floor((new Date(this.application_from)).getTime() / 1000);
    const todate = Math.floor((new Date(this.application_to)).getTime() / 1000);
    if (fromdate > todate && this.application_from && this.application_to) {
      this.dateInvalid = true;
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      return;
    } else {
      this.dateInvalid = false;
    }
    if (this.workFlowInfoForm.invalid) {
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      return;
    }
    if (CommonUtil.extractContent(this.workFlowInfoForm.value.descriptions)?.length > 500) {

      return;
    }
    if (this.workFLowId) {
      var obj: any = {
        products: null,
        ticket_category: null,
        ticket_priority: null
      };
      const conditions = this.workFlowInfoForm.value.conditions;
      var obj: any = {
        products: null,
        ticket_category: null,
        ticket_priority: null
      };
      conditions.forEach((x: any) => {
        obj[x.conditionName] = x.conditionInformation;
      })
      if(!obj.products){
        delete obj.products;
        obj.product_all=this.checkedProduct;
      }
      const workFlow: any = {
        name: this.workFlowInfoForm.value.name,
        applicable_to: obj,
        department_id: this.workFlowInfoForm.value.department_id,
        descriptions: this.workFlowInfoForm.value.descriptions,
        status: this.workFlowInfoForm.value.status,
        application_to: this.application_to || '',
        application_from: this.application_from || '',
        is_global: this.workFlowInfoForm.value.is_global ? 1 : 0
      };

      const body = CommonUtil.trim(workFlow);

      this.workFlowService.clone(this.workFLowId, body, true).subscribe((res: any) => {
        if (res.body.data?.id) {
          this.toast.success(`model.workFlow.success.clone`);
          this.route.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/${ROUTER_UTILS.workflowConfiguration.detail}`, res.body.data?.id]);
        }
      });
    }
  }
  onUpdateStatus(data: any) {
    let toast ='Luồng xử lý nhân bản chưa được cập nhật đủ thông tin';
    if (this.workFlowInfoForm.invalid) {
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      this.toast.error(toast)
      return
    }
    if (data.steps?.data.length == 0 || data?.sla_internal_list.length == 0) {
      this.toast.error(toast)
    } else {
      this.workFlowInfoForm.patchValue(
        {
          status: !this.workFlowInfoForm.value.status
        }
      )
    }
  }
  onChangeProductAllChecked(){
    this.changeProduct=true;
    this.checkedProduct = this.conditions().at(0).get('product_all')?.value;
    let condition0=this.conditions().at(0).get('conditionInformation');
    if (!this.checkedProduct) {
      condition0?.enable();
      condition0?.setValidators([Validators.required]);
      condition0?.updateValueAndValidity();
    } else {
      this.workFlowInfoForm;
      
      this.serviceproduct=[];
      condition0?.setValue('');
      condition0?.setValidators([]);
      condition0?.disable();
      condition0?.updateValueAndValidity();
    }
  }
}
