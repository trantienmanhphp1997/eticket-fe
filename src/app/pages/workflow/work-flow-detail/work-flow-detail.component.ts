import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked, OnChanges } from '@angular/core';
import { BrowserJsPlumbInstance, ContainmentType, newInstance } from '@jsplumb/browser-ui';
// https://docs.jsplumbtoolkit.com/community/lib/events
import { FlowchartConnector } from '@jsplumb/connector-flowchart';
import CommonUtil from '@shared/utils/common-utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { WorkFlowService } from '@shared/service/work-flow.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '@shared/service/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { DepartmentService } from '@shared/service/department.service';
import { TicketCategoryService } from '@shared/service/ticket-category.service';
import { TicketPriorityService } from '@shared/service/ticket-priority.service';
import { TicketSourceService } from '@shared/service/ticket-source.service';
import { IDeactivateComponent } from '@core/guard/decativate.guard';
import { LocalStorageService } from 'ngx-webstorage';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { NzTabsCanDeactivateFn } from 'ng-zorro-antd/tabs';
import { Observable, of } from 'rxjs';
import { SlaInternalComponent } from '../sla-internal/sla-internal.component';
@Component({
  selector: 'app-work-flow-detail',
  templateUrl: './work-flow-detail.component.html',
  styleUrls: ['./work-flow-detail.component.scss']
})
export class WorkFlowDetailComponent implements OnInit, IDeactivateComponent, AfterViewChecked {
  @ViewChild('wrapper', { static: true }) wrapper: ElementRef | any;
  @ViewChild('treeselect', { static: true }) tree: ElementRef | any;
  @ViewChild('sla') sla!: SlaInternalComponent;
  //define form
  form: FormGroup = new FormGroup({});
  workFlowInfoForm: FormGroup = new FormGroup({});
  workFlowSLA: any
  commonUtil: any = CommonUtil;
  department_name: any;
  workflowName: any;
  // other define
  showInfoNode: any = false;
  productList: any = [];
  isUpdating: boolean = false;
  nzSelectedIndex: number = 0;
  workFLowId: any = '';
  workflowInfo: any = ''
  workFlowSetup: any = {
    nodes: [],
    connections: [],
  };
  selectIndex = 0;
  addNodeStatus = false;
  node_width = 0;
  node_height = 0
  line_width = 237;
  workflowSize = 0;
  stepConfig: any = [];
  step: number = 1;
  time: any = 0;
  loadInfo = false;
  formInvalid = false;
  //define node and line for workflow
  instance: BrowserJsPlumbInstance | any;
  nodes: any = [];
  connectorPaintStyle = {
    stroke: "#9E9E9E",
    strokeWidth: 2
  };
  connectorHoverStyle = {
    stroke: "#58b7f6",
    strokeWidth: 4
  };
  sourceEndpoint: any = {
    endpoint: {
      type: 'Dot',
      options: { radius: 5, cssClass: 'endpoint source-endpoint' },
    },
    source: true,
    target: false,
    maxConnections: 4,
    connectorStyle: this.connectorPaintStyle,
    connectorHoverStyle: this.connectorHoverStyle,
    // scope: "jsplumb_defaultscope"
  };

  targetEndpoint: any = {
    endpoint: {
      type: 'Dot',
      options: { radius: 5 },
    },
    maxConnections: 4,
    source: false,
    target: true,
    uniqueEndpoint: true,
    deleteEndpointsOnDetach: false
  };

  dragOptions = {
    zIndex: 2000,
    containment: ContainmentType.notNegative,
  };
  connectionOverlays = [
    {
      type: 'PlainArrow',
      options: {
        location: -1,
        length: 8,
        width: 8,
      },
    },
  ];
  connectorProp = {
    type: FlowchartConnector.type,
    options: {
      stub: [10, 15],
      alwaysRespectStubs: true,
      cornerRadius: 0,
      midpoint: 0.2,
      cssClass: 'connection',
    },
  };
  departmentTree: any = [];
  departmentNode: any = [];
  dateInvalid = false;
  changeProduct = false;
  listCondition: any = [
    {
      value: "products", name: "Sản phẩm dịch vụ",
      info: []
    },
    {
      value: "ticket_priority", name: "Độ ưu tiên", info: []
    },
    {
      value: "ticket_category", name: "Phân loại ticket", info: []
    }
  ];
  updateConfig = false;
  unSaved: boolean = true;
  listConditionNoProduct = []
  serviceproduct = [];
  conditionInforList: any[] = [];
  nodeId: any;
  edit = false;
  chartWidth = 0;
  isSuperAdmin = false;
  clickNode = false;
  checked = false;
  application_to: any;
  application_from: any;
  ticket_category: any;
  ticket_priority: any;
  expandKeys: any = [];
  disableForm = false;
  index: any;
  isUpdate = false;
  checkedProduct = false;
  productSelected: any=[];
  styleTooltip = {
    'min-width': 'max-content',
  };
  constructor(
    private modalService: NzModalService,
    private translateService: TranslateService,
    private workFlowService: WorkFlowService,
    private fb: FormBuilder,
    private productService: ProductService,
    private departmentService: DepartmentService,
    private ticketPriorityService: TicketPriorityService,
    private ticketCategoryService: TicketCategoryService,
    private ticketSourceService: TicketSourceService,
    private router: ActivatedRoute,
    private route: Router,
    private toast: ToastService,
    protected localStorage: LocalStorageService,
    private modal: NzModalService
  ) {
    this.router.paramMap.subscribe((res: any) => {
      this.workFLowId = res.get('id') || '';
    });
    this.isUpdate = !this.workFLowId ? false : true;
  }
  ngAfterViewChecked(): void {
    this.chartWidth = document.getElementsByClassName('chart-wrapper')[0]?.scrollWidth;
    this.workflowSize = Math.floor(this.chartWidth / (150 + this.line_width));
    const nodeWidth = 150 + this.line_width;
    const widthAdd = this.chartWidth - this.workflowSize * nodeWidth
    if (this.workflowSize * nodeWidth > widthAdd) {
      this.chartWidth = this.workflowSize * nodeWidth;
    }

  }

  ngOnInit() {
    this.route.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.queryParams
      .subscribe(params => {
        if (params.index == 0) {
          this.disableForm = true;
          this.nzSelectedIndex = 0
        } else if (params.index == 3) {
          this.disableForm = true;
          this.nzSelectedIndex = params.index - 1;
        } else {
          this.nzSelectedIndex = params.index - 1;
        }
        if (params.update == 3) {
          this.nzSelectedIndex = params.update - 1;
        }
        this.index = 2;
      }
      );
    this.conditionInforList[0] = [];
    const self = this;
    let profile = self.localStorage.retrieve('profile');
    this.application_from = new Date();
    this.isSuperAdmin = profile.is_super_admin;
    this.listConditionNoProduct = this.listCondition?.filter((x: any) => x.value != 'products');
    this.initForm();
    this.init();
    // this.loadServiceProduct();
    this.getAllDepartments(null);
  }

  initForm(workFlow?: any) {
    this.workFlowInfoForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      department_id: [[], [Validators.required]],
      conditions: this.fb.array([this.newCondition()]),
      status: [0, []],
      descriptions: ['', []],
      is_global: [false],
    });

    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      wf_step_id: ['', []],
      note: ['', []],
      department_id: ['', []],
      department_name: ['', []]
    });
    if (this.disableForm) {
      this.workFlowInfoForm.get('name')?.disable();
      this.form.get('name')?.disable();
    }
  }

  newCondition(): FormGroup {
    var conditionName = '';
    if (!this.workFLowId) {
      this.getConditionInforList('products', 0);
      conditionName = 'products';
    }
    return this.fb.group({
      conditionName: new FormControl(conditionName, [Validators.required]),
      conditionInformation: new FormControl([], [Validators.required]),
      product_all: [false],
    });
  }
  loadCondition(applicable_to: any): FormArray {
    const conditions = new FormArray([]);
    var i = 0

    for (var item in applicable_to) {
      this.getConditionInforList(item, i);
      if (item == 'products') {
        this.serviceproduct = applicable_to[item];
      }
      conditions.push(this.fb.group({
        conditionName: new FormControl(item == 'product_all' ? 'products' : item, [Validators.required]),
        conditionInformation: new FormControl(item == 'product_all' ? [] : applicable_to[item], [Validators.required]),
        product_all: [item == 'product_all' ? true : false],
      }))
      i++;
    }
    return conditions;
  }
  addConditionForm() {
    if (this.conditions().invalid) {
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      return;
    }
    this.conditions().push(this.newCondition());
  }

  conditions(): FormArray {
    return this.workFlowInfoForm.get('conditions') as FormArray;
  }

  changeCodition(event: any, index: number) {

    this.conditions().at(index).get('conditionInformation')?.setValue([]);
    this.getConditionInforList(event, index);
  }
  // init data
  init() {
    this.instance = newInstance({
      dragOptions: this.dragOptions,
      connectionOverlays: this.connectionOverlays,
      connector: this.connectorProp,
      container: this.wrapper.nativeElement,
    });

    this.listenAddConnection();

    // this.listenDeleteConnection();

    if (this.workFLowId) {
      this.loadWorkFlows();
      this.step = 2
    }
  }

  //load data
  load() {
    if (this.time === 0 && this.workFLowId) {
      this.time = 1;
      this.clearAll();
      setTimeout(() => this.loadNodes(), 500);
    }
  }
  loadConfigWorkflow() {
    if (this.workFlowInfoForm.invalid) {
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      return;
    }
    const fromdate = Math.floor((new Date(this.application_from)).getTime() / 1000);
    const todate = Math.floor((new Date(this.application_to)).getTime() / 1000);
    if (this.application_from && this.application_to && fromdate > todate) {
      this.dateInvalid = true;
      return;
    } else {
      this.dateInvalid = false;
    }
    this.load();
  }
  clearAll() {
    this.nodes = [];
    this.instance.reset();
  }

  clearAllNode() {
    this.nodes = [];
    this.workFlowSetup.nodes = [],
      this.workFlowSetup.connections = [],
      this.instance.reset();
    this.stepConfig = [];
  }

  loadNodes() {
    this.workFlowSetup.nodes.forEach((i: any) => {
      // this.nodes.push(i);
      this.manageNode(i.id);
    });

    setTimeout(() => this.loadConnections());
  }

  loadConnections() {
    const connectorProp = JSON.parse(JSON.stringify(this.connectorProp));
    this.time = 1;
    this.workFlowSetup?.connections?.forEach((i: any) => {
      connectorProp.options.cssClass = 'connection ' + i?.type;
      this.instance.connect({
        source: document.getElementById(i?.pre_step_id),
        target: document.getElementById(i?.next_step_id),
        anchor: 'Continuous',
        connector: connectorProp,
        overlays: this.connectionOverlays,
      });
    });
    this.instance.setZoom(this.zoomP, true);
  }
  loadWorkFlowsInfo() {

  }
  loadWorkFlows() {
    this.clearAll();
    if (!this.loadInfo) {
      this.workFlowService.getWorkFlowsDetail(this.workFLowId, true).subscribe(
        (res: any) => {
          this.checked = res.body.data?.is_global ? true : false
          this.workFlowSLA = res.body.data.sla_internal_list;
          this.workFlowInfoForm = this.fb.group({
            name: [res.body.data.name, []],
            status: [res.body.data.status, []],
            descriptions: [res.body.data.descriptions, []],
            department_id: [res.body.data.department_ids, [Validators.required]],
            is_global: [this.checked, []]
          });
          if (this.disableForm) {
            this.form.get('name')?.disable();
            this.workFlowInfoForm.get('name')?.disable();
          }
          if (!res.body.data?.check_can_edit) {
            this.disableForm = true;
            this.instance.elementsDraggable = false;
            this.workFlowInfoForm.get('name')?.disable()
          }
          this.application_from = res.body.data?.application_from;
          this.application_to = res.body.data?.application_to;
          this.workflowName = res.body.data?.name;
          this.productSelected=res.body.data?.product_list_name?.filter((item:any) => item.status == 0);
          if (res?.body?.data?.applicable_to) {
            this.workFlowInfoForm.addControl('conditions', this.loadCondition(res?.body?.data?.applicable_to))
            if (res?.body?.data?.applicable_to?.product_all) {
              this.checkedProduct = true;
              this.conditions().at(0).get('conditionInformation')?.disable();
            } else {
              this.checkedProduct = false;
              this.conditions().at(0).get('conditionInformation')?.enable();
            }
          } else {
            this.workFlowInfoForm.addControl('conditions', this.fb.array([this.newCondition()]))
          }
          if (!this.checked) {
            this.workFlowInfoForm.get('department_id')?.setValidators([Validators.required]);
            this.workFlowInfoForm.get('department_id')?.updateValueAndValidity();
          } else {
            this.workFlowInfoForm.get('department_id')?.setValue('');
            this.workFlowInfoForm.get('department_id')?.setValidators([]);
            this.workFlowInfoForm.get('department_id')?.updateValueAndValidity();
          }
          this.stepConfig = res.body.data.stepConfigs.data.map((i: any) => {
            return {
              wf_step_id: i.wf_step_id,
              department_id: i.department_id,
              department_name: i.department_name,
              department_name_path: i.department_name_path,
            }
          });
          var dep_name = '';
          var dep_id: '';
          var department_name_path:'';
          this.workFlowSetup.nodes = res.body?.data?.steps.data.map((e: any) => {
            this.stepConfig.forEach((i: any) => {
              if (i.wf_step_id == e.id) {
                dep_name = i.department_name;
                dep_id = i.department_id;
                department_name_path = i.department_name_path;
              }
            })
            return {
              id: e.id,
              x: e.position_x,
              y: e.position_y,
              name: e.name,
              department_name: dep_name,
              department_id: dep_id,
              department_name_path: department_name_path,
            }
          })

          this.workFlowSetup.connections = res.body?.data?.transactions.data.map((e: any) => {
            return {
              pre_step_id: e.pre_step_id,
              next_step_id: e.next_step_id,
              name: e.name
            }
          })
        }
      );
    }
  }

  // loadServiceProduct() {
  //   const params = {
  //     pageIndex: '',
  //     pageSize: 0,
  //     keyword: ''
  //   }
  //   this.productService.search(params, true).subscribe(res => {
  //     this.productList = res.body?.data
  //   });
  // }

  loadSetupConfigInfo(node: any) {
    const nodeSearch = node.id.indexOf('node_');
    if (nodeSearch >= 0) {
      this.form = this.fb.group({
        name: [node.name, [Validators.required, Validators.maxLength(100)]],
        wf_step_id: [node.id, []],
        department_id: [node.department_id],
        department_name: [node.department_name]
      });
    } else {
      this.stepConfig.forEach((item: any) => {
        if (item.wf_step_id == node.id)
          this.form = this.fb.group({
            name: [node.name, [Validators.required, Validators.maxLength(100)]],
            wf_step_id: [item.wf_step_id, []],
            note: [item.note, []],
            department_id: [item.department_id],
            department_name: [item.department_name]
          });
      }
      );
      if (this.disableForm) {
        this.form.get('name')?.disable();
      }
      this.getDepartmentNode(node.department_id);
    }
  }

  getAllDepartments(deptId: any) {
    this.departmentService.getAllDepartments({ selectedId: deptId, data_context: 'config-workflow' }, false).subscribe(
      (res: any) => {
        this.departmentTree = res?.body
      }
    );
  }
  getDepartmentNode(deptId: any) {
    this.departmentService.getAllDepartments({ selectedId: deptId, data_context: 'config-workflow-node' }, false).subscribe(
      (res: any) => {
        this.departmentNode = res?.body
      }
    );
  }
  getDepartmentNodeName(id: any) {
    this.departmentService.find(id).subscribe(
      (res: any) => {
        this.form.patchValue({
          department_name: res?.body?.data?.dept_name
        })
      }
    )
  }
  createDataTree = (dataset: any, type: any) => {
    const hashTable = Object.create(null);
    if (type == 'product') {
      dataset = dataset.concat(this.productSelected)
    }
    dataset.forEach((aData: any) => {
      var name = '';
      if (type == 'department') {
        name = aData.dept_name
      } else if (type == 'product') {
        name = aData.name
      }
      hashTable[aData.id] = { ...aData, children: [], title: name, key: aData.id, isLeaf: false }
    });
    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children?.push(hashTable[aData.id]);
      }
    });
    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };

  getConditionInforList(conditionType: string, index: number) {
    switch (conditionType) {
      case "product_all":
        this.productService.getAll().subscribe(res => {
          this.conditionInforList[index] = this.createDataTree(res.body?.data, 'product');
          this.listCondition[index].info = res.body?.data;
        })
        break;
      case "products":
        this.productService.getAll().subscribe(res => {
          this.conditionInforList[index] = this.createDataTree(res.body?.data, 'product');
          this.listCondition[index].info = res.body?.data;
        })
        break;
      case "ticket_priority":
        this.ticketPriorityService.getAll(true).subscribe(res => {
          this.conditionInforList[index] = res.body?.data;
          this.listCondition[index].info = res.body?.data;
        })
        break;
      case "ticket_category":
        this.ticketCategoryService.getAll().subscribe(res => {
          this.conditionInforList[index] = res.body?.data;
          this.listCondition[index].info = res.body?.data;
        })
        break;
    }
  }

  //
  getNode(node: any) {

    this.open();
    this.loadSetupConfigInfo(node);
  }
  // listen event
  listenOpenNodeEdit() {
    // this.instance.bind('endpoint:click', (e: any, f: any) => {


    // });
  }
  onUpdate() {
    // console.log(1);
  }

  listenAddConnection(): void {
    let conn: any = this.workFlowSetup;

    this.instance.bind(
      'connection',
      (connInfo: any, originalEvent: any) => {
        let overLay = '';

        if (!conn.connections.some((e: any) => e.pre_step_id == connInfo.sourceId && e.next_step_id == connInfo.targetId)) {
          conn.connections.push({
            pre_step_id: connInfo.sourceId,
            next_step_id: connInfo.targetId,
            name: 'chuyển',
          });

        }

        connInfo.connection.addOverlay({
          type: 'Custom',
          options: {
            location: 0.5,
            id: 'customOverlay',
            cssClass: 'endpointSourceLabel',
            create: (c: any) => {
              const d = document.createElement(this.disableForm ? 'button' : 'input');
              d.setAttribute(
                'id',
                `${connInfo.sourceId}${connInfo.targetId}overlay`
              );

              d.addEventListener('change', (event: any) => {
                overLay = event.target.value;
                conn.connections.map((e: any) => {
                  if (
                    e.pre_step_id === connInfo.sourceId &&
                    e.next_step_id === connInfo.targetId
                  ) {
                    e.name = event.target.value;
                  }
                });
              });
              d.style.cssText = 'background: #ECF5FF; padding: 4px 8px; border:none; text-align:center';
              if (conn.connections.some((e: any) => e.pre_step_id === connInfo.sourceId && e.next_step_id === connInfo.targetId)) {
                if (!this.disableForm) {
                  d.value = conn.connections.find((e: any) => e.pre_step_id === connInfo.sourceId && e.next_step_id === connInfo.targetId).name;
                } else {
                  d.innerHTML = conn.connections.find((e: any) => e.pre_step_id === connInfo.sourceId && e.next_step_id === connInfo.targetId).name;
                }

              }
              d.disabled = true;
              return d;
            },
            visible: true,
            events: {
              click: function (j: any) {
                let f: any = document.getElementById(
                  `${connInfo.sourceId}${connInfo.targetId}overlay`
                );
                f.disabled = false;
              },
            },
          },
        });
      }
    );
    this.workFlowSetup = conn;
  }

  listenDeleteConnection(): void {

    this.instance.bind('connection:dblclick', (conn: any, originalEvent: any) => {
      const line = this.workFlowSetup.connections?.find((i: any) => i?.pre_step_id == conn.sourceId && i?.next_step_id == conn.targetId)
      const form = CommonUtil.modalBase(ModalComponent, {
        title: 'Xác nhận xóa',
        content: `Bạn có chắc muốn xóa Line <b>${line?.name || ''}</b> không?`,
        okText: 'Xóa',
        isVisible: true,
        callback: () => {
          return {
            success: true,
          };
        },
      }, '25%',
        false,
        false,
        true, { top: '20px' }, true
      );
      const modal: NzModalRef = this.modalService.create(form);
      modal.afterClose.subscribe(result => {
        if (result && result?.success) {
          this.instance.deleteConnection(conn);
          this.workFlowSetup.connections = this.workFlowSetup.connections?.filter((e: any) => e.next_step_id !== conn.targetId && e.pre_step_id !== conn.sourceId)
        }
      });
      modal.componentInstance.emitter.subscribe((result: any) => {
        if (result?.success) {
          modal.close();
          this.instance.deleteConnection(conn);
          this.workFlowSetup.connections = this.workFlowSetup.connections?.filter((e: any) => e.next_step_id !== conn.targetId && e.pre_step_id !== conn.sourceId)
        } else {
          modal.close();
        }
      });
    });
  }

  addNode() {
    this.addNodeStatus = true;
    this.open();
    this.form.get('name')?.setValue('Level ');
  }
  createNode() {
    var countNode = 0;
    var index = 0
    if (this.workFlowSetup.nodes) {

      countNode = this.workFlowSetup.nodes.length;
      index = Math.floor(this.workFlowSetup.nodes.length / this.workflowSize)
      if (countNode >= this.workflowSize) {
        if (index % 2 == 0) {
          countNode = this.workFlowSetup.nodes.length % this.workflowSize;
        } else {
          countNode = this.workflowSize - this.workFlowSetup.nodes.length % this.workflowSize;
        }
      }

      if (this.workFlowSetup.nodes.length > 0) {
        this.node_width = 150;

      }
    }

    var xCoor = countNode * (this.node_width + this.line_width);
    var yCoor = index * 120;
    let config: any = {
      id: `node_${this.nodes.length + 1}`,
      name: this.form.value.name,
      department_id: this.form.value.department_id,
    };
    this.form.get('wf_step_id')?.setValue(config.id);
    this.stepConfig.push({ wf_step_id: config.id, department_id: config.department_id })
    this.departmentService.find(config.department_id).subscribe(
      (res: any) => {
        this.workFlowSetup.nodes.push({ ...config, x: xCoor, y: yCoor, department_name: res?.body?.data?.dept_name })
        this.nodes.push(config);
        this.manageNode(config.id);
        this.close();
        this.workFlowSetup.nodes.forEach((i: any, key: any) => {
          if (this.workFlowSetup.nodes.length > 1 && key == this.workFlowSetup.nodes.length - 1) {
            const conn = {
              pre_step_id: this.workFlowSetup.nodes[key - 1].id,
              next_step_id: i.id,
              name: 'chuyển'
            };
            this.workFlowSetup.connections.push(conn)
            setTimeout(() => this.addConnection(conn), 500);
          }
        });
        this.addNodeStatus = false;
      }
    )


  }
  saveNode() {
    this.workFlowSetup.nodes.map((e: any) => {
      if (
        e.id === this.form.value.wf_step_id
      ) {
        e.name = this.form.value.name;
        e.department_id = this.form.value.department_id;
        e.department_name = this.form.value.department_name;
      }
    });
    const stepconfig = this.stepConfig.some((i: any) => i.wf_step_id == this.form.value.wf_step_id);
    if (stepconfig) {
      this.stepConfig.find((i: any, k: any) => {
        if (i.wf_step_id == this.form.value.wf_step_id) {
          this.stepConfig[k].name = this.form.value.name;
          this.stepConfig[k].department_id = this.form.value.department_id;
        }
      });
    } else {
      this.stepConfig.push(this.form.value)
    }
    // setTimeout(() => this.loadNodes(), 500)
    setTimeout(() => this.close(), 500)
  }
  onCloseMask() {
    if (this.disableForm) {
      this.close()
      return
    }
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    if (this.addNodeStatus) {
      this.createNode()
    } else {
      this.saveNode();
    }
  }
  confirmNode() {
    if (this.disableForm) {
      return
    }
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    if (this.addNodeStatus) {
      this.createNode()
    } else {
      this.saveNode();
    }
  }

  manageNode(id: string) {
    setTimeout(() => {
      const el = document.getElementById(id)
      if (el) {
        this.instance.manage(document.getElementById(id));
        // this._addEndpoints(id, ['Right'], ['Left']);
      }

    });
  }

  _addEndpoints = (id: any, sourceAnchors: any, targetAnchors: any) => {
    const element = this.instance.getManagedElement(id);
    for (let i = 0; i < sourceAnchors.length; i++) {
      const sourceUUID = id + sourceAnchors[i];
      this.instance.addEndpoint(element, this.sourceEndpoint, {
        anchor: 'Right',
        uuid: sourceUUID,
        scope: 'target_scope',
      });
    }
    for (var j = 0; j < targetAnchors.length; j++) {
      var targetUUID = id + targetAnchors[j];
      this.instance.addEndpoint(element, this.targetEndpoint, {
        anchor: targetAnchors[j],
        uuid: targetUUID,
        scope: 'target_scope',
      });
    }
  };

  open(): void {
    this.form.patchValue({
      id: null,
      name: null,
      department_id: null,
      department_name: null
    })
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }
  closeWorkflow() {
    this.route.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}`]);
  }
  onCancel() {
    this.visible = false;
    this.route.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}`]);
  }
  onChangeData(type: string, content: any): void {
    this.workFlowInfoForm.get(type)?.setValue(content);
  }

  // submit form event
  onSubmitWorkFlowInfo() {
    const fromdate = Math.floor((new Date(this.application_from)).getTime() / 1000);
    const todate = Math.floor((new Date(this.application_to)).getTime() / 1000);
    if (this.disableForm) {
      return
    }
    if (fromdate > todate && this.application_from && this.application_to) {
      this.dateInvalid = true;
      this.formInvalid = true
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      return;
    } else {
      this.formInvalid = false
      this.dateInvalid = false;
    }
    if (this.workFlowInfoForm.invalid) {
      this.formInvalid = true
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      return;
    }
    if (CommonUtil.extractContent(this.workFlowInfoForm.value.descriptions)?.length > 500) {

      return;
    }
    if (!this.workFLowId) {
      if (this.step < 2) {
        this.step = 2
      }
    } else {
      // this.load()
      var obj: any = {
        products: null,
        ticket_category: null,
        ticket_priority: null
      };
      const conditions = this.workFlowInfoForm.value.conditions;
      var obj: any = {
        products: null,
        ticket_category: null,
        ticket_priority: null
      };
      conditions.forEach((x: any) => {
        obj[x.conditionName] = x.conditionInformation;
      })
      if (!obj.products) {
        delete obj.products;
        obj.product_all = this.checkedProduct;
      }
      const workFlow: any = {
        name: this.workFlowInfoForm.value.name,
        steps: this.workFlowSetup.nodes,
        transactions: this.workFlowSetup.connections,
        step_configs: this.stepConfig,
        applicable_to: obj,
        department_ids: this.workFlowInfoForm.value.department_id,
        descriptions: this.workFlowInfoForm.value.descriptions,
        status: this.workFlowInfoForm.value.status,
        application_to: this.application_to || '',
        application_from: this.application_from || '',
        is_global: this.workFlowInfoForm.value.is_global ? 1 : 0
      };

      const body = CommonUtil.trim(workFlow);

      this.workFlowService.update(this.workFLowId, body, true).subscribe((res: any) => {
        for (var item in res.body.data.applicable_to) {
          if (item == 'products') {
            this.serviceproduct = res.body.data.applicable_to[item];
          }
        }
        if (this.changeProduct) {
          this.sla.loadData()
        }
        this.changeProduct = false
      });
    }
  }
  onSaveConfigWorkFlow() {
    if (this.disableForm) {
      return
    }
    if (!this.workFLowId) {
      this.step = 3
    }
    let nodes = document.querySelectorAll(".box");
    nodes.forEach((i: any) =>
      this.workFlowSetup.nodes.map((e: any) => {
        if (e.id === i.id) {
          e.position_x = i.offsetLeft;
          e.position_y = i.offsetTop;
        }
        delete e.x
        delete e.y
      })
    );
    const conditions = this.workFlowInfoForm.value.conditions;
    var obj: any = {
      products: null,
      ticket_category: null,
      ticket_priority: null
    };
    conditions.forEach((x: any) => {
      obj[x.conditionName] = x.conditionInformation;
    })
    const workFlow: any = {
      name: this.workFlowInfoForm.value.name,
      steps: this.workFlowSetup.nodes,
      transactions: this.workFlowSetup.connections,
      step_configs: this.stepConfig,
      applicable_to: obj,
      department_ids: this.workFlowInfoForm.value.department_id,
      descriptions: this.workFlowInfoForm.value.descriptions,
      status: this.workFlowInfoForm.value.status,
      application_to: this.application_to || '',
      application_from: this.application_from || '',
      is_global: this.workFlowInfoForm.value.is_global ? 1 : 0
    };
    const body = CommonUtil.trim(workFlow);

    this.workFlowService.update(this.workFLowId, body, true).subscribe((res: any) => {
      this.stepConfig = res.body?.data?.stepConfigs?.data.map((i: any) => {
        return {
          wf_step_id: i.wf_step_id,
          department_id: i.department_id,
          department_name: i.department_name,
        }
      });
      var dep_name = '';
      var dep_id: '';
      this.workFlowSetup.nodes = res.body?.data?.steps?.data.map((e: any) => {
        this.stepConfig.forEach((i: any) => {
          if (i.wf_step_id == e.id) {
            dep_name = i.department_name;
            dep_id = i.department_id;
          }
        })
        return {
          id: e.id,
          x: e.position_x,
          y: e.position_y,
          name: e.name,
          department_name: dep_name,
          department_id: dep_id
        }
      })

      this.workFlowSetup.connections = res.body?.data?.transactions.data.map((e: any) => {
        return {
          pre_step_id: e.pre_step_id,
          next_step_id: e.next_step_id,
          name: e.name
        }
      })
      this.clickNode = false;
      this.form.reset();
    });
  }
  onSubmitConfigWorkFlow(type?: any): void {
    if (this.disableForm) {
      return;
    }
    this.edit = true;
    if (this.workFlowInfoForm.invalid) {
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      return;
    }
    const fromdate = Math.floor((new Date(this.application_from)).getTime() / 1000);
    const todate = Math.floor((new Date(this.application_to)).getTime() / 1000);
    if (fromdate > todate && this.application_from && this.application_to) {
      this.dateInvalid = true;
      return;
    } else {
      this.dateInvalid = false;
    }
    let nodes = document.querySelectorAll(".box");
    if (nodes.length > 0) {
      nodes.forEach((i: any) =>
        this.workFlowSetup.nodes.map((e: any) => {
          if (e.id === i.id) {
            e.position_x = i.offsetLeft;
            e.position_y = i.offsetTop;
          }
          delete e.x
          delete e.y
        })
      );
    } else {
      this.workFlowSetup.nodes.map((e: any) => {
        e.position_x = e.x;
        e.position_y = e.y;
        delete e.x
        delete e.y
      })
    }
    const conditions = this.workFlowInfoForm.value.conditions;
    var obj: any = {
      products: null,
      ticket_category: null,
      ticket_priority: null
    };
    conditions.forEach((x: any) => {
      obj[x.conditionName] = x.conditionInformation;
    })
    if (!obj.products) {
      delete obj.products;
      obj.product_all = this.checkedProduct;
    }
    const workFlow: any = {
      name: this.workFlowInfoForm.value.name,
      steps: this.workFlowSetup.nodes,
      transactions: this.workFlowSetup.connections,
      step_configs: this.stepConfig,
      applicable_to: obj,
      department_ids: this.workFlowInfoForm.value.department_id,
      descriptions: this.workFlowInfoForm.value.descriptions,
      status: this.workFlowInfoForm.value.status,
      application_to: this.application_to || '',
      application_from: this.application_from || '',
      is_global: this.workFlowInfoForm.value.is_global ? 1 : 0
    };

    const body = CommonUtil.trim(workFlow);
    let serviceEvent = this.workFLowId ? this.workFlowService.update(this.workFLowId, body, true) : this.workFlowService.createWorkFlowInfo(body, true)

    serviceEvent.subscribe((res: any) => {
      if (res) {
        this.toast.success(`model.ticket.success.${this.workFLowId ? 'update' : 'create'}`);
        // this.route.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/detail/${res.body.data.id}`],  { queryParams: { index: 3 } });
        if (type == 'page') {
          this.route.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/detail/${res.body.data.id}`], { queryParams: { update: 3 } });
        }
      }
    }, (e: any) => {
      this.isUpdating = false;
    },
      () => {
        this.isUpdating = false;
      });
  }

  visible = false;
  // Zoom IN / OUT event
  zoomP = 1;

  zoom(type: any) {
    this.zoomP += type === 'in' ? 0.1 : -0.1;

    this.instance.setZoom(this.zoomP, true);
    this.wrapper.nativeElement.style.transform = `scale(${this.zoomP})`;
  }

  resetZoom() {
    this.zoomP = 1;
    this.instance.setZoom(this.zoomP, true);
    this.wrapper.nativeElement.style.transform = `scale(${this.zoomP})`;
  }

  onMouseWheel(e: any) {
    if (this.disableForm) {
      return
    }
    e.preventDefault();
    if (this.zoomP <= 0.55 && e.deltaY > 0) return;
    if (this.zoomP >= 2 && e.deltaY < 0) return;
    this.zoomP += e.deltaY < 0 ? 0.1 : -0.1;
    this.instance.setZoom(this.zoomP, true);
    this.wrapper.nativeElement.style.transform = `scale(${this.zoomP})`;
  }
  onChangeProduct(empIndex: any) {
    this.changeProduct = true;
    this.conditions().at(empIndex).get('conditionInformation')?.setValue(this.serviceproduct)
  }
  removeCondition(empIndex: any) {
    this.conditions().removeAt(empIndex);
  }
  deleteNode(node: any) {
    if (this.disableForm) {
      return
    }
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa Node <b>${node.name}</b> không?`,
      okText: 'Xóa',
      isVisible: true,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result && result?.success) {
        this.workFlowSetup.connections.forEach((i: any) => {
          if (i.next_step_id == node.id || i.pre_step_id == node.id) {
            var conn = this.instance.getConnections({
              target: i?.next_step_id,
            });
            this.instance.deleteConnection(conn[0]);
          }
        });
        this.instance.removeAllEndpoints(document.getElementById(node.id));
        var connect = {
          pre_step_id: null,
          next_step_id: null,
          name: 'chuyển'
        }
        console.log(this.workFlowSetup.nodes)
        this.workFlowSetup.nodes?.forEach((el: any, key: any) => {
          if (el.id == node.id) {
            if (key != 0 || key != this.workFlowSetup.nodes?.length - 1) {
              connect.pre_step_id = this.workFlowSetup.nodes[key - 1]?.id;
              connect.next_step_id = this.workFlowSetup.nodes[key + 1]?.id;
            }
          }
        });
        this.workFlowSetup.connections = this.workFlowSetup.connections?.filter((i: any) => (i.next_step_id != node.id && i.pre_step_id != node.id));
        this.stepConfig = this.stepConfig?.filter((i: any) => i.wf_step_id != node.id);
        this.nodes = this.nodes?.filter((i: any) => i.wf_step_id != node.id);
        this.workFlowSetup.nodes = this.workFlowSetup.nodes?.filter((i: any) => i.id != node.id);
        if (connect.pre_step_id && connect.next_step_id) {
          setTimeout(() => this.addConnection(connect), 500)
        }
        modal.close();

      } else {
        modal.close();
      }
    });
  }
  canExit(): Observable<boolean> {
    if (this.workFLowId && !this.edit) {
      if (this.clickNode || !this.workFlowInfoForm.untouched || !this.form.untouched || this.changeProduct) {
        return this.canExitPage()
      }
      return of(true);
    }
    return of(true)
  }
  canDeactivate: NzTabsCanDeactivateFn = (fromIndex: number, toIndex: number) => {
    if (toIndex == 1) {
      if (this.time == 0) {
        setTimeout(() => {
          this.clearAll()
          this.loadNodes();
        }, 400)
      } else if (this.time == 1) {
        if (this.updateConfig && !this.disableForm) {
          this.updateConfig = false;
          setTimeout(() => {
            this.clearAll()
            this.loadNodes();
          }, 400)
        }
      }
    }
    switch (fromIndex) {
      case 0:
        if (!this.workFLowId) {
          this.onSubmitWorkFlowInfo();
          if (!this.formInvalid) {
            this.nzSelectedIndex = toIndex
            return true
          } else {
            return false
          }
        } else {
          if (!this.workFlowInfoForm.untouched || this.changeProduct) {
            this.onSubmitWorkFlowInfo();
            if (!this.formInvalid) {
              this.nzSelectedIndex = toIndex
              return true
            } else {
              return false
            }
          } else {
            this.nzSelectedIndex = toIndex
            return true;
          }
        }

      case 1:
        if (this.workFLowId && (this.clickNode || !this.form.untouched)) {
          this.updateConfig = true;
          this.onSaveConfigWorkFlow();
        }
        this.nzSelectedIndex = toIndex
        this.step = 3;
        return true;
      default:
        this.nzSelectedIndex = toIndex
        return true;
    }
  };
  saveWorkFlow: NzTabsCanDeactivateFn = (fromIndex: number, toIndex: number) => {
    if (toIndex == 1) {
      if (this.time == 0) {
        setTimeout(() => {
          this.clearAll()
          this.loadNodes();
        }, 400)
      } else if (this.time == 1) {
        if (this.updateConfig) {
          this.clearAll()
          this.loadNodes();
          this.updateConfig = false;
        }
      }
    }
    switch (fromIndex) {
      case 1:
        if (this.workFLowId && (this.clickNode || !this.form.untouched)) {
          this.updateConfig = true;
          this.onSaveConfigWorkFlow();
        }
        this.nzSelectedIndex = toIndex
        this.onSubmitConfigWorkFlow();
        return true;
      default:
        this.nzSelectedIndex = toIndex
        return true;
    }
  };
  onSubmitWorkFlowConfigIndex() {
    const fromdate = Math.floor((new Date(this.application_from)).getTime() / 1000);
    const todate = Math.floor((new Date(this.application_to)).getTime() / 1000);
    if (fromdate > todate && this.application_from && this.application_to) {
      this.dateInvalid = true;
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      this.nzSelectedIndex = 0
      return;
    } else {
      this.dateInvalid = false;
    }
    if (this.workFlowInfoForm.invalid) {
      CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
      this.nzSelectedIndex = 0
      return;
    }
    if (CommonUtil.extractContent(this.workFlowInfoForm.value.descriptions)?.length > 500) {
      this.nzSelectedIndex = 0
      return;
    }
    this.step = 3
    if (this.workFLowId) {
      if (!this.edit) {
        if (this.clickNode || !this.workFlowInfoForm.untouched || !this.form.untouched || this.changeProduct) {
          this.onSubmitConfigWorkFlow('tab');
        } else {
          this.nzSelectedIndex = 2
        }
      }
    } else {
      this.nzSelectedIndex = 2
    }
  }
  // changeTagSLA(fromIndex: number, toIndex: number): Observable<boolean> {
  //   if (this.disableForm) {
  //     return of(true)
  //   }
  //   return new Observable(observer => {
  //     this.modal.confirm({
  //       nzTitle: 'Xác nhận',
  //       nzContent: 'Thay đổi của bạn chưa được lưu lại. Bạn có muốn lưu lại không?',
  //       nzClassName: 'leavePage',
  //       nzOnOk: () => {
  //         this.onSubmitConfigWorkFlow('tag');

  //         observer.next(true);
  //         observer.complete();
  //         this.nzSelectedIndex = toIndex
  //       },
  //       nzOnCancel: () => {
  //         observer.next(false);
  //         observer.complete();
  //         this.nzSelectedIndex = fromIndex
  //       }
  //     });
  //   });
  // }
  canExitPage(): Observable<boolean> {
    if (this.disableForm) {
      return of(true)
    }
    return new Observable(observer => {
      this.modal.confirm({
        nzTitle: 'Xác nhận',
        nzContent: 'Thay đổi của bạn chưa được lưu lại. Bạn có muốn rời khỏi không?',
        nzClassName: 'leavePage',
        nzOnOk: () => {
          observer.next(true);
          observer.complete();
        },
        nzOnCancel: () => {
          observer.next(false);
          observer.complete();
        }
      });
    });
  }
  onClickNoode() {
    this.clickNode = true;
  }
  onChangeChecked($event: any) {
    this.checked = this.workFlowInfoForm.value.is_global;
    let department_id = this.workFlowInfoForm.get('department_id');
    if (!this.checked) {
      department_id?.setValidators([Validators.required]);
      department_id?.updateValueAndValidity();
    } else {
      department_id?.setValue('');
      department_id?.setValidators([]);
      department_id?.updateValueAndValidity();
    }
  }
  onChangeProductAllChecked() {
    this.changeProduct = true;
    let conditionInfo = this.conditions().at(0).get('conditionInformation');
    this.checkedProduct = this.conditions().at(0).get('product_all')?.value;
    if (!this.checkedProduct) {
      conditionInfo?.enable();
      conditionInfo?.setValidators([Validators.required]);
      conditionInfo?.updateValueAndValidity();
    } else {
      this.workFlowInfoForm;

      this.serviceproduct = [];
      conditionInfo?.setValue('');
      conditionInfo?.setValidators([]);
      conditionInfo?.disable();
      conditionInfo?.updateValueAndValidity();
    }
  }
  onChangeTimeline() {
    if (this.application_from && this.application_to) {
      const fromdate = Math.floor((new Date(this.application_from)).getTime() / 1000);
      const todate = Math.floor((new Date(this.application_to)).getTime() / 1000);
      if (fromdate > todate) {
        this.dateInvalid = true;
        return;
      } else {
        this.dateInvalid = false;
      }
    } else {
      this.dateInvalid = false;
    }
  }
  onChangeDepartment($event: any) {
    if ($event) {
      this.getDepartmentNodeName($event)
    }
  }
  addConnection(i: any) {
    const connectorProp = JSON.parse(JSON.stringify(this.connectorProp));
    connectorProp.options.cssClass = 'connection ' + i?.name;

    this.instance.connect({
      source: document.getElementById(i?.pre_step_id),
      target: document.getElementById(i?.next_step_id),
      anchor: 'Continuous',
      connector: connectorProp,
    });
    this.instance.setZoom(this.zoomP, true);
    this.wrapper.nativeElement.style.transform = `scale(${this.zoomP})`;
  }
  filter($event: any, type: any) {
    if (type == 'ticket_priority') {
      this.ticket_priority = $event;
    } else if (type == 'ticket_category') {
      this.ticket_category = $event;
    }

  }
  onChangeTitle() {
    this.workFlowInfoForm.get('name')?.setValue(this.workFlowInfoForm.value.name?.trim())
  }
  scrollElementNode() {
    this.getDepartmentNode(this.form?.value.department_id || null);

    if (this.form?.value.department_id) {
      setTimeout(() => {
        const el = document.getElementsByClassName('deptNode')[0]?.getElementsByClassName('ant-select-tree-node-selected')[0];
        el?.scrollIntoView()
      }, 500)
    }
    return true;
  }
  scrollElementDeptIds() {
    const department_id = this.workFlowInfoForm?.value.department_id.length > 0 ? this.workFlowInfoForm?.value.department_id[0] : null;
    this.getAllDepartments(department_id);
    if (department_id) {
      setTimeout(() => {
        const el = document.getElementsByClassName('dept')[0]?.getElementsByClassName('ant-select-tree-node-selected')[0];
        el?.scrollIntoView()
      }, 500)
    }

  }
  onUpdateStatus() {
    if (this.disableForm) {
      return
    }
    if (!this.workFlowInfoForm.value.status) {
      let toast = 'Luồng xử lý chưa được cập nhật đủ thông tin';
      if (this.workFlowInfoForm.invalid) {
        CommonUtil.markFormGroupTouched(this.workFlowInfoForm);
        this.toast.error(toast)
        return
      }
      if (this.workFlowSetup.nodes.length == 0 || this.workFlowSLA?.length == 0) {
        this.toast.error(toast)
      } else {
        this.workFlowInfoForm.patchValue(
          {
            status: !this.workFlowInfoForm.value.status
          }
        )
      }
    } else {
      this.workFlowInfoForm.patchValue(
        {
          status: !this.workFlowInfoForm.value.status
        }
      )
    }
  }
  loadInfoWfl() {
    if (!this.loadInfo) {
      this.workFlowService.getWorkFlowsDetail(this.workFLowId, true).subscribe(
        (res: any) => {
          this.workFlowSLA = res.body.data.sla_internal_list;
        }
      )
    }
  }
}
