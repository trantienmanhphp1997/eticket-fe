import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { DeactivateGuard } from '@core/guard/decativate.guard';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { SlaInternalComponent } from '../sla-internal/sla-internal.component';
import { WorkFlowDetailComponent } from './work-flow-detail/work-flow-detail.component';
import { WorkFlowCloneComponent } from './workflow-clone/workflow-clone.component';
import { WorkflowComponent } from './workflow.component';

const routes: Routes = [
  {
    path: '',
    component: WorkflowComponent,
    // canActivate: [AuthGuard],
    data: {
      authorities: [],
      title: 'model.workFlow.title',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: `javascript:void(0)`
        },
      ]
    }
  },
  {
    path: ROUTER_UTILS.workflowConfiguration.create,
    component: WorkFlowDetailComponent,
    // canActivate: [AuthGuard],
    data: {
      authorities: [],
      title: 'model.workFlow.create',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: `javascript:void(0)`
        },
        {
          level: 3,
          title: 'model.workFlow.title',
          path: `admin/${ROUTER_UTILS.workflowConfiguration.root}`,
        }
      ]
    }
  },
  {
    path: `${ROUTER_UTILS.workflowConfiguration.detail}/:id`,
    component: WorkFlowDetailComponent,
    canDeactivate: [DeactivateGuard],
    // canActivate: [AuthGuard],
    data: {
      authorities: [],
      title: 'model.workFlow.update',
      titleOther: 'model.workFlow.detail',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: `javascript:void(0)`
        },
        {
          level: 3,
          title: 'model.workFlow.title',
          path: `admin/${ROUTER_UTILS.workflowConfiguration.root}`
        }
      ]
    }
  },
  {
    path: `${ROUTER_UTILS.workflowConfiguration.view}/:id`,
    component: WorkFlowDetailComponent,
    canDeactivate: [DeactivateGuard],
    // canActivate: [AuthGuard],
    data: {
      authorities: [],
      title: 'model.workFlow.update',
      titleOther: 'model.workFlow.detail',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: `javascript:void(0)`
        },
        {
          level: 3,
          title: 'model.workFlow.title',
          path: `admin/${ROUTER_UTILS.workflowConfiguration.root}`
        }
      ]
    }
  },
  {
    path: `${ROUTER_UTILS.workflowConfiguration.listSla}/:id`,
    component: SlaInternalComponent,
    data: {
      authorities: [],
      title: 'model.workFlow.detail',
      breadcrum: [
        {
          level: 1,
          title: 'model.workFlow.title',
          path: `admin/${ROUTER_UTILS.workflowConfiguration.root}`
        }
      ]
    }
  },
  {
    path: `${ROUTER_UTILS.workflowConfiguration.clone}/:id`,
    component: WorkFlowCloneComponent,
    // canActivate: [AuthGuard],
    data: {
      authorities: [],
      title: 'model.workFlow.clone',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: `javascript:void(0)`
        },
        {
          level: 3,
          title: 'model.workFlow.title',
          path: `admin/${ROUTER_UTILS.workflowConfiguration.root}`
        }
      ]
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    DeactivateGuard
  ]
})
export class WorkflowRoutingModule { }
