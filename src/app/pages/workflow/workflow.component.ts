import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ACTION } from '@shared/constants/common.constant';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { AUTH } from '@shared/constants/auth.constant';
import { CommonService } from '@shared/service/common.service';
import { DepartmentService } from '@shared/service/department.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { WorkFlowService } from '@shared/service/work-flow.service';
import CommonUtil from '@shared/utils/common-utils';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { ProductService } from '@shared/service/product.service';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { ModalConfirmComponent } from '@shared/components/modal-confirm/modal-confirm.component';

@Component({
  selector: 'app-root',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss'],
})
export class WorkflowComponent implements OnInit {
  isCallFirstRequest: boolean = true;
  // page data
  departmentList: any = [];
  productList: any = [];
  deptList: any = [];
  provices: any = []
  total: any = 0;
  department_id: any;
  product_id: any;
  visible: boolean = false;
  commonUtil: any = CommonUtil;
  newOder: any;
  workflowQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    parentId: '',
    orderBy: '',
    sortBy: '',
    keyword: '',
    department_id: '',
    product_id: ''
  };
  chartWidth = 0;
  formModal = {};
  //checked items
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly any[] = [];
  setOfCheckedId = new Set<number>();
  checked = false;
  auth: any = AUTH;

  //tree
  defaultCheckedNode: any = []
  defaultCheckedKeys = ['0-0-0'];
  defaultSelectedKeys = ['0-0-0'];
  defaultExpandedKeys = ['0-0', '0-0-0', '0-0-1'];
  nodes = [];

  constructor(
    private fb: FormBuilder,
    private workFlowService: WorkFlowService,
    private commonService: CommonService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toast: ToastService,
    private router: Router,
    private departmentService: DepartmentService,
    private serviceProductService: ProductService
  ) { }

  searchForm = this.fb.group({
    keyword: [null]
  });

  ngOnInit() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.loadData();
  }

  // load data
  loadData() {
    const params = {
      pageIndex: this.workflowQuery.pageIndex,
      pageSize: this.workflowQuery.pageSize,
      keyword: this.workflowQuery.keyword,

    }
    this.workFlowService.getWorkFlows(params, true).subscribe(
      (res) => {
        this.departmentList = res.body?.data?.map((dept: any) => {
          return {
            ...dept, disabled: dept.dept_code === 'TCT'
          }
        })
        this.total = res.body?.meta?.pagination?.total

      }
    );

  }
  getProVices(): void {
    const params = {
      keyword: ''
    }
    this.commonService.getProvinces(params, true).subscribe(
      (res: any) => {
        this.provices = res.body?.data
      }
    );
  }

  // Events
  onSearch(event: any) {
    const keyword = this.searchForm.value.keyword
    this.workflowQuery.keyword = keyword?.trim() || null;
    // this.department_id = '';
    // this.product_id = '';
    this.searchForm.patchValue({
      keyword: this.workflowQuery.keyword
    })
    this.workflowQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.loadData();
  }

  nzEvent(event: NzFormatEmitEvent): void {
    this.defaultCheckedNode = event.keys
    this.workflowQuery.parentId = this.defaultCheckedNode ? this.defaultCheckedNode.toString() : '',
      this.workflowQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.loadData();
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    this.workflowQuery.sortBy = params.sort.filter(e => e.value !== null).map(e => e.value?.slice(0, -3)).toString();
    this.workflowQuery.orderBy = params.sort.filter(e => e.key !== null && e.key !== undefined && e.value !== null).map(e => e.key).toString();
    this.loadData();
  }

  onRefresh(): void {
    this.loadData()
  }

  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.workflowQuery.pageIndex = pageIndex;
    this.workflowQuery.pageSize = pageSize;
    this.loadData();
  }

  onCreate = () => {
    this.router.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/${ROUTER_UTILS.workflowConfiguration.create}`]);
  }

  onUpdate = (data: any) => {
    if (!data.check_can_edit) {
      return
    }
    if (data?.id) {
      this.router.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/${ROUTER_UTILS.workflowConfiguration.detail}`, data?.id]);
    }
  }

  onView = (data: any) => {
    this.router.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/${ROUTER_UTILS.workflowConfiguration.view}/${data?.id}`]);
  }
  openSlaInternal(workflowId: any) {
    if (workflowId) {
      this.router.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/${ROUTER_UTILS.workflowConfiguration.listSla}`, workflowId]);
    }
  }
  onClone(data: any) {
    this.router.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/${ROUTER_UTILS.workflowConfiguration.clone}/${data?.id}`]);
  }
  onDelete = (data: any) => {
    if (!data.check_can_edit) {
      return
    }
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa luồng xử lý <b>${data?.name}</b> không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let departmentIds = data instanceof Set ? Array.from(data) : [data.id];

    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.workFlowService.delete(departmentIds).subscribe((res: any) => {
          if (res) {
            this.toast.success(`model.workFlow.success.delete`);
            // this.getAllDepartments()
            modal.close();
            this.loadData()
          };
        })
      } else {
        modal.close();
      }
    });
  }
  onDeleteMulti = (data: any) => {
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa luồng xử lý đã chọn không?`,
      okText: 'Xóa',
      isVisible: true,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let departmentIds = data instanceof Set ? Array.from(data) : [data.id];
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.workFlowService.deleteMulti({ workflowIds: departmentIds }).subscribe((res: any) => {
          if (res) {
            this.toast.success(`model.workFlow.success.delete`);
            // this.getAllDepartments()
            modal.close();
            this.checked = true;
            this.setOfCheckedId = new Set();
            this.loadData()
          };
        })
      } else {
        modal.close();
      }
    });
  }
  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);

    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.departmentList.filter((item: any) => !item.disabled);
    this.checked = listOfEnabledData.every((item: any) => this.setOfCheckedId.has(item.id));
    this.indeterminate = listOfEnabledData.some((item: any) => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.departmentList.forEach((item: any) => {
      if (item.check_can_edit) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }
  openFillter() {
    this.visible = true;
    this.getAllDepartmentData();
    this.getAllServiceProductData();
  }
  onClose() {
    this.visible = false;
  }
  onFilterData() {
    if (this.department_id || this.product_id) {
      this.workflowQuery.keyword = '';
      this.searchForm.patchValue({
        keyword: null,
      })
      this.workflowQuery.department_id = this.department_id;
      this.workflowQuery.product_id = this.product_id;
      this.workFlowService.getWorkFlows(this.workflowQuery, true).subscribe(res => {
        this.departmentList = res.body?.data;
      });
      this.visible = false;
    } else {
      this.visible = false;
      this.workflowQuery.department_id = this.department_id;
      this.workflowQuery.product_id = this.product_id;
      this.workFlowService.getWorkFlows(this.workflowQuery, true).subscribe(res => {
        this.departmentList = res.body?.data;
      });
    }
  }
  onResetFilter() {
    this.department_id = '';
    this.product_id = '';
    this.onFilterData();
  }
  getAllDepartmentData() {
    const params = {
      limit: 10000,
      data_context: 'manage-user'
    }
    this.departmentService.getAllDepartments(params, true).subscribe((res) => {
      this.deptList = res.body;
    });
  }
  createDataTree = (dataset: any) => {
    const hashTable = Object.create(null);
    dataset.forEach((aData: any) => hashTable[aData.id] = { ...aData, children: [], title: aData.dept_name, key: aData.id, isLeaf: false });

    const dataTree: any = [];

    dataset.forEach((aData: any) => {
      if (aData.parent_id) {
        hashTable[aData.parent_id]?.children.push(hashTable[aData.id]);
      }
    });

    dataset.forEach((aData: any) => {
      if (hashTable[aData.id].children.length == 0) {
        hashTable[aData.id].isLeaf = true
      }
      else {
        hashTable[aData.id].expanded = false
      }
      if (!aData.parent_id) {

        dataTree.push(hashTable[aData.id]);
      }
    });

    return dataTree;
  };
  getAllServiceProductData() {
    const params = {
      pageIndex: 0,
      pageSize: 0,
    }
    this.serviceProductService.getAll(true).subscribe((res) => {
      this.productList = res.body?.data;
    });
  }
  onChangeDepartment(event: any) {
    this.department_id = event;
  }
  onChangeServiceProduct(event: any) {
    this.product_id = event;

  }
  getServiceProducts(products: any): any {
    var result = [];
    if (products.length > 0) {
      result = products.filter((item: any, key: any) => key != 0)
    }
    return result;
  }
  drop($event: any) {
    const triggerDrag = this.departmentList?.find((x: any, k: any) => k == $event.previousIndex);
    const triggerDragNew = this.departmentList?.find((x: any, k: any) => k == $event.currentIndex);
    // let getFirstValue = this.departmentList.at();
    // if (getFirstValue.order !== 1) {
    //   let getSecondValue = this.departmentList.at(1);
    //   this.newOder = getSecondValue.order;
    // }
    const params = {
      id: triggerDrag?.id,
      // new_order: this.newOder ? this.newOder : $event.currentIndex + 1
      new_order: triggerDragNew?.order
    }
    this.workFlowService.updateOrder(params, true).subscribe(res => {
      this.loadData()
    })
  }
  onUpdateStatus(data: any) {
    if (!data?.status) {
      if (data.steps?.data.length == 0 || data?.sla_internal_list.length == 0) {
        this.toast.error('Luồng xử lý chưa được cập nhật đủ thông tin')
      } else{
        this.openPopup(data);
      }
    } else {
      this.openPopup(data);
    }
  }
  onChangeDi($event: any) {
    console.log($event)
  }
  onViewSLA(data: any) {
    if (data?.id) {
      this.router.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/${ROUTER_UTILS.workflowConfiguration.detail}/${data?.id}`], { queryParams: { index: 3 } });
    }
  }
  openPopup(data: any) {
    data.status=!data?.status;
    const form = CommonUtil.modalBase(ModalConfirmComponent, {
      title: 'Xác nhận thay đổi trạng thái',
      content: `Bạn có chắc muốn thay đổi trạng thái <b>${data?.name}</b> không?`,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      false, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.workFlowService.update(data?.id, {workflow_status:data.status}).subscribe(res => {
          if (res) {
            this.toast.success('Cập nhật thành công');
          } else {
            this.toast.success('Cập nhật thất bại');
          }
          modal.close();
        })
      } else {
        data.status = !data.status;
        modal.close();
      }
    });
  }
}
