import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { WorkFlowDetailComponent } from './work-flow-detail/work-flow-detail.component';
import { WorkflowRoutingModule } from './workflow-routing.module';
import { WorkflowComponent } from './workflow.component';
import { SlaInternalComponent } from './sla-internal/sla-internal.component';
import { DeactivateGuard } from './../../core/guard/decativate.guard';
import { WorkFlowCloneComponent } from './workflow-clone/workflow-clone.component';



@NgModule({
  declarations: [WorkflowComponent, WorkFlowDetailComponent, SlaInternalComponent, WorkFlowCloneComponent],
  imports: [
    CommonModule,
    SharedModule,
    WorkflowRoutingModule,
  ],
  providers: [DeactivateGuard],
  exports: [WorkflowComponent]
})
export class WorkflowModule { }
