import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { TicketPriorityComponent } from './ticket-priority.component'
const routes: Routes = [
  {
    path: '',
    component: TicketPriorityComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.TICKET_PRIORITY_CONFIG.LIST],
      title: 'sidebar.ticket_priority',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình ticket',
          path: `javascript:void(0)`
        },
      ]
    }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketPriorityRoutingModule { }
