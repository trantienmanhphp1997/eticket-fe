import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketPriorityRoutingModule }  from './ticket-priority-routing.module'
import { TicketPriorityComponent } from './ticket-priority.component';
import { SharedModule } from '@shared/shared.module';
import { TicketPriorityDetailComponent } from './ticket-priority-detail/ticket-priority-detail.component'; 


@NgModule({ 
  declarations: [TicketPriorityComponent, TicketPriorityDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    TicketPriorityRoutingModule
  ]
})
export class TicketPriorityModule { }
