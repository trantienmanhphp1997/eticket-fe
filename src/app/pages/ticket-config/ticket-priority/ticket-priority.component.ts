import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TicketPriorityService } from '../../../shared/service/ticket-priority.service';
import { PAGINATION } from '@shared/constants/pagination.constants';
import CommonUtil from '@shared/utils/common-utils';
import { AUTH } from '@shared/constants/auth.constant';
import { TicketPriorityDetailComponent } from './ticket-priority-detail/ticket-priority-detail.component';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { ACTION } from '@shared/constants/common.constant';
import { ModalConfirmComponent } from '@shared/components/modal-confirm/modal-confirm.component';

@Component({
  selector: 'app-ticket-priority',
  templateUrl: './ticket-priority.component.html',
  styleUrls: ['./ticket-priority.component.scss']
})
export class TicketPriorityComponent implements OnInit {
  @Output() emitter = new EventEmitter();

  ticketPriorityList: any = [];
  formModal = {};
  keyword: any = '';
  setOfCheckedId = new Set<number>();
  indeterminate = false;
  checked = false;
  total: any = 0;
  newOder:any;
  pageIndex = PAGINATION.PAGE_DEFAULT;
  pageSize = PAGINATION.SIZE_DEFAULT;
  pageSizeOptions = PAGINATION.OPTIONS;
  listOfCurrentPageData: readonly any[] = [];
  nodes = [];
  nodeCategory = [];
  commonUtil = CommonUtil;
  auth: any = AUTH;
  constructor(
    private ticketPriorityService: TicketPriorityService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toast: ToastService,
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    const params = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      keyword: this.keyword
    }
    this.ticketPriorityService.search(params, true).subscribe(res => {
      this.ticketPriorityList = res.body?.data?.map((dept: any) => {
        return {
          ...dept,
          disabled: dept.check_can_edit ===0,
        }
      })
      this.total = res.body?.meta?.pagination?.total
      console.log(this.ticketPriorityList)
    });
  }
  onCreate = () => {
    const base = CommonUtil.modalBase(TicketPriorityDetailComponent, { nodes: this.nodes, nodeCategory: this.nodeCategory }, '30%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }
  onUpdate = (data: any) => {

    const base = CommonUtil.modalBase(TicketPriorityDetailComponent, { action: ACTION.UPDATE,selectedTicketPriority: data, nodes: this.nodes, nodeCategory: this.nodeCategory, isUpdate: true }, '30%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData();
      }
    });
  }
  onDelete = (data: any) => {
    if (data.check_can_edit==1){
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa độ ưu tiên ticket <b>${data?.name}</b> không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let ticketPriorityId = data instanceof Set ? Array.from(data) : [data.id];
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.ticketPriorityService.delete(ticketPriorityId).subscribe((res: any) => {
          if (res) {
            this.toast.success('Xóa thành công');
            this.loadData()
            modal.close();
            this.setOfCheckedId.clear();
          };
        })
      } else {
        modal.close();
      }
    });
  }
  }
  onMultiDelete(data: any) {
    if (data.check_can_edit==1){
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa độ ưu tiên ticket đã chọn không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let ticketIds = data instanceof Set ? Array.from(data) : [data.id];

    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.ticketPriorityService.delete(ticketIds, true).subscribe(res => {
          this.toast.success(`Xóa thành công`);
          this.setOfCheckedId = new Set<number>();
          modal.close();
          this.loadData()
        })
      } else {
        modal.close();
      }
    });
  }
  }
  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
  }
  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.ticketPriorityList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }
  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  onSearch($event: any) {
    this.keyword = $event.target?.value;
    this.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.loadData();
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.loadData();
  }
  onRefresh(): void {
    this.loadData();
  }
  // onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
  //   this.listOfCurrentPageData = listOfCurrentPageData;
  // }
  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onUpdateStatus(data: any) {
    const form = CommonUtil.modalBase(ModalConfirmComponent, {
      title: 'Xác nhận thay đổi trạng thái',
      content: `Bạn có chắc muốn thay đổi trạng thái <b>${data?.name}</b> không?`,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      false, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.ticketPriorityService.update(data.id, data).subscribe(res => {
          if (res) {
            this.toast.success('Cập nhật thành công');
          } else {
            this.toast.success('Cập nhật thất bại');
          }
          modal.close();
        })
      } else {
        data.status = !data.status;
        modal.close();
      }
    });
  }
  drop($event: any) {
    const triggerDrag = this.ticketPriorityList?.find((x: any, k: any) => k == $event.previousIndex);
    const triggerDragNew = this.ticketPriorityList?.find((x: any, k: any) => k == $event.currentIndex);
    // let getFirstValue = this.ticketPriorityList.at();
    // if (getFirstValue.order !== 1) {
    //   let getSecondValue = this.ticketPriorityList.at(1);
    //   this.newOder = getSecondValue.order;
    // }
    const params = {
      id: triggerDrag?.id,
      // new_order: this.newOder ? this.newOder : $event.currentIndex + 1
      new_order: triggerDragNew?.order
    }
    this.ticketPriorityService.updateOrder(params).subscribe(res => {
      this.loadData()
    })
  }
  onDetail(data: any) {
    const base = CommonUtil.modalBase(TicketPriorityDetailComponent, { action: ACTION.VIEW,selectedTicketPriority: data, nodes: this.nodes, nodeCategory: this.nodeCategory, isDetail: true }, '30%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData();
      }
    });
  }
}
