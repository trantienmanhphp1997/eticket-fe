import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import CommonUtil from '@shared/utils/common-utils';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { VALIDATORS } from '@shared/constants/validators.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TicketPriorityService } from '@shared/service/ticket-priority.service';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';
import { CommonService } from '@shared/service/common.service';
import { ACTION } from '@shared/constants/common.constant';

@Component({
  selector: 'app-ticket-priority-detail',
  templateUrl: './ticket-priority-detail.component.html',
  styleUrls: ['./ticket-priority-detail.component.scss']
})
export class TicketPriorityDetailComponent implements OnInit {
  @Input() selectedTicketPriority: any = []
  @Input() isUpdate: boolean = false;
  @Input() nodes: any;
  @Input() nodeCategory: any;
  @Input() isDetail: boolean = false
  @Input() action: any = ACTION.CREATE
  count = 0;
  isLoading = false
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  VALIDATORS: any = VALIDATORS;
  statusList: any[] = [];
  isUpdating = false;
  actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
  department: any;
  note: any = [];
  form: FormGroup = new FormGroup({});
  viewEvent = ACTION.VIEW.event;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toast: ToastService,
    private ticketPriorityService: TicketPriorityService,
    private commonService: CommonService,
  ) { }

  ngOnInit() {
    this.initForm()
  }

  initForm(): void {
    this.form = this.fb.group({
      color: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedTicketPriority?.color : '#000000', disabled: this.isDetail ? true : false },
      [Validators.required]],
      name: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedTicketPriority?.name : null, disabled: this.isDetail ? true : false },
      { validators: [Validators.required,CustomInputValidator.isName, Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX)], asyncValidators: [CustomInputValidator.hasExited('ticketpriorities', 'name', this.commonService,this.selectedTicketPriority?.name)], updateOn: 'blur' }],
      note: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedTicketPriority?.note : null, disabled: this.isDetail ? true : false }, Validators.maxLength(LENGTH_VALIDATOR.IDS_MAX_LENGTH.MAX)],
      status: [ this.actionToShowData.includes(this.action.event) ? this.selectedTicketPriority?.status : 1],
    });
  }

  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  onSwitch() {
    if (this.form.value.status == false) {
      this.form.patchValue({ status: 0 })
    }
    else {
      this.form.patchValue({ status: 1 })
    }
  }
  onSubmit(): void {
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    this.isLoading = true
    this.count += 1
    if(this.count == 1){
    const ticketPriority: any = {
      ...this.form.value,
    };
    const body = CommonUtil.trim(ticketPriority);
    this.isUpdating = true;
    let serviceEvent = this.action.event === ACTION.UPDATE.event ? this.ticketPriorityService.update(this.selectedTicketPriority.id, body, true) : this.ticketPriorityService.create(body, true)
    serviceEvent.subscribe(res => {
      if (res.body?.data) {
        this.isLoading = false;
        this.modalRef.close({
          success: true,
          value: ticketPriority,
        });
      }
    },
      (e: any) => {
        this.isUpdating = false;
      },
      () => {
        this.isUpdating = false;
      })
    }
  }
  onChangeData(type: string, content: string): void {
    this.form.get(type)?.setValue(content);
  }
  onUpdate() {
    this.action = ACTION.UPDATE;
    this.isDetail = false;
    this.initForm();
  }
}

