import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketCategoryRoutingModule }  from './ticket-category-routing.module'
import { TicketCategoryComponent } from './ticket-category.component';
import { SharedModule } from '@shared/shared.module';
import { TicketCategoryDetailComponent } from './ticket-category-detail/ticket-category-detail.component';


@NgModule({ 
  declarations: [TicketCategoryComponent, TicketCategoryDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    TicketCategoryRoutingModule
  ]
})
export class TicketCategoryModule { }
