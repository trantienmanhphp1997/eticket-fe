import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import CommonUtil from '@shared/utils/common-utils';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { VALIDATORS } from '@shared/constants/validators.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TicketCategoryService } from '@shared/service/ticket-category.service';
import { ACTION } from '@shared/constants/common.constant';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';


@Component({
  selector: 'app-ticket-category-detail',
  templateUrl: './ticket-category-detail.component.html',
  styleUrls: ['./ticket-category-detail.component.scss']
})
export class TicketCategoryDetailComponent implements OnInit {
  @Input() selectedTicketCategory: any = []
  @Input() isUpdate: boolean = false;
  @Input() nodes: any;
  @Input() nodeCategory: any;
  @Input() isDetail: boolean = false
  @Input() action: any = ACTION.CREATE
  count = 0;
  isLoading = false
  viewEvent = ACTION.VIEW.event;
  actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  VALIDATORS: any = VALIDATORS;
  statusList: any[] = [];
  isUpdating = false;
  department: any;
  note: any = [];
  form: FormGroup = new FormGroup({});

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toast: ToastService,
    private ticketCategoryService: TicketCategoryService,
  ) { }

  ngOnInit() {
    this.initForm()
  }

  initForm(): void {
    this.form = this.fb.group({
      code: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedTicketCategory?.code : null, disabled: this.isDetail || this.selectedTicketCategory?.code === 'YCBE' || this.selectedTicketCategory?.code === 'YCFE' ? true : false },
      [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.CODECUS_MAX_LENGTH.MAX)]],
      name: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedTicketCategory?.name : null, disabled: this.isDetail ? true : false },
      [Validators.required, CustomInputValidator.isName, Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX)]],
      note: [{ value: this.actionToShowData.includes(this.action.event) ? this.selectedTicketCategory?.note : null, disabled: this.isDetail ? true : false }, Validators.maxLength(LENGTH_VALIDATOR.IDS_MAX_LENGTH.MAX)],
      status: [this.actionToShowData.includes(this.action.event) ? this.selectedTicketCategory?.status : 1],
    });
    this.note =  this.selectedTicketCategory?.note
  }

  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  onSwitch() {
    if (this.form.value.status == false) {
      this.form.patchValue({ status: 0 })
    }
    else {
      this.form.patchValue({ status: 1 })
    }
  }
  onSubmit(): void {
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    this.isLoading = true
    this.count += 1
    if(this.count == 1){
    const ticketCategory: any = {
      ...this.form.value
    };
    const body = CommonUtil.trim(ticketCategory);
    this.isUpdating = true;
    let serviceEvent = this.action.event === ACTION.UPDATE.event? this.ticketCategoryService.update(this.selectedTicketCategory.id, body, true) : this.ticketCategoryService.create(body, true)
    serviceEvent.subscribe(res => {
      if (res.body?.data) {
      this.isLoading = false
        this.modalRef.close({
          success: true,
          value: ticketCategory,
        });
      }
    },
      (e: any) => {
        this.isUpdating = false;
      },
      () => {
        this.isUpdating = false;
      })
    }
  }
  onChangeData(type: string, content: string): void {
    this.form.get(type)?.setValue(content);
  }
  onUpdate() {
    this.action = ACTION.UPDATE;
    this.isDetail = false;
    this.initForm();
  }
}

