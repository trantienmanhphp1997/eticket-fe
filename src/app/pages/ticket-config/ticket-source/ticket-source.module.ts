import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketSourceRoutingModule }  from './ticket-source-routing.module'
import { TicketSourceComponent } from './ticket-source.component';
import { SharedModule } from '@shared/shared.module';
import { TicketSourceDetailComponent } from './ticket-source-detail/ticket-source-detail.component';


@NgModule({ 
  declarations: [TicketSourceComponent, TicketSourceDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    TicketSourceRoutingModule
  ]
})
export class TicketSourceModule { }
