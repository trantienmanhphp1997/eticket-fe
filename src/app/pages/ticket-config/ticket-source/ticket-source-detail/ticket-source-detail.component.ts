import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import CommonUtil from '@shared/utils/common-utils';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { VALIDATORS } from '@shared/constants/validators.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TicketSourceService } from '@shared/service/ticket-source.service';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';


@Component({
  selector: 'app-ticket-source-detail',
  templateUrl: './ticket-source-detail.component.html',
  styleUrls: ['./ticket-source-detail.component.scss']
})
export class TicketSourceDetailComponent implements OnInit {
  @Input() selectedTicketSource : any = []
  @Input() isUpdate: boolean = false;
  @Input() nodes: any;
  @Input() nodeCategory: any;
  count = 0;
  isLoading = false
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  VALIDATORS : any = VALIDATORS;
  statusList: any[] = [];
  isUpdating = false;
  department: any;
  config: any;
  form: FormGroup = new FormGroup({});
  checked = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toast: ToastService,
    private ticketSourceService: TicketSourceService,
    ) { }

  ngOnInit() {
    this.initForm()
  }

  initForm(): void {
    this.form = this.fb.group({
      type:[{ value: this.isUpdate ? this.selectedTicketSource?.type : null, disabled: false },
        [Validators.required]],
      name:[{ value: this.isUpdate ? this.selectedTicketSource?.name : null, disabled: false },
        [Validators.required,CustomInputValidator.isName, Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX)]],
      note:[{ value: this.isUpdate ? this.selectedTicketSource?.note : null, disabled: false },Validators.maxLength(LENGTH_VALIDATOR.IDS_MAX_LENGTH.MAX)],
      host:[{ value: this.isUpdate ? (this.selectedTicketSource ? JSON.parse(this.selectedTicketSource.config).host :null) : null, disabled: false },],
      port:[{ value: this.isUpdate ? (this.selectedTicketSource ? JSON.parse(this.selectedTicketSource.config).port :null) : null, disabled: false },],
      username:[{ value: this.isUpdate ? (this.selectedTicketSource ? JSON.parse(this.selectedTicketSource.config).username :null) : null, disabled: false },],
      password:[{ value: this.isUpdate ? (this.selectedTicketSource ? JSON.parse(this.selectedTicketSource.config).password :null) : null, disabled: this.checked == false && this.isUpdate  ? true : false },],
    });
  }
 
  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  onSwitch(){
    if(this.form.value.status == false ){
      this.form.patchValue({status: 0})
    }
    else{
      this.form.patchValue({status: 1})
    }
  }
  onSubmit(): void {
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    this.isLoading = true
    this.count += 1
    if(this.count == 1){
    const ticketSource: any = {
      ...this.form.value
    };

    const config: any = {
      host: this.form.value.host,
      port: this.form.value.port,
      username: this.form.value.username,
      password: this.form.value.password,
    };

    ticketSource.config = JSON.stringify(config);
  
    const body = CommonUtil.trim(ticketSource)
    this.isUpdating = true;
    let serviceEvent = this.isUpdate ? this.ticketSourceService.update(this.selectedTicketSource.id, body, true): this.ticketSourceService.create(body,true)
    serviceEvent.subscribe( res => {
      if (res.body?.data) {
        this.isLoading = false;
        this.modalRef.close({
          success: true,
          value: ticketSource,
        });
      }
    },
    (e: any) => {
      this.isUpdating = false;
    },
      () => {
        this.isUpdating = false;
      })
    }
  }
  onChangeData(type: string, content: string): void {
    this.form.get(type)?.setValue(content);
  }
  checkPassword(event:any){
    if(event.target.checked){
      this.form.controls['password'].enable();
    }else{
      this.form.controls['password'].disable();
    }
  }
}

