import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TicketSourceService } from '../../../shared/service/ticket-source.service';
import { PAGINATION } from '@shared/constants/pagination.constants';
import CommonUtil from '@shared/utils/common-utils';
import { AUTH } from '@shared/constants/auth.constant';
import { TicketSourceDetailComponent } from './ticket-source-detail/ticket-source-detail.component';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ModalComponent } from '@shared/components/modal/modal.component';

@Component({
  selector: 'app-ticket-source',
  templateUrl: './ticket-source.component.html',
  styleUrls: ['./ticket-source.component.scss']
})
export class TicketSourceComponent implements OnInit {
  @Output() emitter = new EventEmitter();

  ticketSourceList: any = [];
  keyword: any = '';
  formModal = {};
  setOfCheckedId = new Set<number>();
  indeterminate = false;
  checked = false;
  total: any = 0;
  newOder:any;
  auth: any = AUTH;
  pageIndex = PAGINATION.PAGE_DEFAULT;
  pageSize = PAGINATION.SIZE_DEFAULT;
  pageSizeOptions = PAGINATION.OPTIONS;
  listOfCurrentPageData: readonly any[] = [];
  nodes = [];
  nodeCategory = [];
  commonUtil = CommonUtil
  constructor(
    private ticketSourceService: TicketSourceService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toast: ToastService,
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    const params = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      keyword: this.keyword
    }
    this.ticketSourceService.search(params, true).subscribe(res => {
      this.ticketSourceList = res?.body?.data?.map((dept: any) => {
        return {
          ...dept,
          disabled: dept.check_can_edit ===0,
        }
      })
      this.total = res.body?.meta?.pagination?.total
      console.log(this.ticketSourceList)
    });
  }
  onCreate = () => {
    const base = CommonUtil.modalBase(TicketSourceDetailComponent, { nodes: this.nodes, nodeCategory: this.nodeCategory }, '35%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }
  onUpdate = (data: any) => {
    const base = CommonUtil.modalBase(TicketSourceDetailComponent, { selectedTicketSource: data, nodes: this.nodes, nodeCategory: this.nodeCategory, isUpdate: true }, '35%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData();
      }
    });
  }
  onDelete = (data: any) => {
    if (data.check_can_edit==1){
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa nguồn ticket <b>${data?.name}</b> không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let ticketSourceId = data instanceof Set ? Array.from(data) : [data.id];
    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.ticketSourceService.delete(ticketSourceId).subscribe((res: any) => {
          if (res) {
            this.toast.success('Xóa thành công');
            this.loadData()
            modal.close();
            this.setOfCheckedId.clear();
          };
        })
      } else {
        modal.close();
      }
    });
  }
  }
  onMultiDelete(data: any){
    if (data.check_can_edit==1){
      const form = CommonUtil.modalBase(ModalComponent,{
        title:'Xác nhận xóa',
        content:`Bạn có chắc muốn xóa nguồn ticket đã chọn không?`,
        okText:'Xóa',
        callback: () => {
          return {
            success: true,
          };
        },
      },'25%',
       false,
      false,
      true,{ top: '20px' }, true
      );
      const modal: NzModalRef = this.modalService.create(form);
  
      let ticketIds =  data instanceof Set ? Array.from(data) : [data.id] ; 
      
      modal.componentInstance.emitter.subscribe((result:any) => {
        if (result?.success) {
          this.ticketSourceService.delete(ticketIds, true).subscribe(res => {
              this.toast.success(`Xóa thành công`);
              this.setOfCheckedId=new Set<number>();
              this.checked = true;
              modal.close();
              this.loadData()
          })
        }else{
          modal.close();
        }
      });
    }
 
  }

  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
  }
  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.ticketSourceList.forEach((item: any) => {
      if (item.check_can_edit) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }
  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  onSearch($event: any) {
    this.keyword = $event.target?.value;
    this.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.loadData();
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.loadData();
  }
  onRefresh(): void {
    this.loadData();
  }
  // onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
  //   this.listOfCurrentPageData = listOfCurrentPageData;
  // }
  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  drop($event: any) {
    const triggerDrag = this.ticketSourceList?.find((x: any, k: any) => k == $event.previousIndex);
    const triggerDragNew = this.ticketSourceList?.find((x: any, k: any) => k == $event.currentIndex);
    // let getFirstValue = this.ticketSourceList.at();
    // if (getFirstValue.order !== 1) {
    //   let getSecondValue = this.ticketSourceList.at(1);
    //   this.newOder = getSecondValue.order;
    // }
    const params = {
      id: triggerDrag?.id,
      // new_order: this.newOder ? this.newOder : $event.currentIndex + 1
      new_order: triggerDragNew?.order
    }
    this.ticketSourceService.updateOrder(params).subscribe(res => {
      this.loadData()
    })
  }
}
