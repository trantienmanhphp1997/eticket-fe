import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { TicketStateComponent } from './ticket-state.component'
const routes: Routes = [
  {
    path: '',
    component: TicketStateComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.TICKET_STATUS_CONFIG.LIST],
      title: 'sidebar.ticket_state',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình ticket',
          path: `javascript:void(0)`
        },
      ]
    }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketStateRoutingModule { }
