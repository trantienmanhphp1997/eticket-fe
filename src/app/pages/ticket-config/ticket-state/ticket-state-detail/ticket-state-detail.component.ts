import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VALIDATORS } from '@shared/constants/validators.constant';
import { NzModalRef } from 'ng-zorro-antd/modal';
import CommonUtil from '@shared/utils/common-utils';
import { LENGTH_VALIDATOR } from '@shared/constants/validators.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TicketStateService } from '@shared/service/ticket-state.service';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';


@Component({
  selector: 'app-ticket-state-detail',
  templateUrl: './ticket-state-detail.component.html',
  styleUrls: ['./ticket-state-detail.component.scss']
})
export class TicketStateDetailComponent implements OnInit {
  @Input() selectedTicketState: any = []
  @Input() isUpdate: boolean = false;
  @Input() nodes: any;
  @Input() data: any;
  @Input() nodeCategory: any;
  count = 0;
  isLoading = false
  LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  VALIDATORS: any = VALIDATORS;
  statusList: any[] = [];
  isUpdating = false;
  department: any;
  config_mail: any;
  form: FormGroup = new FormGroup({});
  checkOrder = false;
  lastItem: any;
  lastOrder: any;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toast: ToastService,
    private ticketStateService: TicketStateService,
  ) { }

  ngOnInit() {
    this.initForm();
    // this.lastItem =  Object.values(this.data).pop();
    // this.lastOrder = this.lastItem.order;
  }

  initForm(): void {
    this.form = this.fb.group({
      name: [{ value: this.isUpdate ? this.selectedTicketState?.name : null, disabled: false },
      [Validators.required, CustomInputValidator.isName, Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX)]],
      color: [{ value: this.isUpdate ? this.selectedTicketState?.status_color : '#000000', disabled: false },
      [Validators.required]],
      text_color: [{ value: this.isUpdate ? this.selectedTicketState?.text_color : '#ffffff', disabled: false },
      [Validators.required]],
      // order:[{ value: this.isUpdate ? this.selectedTicketState?.order : null, disabled: false },
      //   [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX)]],
      order: [],
      note: [{ value: this.isUpdate ? this.selectedTicketState?.note : null, disabled: false },],
      status: [{ value: this.isUpdate ? this.selectedTicketState?.status : 1, disabled: false },],
    });
  }

  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
  onSwitch() {
    if (this.form.value.status == false) {
      this.form.patchValue({ status: 0 })
    }
    else {
      this.form.patchValue({ status: 1 })
    }
  }
  onSubmit(): void {
    if (this.form.invalid) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    // if(this.form.value.order <= this.lastOrder){
    //   this.checkOrder = true;
    //   return;
    // }
    this.isLoading = true
    this.count += 1
    if(this.count == 1){
    const ticketCategory: any = {
      ...this.form.value
    };
    const body = CommonUtil.trim(ticketCategory);
    this.isUpdating = true;
    let serviceEvent = this.isUpdate ? this.ticketStateService.update(this.selectedTicketState.id, body, true) : this.ticketStateService.create(body, true)
    serviceEvent.subscribe(res => {
      if (res.body?.data) {
        this.isLoading = false
        this.modalRef.close({
          success: true,
          value: ticketCategory,
        });
      }
    },
      (e: any) => {
        this.isUpdating = false;
      },
      () => {
        this.isUpdating = false;
      })

    }
  }
  onChangeData(type: string, content: string): void {
    this.form.get(type)?.setValue(content);
  }
}

