import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketStateRoutingModule }  from './ticket-state-routing.module'
import { TicketStateComponent } from './ticket-state.component';
import { SharedModule } from '@shared/shared.module';
import { TicketStateDetailComponent } from './ticket-state-detail/ticket-state-detail.component';


@NgModule({ 
  declarations: [TicketStateComponent, TicketStateDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    TicketStateRoutingModule
  ]
})
export class TicketStateModule { }
