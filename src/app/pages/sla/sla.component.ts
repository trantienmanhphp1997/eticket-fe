import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { SlaService } from '@shared/service/sla.service';
import CommonUtil from '@shared/utils/common-utils';
import { TranslateService } from '@ngx-translate/core';
import { AUTH } from '@shared/constants/auth.constant';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { Router } from '@angular/router';
import { StatusSlaComponent } from './status-sla/status-sla.component';
import { ProductService } from '@shared/service/product.service';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { ModalConfirmComponent } from '@shared/components/modal-confirm/modal-confirm.component';
@Component({
  selector: 'app-customer',
  templateUrl: './sla.component.html',
  styleUrls: ['./sla.component.scss']
})
export class SlaComponent implements OnInit {
  @Output() emitter = new EventEmitter();

  slaQuery = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    parentId: '',
    orderBy: '',
    sortBy: '',
    keyword: '',
    product_id: ''
  };
  formModal = {};
  // page data
  slaList: any = [];
  total: any = 0;
  product_id: any;
  visible: boolean = false;
  pageIndex = PAGINATION.PAGE_DEFAULT;
  pageSize = PAGINATION.SIZE_DEFAULT;
  pageSizeOptions = PAGINATION.OPTIONS;
  search = null;
  auth: any = AUTH;
  //checked items
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly any[] = [];
  setOfCheckedId = new Set<number>();
  checked = false;
  testChecked = true;
  newOder: any;
  productList: any = [];
  //form 
  searchForm = this.fb.group({
    keyword: [null]
  });
  commonUtil: any = CommonUtil
  constructor(
    private slaService: SlaService,
    private fb: FormBuilder,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toast: ToastService,
    private router: Router,
    private serviceProductService: ProductService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }
  // load data
  loadData() {
    const params = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      search: this.search,
      include: 'products'
    }
    this.slaService.getSla(params, true).subscribe(
      (res) => {
        this.slaList = res.body?.data.map((dept: any) => {
          return {
            ...dept, disabled: dept.check_can_edit ===0, 
          }
        })
        console.log(this.slaList);
        this.total = res.body?.meta?.pagination.total
      }
    );
  }

  getServiceProducts(products: any): any {
    var result = [];
    if (products.length > 0) {
      result = products.filter((item: any) => item.id != products[0].id)
    }
    return result;
  }
  // Events
  onSearch($event: any) {
    this.pageIndex = PAGINATION.PAGE_DEFAULT;
    this.search = $event.target?.value.trim() || null;
    // this.product_id = '';
    this.searchForm.patchValue({
      keyword: this.search
    })
    this.loadData();
  }
  onCreate = () => {
    this.router.navigate([`admin/${ROUTER_UTILS.sla.root}/${ROUTER_UTILS.sla.create}`]);
  }
  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }
  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.loadData();
  }
  onUpdate = (data?: any) => {
    if(!data.check_can_edit){
      return;
    }
    if (data.id) {
      this.router.navigate([`admin/${ROUTER_UTILS.sla.root}/${ROUTER_UTILS.sla.view}`, data.id]);
    }
  }
  onView = (data?: any) => {
    if (data.id) {
      this.router.navigate([`admin/${ROUTER_UTILS.sla.root}/${ROUTER_UTILS.sla.detail}`, data.id]);
    }
  }
  onDelete = (data: any) => {
    if(!data.check_can_edit){
      return;
    }
      const form = CommonUtil.modalBase(ModalComponent, {
        title: 'Xác nhận xóa',
        content: `Bạn có chắc muốn xóa SLA <b>${data?.title}</b> không?`,
        okText: 'Xóa',
        callback: () => {
          return {
            success: true,
          };
        },
      }, '450px',
        false,
        false,
        true, { top: '20px' }, true
      );
      const modal: NzModalRef = this.modalService.create(form);
  
      let slaIds = data instanceof Set ? Array.from(data) : [data.id];
      modal.componentInstance.emitter.subscribe((result: any) => {
        if (result?.success) {
          this.slaService.deleteSla(slaIds).subscribe((res: any) => {
            if (res) {
              this.toast.success(`Xóa SLA thành công`);
              modal.close();
              this.loadData()
            };
          })
        } else {
          modal.close();
        }
      });
  }

  onMultiDelete(data: any) {
    const form = CommonUtil.modalBase(ModalComponent, {
      title: 'Xác nhận xóa',
      content: `Bạn có chắc muốn xóa SLA đã chọn không?`,
      okText: 'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    }, '450px',
      false,
      false,
      true, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let slaIds = data instanceof Set ? Array.from(data) : [data.id];

    modal.componentInstance.emitter.subscribe((result: any) => {
      if (result?.success) {
        this.slaService.deleteSla(slaIds, true).subscribe(res => {
          this.toast.success(`Xóa SLA thành công`);
          this.setOfCheckedId = new Set<number>();
          modal.close();
          this.loadData()
        })
      } else {
        modal.close();
      }
    });
  }

  onAllChecked(checked: boolean): void {

  }
  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }
  onReset() {
    this.searchForm.patchValue({
      keyword: null,
    })
    this.loadData();
  }

  onUpdateStatus(data: any) {
    if (data.status == 1) {
      data.status = false
    } else {
      data.status = true
    }
    const form = CommonUtil.modalBase(ModalConfirmComponent, {
      title: 'Xác nhận thay đổi trạng thái',
      content: `Bạn có chắc muốn thay đổi trạng thái <b>${data?.title}</b> không?`,
      callback: () => {
        return {
          success: true,
        };
      },
    }, '25%',
      false,
      false,
      false, { top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.slaService.update(data.id, data ,true).subscribe(res => {
          if (res) {
            this.toast.success('Cập nhật thành công');
          } else {
            this.toast.success('Cập nhật thất bại');
          }
          modal.close();
        })
      } else {
        data.status = !data.status;
        modal.close();
      }
    });
  }

  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.slaList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }
  openFillter() {
    this.visible = true;
    this.getAllServiceProductData();
  }
  onClose() {
    this.visible = false;
  }
  onFilterData() {
    if (this.product_id) {
      this.slaQuery.keyword = '';
      this.searchForm.patchValue({
        keyword: null,
      })
      this.slaQuery.product_id = this.product_id;
      this.slaService.getSla(this.slaQuery, true).subscribe(res => {
        this.slaList = res.body?.data;
      });
      this.visible = false;
    } else {
      this.visible = false;
      this.slaQuery.product_id = this.product_id;
      this.slaService.getSla(this.slaQuery, true).subscribe(res => {
        this.slaList = res.body?.data;
      });
    }
  }
  onResetFilter() {
    this.product_id = '';
    this.onFilterData();
  }
  getAllServiceProductData() {
    const params = {
      pageIndex: 0,
      pageSize: 0,
    }
    this.serviceProductService.getAll(true).subscribe((res) => {
      this.productList = res.body?.data;
    });
  }
  onChangeServiceProduct(event: any) {
    this.product_id = event;
  }

  drop($event: any) {
    const triggerDrag = this.slaList?.find((x: any, k: any) => k == $event.previousIndex);
    const triggerDragNew = this.slaList?.find((x: any, k: any) => k == $event.currentIndex);
    // let getFirstValue = this.slaList.at();
    // if (getFirstValue.order !== 1) {
    //   let getSecondValue = this.slaList.at(1);
    //   this.newOder = getSecondValue.order;
    // }
    const params = {
      id: triggerDrag?.id,
      // new_order: this.newOder ? this.newOder : $event.currentIndex + 1
      new_order: triggerDragNew?.order
    }
    this.slaService.updateOrder(params, true).subscribe(res => {
      this.loadData()
    })
  }

}
