import { analyzeAndValidateNgModules, ThisReceiver } from '@angular/compiler';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LENGTH_VALIDATOR, VALIDATORS} from '@shared/constants/validators.constant';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ProductService } from '@shared/service/product.service';
import { SlaService } from '@shared/service/sla.service';
import { TicketCategoryService } from '@shared/service/ticket-category.service';
import { TicketPriorityService } from '@shared/service/ticket-priority.service';
import { TicketSourceService } from '@shared/service/ticket-source.service';
import { UserService } from '@shared/service/user.service';
import CommonUtil from '@shared/utils/common-utils';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { trim, values } from 'lodash';
import { collapseTextChangeRangesAcrossMultipleVersions } from 'typescript';

@Component({
  selector: 'app-sla-create',
  templateUrl: './sla-create.component.html',
  styleUrls: ['./sla-create.component.scss']
})
export class SlaCreateComponent implements OnInit {
   selectedSLA: any = []
   isUpdate = false;
   colorPriority:any;
   isShowtitle = true ;
   isShowtitle2 = true ;
   LENGTH_VALIDATOR: any = LENGTH_VALIDATOR;
  slaForm = new FormGroup({
    title: new FormControl('', [Validators.required,Validators.maxLength(LENGTH_VALIDATOR.TITLE_MAX_LENGTH.MAX)]),
    status: new FormControl(0),
    description: new FormControl(''),
    conditionForm: new FormArray([], [Validators.required]),
    ticketPriorities0: new FormArray([], [Validators.required]),
    ticketPriorities1: new FormArray([], [Validators.required]),
    handlingStaffForm: new FormArray([]),
    reportForm: new FormArray([]),
  });
  ticketPriorities0 = this.slaForm.get('ticketPriorities0') as FormArray;
  ticketPriorities1 = this.slaForm.get('ticketPriorities1') as FormArray;
  conditionForm = this.slaForm.get('conditionForm') as FormArray; 
  handlingStaffForm = this.slaForm.get('handlingStaffForm') as FormArray;
  reportForm = this.slaForm.get('reportForm') as FormArray;
  form: FormGroup = new FormGroup({});
  listTP: any = [];
  isShowInfor: boolean = false;
  condition: any = [];
  ticketPriorityList0: any = [];
  ticketPriorityList1: any = [];
  conditionInforList: any = [];
  productList: any = [];
  ticketSourceList: any = [];
  isShowCondition = true;
  handlingStaffList: any = [];
  sub: any;
  id: any;
  listConditions : any =[];
  isAddconditon = false;
  nameCodition : any = [];
  showError: any = [];
  isShowDescription : boolean = false;
  showErrotitle: boolean = false;
  hideButton: boolean = false;
  detailSla: boolean = false;
  isShowProduct: any =[];
  listCondition = [
    {
      value: "products", name: "Sản phẩm dịch vụ",
    },
    {
      value: "sources", name: "Kênh",
    },
    {
      value: "ticket_category", name: "Phân loại ticket",
    }
  ];
  conditionType = [
    {
      value: 0, name: " Trước thời hạn phản hồi lần đầu"
    },
    {
      value: 1, name: " Quá thời hạn phản hồi lần đầu"
    },
    {
      value: 2, name: " Trước thời hạn xử lý"
    },
    {
      value: 3, name: " Quá thời hạn xử lý"
    }
  ]
  activateTime = [
    {
      value: 0, name: "Tất cả các giờ"
    },
    {
      value: 1, name: "Giờ hành chính"
    }
  ];

  constructor(
    private fb: FormBuilder,
    private slaService: SlaService,
    private router: Router,
    private toast: ToastService,
    private ticketPriorityService: TicketPriorityService,
    private ticketCategoryService: TicketCategoryService,
    private productService: ProductService,
    private ticketSourceService: TicketSourceService,
    private userService : UserService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.getHandlingStaffList();
    this.listConditions= this.listCondition;
    this.activatedRoute.url.subscribe( res =>{
        if(res[0].path == 'detail'){
          this.detailSla = true;
        }
    })
    this.sub = this.activatedRoute.paramMap.subscribe(params => {

      this.id = params.get('id') || 0;
      if(this.id){
        this.isUpdate= true;
       
        const param = {
          include: 'ticketPriorities0,ticketPriorities1'
        }
        this.slaService.detail(this.id, param, true).subscribe( res => {

          this.selectedSLA = res.body?.data;
          
          this.slaForm.patchValue({
            title: this.selectedSLA.title,
            status: this.selectedSLA.status,
            description: this.selectedSLA.description
          });
         
          if(this.selectedSLA.ticketPriorities0.data){
            this.selectedSLA.ticketPriorities0.data.filter((item:any)=>item.status == 1);
            this.selectedSLA.ticketPriorities0.data?.forEach((item: any) => {
              const group = new FormGroup({
                ticket_priority_id: new FormControl(item.ticket_priority_id ),
                ticket_priority_name: new FormControl({value:item.ticket_priority_name, disabled: this.selectedSLA.check_can_edit==0  || this.detailSla ? true : false }),
                first_response_time_h: new FormControl({value: item.first_response_time_h, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false}),
                first_response_time_d: new FormControl({value:item.first_response_time_d, disabled: this.selectedSLA.check_can_edit==0  || this.detailSla ? true : false}),
                first_response_time_m: new FormControl({value:item.first_response_time_m, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false}),
                resolution_time_d:new FormControl({value:item.resolution_time_d, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false}),
                resolution_time_h: new FormControl({value:item.resolution_time_h, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false}),
                resolution_time_m:new FormControl({value:item.resolution_time_m, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false}),
                calender_type: new FormControl(0),
                escalation: new FormControl({value: item.escalation, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false}),
                colorSla:new FormControl(item.ticket_priority_color)
              });
              this.ticketPriorities0.push(group);
              this.ticketPriorityList0  =   this.ticketPriorities0.controls;    
            });
          }
          if(this.selectedSLA.ticketPriorities1.data){
            this.selectedSLA.ticketPriorities1.data.filter((item:any)=>item.status == 1);
            this.selectedSLA.ticketPriorities1.data?.forEach((item: any) => {
              const group = new FormGroup({
                ticket_priority_id: new FormControl(item.ticket_priority_id),
                ticket_priority_name: new FormControl({value:item.ticket_priority_name, disabled:true}),
                first_response_time_h: new FormControl({value:item.first_response_time_h , disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false }),
                first_response_time_d: new FormControl({value:item.first_response_time_d , disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false }),
                first_response_time_m: new FormControl({value:item.first_response_time_m , disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false }),
                resolution_time_d:new FormControl({value:item.resolution_time_d , disabled: this.selectedSLA.check_can_edit==0 || this.detailSla  ? true : false }),
                resolution_time_h: new FormControl({value:item.resolution_time_h , disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false }),
                resolution_time_m:new FormControl({value:item.resolution_time_m , disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false }),
                calender_type: new FormControl(1),
                escalation: new FormControl({value:item.escalation , disabled: this.selectedSLA.check_can_edit==0 || this.detailSla? true : false }),
                colorSla:new FormControl(item.ticket_priority_color)
              });
              this.ticketPriorities1.push(group);
              this.ticketPriorityList1  =   this.ticketPriorities1.controls;    
            });
          }
         
          let isShowProductAll : boolean = false
          if(this.selectedSLA.applicable_to.product_all){
            isShowProductAll = true
          }
            delete  this.selectedSLA.applicable_to['product_all'];
            Object.keys(this.selectedSLA.applicable_to).forEach((key, value) =>{
              this.getConditionInforList(key, value);
            });
            
            this.getConditionInforList('products' ,(Object.keys(this.selectedSLA.applicable_to).length));
            Object.keys(this.selectedSLA.applicable_to).forEach((key, value) =>{
                const condition = new FormGroup({
                  conditionName: new FormControl({value: key, disabled:  true },[Validators.required]),
                  conditionInformation: new FormControl({value:this.selectedSLA.applicable_to[key], disabled:  true  } ,[Validators.required]),
                  product_all: new FormControl({value: this.selectedSLA.applicable_to.product_all , disabled:  true  }),
                });
                this.conditionForm.push(condition);
            }) 
            if(isShowProductAll){
             
              this.isShowProduct[Object.keys(this.selectedSLA.applicable_to).length] =true
              const condition = new FormGroup({
                conditionName: new FormControl({value: 'products', disabled:  true  },[Validators.required]),
                conditionInformation: new FormControl({value:'', disabled: true } ,[Validators.required]),
                product_all: new FormControl({value:  true , disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false }),
              });
              this.conditionForm.push(condition);
            }
          if(this.selectedSLA.reminders){
            this.selectedSLA.reminders?.forEach((item: any) => {
              const staff = new FormGroup({
                type: new FormControl({value:item.type, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false },[Validators.required]),
                reminder_time_d: new FormControl({value:item.reminder_time_d, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false },[Validators.required]),
                reminder_time_h: new FormControl({value:item.reminder_time_h, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false },[Validators.required]),
                reminder_time_m: new FormControl({value:item.reminder_time_m, disabled: this.selectedSLA.check_can_edit==0  || this.detailSla? true : false },[Validators.required]),
                agent_ids: new FormControl({value:item.agent_ids, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false },[Validators.required]),
              });
          
              this.handlingStaffForm.push(staff);
              if(this.handlingStaffForm.length > 0){
                this.isShowtitle = false
              }
            });
          }
         if( this.selectedSLA.escalations){
          this.selectedSLA.escalations?.forEach((item: any) => {
            const staff = new FormGroup({
              type: new FormControl({value:item.type, disabled: this.selectedSLA.check_can_edit==0  || this.detailSla ? true : false },[Validators.required]),
              reminder_time_d: new FormControl({value:item.reminder_time_d, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false },[Validators.required]),
              reminder_time_h: new FormControl({value:item.reminder_time_h, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false },[Validators.required]),
              reminder_time_m: new FormControl({value:item.reminder_time_m, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false },[Validators.required]),
              agent_ids: new FormControl({value:item.agent_ids, disabled: this.selectedSLA.check_can_edit==0 || this.detailSla ? true : false }),
            });
            this.reportForm.push(staff);
            if(this.handlingStaffForm.length > 0){
              this.isShowtitle2 = false
            }
          });
         }
          if(this.selectedSLA.check_can_edit==0){
            this.hideButton = true;
          }
         
        });
       
      }
      else{
        this.isUpdate = false;
        this.hideButton = false;
        const condition = new FormGroup({
          conditionName: new FormControl([], [Validators.required]),
          conditionInformation: new FormControl([], [Validators.required]),
          product_all: new FormControl(),
        })
        this.conditionForm.push(condition);
        this.initPriotyForm();
        this.initPriotyForm2();
    }   
    });
  }
  onChangeChecked(event: any, i: number){
      this.conditionForm.controls.forEach((element: any,index: number) => {
        if(event&& i==index){
          console.log(element);
          element.controls['conditionInformation'].disable()
          element.controls.conditionInformation.setValue([])
        }
        else{
          element.controls['conditionInformation'].enable()
        }
      });
  }
  PriotyForm(){
    this.ticketPriorityService.list().subscribe(res => {
      this.ticketPriorityList0 = res.body?.data?.filter((item:any)=>item.status ==1);
      this.ticketPriorityList0.forEach((item: any) => {
        const group = new FormGroup({
          ticket_priority_id: new FormControl(item.id),
          ticket_priority_name: new FormControl({value:item.name, disabled:true}),
          first_response_time_d: new FormControl(''),
          first_response_time_h: new FormControl(''),
          first_response_time_m: new FormControl(30),
          resolution_time_d: new FormControl(''),
          resolution_time_h: new FormControl(''),
          resolution_time_m: new FormControl(30),
          calender_type: new FormControl(0),
          escalation: new FormControl(1),
          colorSla:new FormControl(item.color)
        });
        this.ticketPriorities0.push(group)
      })
    });
  }
  initPriotyForm() {
    this.ticketPriorityService.list().subscribe(res => {
      this.ticketPriorityList0 = res.body?.data?.filter((item:any)=>item.status ==1);
      this.ticketPriorityList0.forEach((item: any) => {
        const group = new FormGroup({
          ticket_priority_id: new FormControl(item.id),
          ticket_priority_name: new FormControl({value:item.name, disabled:true}),
          first_response_time_d: new FormControl(''),
          first_response_time_h: new FormControl(''),
          first_response_time_m: new FormControl(30),
          resolution_time_d: new FormControl(''),
          resolution_time_h: new FormControl(''),
          resolution_time_m: new FormControl(30),
          calender_type: new FormControl(0),
          escalation: new FormControl(1),
          colorSla:new FormControl(item.color)
        });
        this.ticketPriorities0.push(group)
      })
    });
  }
  initPriotyForm2() {
    this.ticketPriorityService.list().subscribe(res => {
      this.ticketPriorityList1 = res.body?.data?.filter((item:any)=>item.status ==1);
      this.ticketPriorityList1.forEach((item: any) => {
        const group = new FormGroup({
          ticket_priority_id: new FormControl(item.id),
          ticket_priority_name: new FormControl({value:item.name, disabled:true}),
          first_response_time_d: new FormControl(''),
          first_response_time_h: new FormControl(''),
          first_response_time_m: new FormControl(30),
          resolution_time_d: new FormControl(''),
          resolution_time_h: new FormControl(''),
          resolution_time_m: new FormControl(30),
          calender_type: new FormControl(1),
          escalation: new FormControl(1),
          colorSla:new FormControl(item.color)
        });
        this.ticketPriorities1.push(group)
      })
    });
  }
  changeCodition(event: any, index: number) {
    if(event == 'products'){
      this.isShowProduct[index] = true;
    }
    else{
      this.isShowProduct[index] = false;
    }
    this.getConditionInforList(event, index);
    if(event){
      const values =this.conditionForm.controls[index].value.conditionName;
      const name = this.listConditions.find( (e: { value: any; }) => e.value === values);
      this.listCondition = this.listCondition.filter(function(e){
        return e.value!== name.value;
      })
    }
    if( this.nameCodition && event){
      const valuess = this.listConditions.find( (e: { name: any; }) => e.name ===  this.nameCodition);
      this.listCondition.push(valuess);
    }
  }
  changeCodition2( event : any, i : number){
      this.nameCodition = event.target.innerText;
       this.conditionForm.controls[i].patchValue({
          conditionInformation: null
      });
      this.conditionForm

  }
  addCondition(event:any) {
    if (this.conditionForm.invalid) {
      CommonUtil.markFormGroupTouched(this.slaForm);
      return;
    }
    if( this.conditionForm.length == 2){
      this.isShowCondition = false;
    }
    else{
      this.isShowCondition = true;
    }
    // let count = event.detail;
    // if(this.id && count === 1){
    //   Object.keys(this.selectedSLA.applicable_to).forEach((key) =>{
    //     const name = this.listConditions.find( (e: { value: any; }) => e.value === key);
    //     this.listCondition = this.listCondition.filter(function(e){
    //       return e.value!== name.value;
    //     })
    //   })
  
    // }
    const conditions = new FormGroup({
      conditionName: new FormControl('', [Validators.required]),
      conditionInformation: new FormControl([], [Validators.required]),
      product_all: new FormControl(),
    });

    this.conditionForm.push(conditions)
  }
  removeCondition(i:number){
    this.isShowProduct[i] = false;
    const values = this.conditionForm.controls[i].value.conditionName;
    const name = this.listConditions.find( (e: { value: any; }) => e.value === values);
   
    if (this.conditionForm.length > 1 && this.conditionForm.length <= 3) { 
     if(values){
      this.listCondition = this.listCondition.filter(function(e){
        return e.value!= name?.value;
      })  
      this.listCondition.push(name);
     }
      this.isShowCondition = true;
    
      this.conditionForm.removeAt(i);
    } else if (this.conditionForm.length == 1){
      if(values){
        this.listCondition.push(name);
       }

      this.conditionForm.removeAt(0);
      const conditions = new FormGroup({
        conditionName: new FormControl('', [Validators.required]),
        conditionInformation: new FormControl([], [Validators.required])
      });
  
      this.conditionForm.push(conditions)
    }
  }
  removeHandlingStaff(i:number){
    if (this.handlingStaffForm.length > 1) {
      this.handlingStaffForm.removeAt(i)
    } else {
      this.handlingStaffForm.removeAt(i)
    }
  }
  removeReportForm(i:number){
    if (this.reportForm.length > 1) {
      this.reportForm.removeAt(i)
    } else {
      this.reportForm.removeAt(i)
    }
  }
  getHandlingStaffList(){
    this.userService.getAll(true).subscribe(res => {
      this.handlingStaffList= res.body?.data;
    })
  }
  addStaff() {
    if(this.handlingStaffForm.controls.length === 0){
    }
    else{
      if (this.handlingStaffForm.invalid) {
        CommonUtil.markFormGroupTouched(this.slaForm);
        return;
      }
    }
  
    this.isShowtitle = false;
    const staff = new FormGroup({
      type: new FormControl('', [Validators.required]),
      reminder_time_d: new FormControl(''),
      reminder_time_h: new FormControl(''),
      reminder_time_m: new FormControl('', [Validators.required]),
      agent_ids: new FormControl([], [Validators.required]),
    });

    this.handlingStaffForm.push(staff)
  }
  addReport() {
    if(this.reportForm.controls.length === 0){
    }
    else{
      if (this.reportForm.invalid) {
        CommonUtil.markFormGroupTouched(this.slaForm);
        return;
      }
    }
  
    this.isShowtitle2 = false;
    const report = new FormGroup({
      type: new FormControl('', [Validators.required]),
      reminder_time_d: new FormControl(''),
      reminder_time_h: new FormControl(''),
      reminder_time_m: new FormControl('', [Validators.required]),
      agent_ids: new FormControl([], [Validators.required]),
    });

    this.reportForm.push(report)
  }
  getConditionInforList(conditionType: string, index: number) {
    switch (conditionType) {
      case "products":
        this.productService.getAll(true).subscribe(res => {
          this.conditionInforList[index] = res.body?.data;
        })
        break;
      case "sources":
        this.ticketSourceService.getAll(true).subscribe(res => {
          this.conditionInforList[index] = res.body?.data;
        })
        break;
      case "ticket_category":
        this.ticketCategoryService.getAll(true).subscribe(res => {
          this.conditionInforList[index] = res.body?.data;
        })
        break;
    }
  }
 
  onChangeData(type: string, content: string): void {
    this.slaForm.get(type)?.setValue(content);
    if (CommonUtil.extractContent(this.slaForm.value.description)?.length > 500) {
      this.isShowDescription = true
    }
    else if (CommonUtil.extractContent(this.slaForm.value.description)?.length <= 500) {
      this.isShowDescription = false
    }
  }
  checktile(){
    if(this.slaForm.controls.title.value === ''){
      this.showErrotitle = false;
    }
    else{
      if(this.slaForm.controls.title.value.trim() === ''){
        this.showErrotitle = true;
      }
      else{
        this.showErrotitle = false;
      }
    }
   
  }
  onSubmit() {
    if (this.slaForm.invalid || this.showErrotitle) {
      CommonUtil.markFormGroupTouched(this.slaForm);
      return;
    }
    if (CommonUtil.extractContent(this.slaForm.value.description)?.length > 500) {
      return;
    }
    
    let appliedConditions = {};
    this.conditionForm.controls.forEach(element => {
      appliedConditions = {
        ...appliedConditions,
        [`${element.value.conditionName}`]: element.value.conditionInformation,
        product_all:  element.value.product_all
       }
    });
    const sla: any = {
      ...this.slaForm.value,
      applicable_to: appliedConditions,
      reminders: this.slaForm.value.handlingStaffForm,
      escalations: this.slaForm.value.reportForm,
    };
    const body = CommonUtil.trim(sla);
    let manglog: any = [];
   
    this.ticketPriorities0.controls.forEach((item: any, i: number) =>{
      let sum : number = 0;
      let sum1 : number = 0;
         sum = (item.controls.first_response_time_d.value)*60 + (item.controls.first_response_time_h.value) + ((item.controls.first_response_time_m.value)/60);
         sum1 = (item.controls.resolution_time_d.value)*60 + (item.controls.resolution_time_h.value) + ((item.controls.resolution_time_m.value)/60)
         if(sum > sum1){
          manglog.push(1);
          this.showError[i] = true;
        }
        else if (sum < sum1){
           manglog.push(0);
           this.showError[i] = false;
        }
    })
    // debugger
    // let summ = 0;
    let checkMang : boolean = false ;
    // for (let i = 0; i < manglog.length; i++){
    //   summ += manglog[i];
    // }
    checkMang = manglog.every((item: any) =>{
      return item === 0
    })
      if(checkMang){
        let serviceEvent = this.isUpdate ? this.slaService.update(this.selectedSLA.id, body, true) : this.slaService.create(body, true)
        serviceEvent.subscribe(res => {
          if (res.body?.data) {
            this.toast.success(`model.sla.create.success.${this.isUpdate ? 'update' : 'create'}`);
            this.router.navigate([`admin/${ROUTER_UTILS.sla.root}`]);
          }
        })
      }
  }
  onCancel(){
    this.router.navigate([`admin/${ROUTER_UTILS.sla.root}`]);
  }
  getColorSla(item:any){
    return item.value.colorSla
  }
}
