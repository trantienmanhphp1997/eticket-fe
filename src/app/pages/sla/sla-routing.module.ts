import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { SlaCreateComponent } from './sla-create/sla-create.component';
import { SlaComponent } from './sla.component';

const routes: Routes = [
  {
    path: '',
    component: SlaComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.SLA.LIST],
      title: 'model.sla.title',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: 'javascript:void(0)'
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: 'javascript:void(0)'
        }
      ]
    }
  }
,
  {
    path: ROUTER_UTILS.sla.create,
    component: SlaCreateComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.SLA.CREATE],
      title: 'model.sla.create.title',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: 'javascript:void(0)'
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: 'javascript:void(0)'
        },
        {
          level: 1,
          title: 'Danh sách SLA',
          path: `admin/${ROUTER_UTILS.sla.root}`
        }
      ]
    }
  }
,
  {
    path: `${ROUTER_UTILS.sla.view}/:id`,
    component: SlaCreateComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.SLA.UPDATE],
      title: 'model.sla.create.titleDetail',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: 'javascript:void(0)'
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: 'javascript:void(0)'
        },
        {
          level: 1,
          title: 'Danh sách SLA',
          path: `admin/${ROUTER_UTILS.sla.root}`
        }
      ]
    }
  },
  {
    path: `${ROUTER_UTILS.sla.detail}/:id`,
    component: SlaCreateComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.SLA.UPDATE],
      title: 'Chi tiết SLA',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: 'javascript:void(0)'
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: 'javascript:void(0)'
        },
        {
          level: 1,
          title: 'Danh sách SLA',
          path: `admin/${ROUTER_UTILS.sla.root}`
        }
      ]
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SlaRoutingModule { }
