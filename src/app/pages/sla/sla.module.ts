import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { SlaCreateComponent } from './sla-create/sla-create.component';
import { SlaRoutingModule } from './sla-routing.module';
import { SlaComponent } from './sla.component';
import { StatusSlaComponent } from './status-sla/status-sla.component';




@NgModule({
  declarations: [SlaComponent, SlaCreateComponent, StatusSlaComponent],
  imports: [
    CommonModule,
    SharedModule,
    SlaRoutingModule,
  ],
  exports: [SlaComponent]
})
export class SlaModule { }
