import { Component, Input, OnInit } from '@angular/core';
import { SlaService } from '@shared/service/sla.service';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-status-sla',
  templateUrl: './status-sla.component.html',
  styleUrls: ['./status-sla.component.scss']
})
export class StatusSlaComponent implements OnInit {
  @Input() data: any =[];
  statusname = '';
  isClose = false;
  constructor(
    private slaService: SlaService,
    private modalRef: NzModalRef,
  ) { }
  ngOnInit(): void {
    if( this.data.status == 1){
      this.statusname ='khóa';
      this.isClose= !this.isClose;
  }
  else{
    this.statusname ='mở '
  }
  }
  onLock(status: any){
    this.slaService.updateStatus( this.data.id , status ).subscribe( res =>{
      if (res.body?.data) {
        this.modalRef.close({
          success: true,
        });
      }
    })
  }
  onCancel(): void {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
}
