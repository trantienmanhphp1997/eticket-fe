import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from '@shared/service/helpers/toast.service';
import { SlaInternalService } from '@shared/service/sla-internal.service';
import { SlaService } from '@shared/service/sla.service';
import { WorkFlowService } from '@shared/service/work-flow.service';
import CommonUtil from '@shared/utils/common-utils';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
@Component({
  selector: 'app-sla-internal-create',
  templateUrl: './sla-internal-create.component.html',
  styleUrls: ['./sla-internal-create.component.scss']
})
export class SlaInternalCreateComponent implements OnInit {
  isUpdate: boolean = false;
  showSLA: boolean = false;
  nzSelectedIndex: number = 0;
  listSla: any = [];
  listWorkflow: any = [];
  selectedSLA : any = [];
  listdetailWorkflow: any = [];
  ticketPriorityList: any = [];
  listResolution: any = [];
  idWorkflow : any = [];
  idSla: any = [];
  form: FormGroup = new FormGroup({});
  sub: any;
  id: any;
  nameSla: any = [] ;
  nameWordflow: any = [];
  idSlaInternal: any = [];
  listSlainternal: any =  [] ;
  showError: any = [];
  showError1: any = [];
  isErrorRe: any =[];
  disableForm=false
  constructor(
    private slaInternalService : SlaInternalService,
    private slaService : SlaService,
    private workFlowService : WorkFlowService,
    private fb: FormBuilder,
    private toast: ToastService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.route.queryParams
      .subscribe(params => {
        if (params.view == 'detail') {
          this.disableForm = true;
        } 
  });
   }

  ngOnInit(): void {
    this.sub = this.activatedRoute.url.subscribe(params => {
      this.idSla = params[1]?.path;
      this.idWorkflow = params[2]?.path;
      this.idSlaInternal = params[3]?.path;
      this.slaService.update( this.idSla , true).subscribe( res => {
        this.nameSla  = res.body?.data
      })
      this.slaInternalService.updateWorflow( this.idWorkflow , true).subscribe( res => {
        this.nameWordflow  = res.body?.data
      })
      this.workFlowService.getWorkFlowsDetail(this.idWorkflow, true).subscribe((res: any) => {
        this.listdetailWorkflow = res.body?.data.steps.data;
      });
      if(this.idSlaInternal){
        this.slaInternalService.detailSlaInternal( this.idSlaInternal , true).subscribe( res => {
          this.listSlainternal = res.body?.data
        })
      }
    })
    this.getDetailSla();
    this.getDetailSla();
  }
  getDetailSla(){
    const param = {
      include: 'ticketPriorities0,ticketPriorities1'
    }
    this.slaService.detail(this.idSla, param, true).subscribe( res => {
      this.showSLA = false;
      this.selectedSLA = res.body?.data;
      this.form = this.fb.group({
        sla_id: this.idSla,
        workflow_id:  this.idWorkflow,
        target0: this.idSlaInternal? this.initCondition(this.listSlainternal?.target0) : this.Condition(),
        target1: this.idSlaInternal? this.initCondition1(this.listSlainternal?.target1) : this.Condition1()
      });
    })
  }

  target0(): FormArray {
    return this.form.get('target0') as FormArray;
  }
  target1(): FormArray {
    return this.form.get('target1') as FormArray;
  }
  subConditions(empIndex: number): FormArray {    
    return this.target0()
      .at(empIndex)
      .get('steps') as FormArray;

  }
  subConditions1(empIndex: number): FormArray {    
    return this.target1()
      .at(empIndex)
      .get('steps') as FormArray;

  }
  Condition(): FormArray {
    const group = new FormArray([]);
    this.selectedSLA.ticketPriorities0.data.filter((item:any)=>item.status == 1)
    
    this.selectedSLA.ticketPriorities0.data.forEach((item: any, index: number) => {
      group.push(this.fb.group({
        ticket_priority_name: new FormControl({value:item.ticket_priority_name, disabled:true}),
        ticket_priority_id: item.ticket_priority_id,
        resolution_time_d: new FormControl({value:item.resolution_time_d , disabled:this.disableForm}),
        resolution_time_h: new FormControl({value:item.resolution_time_h, disabled:this.disableForm}),
        resolution_time_m: new FormControl({value:item.resolution_time_m, disabled:this.disableForm}),
        colorSla: new FormControl(item.ticket_priority_color),
        steps:  this.SubCondition()
      }))
    })
    return group;
  }
  Condition1(): FormArray {
    const group = new FormArray([]);
    this.selectedSLA.ticketPriorities1.data.filter((item:any)=>item.status == 1)
    this.selectedSLA.ticketPriorities1.data.forEach((item: any, index: number) => {
      group.push(this.fb.group({
        ticket_priority_name: new FormControl({value:item.ticket_priority_name, disabled:true}),
        ticket_priority_id: item.ticket_priority_id,
        resolution_time_d: new FormControl({value:item.resolution_time_d , disabled:this.disableForm}),
        resolution_time_h: new FormControl({value:item.resolution_time_h, disabled:this.disableForm}),
        resolution_time_m: new FormControl({value:item.resolution_time_m, disabled:this.disableForm}),
        colorSla: new FormControl(item.ticket_priority_color),
        steps:  this.SubCondition1()
      }))
    })
    return group;
  }
  SubCondition(): FormArray {
    const group = new FormArray([]);
      this.listdetailWorkflow.forEach((item: any, index: number) => {
        group.push(this.fb.group({
          step_id: item.id,
          resolution_time_d: new FormControl({value:'', disabled:this.disableForm}),
          resolution_time_h: new FormControl({value:'', disabled:this.disableForm}),
          resolution_time_m: new FormControl({value:'', disabled:this.disableForm}),
        }))
      })
    return group;
  }
  SubCondition1(): FormArray {
    const group = new FormArray([]);
      this.listdetailWorkflow.forEach((item: any, index: number) => {
        group.push(this.fb.group({
          step_id: item.id,
          resolution_time_d: new FormControl({value:'', disabled:this.disableForm}),
          resolution_time_h: new FormControl({value:'', disabled:this.disableForm}),
          resolution_time_m: new FormControl({value:'', disabled:this.disableForm}),
        }))
      })
    return group;
  }
  initCondition(SlaInternal: any ): FormArray {
    const group = new FormArray([]);
    SlaInternal?.forEach((item: any, index: number) => {
      group.push(this.fb.group({
        ticket_priority_name: new FormControl({value:item.ticket_priority_name, disabled:true}),
        ticket_priority_id: item.ticket_priority_id,
        resolution_time_d: new FormControl({value:item.resolution_time_d, disabled: false}),
        resolution_time_h: new FormControl({value:item.resolution_time_h, disabled:false}),
        resolution_time_m: new FormControl({value:item.resolution_time_m, disabled:false}),
        colorSla: new FormControl(item.ticket_priority_color),
        steps:  this.initSubCondition(item.steps, index)
      }))
    })
    return group;
  }
  initCondition1(SlaInternal: any ): FormArray {
    const group = new FormArray([]);
    SlaInternal?.forEach((item: any, index: number) => {
      group.push(this.fb.group({
        ticket_priority_name: new FormControl({value:item.ticket_priority_name, disabled:true}),
        ticket_priority_id: item.ticket_priority_id,
        resolution_time_d: new FormControl({value:item.resolution_time_d, disabled:false}),
        resolution_time_h: new FormControl({value:item.resolution_time_h, disabled:false}),
        resolution_time_m: new FormControl({value:item.resolution_time_m, disabled:false}),
        colorSla: new FormControl(item.ticket_priority_color),
        steps:  this.initSubCondition1(item.steps, index)
      }))
    })
    return group;
  }
  initSubCondition( steps: any , index: any): FormArray {
    const group = new FormArray([]);
      steps.forEach((item: any, index: number) => {
        group.push(this.fb.group({
          step_id: item.step_id,
          resolution_time_d: new FormControl({value:item.resolution_time_d, disabled:this.disableForm}),
          resolution_time_h: new FormControl({value:item.resolution_time_h, disabled:this.disableForm}),
          resolution_time_m: new FormControl({value:item.resolution_time_m, disabled:this.disableForm}),
        }))
      })
    return group;
  }
  initSubCondition1( steps: any , index: any): FormArray {
    const group = new FormArray([]);
      steps.forEach((item: any, index: number) => {
        group.push(this.fb.group({
          step_id: item.step_id,
          resolution_time_d: new FormControl({value:item.resolution_time_d, disabled:this.disableForm}),
          resolution_time_h: new FormControl({value:item.resolution_time_h, disabled:this.disableForm}),
          resolution_time_m: new FormControl({value:item.resolution_time_m, disabled:this.disableForm}),
        }))
      })
    return group;
  }
  getColorSla(item:any){
    return item.value.colorSla
  }
  getD(item:any){
   const d = item.value.resolution_time_d
    if(d==null){
      return false
    }
    else{
      return true
    }
  }
  getG(item:any){
    const h = item.value.resolution_time_h
     if(h==null){
       return false
     }
     else{
       return true
     }
   }
   getM(item:any){
    const m = item.value.resolution_time_m
     if(m==null){
       return false
     }
     else{
       return true
     }
   }
   onSubmit(){
    const SLA: any = {
      ...this.form.value,
    };
    const body: any = CommonUtil.trim(SLA);
    let manglog: any = [];
    let manglog2: any = [];
    body.target0.forEach((item: any, index: number)=>{
      let  sumt = (item.resolution_time_d)*24*60 + (item.resolution_time_h)*60 + (item.resolution_time_m) ;
      let tong : number = 0;
      let sum : number = 0;
      item.steps.forEach((steps: any, i: number)=>{
        sum  = (steps.resolution_time_d)*24*60 + (steps.resolution_time_h)*60 +(steps.resolution_time_m);
        tong = Number(tong) +Number(sum);
        // if( steps.resolution_time_d =='' && steps.resolution_time_h =='' && steps.resolution_time_m ==''){
        //   manglog2.push(1);
        // }
        // else{
        //   manglog2.push(0);
        //   this.isErrorRe[i]= false;
        // }
      })
      
      if(sumt < tong){
        manglog.push(1);
        this.showError[index] = true;
      }
      else if (sumt >= tong){
         manglog.push(0);
         this.showError[index] = false;
      }
    })
    body.target1.forEach((item: any, index: number)=>{
      let  sumt = (item.resolution_time_d)*24*60 + (item.resolution_time_h)*60 + (item.resolution_time_m) ;
      let tong : number = 0;
      let sum : number = 0;
      item.steps.forEach((steps: any, i: number)=>{
        sum  = (steps.resolution_time_d)*24*60 + (steps.resolution_time_h)*60 +(steps.resolution_time_m);
        tong = Number(tong) +Number(sum);
        // if( steps.resolution_time_d =='' && steps.resolution_time_h =='' && steps.resolution_time_m ==''){
        //   manglog2.push(1);
        // }
        // else{
        //   manglog2.push(0);
        //   this.isErrorRe[i]= false;
        // }
      })
      
      if(sumt < tong){
        manglog2.push(1);
        this.showError1[index] = true;
      }
      else if (sumt >= tong){
         manglog2.push(0);
         this.showError1[index] = false;
      }
    })
    let summ = 0;
    let summ2 = 0;
    for (let i = 0; i < manglog.length; i++){
      summ += manglog[i];
    }
    for (let j = 0; j < manglog2.length; j++){
      summ2 += manglog2[j];
    }
      if(summ == 0 && summ2 ==0){
        let serviceEvent = this.idSlaInternal ? this.slaInternalService.updateSlaInternal( this.idSlaInternal, body, true) : this.slaInternalService.create(body, true)
        serviceEvent.subscribe(res => {
            if(res){  
              if(this.idSlaInternal){
                this.toast.success(`Cập nhật thành công !`);
              }
              else{
                this.toast.success(`Tạo mới thành công !`);
              }
              this.router.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/${ROUTER_UTILS.workflowConfiguration.detail}`, this.idWorkflow]);
            }
        })
      }
      // else if(summ2 != 0){
      //   this.toast.error('Thời gian của các level bắt buộc phải nhập');
      // }
  }
  onCancel(){
    this.router.navigate([`admin/${ROUTER_UTILS.workflowConfiguration.root}/${ROUTER_UTILS.workflowConfiguration.detail}/${this.idWorkflow}`],{ queryParams:{ index: 3}});
  }
}