import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { SlaService } from '@shared/service/sla.service';
import CommonUtil from '@shared/utils/common-utils';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { ActivatedRoute, Router } from '@angular/router';
import { SlaInternalService } from '@shared/service/sla-internal.service';
import { ModalComponent } from '@shared/components/modal/modal.component';

@Component({
  selector: 'app-sla-internal',
  templateUrl: './sla-internal.component.html',
  styleUrls: ['./sla-internal.component.scss']
})
export class SlaInternalComponent implements OnInit {

  // page data
  slaList: any = [];
  total: any = 0;
  pageIndex = PAGINATION.PAGE_DEFAULT;
  pageSize = PAGINATION.SIZE_DEFAULT;
  pageSizeOptions = PAGINATION.OPTIONS;
  search = null;
  //checked items
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly any[] = [];
  setOfCheckedId = new Set<number>();
  checked = false;
  testChecked = true;
  //form 
  searchForm = this.fb.group({
    keyword: [null]
  });
  sub: any;
  id: any;
  commonUtil: any = CommonUtil
  constructor(
    private slaInternalService: SlaInternalService,
    private fb: FormBuilder,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toast: ToastService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.sub = this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id') || 0;
    })
    if(this.id){
      this.loadData();
    }

  }
  // load data
  loadData() {
    const params = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      search: this.search
    }
    this.slaInternalService.getSlaInternal(this.id, params , true).subscribe(
      (res) => {
        this.slaList = res.body?.data.map((dept: any) => {
          return {
            ...dept, disabled: dept.sla_internals_id === ''
          }
        })
        this.total = res.body?.meta?.pagination.total
      }
    );
  }

  getServiceProducts(products: any): any {
    var result = [];
    if (products.length > 0) {
      result = products.filter((item: any) => item.id != products[0].id)
    }
    return result;
  }
  // Events
  onSearch($event: any) {
    this.search = $event.target?.value.trim();
      this.searchForm.patchValue({
        keyword:  this.search
      })
      this.pageIndex = PAGINATION.PAGE_DEFAULT;
      this.loadData();
  }
  onCreate = (data : any ) => {
    this.router.navigate([`admin/${ROUTER_UTILS.slaInternal.root}/${ROUTER_UTILS.slaInternal.create}/${data.id}`, this.id]);
  }
  detail = (data : any ) => {
    this.router.navigate([`admin/${ROUTER_UTILS.slaInternal.root}/${ROUTER_UTILS.slaInternal.detail}/${data.id}`, this.id, data.sla_internals_id]);
  }
  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }
  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.loadData();
  }
  onUpdate = (data?: any) => {
    if (data.id) {
      this.router.navigate([`admin/${ROUTER_UTILS.sla.root}/${ROUTER_UTILS.sla.view}`, data.id,]);
    }
  }
  onDelete = (data: any) => {
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc muốn xóa SLA nội bộ không?`,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'450px',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    let slaIds =  data instanceof Set ? Array.from(data) : [data.sla_internals_id] ; 
    modal.afterClose.subscribe(result => {
      if (result?.success) {
        this.slaInternalService.delete(slaIds).subscribe((res: any) => {
          if (res) {
            this.toast.success('Xóa SLA nội bộ thành công !');
            this.loadData()
          };
        })
      }
    });
  }
  onDeleteAll = (data: any) => {
    const form = CommonUtil.modalConfirm(
      this.translateService,
      'model.slaInternal.deletemultiSlaTitle',
      'model.slaInternal.deletemultiSingleSLAContent',
      { name: data.title || '' },
      () => {
        return {
          success: true,
        };
      },
      () => {
        return {
          success: false,
        };
      },
      'product.delete.btnDelete'
    );
    const modal: NzModalRef = this.modalService.confirm(form);

    let slaIds =  data instanceof Set ? Array.from(data) : [data.id] ; 
    modal.afterClose.subscribe(result => {
      if (result?.success) {
        this.slaInternalService.delete(slaIds).subscribe((res: any) => {
          if (res) {
            this.toast.success('Xóa thành công !');
            this.loadData();
            this.refreshCheckedStatus();
          };
        })
      }
    });
  }

  onAllChecked(checked: boolean): void {

  }
  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }
  onReset() {
    this.searchForm.patchValue({
      keyword: null,
    })
    this.loadData();
  }

  updateAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.slaList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.sla_internals_id, checked)
      }
    });
    this.refreshCheckedStatus();
  }

}
