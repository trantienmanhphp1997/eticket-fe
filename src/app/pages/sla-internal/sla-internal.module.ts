import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { SlaInternalRoutingModule } from './sla-internal-routing.module';
import { SlaInternalComponent } from './sla-internal.component';
import { SlaInternalCreateComponent } from './sla-internal-create/sla-internal-create.component';

@NgModule({
  declarations: [SlaInternalComponent, SlaInternalCreateComponent],
  imports: [
    CommonModule,
    SharedModule,
    SlaInternalRoutingModule,
  ],
  exports: [SlaInternalComponent]
})
export class SlaInternalModule { }
