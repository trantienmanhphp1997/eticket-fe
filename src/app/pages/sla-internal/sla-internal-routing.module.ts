import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { SlaInternalComponent } from './sla-internal.component';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { SlaInternalCreateComponent } from './sla-internal-create/sla-internal-create.component';

const routes: Routes = [
  {
    path: '',
    component: SlaInternalComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.SLA.LIST],
      title: 'Quản lý SLA nội bộ'
    }
  },
  {
    path: `${ROUTER_UTILS.slaInternal.create}/:id/:id`,
    component: SlaInternalCreateComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.SLA.CREATE],
      title: 'Tạo mới SLA nội bộ',
    }
  },
  {
    path: `${ROUTER_UTILS.slaInternal.detail}/:id/:id/:id`,
    component: SlaInternalCreateComponent,
    canActivate: [AuthGuard],
    data: {
      authorities: [AUTH.SLA.CREATE],
      title: 'Cập nhật SLA nội bộ',
      titleOther: 'Chi tiết SLA nội bộ',
      breadcrum: [
        {
          level: 1,
          title: 'Cấu hình hệ thống',
          path: `javascript:void(0)`
        },
        {
          level: 2,
          title: 'Cấu hình luồng xử lý',
          path: `javascript:void(0)`
        },
        {
          level: 3,
          title: 'model.workFlow.title',
          path: `admin/${ROUTER_UTILS.workflowConfiguration.root}`
        }
      ]
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SlaInternalRoutingModule { }
