import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzUploadChangeParam, NzUploadFile } from 'ng-zorro-antd/upload';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TicketService } from '@shared/service/ticket.service';
import { TicketPriorityService } from '@shared/service/ticket-priority.service';
import { UserService } from '@shared/service/user.service';
import CommonUtil from '@shared/utils/common-utils';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TaskService } from '@shared/service/task.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { of } from 'rxjs';
import { MediaService } from '@shared/service/media.service';

@Component({
    selector: 'app-task-create',
    templateUrl: './task-create.component.html',
    styleUrls: ['./task-create.component.css']
})

export class TaskCreateComponent implements OnInit {
    form: FormGroup = new FormGroup({});
    dateFormat = 'dd/MM/yyyy';
    ticketList: any = [];
    ticketPriorityList: any = [];
    userList: any = [];
    fileList: NzUploadFile[] = [];
    files: any
    constructor(
        private msg: NzMessageService,
        private router: Router,
        private fb: FormBuilder,
        private ticketService: TicketService,
        private ticketPriorityService: TicketPriorityService,
        private userService: UserService,
        private toast: ToastService,
        private taskService: TaskService,
        private mediaService: MediaService
    ) { }

    ngOnInit() {
        this.initForm();
        this.getTicketData();
        this.getTicketPriorityData();
        this.getUserData();
    }

    initForm(): void {
        this.form = this.fb.group({
            name: '',
            type: '',
            priority: '',
            start_time: '',
            end_time: '',
            description: '',
            ticket_id: '',
            reporter_id: '',
            assignee_id: ''
        });
    }

    handleUpload = (item: any) => {
        let param = {
            'file': this.files,
            'key': 'task'
        }
        // const data= new FormData()
        // 	data.append('file', item.file)
        // return this.mediaService.upload(param).subscribe((res: any) => {

        // })
    }


    beforeUpload = (file: NzUploadFile): boolean => {
        const myReader = new FileReader();
        this.fileList = this.fileList.concat(file);
        this.files = myReader.readAsDataURL(file as any);
        myReader.onloadend = (e) => {
            this.files = myReader.result;
            console.log(myReader.result);
            var filea = this.dataURLtoFile(myReader.result,'hello.txt');
            console.log(filea);
          };
       
        return true;
    };

    dataURLtoFile(dataurl: any, filename: any) {
 
        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), 
            n = bstr.length, 
            u8arr = new Uint8Array(n);
            
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        
        return new File([u8arr], filename, {type:mime});
    }
    
    handleChange({ file, fileList }: NzUploadChangeParam): void {
        const status = file.status;
        if (status !== 'uploading') {
            console.log(file, fileList);
        }
        if (status === 'done') {
            this.msg.success(`${file.name} file uploaded successfully.`);
        } else if (status === 'error') {
            this.msg.error(`${file.name} file upload failed.`);
        }
    }

    onCancel() {
        this.router.navigate([`admin/${ROUTER_UTILS.task.root}`]);
    }
    getTicketData() {
        const params = {
            pageIndex: '',
            pageSize: 0
        }
        this.ticketService.getAll(params, true).subscribe((res) => {
            this.ticketList = res.body?.data;
        });
    }
    getTicketPriorityData() {
        const params = {
            pageIndex: '',
            pageSize: 0
        }
        this.ticketPriorityService.getAllTicketPriority(params, true).subscribe((res) => {
            this.ticketPriorityList = res.body?.data;
        });
    }
    getUserData() {
        const params = {
            pageIndex: '',
            pageSize: 0
        }
        this.userService.getUser(params, true).subscribe((res) => {
            this.userList = res.body?.data;
        });
    }

    onSubmit() {
        if (this.form.invalid) {
            CommonUtil.markFormGroupTouched(this.form);
            return;
        }
        const params = {
            ...this.form.value,
            start_time: moment(this.form.value.start_time).toISOString(),
            end_time: moment(this.form.value.end_time).toISOString()
        }
        const body = CommonUtil.trim(params);
        this.taskService.create(body, true).subscribe(res => {
            if (res.body?.data) {
                this.toast.success(`Tạo công việc thành công`);
                this.router.navigate([`admin/${ROUTER_UTILS.task.root}`]);
            }
        });
    }

    onOk(result: Date | Date[] | null): void {
    }

    onChangeData(type: string, content: string): void {
        this.form.get(type)?.setValue(content);
    }

}