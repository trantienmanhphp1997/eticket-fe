import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzUploadChangeParam } from 'ng-zorro-antd/upload';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TicketService } from '@shared/service/ticket.service';
import { TicketPriorityService } from '@shared/service/ticket-priority.service';
import { UserService } from '@shared/service/user.service';
import CommonUtil from '@shared/utils/common-utils';
import { ToastService } from '@shared/service/helpers/toast.service';
import { TaskService } from '@shared/service/task.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-task-detail',
    templateUrl: './task-detail.component.html',
    styleUrls: ['./task-detail.component.css']
})

export class TaskDetailComponent implements OnInit {

    form: FormGroup = new FormGroup({});
    dateFormat = 'dd/MM/yyyy';
    ticketList: any = [];
    ticketPriorityList: any = [];
    userList: any = [];
    taskId: any = [];
    taskData: any = [];
    taskRelateList: any = [];
    ticketId: any;
    isShowDescription: boolean  = false;
    description : any = [];

    constructor(
        private msg: NzMessageService,
        private router: Router,
        private fb: FormBuilder,
        private ticketService: TicketService,
        private ticketPriorityService: TicketPriorityService,
        private userService: UserService,
        private toast: ToastService,
        private taskService: TaskService,
        private route: ActivatedRoute,
    ) {

        this.route.paramMap.subscribe((res) => {
            this.taskId = res.get('id') || '';
        });
    }

    ngOnInit() {
        this.initForm(null);
        this.getTicketData();
        this.getTicketPriorityData();
        this.getUserData();
        this.loadTaskById();

    }

    loadTaskById() {
        this.taskService.getTaskById(this.taskId, true).subscribe(res => {
            this.taskData = res.body?.data;
            this.initForm(res.body?.data);
            this.getListTaskRelated(res.body?.data);

            // this.onChangeData('description',res.body?.data.description);
        });
    }

    initForm(res: any): void {
        this.form = this.fb.group({
            name: [{ value: res?.name, disabled: false }, []],
            type: '',
            priority: [{ value: res?.priority, disabled: false }, []],
            start_time: [{ value: res?.start_time, disabled: false }, []],
            end_time: [{ value: res?.end_time, disabled: false }, []],
            description: [{ value: this.taskId? res?.description : null, disabled: false }, []],
            ticket_id: '',
            reporter_id: [{ value: res?.reporter_id, disabled: false }, []],
            assignee_id: [{ value: res?.assignee_id, disabled: false }, []],
            progress: '',
            status: '',
        });
        this.description = this.taskData.description;
    }

    getTicketData() {
        const params = {
            pageIndex: '',
            pageSize: 0
        }
        this.ticketService.getAll(params, true).subscribe((res) => {
            this.ticketList = res.body?.data;
        });
    }

    getListTaskRelated(res: any) {
        const param = {
            ticketId: res?.ticket_id
        }
        this.taskService.getTaskRelated(this.taskId, param).subscribe(res => {
            this.taskRelateList = res.body?.data;
        });
    }
    getTicketPriorityData() {
        const params = {
            pageIndex: '',
            pageSize: 0
        }
        this.ticketPriorityService.getAllTicketPriority(params, true).subscribe((res) => {
            this.ticketPriorityList = res.body?.data;
        });
    }
    getUserData() {
        const params = {
            pageIndex: '',
            pageSize: 0
        }
        this.userService.getUser(params, true).subscribe((res) => {
            this.userList = res.body?.data;
        });
    }

    handleChange({ file, fileList }: NzUploadChangeParam): void {
        const status = file.status;
        if (status !== 'uploading') {
        }
        if (status === 'done') {
            this.msg.success(`${file.name} file uploaded successfully.`);
        } else if (status === 'error') {
            this.msg.error(`${file.name} file upload failed.`);
        }
    }
    onOk(result: Date | Date[] | null): void {
    }
    onSubmit() {
        this.isShowDescription = false
        if (this.form.invalid) {
            CommonUtil.markFormGroupTouched(this.form);
            return;
        }
        const params = {
            ...this.form.value,
            start_time: moment(this.form.value.start_time).toISOString(),
            end_time: moment(this.form.value.end_time).toISOString()
        }
        const body = CommonUtil.trim(params);
        this.taskService.updateTask(this.taskId, body, true).subscribe(res => {
            if (res.body?.data) {
                this.loadTaskById();
                this.toast.success(`Cập nhật công việc thành công`);
            }
        });
    }
    onChangeData(type: string, content: string): void {
        this.form.get(type)?.setValue(content);
    }
    showDiscription() {
        this.isShowDescription = true;
    }
}
