import { Component, OnInit } from '@angular/core';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { TaskService } from '@shared/service/task.service';
import { Router } from '@angular/router';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { NzTableQueryParams } from 'ng-zorro-antd/table';

@Component({
    selector: 'app-task',
    templateUrl: './task.component.html',
    styleUrls: ['./task.component.css']
})

export class TaskComponent implements OnInit {

    setOfCheckedId = new Set<number>();
    checked = false;
    indeterminate = false;
    isCallFirstRequest: boolean = true;
    total: any = 0;
    taskList: any = [];
    listOfCurrentPageData: readonly any[] = [];
    taskQuery = {
        pageIndex: PAGINATION.PAGE_DEFAULT,
        pageSize: PAGINATION.SIZE_DEFAULT,
        pageSizeOptions: PAGINATION.OPTIONS,
        orderBy: '',
        sortBy: '',
        keyword: '',
    }
    constructor(
        private taskService: TaskService,
        private router: Router,
    ) { }

    ngOnInit() {
        this.loadData();
    }

    loadData() {
        this.taskService.getTasks(this.taskQuery, true).subscribe(res => {
            this.taskList = res.body?.data;
            this.total = res.body?.meta?.pagination?.total
        });
    }

    onSearch(event: any) {
        if (event) {
            this.taskQuery.keyword = event.target?.value;
            this.taskQuery.pageIndex = PAGINATION.PAGE_DEFAULT;
            this.loadData();
          }
    }
    onRefresh() {

    }
    onDelete(data: any) {

    }
    onCreateTask() {
        this.router.navigate([`admin/${ROUTER_UTILS.task.root}/${ROUTER_UTILS.task.create}`]);
    }
    onFilter() {

    }
    onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
        this.listOfCurrentPageData = listOfCurrentPageData;
        this.refreshCheckedStatus();
    }

    refreshCheckedStatus(): void {
        const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
        this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
        this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
    }
    onQueryParamsChange(params: NzTableQueryParams): void {
        if (this.isCallFirstRequest) {
            this.isCallFirstRequest = false;
            return;
        }
        this.taskQuery.sortBy = params.sort.filter((e: any) => e.value !== null).map((e: any) => e.value?.slice(0, -3)).toString();
        this.taskQuery.orderBy = params.sort.filter((e: any) => e.key !== null && e.key !== undefined && e.value !== null).map((e: any) => e.key).toString();
        this.loadData();
    }
    updateAllChecked(event: any) {

    }
    onQuerySearch(event: any) {
        const { pageIndex, pageSize } = event;
        this.taskQuery.pageIndex = pageIndex;
        this.taskQuery.pageSize = pageSize;
        this.loadData();
    }
    onUpdate(taskId: any) {
        console.log(taskId);
        this.router.navigate([`admin/${ROUTER_UTILS.task.root}/${ROUTER_UTILS.task.detail}/${taskId}`]);
    }
    onItemChecked(id: any, checked: boolean): void {
    }
}