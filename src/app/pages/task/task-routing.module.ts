import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AUTH } from '@shared/constants/auth.constant';
import { TaskComponent } from './task.component';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { TaskCreateComponent } from './task-create/task-create.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';

const routes: Routes = [
    {
      path: '',
      component: TaskComponent,
      canActivate: [AuthGuard],
      data: {
        authorities: [AUTH.TASK.LIST],
        title: 'Quản lý công việc'
      }
    },
    {
      path: ROUTER_UTILS.task.create,
      component: TaskCreateComponent,
      canActivate: [AuthGuard],
      data: {
        authorities: [AUTH.TASK.CREATE],
        title: 'Tạo mới công việc',
        breadcrum: [
          {
            level: 1,
            title: 'Quản lý công việc',
            path: `admin/${ROUTER_UTILS.task.root}`
          }
        ]
      }
    },
    {
      path: `${ROUTER_UTILS.task.detail}/:id`,
      component: TaskDetailComponent,
      canActivate: [AuthGuard],
      data: {
        authorities: [AUTH.TASK.CREATE],
        title: 'Chi tiết công việc',
        breadcrum: [
          {
            level: 1,
            title: 'Quản lý công việc',
            path: `admin/${ROUTER_UTILS.task.root}`
          }
        ]
      }
    },
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class WelcomeRoutingModule { }