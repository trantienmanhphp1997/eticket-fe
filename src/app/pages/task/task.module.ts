import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { WelcomeRoutingModule } from './task-routing.module';
import { TaskComponent } from './task.component';
import { TaskCreateComponent } from './task-create/task-create.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';

@NgModule({
    declarations: [
        TaskComponent,
        TaskCreateComponent,
        TaskDetailComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        WelcomeRoutingModule,
    ],
    exports: [
        TaskComponent,
    ]
})
export class TicketModule { }
