import { Component, ElementRef, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { DashboardService } from '@shared/service/dashboard.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { ProductService } from '@shared/service/product.service';
import { TicketPriorityService } from '@shared/service/ticket-priority.service';
import CommonUtil from '@shared/utils/common-utils';
import { Chart } from 'chart.js';
import { ChartType, ChartOptions } from 'chart.js';
import * as moment from 'moment';
import { Color } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { DepartmentService } from '@shared/service/department.service';
import {FormGroup, FormControl} from '@angular/forms';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css'],
})
export class WelcomeComponent implements OnInit {
  selected: any = { startDate: null, endDate: null };
  visible = false;
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });
  count_product: any;
  count_product1: any;
  today= new Date().getFullYear();
  yearSelct  = new Date().setFullYear(new Date().getFullYear(), 0, 1);
  @ViewChild('canvas') _chart: ElementRef | undefined;
  @ViewChild('canvas0') _chart0: ElementRef | undefined;
  year: any = '';
  yearFillter: any ;
  quy: any = null;
  quy1: any = 'Năm';
  active: any = [];
  active1: any = 1;
  listProductall: any = [];
  profile:any;
  noData: boolean= false;
  checkQueryDate: boolean = false ;
  typeInput: boolean = false;
  // public pieChartPlugins=[{
  //   beforeInit: function(chart:any, options:any) {
  //     const fitValue=chart.legend.fit
  //     chart.legend.fit = function fit() {
  //       return this.width -= 20;
  //     };
  //   }
  // }];
  public pieChartOptions: any = {
    legend: {
      display: true,
      position: 'right',
      fullWidth: false,
      reverse: true,
      labels: {
        boxWidth: 10,
        usePointStyle: true,
        padding: 20,
        textAlign: 'left'
      },
      layout: {
        padding: 0,
      }
    },
    hover: {
      mode: 'nearest',
      intersect: true
    },
    plugins: {
      datalabels: {
        display: false
      }
    },

    tooltips: {
      enabled: false,
      mode: 'index',
      position: 'nearest',
      callbacks: {
        label: function (tooltipItem: any, data: any) {
          var dataset = data.datasets[tooltipItem.datasetIndex];
          var currentValue = dataset.data[tooltipItem.index];
          return '<b>' + data.labels[tooltipItem.index] + ':</b> ' + currentValue + ' %';
        }
      },
      custom: function (context: any) {
        var tooltipEl = document.getElementById('chartjs-tooltip');
        const tooltip = context

        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjs-tooltip';
          tooltipEl.innerHTML = "<table></table>";

          document.body.appendChild(tooltipEl);
        }

        // Hide if no tooltip
        if (tooltip.opacity === 0) {
          tooltipEl.style.opacity = '0';
          return;
        }
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltip.yAlign) {
          tooltipEl.classList.add(tooltip.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }
        function getBody(bodyItem: any) {
          return bodyItem.lines;
        }
        // Set Text
        if (tooltip.body) {
          var titleLines = tooltip.title || [];
          var bodyLines = tooltip.body.map(getBody);
          var innerHtml = '<thead>';
          titleLines.forEach(function (title: any) {
            innerHtml += '<tr><th>' + title + '</th></tr>';
            innerHtml += '<tr><th></th></tr>';
          });
          innerHtml += '</thead><tbody>';
          bodyLines.forEach(function (body: any, i: any) {
            var colors = tooltip.labelColors[i];
            var style = 'background:' + colors.backgroundColor + '; border-color:' + colors.borderColor;
            style += '; border-color:' + colors.borderColor;
            style += '; border-width: 2px';
            var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
            innerHtml += '<tr><td>' + span + body + '</td></tr>';
          });
          innerHtml += '</tbody>';
          var tableRoot = tooltipEl.querySelector('table');
          if (tableRoot) {
            tableRoot.innerHTML = innerHtml;
          }
        }
        var bodyRect = document.body.getBoundingClientRect();
        const position = document.getElementById('chart3')?.getBoundingClientRect();
        const positionX = position?.left || 0;
        const positionY = position?.top || 0;
        const scrollTop = positionY - bodyRect.top;
        const leftX = positionX + window.pageXOffset + tooltip.caretX;
        const left = bodyRect.width - leftX > 100 ? leftX + 70 : leftX - 30;
        tooltipEl.style.left = left + 'px';
        tooltipEl.style.top = scrollTop + tooltip.caretY - 60 + 'px';
        tooltipEl.style.position = 'absolute';
        tooltipEl.style.opacity = '1';
        tooltipEl.style.borderColor = '#000';
        tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
        tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
        tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
        tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
        tooltipEl.style.pointerEvents = 'none';
      }
    },
    responsive: true,

  };
 findLabel = (labels:any, evt:any) => {
    let found = false;
    let res = null;
  
    labels.forEach((l:any) => {
      l.labels.forEach((label:any) => {
        if (evt.x > label.x && evt.x < label.x2 && evt.y > label.y && evt.y < label.y2) {
          res = label.label;
          found = true;
        }
      });
    });
  
    return [found, res];
  };
  
   getLabelHitboxes = (scales:any) => (Object.values(scales).map((s:any) => ({
    scaleId: s.id,
    labels: s._labelItems.map((e:any, i:any) => ({
      x: e.translation[0] - s._labelSizes.widths[i] / 2,
      x2: e.translation[0] + s._labelSizes.widths[i] / 2,
      y: e.translation[1] - s._labelSizes.heights[i] / 2,
      y2: e.translation[1] + s._labelSizes.heights[i] / 2,
      label: e.label,
      index: i
    }))
  })));
  public barChartOptions0: any = {
    legend: {
      display: true,
      position: 'bottom',
      labels: {
        boxWidth: 10,
      },
    },
    animation: {
      animateScale: true,
      animateRotate: true,
      
    },
    maintainAspectRatio:true,
    scales: {
      xAxes: [
        {
          barPercentage: 1,
          categoryPercentage: 1 / 2,
          gridLines: {
            display: false,
          },
          ticks: {
            autoSkip: false,
            maxRotation: 45,
            minRotation: 45,
            callback: function (value: any) {
              return CommonUtil.extractContent('<p title=' + value + '>' + CommonUtil.limitWord(value, 10) + '</p>');
            },
          }
        },
      ],
      yAxes: [
        {
          
          gridLines: {
            borderDash: [8, 4],
          },
          ticks: {
            beginAtZero: true,
          }
        },
      ],
    },
    hover: {
      mode: 'index',
      intersect: false,
    },
    plugins: {
      datalabels: {
        display: false,
      },
      id: 'customHover',
      afterEvent: (chart: any, event: any) => {
        const evt = event.event;

        if (evt.type !== 'mousemove') {
          return;
        }

        const [found, label] = this.findLabel(this.getLabelHitboxes(chart.scales), evt);

      }

    },
    tooltips: {
      enabled: false,
      intersect : false,
      mode:'index',
      position: 'nearest',
      
      
      custom: function (context: any) {
        var tooltipEl = document.getElementById('chartjs-tooltip');
        const tooltip = context

        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjs-tooltip';
          tooltipEl.innerHTML = "<table id='table-tooltip'></table>";

          document.body.appendChild(tooltipEl);
        }

        // Hide if no tooltip
        if (tooltip.opacity === 0) {
          tooltipEl.style.opacity = '0';
          return;
        }
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltip.yAlign) {
          tooltipEl.classList.add(tooltip.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }
        function getBody(bodyItem: any) {
          return bodyItem.lines;
        }
        // Set Text
        if (tooltip.body) {
          var titleLines = tooltip.title || [];
          var bodyLines = tooltip.body.map(getBody);
          var innerHtml = '<thead>';
          titleLines.forEach(function (title: any) {
            innerHtml += '<tr><th>' + title + '</th></tr>';
            innerHtml += '<tr><th></th></tr>';
          });
          innerHtml += '</thead><tbody>';
          bodyLines.forEach(function (body: any, i: any) {
            var colors = tooltip.labelColors[i];
            var style = 'background:' + colors.backgroundColor + '; border-color:' + colors.borderColor;
            style += '; border-color:' + colors.borderColor;
            style += '; border-width: 2px';
            var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
            innerHtml += '<tr><td>' + span + body + '</td></tr>';
          });
          innerHtml += '</tbody>';
          var tableRoot = tooltipEl.querySelector('table');
          if (tableRoot) {
            tableRoot.innerHTML = innerHtml;
          }
        }
        var bodyRect = document.body.getBoundingClientRect();
        const position = document.getElementById('chart0')?.getBoundingClientRect();
        const positionX = position?.left || 0;
        const positionY = position?.top || 0;
        const scrollTop = positionY - bodyRect.top;
        const leftX = positionX + window.pageXOffset + tooltip.caretX;
        const left = bodyRect.width - leftX > 100 ? leftX + 70 : leftX - 30;
        tooltipEl.style.left = left + 'px';
        tooltipEl.style.top = scrollTop + tooltip.caretY - 60 + 'px';
        tooltipEl.style.position = 'absolute';
        tooltipEl.style.opacity = '1';
        tooltipEl.style.borderColor = '#000';
        tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
        tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
        tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
        tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
        tooltipEl.style.pointerEvents = 'none';
      }
    },
  };
  public barChartOptions2: any = {
    legend: {
      display: true,
      position: 'bottom',
      labels: {
        boxWidth: 10,
      },
    },
    animation: {
      animateScale: true,
      animateRotate: true,
      
    },
    maintainAspectRatio:true,
    scales: {
      xAxes: [
        {
          barPercentage: 1,
          categoryPercentage: 1 / 2,
          gridLines: {
            display: false,
          },
          ticks: {
            autoSkip: false,
            maxRotation: 45,
            minRotation: 45,
            callback: function (value: any) {
              return CommonUtil.extractContent('<p title=' + value + '>' + CommonUtil.limitWord(value, 10) + '</p>');
            },
          }
        },
      ],
      yAxes: [
        {
          
          gridLines: {
            borderDash: [8, 4],
          },
          ticks: {
            beginAtZero: true,
          }
        },
      ],
    },
    hover: {
      mode: 'index',
      intersect: false,
    },
    plugins: {
      datalabels: {
        display: false,
      },
      id: 'customHover',
      afterEvent: (chart: any, event: any) => {
        const evt = event.event;

        if (evt.type !== 'mousemove') {
          return;
        }

        const [found, label] = this.findLabel(this.getLabelHitboxes(chart.scales), evt);

      }

    },
    tooltips: {
      enabled: false,
      intersect : false,
      mode:'index',
      position: 'nearest',
      callbacks: {
        title: function (context: any) {  
          const  day = 'Tháng ' + context[0].label;
          return day ;
        }
      },
      
      custom: function (context: any) {
        var tooltipEl = document.getElementById('chartjs-tooltip');
        const tooltip = context

        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjs-tooltip';
          tooltipEl.innerHTML = "<table id='table-tooltip'></table>";

          document.body.appendChild(tooltipEl);
        }

        // Hide if no tooltip
        if (tooltip.opacity === 0) {
          tooltipEl.style.opacity = '0';
          return;
        }
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltip.yAlign) {
          tooltipEl.classList.add(tooltip.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }
        function getBody(bodyItem: any) {
          return bodyItem.lines;
        }
        // Set Text
        if (tooltip.body) {
          var titleLines = tooltip.title || [];
          var bodyLines = tooltip.body.map(getBody);
          var innerHtml = '<thead>';
          titleLines.forEach(function (title: any) {
            innerHtml += '<tr><th>' + title + '</th></tr>';
            innerHtml += '<tr><th></th></tr>';
          });
          innerHtml += '</thead><tbody>';
          bodyLines.forEach(function (body: any, i: any) {
            var colors = tooltip.labelColors[i];
            var style = 'background:' + colors.backgroundColor + '; border-color:' + colors.borderColor;
            style += '; border-color:' + colors.borderColor;
            style += '; border-width: 2px';
            var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
            innerHtml += '<tr><td>' + span + body + '</td></tr>';
          });
          innerHtml += '</tbody>';
          var tableRoot = tooltipEl.querySelector('table');
          if (tableRoot) {
            tableRoot.innerHTML = innerHtml;
          }
        }
        var bodyRect = document.body.getBoundingClientRect();
        const position = document.getElementById('chart2')?.getBoundingClientRect();
        const positionX = position?.left || 0;
        const positionY = position?.top || 0;
        const scrollTop = positionY - bodyRect.top;
        const leftX = positionX + window.pageXOffset + tooltip.caretX;
        const left = bodyRect.width - leftX > 100 ? leftX + 70 : leftX - 30;
        tooltipEl.style.left = left + 'px';
        tooltipEl.style.top = scrollTop + tooltip.caretY - 60 + 'px';
        tooltipEl.style.position = 'absolute';
        tooltipEl.style.opacity = '1';
        tooltipEl.style.borderColor = '#000';
        tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
        tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
        tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
        tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
        tooltipEl.style.pointerEvents = 'none';
      }
    },
  };
  public barChartOptions3: any = {
    legend: {
      display: true,
      position: 'bottom',
      labels: {
        boxWidth: 10,
      },
    },
    animation: {
      animateScale: true,
      animateRotate: true,
      
    },
    maintainAspectRatio:true,
    scales: {
      xAxes: [
        {
          barPercentage: 1,
          categoryPercentage: 1 / 2,
          gridLines: {
            display: false,
          },
          ticks: {
            autoSkip: false,
            maxRotation: 45,
            minRotation: 45,
            callback: function (value: any) {
              return CommonUtil.extractContent('<p title=' + value + '>' + CommonUtil.limitWord(value, 10) + '</p>');
            },
          }
        },
      ],
      yAxes: [
        {
          
          gridLines: {
            borderDash: [8, 4],
          },
          ticks: {
            beginAtZero: true,
          }
        },
      ],
    },
    hover: {
      mode: 'index',
      intersect: false,
    },
    plugins: {
      datalabels: {
        display: false,
      },
      id: 'customHover',
      afterEvent: (chart: any, event: any) => {
        const evt = event.event;

        if (evt.type !== 'mousemove') {
          return;
        }

        const [found, label] = this.findLabel(this.getLabelHitboxes(chart.scales), evt);

      }

    },
    tooltips: {
      enabled: false,
      intersect : false,
      mode:'index',
      position: 'nearest',
      callbacks: {
        title: function (context: any) {  
          const  day = 'Tháng ' + context[0].label;
          return day ;
        }
      },
      
      custom: function (context: any) {
        var tooltipEl = document.getElementById('chartjs-tooltip');
        const tooltip = context

        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjs-tooltip';
          tooltipEl.innerHTML = "<table id='table-tooltip'></table>";

          document.body.appendChild(tooltipEl);
        }

        // Hide if no tooltip
        if (tooltip.opacity === 0) {
          tooltipEl.style.opacity = '0';
          return;
        }
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltip.yAlign) {
          tooltipEl.classList.add(tooltip.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }
        function getBody(bodyItem: any) {
          return bodyItem.lines;
        }
        // Set Text
        if (tooltip.body) {
          var titleLines = tooltip.title || [];
          var bodyLines = tooltip.body.map(getBody);
          var innerHtml = '<thead>';
          titleLines.forEach(function (title: any) {
            innerHtml += '<tr><th>' + title + '</th></tr>';
            innerHtml += '<tr><th></th></tr>';
          });
          innerHtml += '</thead><tbody>';
          bodyLines.forEach(function (body: any, i: any) {
            var colors = tooltip.labelColors[i];
            var style = 'background:' + colors.backgroundColor + '; border-color:' + colors.borderColor;
            style += '; border-color:' + colors.borderColor;
            style += '; border-width: 2px';
            var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
            innerHtml += '<tr><td>' + span + body + '</td></tr>';
          });
          innerHtml += '</tbody>';
          var tableRoot = tooltipEl.querySelector('table');
          if (tableRoot) {
            tableRoot.innerHTML = innerHtml;
          }
        }
        var bodyRect = document.body.getBoundingClientRect();
        const position = document.getElementById('chart3')?.getBoundingClientRect();
        const positionX = position?.left || 0;
        const positionY = position?.top || 0;
        const scrollTop = positionY - bodyRect.top;
        const leftX = positionX + window.pageXOffset + tooltip.caretX;
        const left = bodyRect.width - leftX > 100 ? leftX + 70 : leftX - 30;
        tooltipEl.style.left = left + 'px';
        tooltipEl.style.top = scrollTop + tooltip.caretY - 60 + 'px';
        tooltipEl.style.position = 'absolute';
        tooltipEl.style.opacity = '1';
        tooltipEl.style.borderColor = '#000';
        tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
        tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
        tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
        tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
        tooltipEl.style.pointerEvents = 'none';
      }
    },
  };
  public barChartOptions: any = {
    legend: {
      display: true,
      position: 'top',
      labels: {
        boxWidth: 10,
      },
    },
    animation: {
      animateScale: true,
      animateRotate: true,
    },
    scales: {
      xAxes: [
        {
          barPercentage: 1,
          gridLines: {
            display: false,
          },
          ticks: {
            autoSkip: false,
            maxRotation: 45,
            minRotation: 45,
            callback: function (value: any) {
              return CommonUtil.extractContent('<p title=' + value + '>' + CommonUtil.limitWord(value, 10) + '</p>');
            },
          }
        },
      ],
      yAxes: [
        {
          id: 'A',
          gridLines: {
            borderDash: [8, 4],
          },
          ticks: {
            beginAtZero: true,
          }
        },
        {
          id: 'B',
          position: 'right',
          gridLines: {
            borderDash: [8, 4],
          },
          ticks: {
            beginAtZero: true,
          }
        },
      ],
    },
    hover: {
      mode: 'index',
      intersect: false,
    },
    plugins: {
      datalabels: {
        display: false,
      },
      id: 'customHover',
      afterEvent: (chart: any, event: any) => {
        const evt = event.event;

        if (evt.type !== 'mousemove') {
          return;
        }

        const [found, label] = this.findLabel(this.getLabelHitboxes(chart.scales), evt);


      }

    },
    tooltips: {
      enabled: false,
      intersect : false,
      mode:'index',
      position: 'nearest',
      callbacks: {
        label:
         function (item: any, data: any) {
          if(item.value !=  data.datasets[0].data[item.index]){
            return 'Số lượng: ' + item.value;
          }
          else{
            return 'Tỷ lệ: ' + item.value + '%';
          }
        }
      },
      
      custom: function (context: any) {
        var tooltipEl = document.getElementById('chartjs-tooltip');
        const tooltip = context

        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjs-tooltip';
          tooltipEl.innerHTML = "<table id='table-tooltip'></table>";

          document.body.appendChild(tooltipEl);
        }

        // Hide if no tooltip
        if (tooltip.opacity === 0) {
          tooltipEl.style.opacity = '0';
          return;
        }
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltip.yAlign) {
          tooltipEl.classList.add(tooltip.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }
        function getBody(bodyItem: any) {
          return bodyItem.lines;
        }
        // Set Text
        if (tooltip.body) {
          var titleLines = tooltip.title || [];
          var bodyLines = tooltip.body.map(getBody);
          var innerHtml = '<thead>';
          titleLines.forEach(function (title: any) {
            innerHtml += '<tr><th>' + title + '</th></tr>';
            innerHtml += '<tr><th></th></tr>';
          });
          innerHtml += '</thead><tbody>';
          bodyLines.forEach(function (body: any, i: any) {
            var colors = tooltip.labelColors[i];
            var style = 'background:' + colors.backgroundColor + '; border-color:' + colors.borderColor;
            style += '; border-color:' + colors.borderColor;
            style += '; border-width: 2px';
            var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
            innerHtml += '<tr><td>' + span + body + '</td></tr>';
          });
          innerHtml += '</tbody>';
          var tableRoot = tooltipEl.querySelector('table');
          if (tableRoot) {
            tableRoot.innerHTML = innerHtml;
          }
        }
        var bodyRect = document.body.getBoundingClientRect();
        const position = document.getElementById('chart1')?.getBoundingClientRect();
        const positionX = position?.left || 0;
        const positionY = position?.top || 0;
        const scrollTop = positionY - bodyRect.top;
        const leftX = positionX + window.pageXOffset + tooltip.caretX;
        const left = bodyRect.width - leftX > 100 ? leftX + 70 : leftX - 30;
        tooltipEl.style.left = left + 'px';
        tooltipEl.style.top = scrollTop + tooltip.caretY - 60 + 'px';
        tooltipEl.style.position = 'absolute';
        tooltipEl.style.opacity = '1';
        tooltipEl.style.borderColor = '#000';
        tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
        tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
        tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
        tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
        tooltipEl.style.pointerEvents = 'none';
      }
    },
    responsive: true,
  };
  public barChartOptions4: any = {
    legend: {
      display: true,
      position: 'bottom',
      labels: {
        boxWidth: 10,
      },
    },
    animation: {
      animateScale: true,
      animateRotate: true,
    },
    scales: {
      xAxes: [
        {
          barPercentage: 1,
          categoryPercentage: 1 / 2,
          gridLines: {
            display: false,
          },
          ticks: {
          }
        },
      ],
      yAxes: [
        {
          gridLines: {
            borderDash: [8, 4],
          },
          ticks: {
            beginAtZero: true,
          }
        },
      ],
    },
    hover: {
      mode: 'nearest',
      intersect: true
    },
    plugins: {
      datalabels: {
         display: false,
      }
    },
    layout: {
      padding: {
        top: 32
      }
    },
    tooltips: {
      enabled: false,
      mode: 'index',
      position: 'nearest',
      custom: function (context: any) {
        var tooltipEl = document.getElementById('chartjs-tooltip');
        const tooltip = context

        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjs-tooltip';
          tooltipEl.innerHTML = "<table id='table-tooltip'></table>";

          document.body.appendChild(tooltipEl);
        }

        // Hide if no tooltip
        if (tooltip.opacity === 0) {
          tooltipEl.style.opacity = '0';
          return;
        }
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltip.yAlign) {
          tooltipEl.classList.add(tooltip.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }
        function getBody(bodyItem: any) {
          return bodyItem.lines;
        }
        // Set Text
        if (tooltip.body) {
          var titleLines = tooltip.title || [];
          var bodyLines = tooltip.body.map(getBody);
          var innerHtml = '<thead>';
          titleLines.forEach(function (title: any) {
            innerHtml += '<tr><th>' + title + '</th></tr>';
            innerHtml += '<tr><th></th></tr>';
          });
          innerHtml += '</thead><tbody>';
          bodyLines.forEach(function (body: any, i: any) {
            var colors = tooltip.labelColors[i];
            var style = 'background:' + colors.backgroundColor + '; border-color:' + colors.borderColor;
            style += '; border-color:' + colors.borderColor;
            style += '; border-width: 2px';
            var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
            innerHtml += '<tr><td>' + span + body + '</td></tr>';
          });
          innerHtml += '</tbody>';
          var tableRoot = tooltipEl.querySelector('table');
          if (tableRoot) {
            tableRoot.innerHTML = innerHtml;
          }
        }
        var bodyRect = document.body.getBoundingClientRect();
        const position = document.getElementById('chart4')?.getBoundingClientRect();
        const positionX = position?.left || 0;
        const positionY = position?.top || 0;
        const scrollTop = positionY - bodyRect.top;
        const leftX = positionX + window.pageXOffset + tooltip.caretX;
        const left = bodyRect.width - leftX > 200 ? leftX + 70 : leftX - 30;
        tooltipEl.style.left = left + 'px';
        tooltipEl.style.top = scrollTop + tooltip.caretY - 60 + 'px';
        tooltipEl.style.position = 'absolute';
        tooltipEl.style.opacity = '1';
        tooltipEl.style.borderColor = '#000';
        tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
        tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
        tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
        tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
        tooltipEl.style.pointerEvents = 'none';
      }
    },
    responsive: true,
  };
  public barChartType: ChartType = 'bar';
  public barChartType1: ChartType = 'line';
  public barChartType2: ChartType = 'line';
  public barChartType3: ChartType = 'doughnut';
  public barChartLegend = false;
  public barChartLegend1 = true; // chú thích  màu gì
  public barChartLegend0 = true; 
  public barChartPlugins = [pluginDataLabels];
  public barChartLabels = [];
  public barChartLabels0 = [];
  barChartLabels2: any = [];
  barChartLabels3: any = [];
  barChartLabels4: any = [];
  public barChartColors: Color[] = [
    {
      backgroundColor: []
    }
  ];
  public barChartColors4: Color[] = [
    {
      backgroundColor: []
    }
  ];
  public lineChartColors: Color[] = [
    {
      backgroundColor: '#0060AF',
      borderColor: '#0060AF',
      borderWidth: 1,
    },
  ];
  public lineChartColors3: Color[] = [
    {
      backgroundColor: '#0060AF',
      borderColor: '#0060AF',
      borderWidth: 1,
    },
  ];
  public doughnutChartColors: Color[] = [
    {
      backgroundColor: ['#299EFD', '#F9D67C', '#AEDBFF', '#ED1C24'],
    }
  ];
  public barChartData = [
    { data: [], label: '', yAxisID:'' },
  ]
  public barChartData0 = [
    { data: [], label: '' },
  ]
  public barChartData2 = [
    { data: [], label: '', type: 'line', fill: false, lineTension: 0, },
  ]
  public barChartData3 = [
    { data: [], label: '', type: 'line', fill: false, lineTension: 0, },
  ]
  public barChartData4 = [
    { data: [], label: '' },
  ]
  statusTicket: any;
  listProduct: any = [];
  listDepartment: any = [];
  product_id: any = [];
  department_id: any = [];
  department_id_ticket: any = [];
  product_id_sticket: any =[];
  product_id_ticketC: any = [];
  department_id_ticketC: any = [];
  department_id_priority: any = [];
  product_id_priority: any =[];
  month: any;
  permision=false;
  start_date: any;
  end_date: any;
  listProductSelect: any = [];
  width=170;
  width1=100;
  checkMonth:any;
  ischeckTime: boolean = false;
  constructor(
    private dashboardService: DashboardService,
    private productService: ProductService,
    private departmentService: DepartmentService,
    private toastService: ToastService,
    private localStorage: LocalStorageService,
  ) { }

  clearDate(event: any){
    event.stopPropagation();
    this.start_date= '';
    this.end_date =''
    this.loadData();
  }
  clearQuy(){
    this.quy = '';
    this.active =''
    this.loadData();
  }
  prevYear() {
    this.today--;
  }
  nextYear() {
    this.today++;
  }
  chooseQuy(quy: number) {
    this.visible = false;
    this.quy = quy;
    this.year = this.today;
    this.active = quy;
    this.month = '';
    this.yearFillter = '',
    this.checkQueryDate = true
    const params = {
      quy:this.quy,
      year: this.year,
      active1: 2,
      quy1: 'Quý'
    }
   const quyLocalsotorage = CommonUtil.trim(params);
    this.localStorage.store('quy', { params: quyLocalsotorage });
    location.reload();
  }
  chooseQuy1(active:any ,quy1: string) {
    this.localStorage.clear(LOCAL_STORAGE.QUY)
    this.visible = false;
    this.quy1 = quy1;
    this.year =  new Date().getFullYear();
    this.active1 = active;
    this.month = '';
    this.yearFillter = '';
    const date =new Date()
    if(active== 4){
      this.quy = '';
      this.selected.startDate = moment().startOf('year');
      this.selected.endDate =  moment().endOf('year');
    }
    else if(active==1){
      this.yearFillter = new Date();
      this.selected.startDate = null;
      this.selected.endDate =  null; 
      this.quy = '';
    }
   else if(active ==3){
    this.month = new Date().setFullYear(date.getFullYear(), date.getMonth());
    this.selected.startDate = null;
    this.selected.endDate =  null; 
    }
    else if(active == 2){
      const month_select = new Date().getMonth();
      this.selected.startDate = null;
      this.selected.endDate =  null; 
      if(month_select < 4){
          this.quy = 1
      }
      else if(month_select < 7 && month_select > 3 ){
        this.quy = 2
      }
      else if(month_select < 10 && month_select > 6 ){
        this.quy =3
      }
      else if(month_select <= 12 && month_select > 9 ){
        this.quy = 4
      }
    }

      const params = {
        start_time:this.selected?.startDate?.toISOString(),
        end_time: this.selected?.endDate?.toISOString(),
        active1:this.active1,
        quy1:quy1,
        month: this.month,
        quy: this.quy,
        year: this.year,
      }
      const quyLocalsotorage = CommonUtil.trim(params);
      this.localStorage.store('quy', { params: quyLocalsotorage });
      location.reload();
    
  }
  calendarLocale = {
    format: 'DD/MM/YYYY',
    daysOfWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    monthNames: ['Th1', 'Th2', 'Th3', 'Th4', 'Th5', 'Th6', 'Th7', 'Th7', 'Th9', 'Th10', 'Th11', 'Th12'],
    firstDay: 1
  };
  ranges = {
    'Hôm nay': [moment(), moment()],
    'Tuần này': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
    'Năm này':  [moment().startOf('year'), moment().endOf('year')],
    '1 tuần trước': [moment().subtract(6, 'days'), moment()],
    '1 tháng trước': [moment().subtract(31, 'days'), moment()],
    '1 năm trước': [moment().subtract(1, 'year'), moment()],
  };
  ngOnInit() {
    this.yearFillter = new Date();
    const profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
    if (profile) {
      if(profile.is_super_admin){
        this.permision=true
      }
    const chartInstance = Chart.instances;
    this.loadData()
    this.getListProduct();
    this.getListDepartment();
    }
  }
  getListProduct() {
    this.productService.getAlltree().subscribe((res: any)=>{
      this.listProduct = res.body?.data?.sort((a: any, b: any) => a.name.localeCompare(b.name));
    });
  }
  getListDepartment() {
    this.departmentService.getAlltree().subscribe(res=>{
      this.listDepartment = res.body;
    });
  }
  loadData() {
    const queryParams =   this.localStorage.retrieve(LOCAL_STORAGE.QUY);
    this.quy =  queryParams?.params?.quy ? queryParams?.params?.quy : null;
    this.year =  queryParams?.params?.year ? queryParams?.params?.year : null; 
     if(queryParams){
      this.active1 = queryParams?.params?.active1;
      this.quy1 = queryParams?.params?.quy1;
      this.today = this.year
      this.active = this.quy 
      if( queryParams?.params?.month){
        this.month = new Date(queryParams?.params?.month)
      }
      if(queryParams?.params?.start_time){
        this.selected.startDate =queryParams?.params?.start_time? new Date(queryParams?.params?.start_time) : null;
        this.selected.endDate = queryParams?.params?.start_time ? new Date(queryParams?.params?.end_time) : null
       }
     } 
    this.listStatus();
    this.getDashboardCountProduct();
    this.getDashboardCountTicket();
    this.getDashboardCountTicket1();
    this.getDashboardTicketSLAPriority()
  }
  listStatus() {
    const param = {
      quarter: this.quy && this.year ? `${this.quy}-${this.year}` : '',
      month: this.month ? new Date(this.month).toISOString() : '',
      from_date:  this.selected?.startDate ? this.selected?.startDate.toISOString():'' ,
      to_date:  this.selected?.startDate ? this.selected?.endDate.toISOString() :'',
      year: this.yearFillter ? new Date(this.yearFillter).getFullYear() : '',
    }
    this.dashboardService.getStastus(param).subscribe((res: any) => {
      this.statusTicket = res.body?.data;
    })
  }
  ngModelChange(event:any){
    if(this.selected?.startDate && event.startDate){
      const params = {
        start_time:this.selected?.startDate.toISOString(),
        end_time: this.selected?.endDate.toISOString(),
        active1: 4,
        quy1: 'Thời gian'
      }
     const quyLocalsotorage = CommonUtil.trim(params);
      this.localStorage.store('quy', { params: quyLocalsotorage });
    }
    if(event.type=='change'){
      this.typeInput = true;
      const date = event.target.value ;
      const vitri: number = date.search("-");
      const SD =  date.slice(0,Number(vitri-1));
      const pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
      const ED =  date.slice(Number(vitri+2),date.length);
      const params = {
        start_time:new Date(SD.replace(pattern,'$3-$2-$1')),
        end_time: new Date(ED.replace(pattern,'$3-$2-$1')),
        active1: 4,
        quy1: 'Thời gian'
      }
     const quyLocalsotorage = CommonUtil.trim(params);
      this.localStorage.store('quy', { params: quyLocalsotorage });
      location.reload();
    }
    if(event.startDate){
      location.reload();
    }
  }

onChangeProduct($event: any) {
  this.product_id = $event
  this.getDashboardCountProduct();
}
onChangeDepartment($event: any) {
  this.department_id = $event
  this.getDashboardCountProduct();
}
onChangeDepartmentT($event: any) {
  this.department_id_ticket = $event
  this.getDashboardCountTicket();
}
onChangeProductT($event: any) {
  this.product_id_sticket = $event
  this.getDashboardCountTicket();
}
onChangeDepartmentTC($event: any) {
  this.department_id_ticketC = $event
  this.getDashboardCountTicket1();
}
onChangeProductTC($event: any) {
  this.product_id_ticketC = $event
  this.getDashboardCountTicket1();
}
onChangeDepartmentPriority($event: any) {
  this.department_id_priority = $event
  this.getDashboardTicketSLAPriority();
}
onChangeProductPriority($event: any) {
  this.product_id_priority = $event;
  this.getDashboardTicketSLAPriority();
}
getDashboardCountTicket() {
    const param = {
      type:'executed',
      department_id: this.department_id_ticket.toString(),
      product_id: this.product_id_sticket.toString(),
      year: this.yearFillter ? new Date(this.yearFillter).getFullYear() : '',
      quarter: this.quy && this.year ? `${this.quy}-${this.year}` : '',
      month: this.month ? new Date(this.month).toISOString() : '',
      from_date:  this.selected?.startDate ? this.selected?.startDate.toISOString():'' ,
      to_date:  this.selected?.startDate ? this.selected?.endDate.toISOString() :'',
    }
    this.dashboardService.getDashboardCountTicketByYear(param).subscribe((res: any) => {
      let dataSet = res.body?.data;
      let label = [];
      var data: any = [];     
      for (let item in dataSet) {
        label.push(item)
        data.push(dataSet[item])
      }
      this.count_product1 = data.length
      if(data?.length>89 ){
        this.width1=280;
      }
      else if(data?.length>33){
        this.width1=190 ;
      }
      else  if(data?.length<33){
        this.width1=100
      }
      this.barChartLabels2 = label
      this.barChartData2 = [{ data: data, label: '', type: 'line', fill: false, lineTension: 0, }];
    })
  }
  getDashboardCountTicket1() {
    const param = {
      department_id: this.department_id_ticketC.toString(),
      product_id: this.product_id_ticketC.toString(),
      quarter: this.quy && this.year ? `${this.quy}-${this.year}` : '',
      year: this.yearFillter ? new Date(this.yearFillter).getFullYear() : '',
      month: this.month ? new Date(this.month).toISOString() : '',
      from_date:  this.selected?.startDate ? this.selected?.startDate.toISOString():'' ,
      to_date:  this.selected?.startDate ? this.selected?.endDate.toISOString() :'',
    }
    this.dashboardService.getDashboardCountTicketByYear(param).subscribe((res: any) => {
      let dataSet = res.body?.data;
      let label = [];
      var data: any = [];
      for (let item in dataSet) {
        label.push(item)
        data.push(dataSet[item])
      }
      this.barChartLabels3 = label
      this.barChartData3= [{ data: data, label: '', type: 'line', fill: false, lineTension: 0, }];
    })
  }
  getDashboardTicketSLAPriority() {
    const param = {
      department_id: this.department_id_priority.toString(),
      year: this.yearFillter ? new Date(this.yearFillter).getFullYear() : '',
      product_id: this.product_id_priority.toString(),
      quarter: this.quy && this.year ? `${this.quy}-${this.year}` : '',
      month: this.month ? new Date(this.month).toISOString() : '',
      from_date:  this.selected?.startDate ? this.selected?.startDate.toISOString():'' ,
      to_date:  this.selected?.startDate ? this.selected?.endDate.toISOString() :'',
    }
    this.dashboardService.getDashboardTicketSLAPriority(param).subscribe((res: any) => {
      let dataSet = res.body?.data
      this.barChartLabels4 = dataSet?.map((item: any) => item?.priority_name);
      const data = dataSet?.map((item: any) => item?.total_tickets);
      const data1 = dataSet?.map((item: any) => item?.total_tickets_reached_sla);
      const data2 = dataSet?.map((item: any) => item?.total_tickets_violate_sla);
      const color = []; 
      const color1 = [];
      const color2 = [];
      for( let i = 0 ; i < data.length; i++){
        color.push('#004BFF')
      }
      for( let i = 0 ; i < data.length; i++){
        color1.push('#FBB454')
      }
      for( let i = 0 ; i < data.length; i++){
        color2.push('#F637EC')
      }
      this.barChartColors4 = [ { backgroundColor: color },  { backgroundColor: color1 },{   backgroundColor: color2 } ]
      this.barChartData4 = [
        { data: data, label: 'Số lượng Ticket'  },
        { data: data1, label:  this.permision ? 'Số lượng ticket đạt SLA' : 'Số lượng ticket đạt KPI'},
        { data: data2, label:  this.permision ? 'Số lượng ticket vi phạm SLA': 'Số lượng ticket vi phạm KPI' }
      ]
    })
  }

  getDashboardCountProduct() {
    const param = {
      year: this.yearFillter ? new Date(this.yearFillter).getFullYear() : '',
      product_id: this.product_id.toString(),
      quarter: this.quy && this.year ? `${this.quy}-${this.year}` : '',
      month: this.month ? new Date(this.month).toISOString() : '',
      from_date:  this.selected?.startDate ? this.selected?.startDate.toISOString():'' ,
      to_date:  this.selected?.startDate ? this.selected?.endDate.toISOString() :'',
      department_id: this.department_id.toString()
    }
    this.dashboardService.getDashboardCountProduct(param, true).subscribe((res: any) => {
      let dataSet = res.body?.data.filter((item:any)=>item?.quantity>0);
      const data = dataSet?.map((item: any) =>  item?.quantity );
      debugger
      this.count_product = dataSet?.map((item: any) =>  item?.quantity );
      if(data?.length>15){
        this.width=170
      }else if (data?.length<16){
        this.width=100
      }
      this.barChartLabels0 = dataSet?.map((item: any) => item?.product_name);
      const data1 = dataSet?.map((item: any) =>  item?.ticket_reach_sla );
      const data2 = dataSet?.map((item: any) =>  item?.ticket_violate_sla );
      const color = []; 
      const color1 = [];
      const color2 = [];
      for( let i = 0 ; i < 100; i++){
        color.push('#195EFF')
      }
      for( let i = 0 ; i < 100; i++){
        color1.push('#20C997')
      }
      for( let i = 0 ; i < 100; i++){
        color2.push('#F637EC')
      }
      this.barChartColors = [ { backgroundColor: color },  { backgroundColor: color1 },{   backgroundColor: color2 }]
      this.barChartData0 = [
        { data: data, label: 'Số lượng ticket ' },
        { data: data1, label: this.permision ? 'Số lượng ticket đạt SLA ' :  'Số lượng ticket đạt KPI  '},
        { data: data2, label: this.permision ? 'Số lượng ticket quá hạn SLA ' : 'Số lượng ticket quá hạn KPI '}
      ]
    })
  }
  onSearchMonth($event: any) {
    this.localStorage.clear(LOCAL_STORAGE.QUY)
    this.month =  new Date( $event);
    this.start_date='',
    this.end_date='',
    this.quy = '';
    this.year = '';
    this.yearFillter ='';
    this.loadData();
  }
  onSearchYear($event: any) {
    this.start_date='',
    this.end_date='',
    this.quy = '';
    this.year = '';
    this.loadData();
  }
  onSearchDate($event: any) {
    this.yearFillter ='';
    this.quy = '';
    this.month = '';
    this.year = '';
    this.loadData();
  }
}
function findLabel(labels: any, evt: any) {
  let found = false;
  let res = null;
  labels.forEach((l: any) => {
    l.labels.forEach((label: any) => {
      if (evt.x > label.x && evt.x < label.x2 && evt.y > label.y && evt.y < label.y2) {
        res = label.label;
        found = true;
      }
    });
  });
  return [found, res];
};
