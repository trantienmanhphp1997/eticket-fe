import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { WelcomeRoutingModule } from './welcome-routing.module';
import { WelcomeComponent } from './welcome.component';




@NgModule({
  declarations: [WelcomeComponent],
  imports: [
    CommonModule,
    SharedModule,
    WelcomeRoutingModule,
    NgxDaterangepickerMd.forRoot()
  ],
  exports: [WelcomeComponent]
})
export class WelcomeModule { }
