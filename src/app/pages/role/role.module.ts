import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { RoleCreateComponent } from './role-create/role-create.component';
import { RoleRoutingModule } from './role-routing.module';
import { RoleComponent } from './role.component';
import { StatusRoleComponent } from './status-role/status.role.component';

@NgModule({
  declarations: [RoleComponent, RoleCreateComponent,StatusRoleComponent],
  imports: [
    CommonModule,
    SharedModule,
    RoleRoutingModule
  ],
  exports: [RoleComponent]
})
export class RoleModule { }