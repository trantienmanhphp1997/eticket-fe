import { Component, OnInit, Input } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastService } from '@shared/service/helpers/toast.service';
import { RoleService } from '@shared/service/role.service';

@Component({
  selector: 'app-status-role',
  templateUrl: './status.role.component.html',
  styleUrls: ['./status.role.component.css']
})

export class StatusRoleComponent implements OnInit {
  @Input() data: any = [];

  statusname = '';
  isClose = false;
  constructor(
    private modalRef: NzModalRef,
    private toast: ToastService,
    private roleService: RoleService
  ) { }
  ngOnInit() {
    if (this.data.status == true) {
      this.statusname = 'khóa '
      
      this.isClose = !this.isClose;
    }
    else {
      this.statusname = 'mở';
    }

  }
  onLock(status: any) {
    const statusUser = {
      status: status
    }
    // this.modalService.update(this.data.id, statusUser, true).subscribe(res => {
    //   this.toast.success(`Thay đổi trạng thái thành công`);
    //   this.modalRef.close({
    //     success: true,
    //   });
    // })
    this.roleService.update(statusUser, this.data.id).subscribe(res => {
      if (res?.body?.data) {
        this.toast.success(`Thay đổi trạng thái thành công`);
        this.modalRef.close({
          success: true,
        });
      }
    })
  }

  onCancel() {
    this.modalRef.close({
      success: false,
      value: null,
    });
  }
}