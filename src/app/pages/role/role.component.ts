import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { ROLE_LOCK, ROLE_UNLOCK } from '@shared/constants/role.constant';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from '@shared/service/helpers/toast.service';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { AUTH } from '@shared/constants/auth.constant';
import { RoleService } from '@shared/service/role.service';
import CommonUtil from '@shared/utils/common-utils';
import { RoleCreateComponent } from './role-create/role-create.component';
import { ACTION } from '@shared/constants/common.constant';
import { ModalComponent } from '@shared/components/modal/modal.component';
import { StatusRoleComponent } from './status-role/status.role.component';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

  isCallFirstRequest: boolean = true;
  auth:any = AUTH;
  //form 
  searchForm = this.fb.group({
    keyword: [null]
  });
  roleSearch = {
    pageIndex: PAGINATION.PAGE_DEFAULT,
    pageSize: PAGINATION.SIZE_DEFAULT,
    pageSizeOptions: PAGINATION.OPTIONS,
    orderBy: '',
    sortBy: '',
    search: null,
  }
  //checked items
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly any[] = [];
  setOfCheckedId = new Set<number>();
  checked = false;

  roleList: any;
  total: any = 0;
  status: any;
  roleLock = ROLE_LOCK;
  commonUtil: any = CommonUtil
  constructor(
    private fb: FormBuilder,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private toastService: ToastService,
    private roleService: RoleService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  // load data
  loadData() {
    const params = {
      pageIndex: this.roleSearch.pageIndex,
      pageSize: this.roleSearch.pageSize,
      search: this.searchForm.value.keyword,
      orderBy: this.roleSearch.orderBy,
      sortBy: this.roleSearch.sortBy,
      includes: 'permissions',

    }
    this.roleService.getRole(params, true).subscribe(
      (res) => {
        this.roleList = res.body?.data?.map((dept: any) => {
          return {
            ...dept,
            disabled: dept.check_can_edit ===0,
          }
        })
        this.total = res.body?.meta?.pagination.total;
      }
    );
  }

  // Events
  onSearch($event: any) {
    if ($event) {
      this.roleSearch.pageIndex = 1;
      this.loadData();
    }
  }

  onReset() {
    this.searchForm.patchValue({
      keyword: null,
    })
    this.loadData();
  }
  onCreate() {
    const base = CommonUtil.modalBase(RoleCreateComponent, { action: ACTION.CREATE, }, '45%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }
  onUpdate(data: any) {
    const base = CommonUtil.modalBase(RoleCreateComponent, { action: ACTION.UPDATE, selectedRole: data, isUpdate: true }, '45%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData()
      }
    });
  }
  onDelete(data: any) {
    if (data.check_can_edit==1){
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc muốn xóa vai trò <b>${data?.display_name}</b> không?`,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'25%',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);

    
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.roleService.delete(data?.id, true).subscribe(res => {
          if (res) {
            this.toastService.success(`model.role.success.delete`);
            // this.getAllDepartments()
            modal.close();
            this.loadData()
          };
        })
      }else{
        modal.close();
      }
    });
  }
  }
  onDetail(data: any) {
    const base = CommonUtil.modalBase(RoleCreateComponent, { action: ACTION.VIEW, selectedRole: data, isUpdate: true }, '45%');
    const modal: NzModalRef = this.modalService.create(base);
  }
  onMultiDelete(data: any) {
    const form = CommonUtil.modalBase(ModalComponent,{
      title:'Xác nhận xóa',
      content:`Bạn có chắc muốn xóa vai trò đã chọn không?`,
      okText:'Xóa',
      callback: () => {
        return {
          success: true,
        };
      },
    },'25%',
     false,
    false,
    true,{ top: '20px' }, true
    );
    const modal: NzModalRef = this.modalService.create(form);
    let roleIds =  data instanceof Set ? Array.from(data) : [data.id] ;
    
    modal.componentInstance.emitter.subscribe((result:any) => {
      if (result?.success) {
        this.roleService.multiDelete(roleIds, true).subscribe(res => {
          if (res) {
            this.toastService.success(`model.role.success.delete`);
            this.setOfCheckedId =new Set()
            modal.close();
            this.loadData()
          };
        })
      }else{
        modal.close();
      }
    });
  }
  // onLock(data: any) {
  //   const form = CommonUtil.modalConfirm(
  //     this.translateService,
  //     'model.role.titleLock',
  //     'model.role.lockContent',
  //     { name: data.name || '' },
  //     () => {
  //       return {
  //         success: true,
  //       };
  //     },
  //     () => {
  //       return {
  //         success: false,
  //       };
  //     },
  //     'model.role.lock'
  //   );
  //   const modal: NzModalRef = this.modalService.confirm(form);
  //   modal.afterClose.subscribe(result => {
  //     if (result?.success) {
  //       this.status = ROLE_LOCK;
  //       this.update(data?.id);
  //     }
  //   })

  // }

  // onUnlock(data: any) {
  //   const form = CommonUtil.modalConfirm(
  //     this.translateService,
  //     'model.role.titleUnLock',
  //     'model.role.unlockContent',
  //     { name: data.name || '' },
  //     () => {
  //       return {
  //         success: true,
  //       };
  //     },
  //     () => {
  //       return {
  //         success: false,
  //       };
  //     },
  //     'model.role.unlock'
  //   );
  //   const modal: NzModalRef = this.modalService.confirm(form);
  //   modal.afterClose.subscribe(result => {
  //     if (result?.success) {
  //       this.status = ROLE_UNLOCK;
  //       this.update(data?.id);
  //     }
  //   })
  // }
  onUnlock(data:any){
    const base = CommonUtil.modalBase(StatusRoleComponent, { data }, '30%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData();
      }
    });
  }
  onLock(data:any){
    const base = CommonUtil.modalBase(StatusRoleComponent, { data }, '30%');
    const modal: NzModalRef = this.modalService.create(base);
    modal.afterClose.subscribe(result => {
      if (result && result?.success) {
        this.loadData();
      }
    });
  }
  update(id: any) {
    const role = {
      status: this.status
    }
    this.roleService.update(role, id).subscribe(res => {
      if (res?.body?.data) {
        this.loadData();
        this.toastService.success(`model.role.success.${this.status == ROLE_LOCK ? 'lock' : 'unlock'}`);
      }
    }, err => {
      this.toastService.error(err);
    })
  }
  //check item
  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }
  onAllChecked(checked: boolean): void {
    this.indeterminate = false;
    this.roleList.forEach((item: any) => {
      if (!item.disabled) {
        this.updateCheckedSet(item.id, checked)
      }
    });
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange(listOfCurrentPageData: readonly any[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }
  onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    this.roleSearch.sortBy = params.sort.filter((e: any) => e.value !== null).map((e: any) => e.value?.slice(0, -3)).toString();
    this.roleSearch.orderBy = params.sort.filter((e: any) => e.key !== null && e.key !== undefined && e.value !== null).map((e: any) => e.key).toString();
    this.loadData();
  }
  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }
  onQuerySearch(params: any): void {
    const { pageIndex, pageSize } = params;
    this.roleSearch.pageIndex = pageIndex;
    this.roleSearch.pageSize = pageSize;
    this.loadData();
  }
  exportData(){
    this.roleService.export()
  }
}
