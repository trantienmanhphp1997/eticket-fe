import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ACTION } from '@shared/constants/common.constant';
import { LENGTH_VALIDATOR, VALIDATORS } from '@shared/constants/validators.constant';
import { CommonService } from '@shared/service/common.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { PermissionService } from '@shared/service/permission.service';
import { RoleService } from '@shared/service/role.service';
import CommonUtil from '@shared/utils/common-utils';
import { AUTH } from '@shared/constants/auth.constant';
import { CustomInputValidator } from '@shared/validators/custom-input.validator';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzTabSetComponent } from 'ng-zorro-antd/tabs';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
    selector: 'app-role-create',
    templateUrl: './role-create.component.html',
    styleUrls: ['./role-create.component.scss']
})
export class RoleCreateComponent implements OnInit {

    @Input() isUpdate: boolean = false;
    @Input() action: any = ACTION.CREATE
    @Input() selectedRole: any = [];
    @ViewChild(NzTabSetComponent, { static: true })
    private tabs!: NzTabSetComponent;
    groupName: any[] = [];
    count = 0;
     isLoading = false
    allChecked: any;
    indeterminate: any;
    viewEvent = ACTION.VIEW.event;
    auth:any = AUTH;
    role: any;
    actionNow=ACTION.CREATE;
    actionToShowData = [ACTION.UPDATE.event, ACTION.VIEW.event];
    listPermissionChecked: any;
    listPermission: any;
    setOfCheckedId = new Set<number>();
    lengthValidator = LENGTH_VALIDATOR;

    checkedIds: any[] = [];
    index = 0;
    //form 
    roleForm: FormGroup = new FormGroup({});
    constructor(
        private fb: FormBuilder,
        private toastService: ToastService,
        private modalRef: NzModalRef,
        private permissionService: PermissionService,
        private roleService: RoleService,
        private commonService: CommonService,
    ) { }
    ngOnInit() {
        this.actionNow=this.action;
        this.loadData()
    }
    initForm(): void {
        this.roleForm = this.fb.group({
            display_name: [{ value: this.actionToShowData.includes(this.actionNow.event) ? this.selectedRole.display_name : '', disabled: this.actionNow.event === ACTION.VIEW.event ? true : false }, [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.NAME_MAX_LENGTH.MAX), CustomInputValidator.isName]],
            name: [{ value: this.actionToShowData.includes(this.actionNow.event) ? this.selectedRole.name : '', disabled: this.isUpdate ? true : false },
            {
                validators: [Validators.required, Validators.maxLength(LENGTH_VALIDATOR.CODECUS_MAX_LENGTH.MAX), CustomInputValidator.notBlankValidator, Validators.pattern(VALIDATORS.CODE)],
                asyncValidators: [CustomInputValidator.hasExited('roles', 'name', this.commonService, this.selectedRole?.name)], updateOn: 'blur'
            }],
            description: [{ value: this.actionToShowData.includes(this.actionNow.event) ? this.selectedRole.description : '', disabled: this.actionNow.event === ACTION.VIEW.event ? true : false }, [Validators.maxLength(LENGTH_VALIDATOR.DES_MAX_LENGTH.MAX)]],
        });
        if (this.isUpdate) {
            this.listPermissionChecked = this.selectedRole?.permissions?.data;
            this.listPermissionChecked.forEach((item: any) => {
                this.checkedIds.push(item?.id);
            })
        }

    }
    loadData() {
        this.initForm();
        this.getListPermission();
    }
    getListPermission() {
        this.permissionService.findAll().subscribe(res => {
            this.listPermission = res?.body;
            for (const property in this.listPermission) {
                this.groupName.push(property);
                this.listPermission[property].forEach((e: any, i: any) => {
                    this.listPermission[property][i] = { label: e.display_name, value: e.id, checked: this.checkedIds.includes(e?.id) }
                    if (this.checkedIds.includes(e?.id)) {
                        this.setOfCheckedId.add(e?.id);
                    }
                });
            }
            this.onLabelChecked();
            if (this.isUpdate) {
                this.setCheckedStatus();
            }
        }, err => {
            this.toastService.error(err)
        })
    }
    assignPermission() {
        let permissionsIds = Array.from(this.setOfCheckedId);
        const params = {
            role_id: this.role?.id,
            permissions_ids: permissionsIds
        } 
            this.permissionService.assignPermissionToRole(params).subscribe(res => {
            })
    }


    //Event
    onSubmit() {
        if (this.roleForm.invalid) {
            this.tabs.nzSelectedIndex = 0;
            CommonUtil.markFormGroupTouched(this.roleForm);
            return;
        }
        this.isLoading = true
        this.count += 1
        if(this.count == 1){
        const role = {
            ...this.roleForm.value
        }
        const param = CommonUtil.trim(role);
        const eventService = this.isUpdate ? this.roleService.update(param, this.selectedRole?.id) : this.roleService.create(param);
        eventService.subscribe(res => {
            if (res?.body?.data) {
                this.isLoading = false
                this.role = res?.body?.data;
                this.assignPermission();
                this.toastService.success(`model.role.success.${this.isUpdate ? 'update' : 'create'}`);
                this.modalRef.close({
                    success: true,
                    value: param,
                });
            }
        }, err => {
            this.toastService.error(err)
        })
    }
    }

    onCancel() {
        this.modalRef.close({
            success: false,
            value: null,
        });
    }
    updateAllChecked(property: any): void {
        this.indeterminate[property] = false;
        this.listPermission[property].forEach((item: any) => {
            this.updateCheckedSet(item.value, this.allChecked[property]);
        });
        if (this.allChecked[property]) {
            this.listPermission[property] = this.listPermission[property].map((item: any) => ({
                ...item,
                checked: true
            }));
        } else {
            this.listPermission[property] = this.listPermission[property].map((item: any) => ({
                ...item,
                checked: false
            }));
        }
    }
    onLabelChecked() {
        const checkedArray: any[] = [];
        const indeterminateArray: any[] = [];
        this.groupName.forEach((item: any) => {
            checkedArray[item] = false;
            indeterminateArray[item] = false;
        }
        )
        this.allChecked = JSON.parse(JSON.stringify(Object.assign({}, checkedArray)));
        this.indeterminate = JSON.parse(JSON.stringify(Object.assign({}, indeterminateArray)));

    }

    updateCheckedSet(id: any, checked: boolean): void {
        if (checked) {
            this.setOfCheckedId.add(id);
        } else {
            this.setOfCheckedId.delete(id);
        }
    }
    updateSingleChecked(property: any): void {

        this.listPermission[property].forEach((item: any) => {
            this.updateCheckedSet(item.value, item.checked);
        });
        this.refreshCheckedStatus(property);
    }
    refreshCheckedStatus(property: any) {
        if (this.listPermission[property].every((item: any) => !item.checked)) {
            this.allChecked[property] = false;
            this.indeterminate[property] = false;
        } else if (this.listPermission[property].every((item: any) => item.checked)) {
            this.allChecked[property] = true;
            this.indeterminate[property] = false;
        } else {
            this.indeterminate[property] = true;
        }
    }
    setCheckedStatus() {
        this.groupName.forEach((item: any) => {
            this.refreshCheckedStatus(item);
        })
    }
    getClassInvalid(name: string) {
        if (this.roleForm.get(name)?.errors) {
            return 'invalid';
        }
        return '';
    }
    onUpdate(){
        this.actionNow=ACTION.UPDATE;
        this.initForm();
    }
}