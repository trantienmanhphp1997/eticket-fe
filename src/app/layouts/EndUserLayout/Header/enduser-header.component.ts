import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AUTH_PATH } from '@shared/constants/base.constant';
import { AuthService } from '@shared/service/auth/auth.service';
import { LoadingService } from '@shared/service/helpers/loading.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import CommonUtil from '@shared/utils/common-utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { environment } from '@env/environment';
import { UserService } from '@shared/service/user.service';
import { ChangePasswordProfileComponent } from 'src/app/pages/profile/change-password/change-password.component';
import { interval, Subject } from 'rxjs';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { NotificationService } from '@shared/service/notification.service';
import { startWith, switchMap, repeatWhen, takeUntil } from 'rxjs/operators';
import { NOTIFI, WAITING } from '@shared/constants/notification.constant';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { LocalStorageService } from 'ngx-webstorage';
import { AngularFireMessaging } from '@angular/fire/messaging';

@Component({
    selector: 'app-enduser-header',
    templateUrl: './enduser-header.component.html',
    styleUrls: ['./enduser-header.component.css']
})

export class EnduserHeaderComponent implements OnInit {

    @Input() currentUser: any
    checkAdmin: any;
    commonUtil: any = CommonUtil
    imgFile: any;
    urlgetway = environment.fileDomain
    is_user_sync = environment.is_user_sync
    data: any;
    visible = false;
    notifiList: any = [];
    msgInput: string = 'lorem ipsum';
    totalNotifi = 0;
    private readonly _stop = new Subject<void>();
    private readonly _start = new Subject<void>();
    isLoading = false;
    limit = PAGINATION.SIZE_DEFAULT;
    total = 0;
    dataProfile: any;
    scrollNotify = new Subject();
    visibleNotify = false;
    pageIndex = PAGINATION.PAGE_DEFAULT;
    constructor(
        public loadingService: LoadingService,
        private authService: AuthService,
        private router: Router,
        private toast: ToastService,
        private modalService: NzModalService,
        private localStorage: LocalStorageService,
        private noticationService: NotificationService,
        private afMessaging: AngularFireMessaging,
        private $localStorage: LocalStorageService,
    ) { }
    ngOnInit(): void {

        this.imgFile = this.currentUser?.avatarProfile?.data?.url;
        this.checkAdmin = this.currentUser.is_admin;
        this.scrollNotify.pipe(
            debounceTime(100),
            distinctUntilChanged())
            .subscribe(value => {
                this.onScroll(value);
            });
        const notification = this.localStorage.retrieve(LOCAL_STORAGE.NOTICE)
        this.totalNotifi = notification?.unread || 0;
        this.afMessaging.messages.subscribe(
            (payload: any) => {
                if (this.visibleNotify) {
                    this.pageIndex = PAGINATION.PAGE_DEFAULT
                    this.getNotification()
                } else {
                    this.total++;
                    this.totalNotifi++
                    this.localStorage.store(LOCAL_STORAGE.NOTICE, { unread: this.totalNotifi });
                }
            });
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        // this.getDataUser();
    }
    // getDataUser() {
    //     this.userService.find(this.currentUser.id, true).subscribe((res) => {
    //         this.dataProfile = res.body?.data;
    //         this.imgFile = this.dataProfile?.avatarProfile?.data?.url;
    //     });
    // }
    onCreateTicket() {
        this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}/${ROUTER_UTILS.endUserTicket.create}`]);
    }
    onPage() {
        this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}`]);
    }
    logout(): void {
        this.authService.clear();
        this.router.navigate([AUTH_PATH]);
        this.toast.success('model.logout.success.authenticate');
        // this.authService.logout().subscribe((response: any) => {
        //   if (response.code === STATUS.SUCCESS_200) {
        //     this.authService.clear();
        //     this.router.navigate(['/authentication/login']);
        //     this.toast.success('model.logout.success.authenticate');
        //   }
        // });
    }
    // getDataProfile(){
    //     this.authService.myProfile().subscribe(res => {
    //         this.currentUser = res?.body?.data.is_admin;
    //     });
    // }
    toAdmin() {
        const profile = this.$localStorage.retrieve(LOCAL_STORAGE.PROFILE);
            switch(  profile.permission_group_code[0]){
                case "access-dashboard":
                    this.router.navigate([`admin/${ROUTER_UTILS.base.dashboard}`]);
                    break;
                case "list-ticket":
                    this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}`]);
                    break;
                case "list-product":
                    this.router.navigate([`admin/${ROUTER_UTILS.serviceProducts.root}`]);
                    break;
                case "list-departmen":
                    this.router.navigate([`admin/${ROUTER_UTILS.organization.root}`]);
                     break;
                 case "list-customer":
                    this.router.navigate([`admin/${ROUTER_UTILS.customer.root}`]);
                    break;
                case "list-users":
                    this.router.navigate([`admin/${ROUTER_UTILS.user.root}`]);
                    break;
                default:
                    this.router.navigate([`${ROUTER_UTILS.base.home}`]);
            }
    }
    onUpdateProfile() {
        this.router.navigate([`${ROUTER_UTILS.endUserProfile.root}/${ROUTER_UTILS.endUserProfile.detail}/${this.currentUser.id}`]);

    }
    onChangePassword() {
        const base = CommonUtil.modalBase(ChangePasswordProfileComponent, { profileId: this.currentUser.id }, '35%');
        const modal: NzModalRef = this.modalService.create(base);
        modal.afterClose.subscribe(result => {
        });
    }
    getNotification() {
        if (this.pageIndex == PAGINATION.PAGE_DEFAULT) {
            this.notifiList = [];
        }
        this.noticationService.getNotifi({ limit: PAGINATION.SIZE_DEFAULT, page: this.pageIndex }).subscribe((res: any) => {
            res?.body.data.forEach((item: any) => {
                this.notifiList.push(item)
            });
            this.totalNotifi = res.body.meta.unread;
            this.isLoading = false;
            this.localStorage.store('notification', { unread: res.body.meta.unread });
            this.total = res?.body.meta.pagination.total
        })
    }
    viewTicket(item: any) {
        if (item.read_at) {
            this.reloadAfterViewTicket(item)
        } else {
            this.noticationService.readNotifi(item?.id).subscribe((res: any) => {
                this.totalNotifi--;
                this.getNotification()
                this.localStorage.store(LOCAL_STORAGE.NOTICE, { unread: this.totalNotifi });
                this.reloadAfterViewTicket(item)
            });
        }
    }
    reloadAfterViewTicket(item: any) {
        const permissionName = this.currentUser.permissions.map((item: any) => item.name);
        var position = this.router.url.search(`${ROUTER_UTILS.ticket.root}/${ROUTER_UTILS.ticket.view}`);
        if (permissionName.includes('department-handle-entry') && permissionName.includes('list-ticket')) {
            setTimeout(() => {
                this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}/${ROUTER_UTILS.ticket.view}/${item?.data?.ticket_id}`]);
            }, 400)
        } else {
            position = this.router.url.search(`${ROUTER_UTILS.endUserTicket.root}/${ROUTER_UTILS.endUserProfile.detail}`);
            setTimeout(() => {
                this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}/${ROUTER_UTILS.endUserProfile.detail}/${item?.data?.ticket_id}`]);
            }, 400)
        }
    }
    onViewAll() {

        if (this.totalNotifi == 0) {
            return
        }
        this.noticationService.getNotifiAll().subscribe((res: any) => {
            if(res.body){
                this.pageIndex = PAGINATION.PAGE_DEFAULT;
                this.getNotification()
            }
        });
    }

    sendButtonClick() {
        this.noticationService.sendMessage(this.msgInput);
    }

    onScroll(event: any) {
        if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight - 10) {
            if (this.limit >= this.total) {
                return
            }
            this.limit += 10;
            this.pageIndex++;
            setTimeout(() => {
                this.isLoading = true;
                this.getNotification()
            }, 800)
        }
    }
    getTitleNotifi(item: any) {
        var title = item.message_type == WAITING ? ' <span class="text-danger">' + item.message_level + '</span></b>' : ' ' + item.message_level + '</b>'
        title = '<b>' + item.message_type + title
        return title;
    }
    change($event: any) {
        if ($event) {
            this.getNotification()
        }
    }
}
