import { Component, HostListener, OnInit } from '@angular/core';
import { environment } from '@env/environment';
@Component({
    selector: 'app-enduser-footer',
    templateUrl: './enduser-footer.component.html',
    styleUrls: ['./enduser-footer.component.css']
})

export class EnduserFooterComponent implements OnInit { 
    url = environment.domain
    year:any
    ngOnInit(): void {
        var currentTime = new Date()
        this.year = currentTime.getFullYear();
    }
}