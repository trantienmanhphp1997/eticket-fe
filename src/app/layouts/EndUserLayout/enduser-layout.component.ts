import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { AUTH_PATH } from '@shared/constants/base.constant';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { UserPrimary } from '@shared/models/user-primary.model';
import { IUser } from '@shared/models/user.model';
import { AuthService } from '@shared/service/auth/auth.service';
import { LoadingService } from '@shared/service/helpers/loading.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { LocalStorageService } from 'ngx-webstorage';


@Component({
    selector: 'app-enduser-layout',
    templateUrl: './enduser-layout.component.html',
    styleUrls: ['./enduser-layout.component.css']
})

export class EnduserLayoutComponent implements OnInit { 

    currentUser: any;
    
    constructor(
        public loadingService: LoadingService,
        private authService: AuthService,
        private router: Router,
        private toast: ToastService,
        private localStorage: LocalStorageService
        ){}
    ngOnInit(): void {
        const profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE);
      const token = this.authService.getToken();
      if (!profile) {
        if (token) {
          this.authService.myProfile().subscribe((response:any) => {
            this.currentUser = response?.body?.data as IUser;
            this.localStorage.store(LOCAL_STORAGE.PROFILE, this.currentUser);
            this.authService.myAuthorities(this.currentUser?.id).subscribe(res => {
              this.currentUser.userPrimary = res.body?.data as UserPrimary;
              this.localStorage.store(LOCAL_STORAGE.PROFILE, this.currentUser);
            });
          });
        } else {
          this.router.navigate([AUTH_PATH]);
        }
      } else {
        this.currentUser = profile;
      }
    }
}