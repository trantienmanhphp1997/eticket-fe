import { Component, OnInit } from '@angular/core';
import { LoadingService } from '@shared/service/helpers/loading.service';


@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss']
})
export class DefaultLayoutComponent implements OnInit {

  constructor(
    public loadingService: LoadingService,
  ) { }

  ngOnInit(): void {
  }

}
