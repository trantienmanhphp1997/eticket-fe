import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '@shared/shared.module';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { IconsProviderModule } from '../icons-provider.module';
import { AdminLayoutComponent } from './AdminLayout/admin-layout.component';
import { AdminHeaderComponent } from './AdminLayout/Header/ad-header.component';
import { SideBarComponent } from './AdminLayout/SideBar/sidebar.component';
import { DefaultLayoutComponent } from './DefaultLayout/default-layout.component';
import { EnduserLayoutComponent } from './EndUserLayout/enduser-layout.component';
import { EnduserSidebarComponent } from './EndUserLayout/Sidebar/enduser-sidebar.component';
import { EnduserHeaderComponent } from './EndUserLayout/Header/enduser-header.component';
import { EnduserFooterComponent } from './EndUserLayout/Footer/enduser-footer.component';



@NgModule({
  declarations: [
    AdminLayoutComponent,
    AdminHeaderComponent,
    SideBarComponent,
    DefaultLayoutComponent,
    EnduserLayoutComponent,
    EnduserSidebarComponent,
    EnduserHeaderComponent,
    EnduserFooterComponent,
  ],
  imports:
    [
      CommonModule,
      SharedModule,
      NgbModule,
      IconsProviderModule,
      NzLayoutModule,
      NzMenuModule,
      NzDropDownModule,
      NzAvatarModule,
      NzSpinModule,
    ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
})
export class LayoutModule {
}
