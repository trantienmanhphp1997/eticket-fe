import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, ActivationEnd, Router } from '@angular/router';
import { AUTH_PATH, LANGUAGE_EN, LANGUAGE_VI } from '@shared/constants/base.constant';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { UserPrimary } from '@shared/models/user-primary.model';
import { IUser } from '@shared/models/user.model';
import { AuthService } from '@shared/service/auth/auth.service';
import { LoadingService } from '@shared/service/helpers/loading.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { NotificationService } from '@shared/service/notification.service';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'admin-layout',
  templateUrl: 'admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})

export class AdminLayoutComponent implements OnInit {
  visible = false;
  isCollapsed = false;
  currentUser: any;
  currentLanguage = '';
  title = '';
  breadcrum: any = [];
  isDashboard = false;
  VI = LANGUAGE_VI;
  EN = LANGUAGE_EN;
  view =false;
  pageIndex: any;
  unread= 0;
  constructor(
    public loadingService: LoadingService,
    private authService: AuthService,
    private router: Router,
    private toast: ToastService,
    private route: ActivatedRoute,
    private localStorage: LocalStorageService,
    private noticationService:NotificationService,
  ) {
    this.route.queryParams
      .subscribe((params) => {
        if (params?.index == 3||params?.index ==0||params?.view=='detail') {
          this.view = true;
        }else{
          this.view = false;
        }
      }
      );
      
    this.currentLanguage = this.localStorage.retrieve(LOCAL_STORAGE.LANGUAGE) || LANGUAGE_VI;
    this.router.events.subscribe(event => {
      if (event instanceof ActivationEnd) {
        if (event?.snapshot?.data?.title) {
          if(this.view){
            this.title = event?.snapshot?.data?.titleOther?.toString() || 'common.title';
          }else{
            this.title = event?.snapshot?.data?.title.toString() || 'common.title';
          }
          this.breadcrum = event?.snapshot?.data?.breadcrum || []
        }
        if (event?.snapshot?.url[0]?.path) {
          this.isDashboard = event?.snapshot?.url[0]?.path === '/admin/dashboard';
        }
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any): void {
    if (event.target.innerWidth <= 1366) {
      this.isCollapsed = true;
    }
  }

  ngOnInit(): void {
    
    const profile = this.localStorage.retrieve(LOCAL_STORAGE.PROFILE); 
    const token = this.authService.getToken();
    if (!profile) {
      if (token) {
        this.authService.myProfile().subscribe((response: any) => {
          this.currentUser = response?.body?.data as IUser;
          this.localStorage.store(LOCAL_STORAGE.PROFILE, this.currentUser);
          this.authService.myAuthorities(this.currentUser?.id).subscribe(res => {
            this.currentUser.userPrimary = res.body?.data as UserPrimary;
            this.localStorage.store(LOCAL_STORAGE.PROFILE, this.currentUser);
          });
        });
      } else {
        this.router.navigate([AUTH_PATH]);
      }
    } else {
      this.currentUser = profile;
    }
    
  }

  getShortName(fullName: string): string {
    if (!fullName) {
      return 'User Name';
    }
    const list = fullName.split(' ');
    if (list.length > 5) {
      return list[0] + ' ' + list[list.length - 1];
    } else {
      return fullName;
    }
  }

  onChangeLanguage(language: string): void {
    if (this.currentLanguage !== language) {
      this.localStorage.store(LOCAL_STORAGE.LANGUAGE, language);
      location.reload();
    } else {
      this.visible = false;
    }
  }

  navigateDashboard(): void {
    if (this.currentUser.is_admin) {
      this.router.navigate(['/admin/ticket-management']);
    }
    else {
      this.router.navigate(['/ticket']);
    }

  }

  collapse(value: boolean): void {
    this.isCollapsed = value;
  }
}