import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AUTH } from '@shared/constants/auth.constant';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { SidebarConstant } from './sidebar.constants';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
@Component({
    selector: 'app-sidebar',
    templateUrl: 'sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})

export class SideBarComponent implements OnInit {
    @Input() isCollapsed = false;
    menus: any = SidebarConstant;

    ROUTER_UTILS = ROUTER_UTILS;
    href: string;
    constructor(
        private router : Router,
        private localStorage: LocalStorageService,
    ) {
        this.href = this.router.url;
      
     }

    ngOnInit() {
        if(
            this.href == '/admin/service-products-management' ||
            this.href == '/admin/dashboard' ||
            this.href == '/admin/organization-management' ||
            this.href == '/admin/customer-management' ||
            this.href == '/admin/user' ||
            this.href == '/admin/ticket-source' ||
            this.href == '/admin/report-kpi-tickets' ||
            this.href == '/admin/report-tickets' ||
            this.href == '/admin/workflow-configuration' ||
            this.href == '/admin/ticket-category' ||
            this.href == '/admin/ticket-priority' ||
            this.href == '/admin/ticket-state' ||
            this.href == '/admin/trigger' ||
            this.href == '/admin/sla-management' ||
            this.href == '/admin/role-management' ||
            this.href == '/admin/user-group-management' ||
            this.href == '/admin/report-outdate-tickets' 
        ){
            this.localStorage.clear(LOCAL_STORAGE.URL);
        }
        if(
            this.href == '/admin/service-products-management' ||
            this.href == '/admin/ticket-management' ||
            this.href == '/admin/organization-management' ||
            this.href == '/admin/customer-management' ||
            this.href == '/admin/user' ||
            this.href == '/admin/ticket-source' ||
            this.href == '/admin/report-kpi-tickets' ||
            this.href == '/admin/report-tickets' ||
            this.href == '/admin/workflow-configuration' ||
            this.href == '/admin/ticket-category' ||
            this.href == '/admin/ticket-priority' ||
            this.href == '/admin/ticket-state' ||
            this.href == '/admin/trigger' ||
            this.href == '/admin/sla-management' ||
            this.href == '/admin/role-management' ||
            this.href == '/admin/user-group-management' ||
            this.href == '/admin/report-outdate-tickets' 
        ){
            this.localStorage.clear(LOCAL_STORAGE.QUY);
        }
    }

    renderPathImageIcon(name:any){
        return `assets/images/icon/${name}.svg`
    }
    checkExpand(child:any){   
        if(child.some((item:any)=>item.children)){
            return child.some((item:any)=>item.children.some((childMenu:any)=>this.href?.includes('/admin/'+childMenu.path)));
        }
        return child.some((item:any)=>this.href?.includes('/admin/'+item.path));
    }
}