import { AUTH } from "@shared/constants/auth.constant";
import { ROUTER_UTILS } from "@shared/utils/router.utils";


export const SidebarConstant =  [
  {
      level: 1,
      title: 'sidebar.dashboard',
      path: `${ROUTER_UTILS.base.dashboard}`,
      icon: 'home',
      image:'home',
      open: true,
      selected: false,
      disabled: false,
      authorities: [AUTH.ADMIN.ACCESS_DASHBOARD],
      // children: []
  },
  {
      level: 1,
      path: `${ROUTER_UTILS.ticket.root}`,
      title: 'sidebar.titket_management',
      icon: '',
      image:'ticket',
      open: true,
      selected: false,
      disabled: false,
      authorities: [AUTH.TICKET.LIST,AUTH.TICKET.CREATE,AUTH.TICKET.UPDATE,AUTH.TICKET.DELETE,AUTH.TICKET.CHANGE_LEVEL,AUTH.TICKET.VIEW,AUTH.TICKET.HANDLE_ENTRY,AUTH.TICKET.HANDLE_ENTRY],
      // children: []
  },
//   {
//     level: 1,
//     path: `${ROUTER_UTILS.task.root}`,
//     title: 'Quản lý công việc',
//     icon: 'audit',
//     authorities: [AUTH.TASK.LIST]
//   },
  {
      level: 1,
      path: `${ROUTER_UTILS.serviceProducts.root}`,
      title: 'sidebar.service_products_management',
      icon: 'team',
      image:'serviceProduct',
      open: true,
      selected: false,
      disabled: false,
      authorities: [AUTH.PRODUCT.LIST],
      // children: []
  },
  {
      level: 1,
      path: `${ROUTER_UTILS.organization.root}`,
      title: 'sidebar.organization_management',
      icon: 'appstore',
      image:'department',
      open: true,
      selected: false,
      disabled: false,
      authorities: [AUTH.DEPARTMENT.LIST],
      // children: []
  },
  {
      level: 1,
      path: `${ROUTER_UTILS.customer.root}`,
      title: 'sidebar.customer_management',
      icon: 'user-switch',
      image:'customers',
      open: true,
      selected: false,
      disabled: false,
      authorities: [AUTH.CUSTOMER.LIST],
      // children: []
  },
  {
      level: 1,
      path: `${ROUTER_UTILS.user.root}`,
      title: 'sidebar.user',
      icon: 'appstore',
      image:'users',
      open: true,
      selected: false,
      disabled: false,
      authorities: [AUTH.USER.LIST],
      // children: []
  },
  {
      level: 1,
    //   path: `${ROUTER_UTILS.report.root}`,
      title: 'sidebar.report',
      icon: 'area-chart',
      image:'report',
      open: true,
      selected: false,
      disabled: false,
      authorities:  [AUTH.REPORT.TICKETS,AUTH.REPORT.KPI,AUTH.REPORT.OUTDATE_TICKET,AUTH.REPORT.REQUEST_UPGRADE],
      children: [
        {
            level: 2,
            path: `${ROUTER_UTILS.reportTickets.root}`,
            title: 'sidebar.report_tickets',
            authorities:  [AUTH.REPORT.TICKETS],
        },
        {
            level: 2,
            path: `${ROUTER_UTILS.reportKpi.root}`,
            title: 'sidebar.report_kpi',
            authorities:  [AUTH.REPORT.KPI],
        },
        {
            level: 2,
            path: `${ROUTER_UTILS.reportOutdate.root}`,
            title: 'sidebar.report_outdate',
            authorities:  [AUTH.REPORT.OUTDATE_TICKET],
        },
        {
            level: 2,
            path: `${ROUTER_UTILS.reportRequestUpgrade.root}`,
            title: 'sidebar.report_request_upgrade',
            authorities:  [AUTH.REPORT.REQUEST_UPGRADE],
        },
      ]
  },
  {
      level: 1,
      // path: `${ROUTER_UTILS.user.root}`,
      title: 'sidebar.system_configuration',
      icon: 'setting',
      open: true,
      selected: false,
      disabled: false,
      authorities: [AUTH.SLA.LIST, AUTH.SYSTEM_WORKFLOW.LIST, AUTH.TRIGGER.LIST,AUTH.TICKET_CATEGORY_CONFIG.LIST, AUTH.TICKET_PRIORITY_CONFIG.LIST,AUTH.TICKET_SOURCE_CONFIG.LIST
                    , AUTH.TICKET_STATUS_CONFIG.LIST, AUTH.GROUP.LIST, AUTH.ROLE.LIST],
      children: [
          {
              level: 2,
              // path: `${ROUTER_UTILS.ticketConfiguration.root}`,
              title: 'sidebar.ticket_source_configuration',
              authorities: [AUTH.TICKET_SOURCE_CONFIG.LIST, AUTH.TICKET_CATEGORY_CONFIG.LIST, AUTH.TICKET_PRIORITY_CONFIG.LIST, AUTH.TICKET_STATUS_CONFIG.LIST],
              children: [
                  {
                      level: 3,
                      path: `${ROUTER_UTILS.ticketSource.root}`,
                      title: 'sidebar.ticket_source',
                      root: true,
                      image:'',
                      authorities: [AUTH.TICKET_SOURCE_CONFIG.LIST],
                  },
                  {
                      level: 3,
                      path: `${ROUTER_UTILS.ticketCategory.root}`,
                      title: 'sidebar.ticket_category',
                      root: true,
                      image:'',
                      authorities: [AUTH.TICKET_CATEGORY_CONFIG.LIST],
                  },
                  {
                      level: 3,
                      path: `${ROUTER_UTILS.ticketPriority.root}`,
                      title: 'sidebar.ticket_priority',
                      root: true,
                      image:'',
                      authorities: [AUTH.TICKET_PRIORITY_CONFIG.LIST],
                  },
                  {
                      level: 3,
                      path: `${ROUTER_UTILS.ticketState.root}`,
                      title: 'sidebar.ticket_state',
                      root: true,
                      image:'',
                      authorities: [AUTH.TICKET_STATUS_CONFIG.LIST],
                  },
              ]
          },
          {
              level: 2,
              // path: `${ROUTER_UTILS.flowConfiguration.root}`,
              title: 'sidebar.process_flow_configuration',
              authorities: [AUTH.SYSTEM_WORKFLOW.LIST,AUTH.TRIGGER.LIST,AUTH.SLA.LIST],
              children: [
                  {
                      level: 3,
                      path: `${ROUTER_UTILS.workflowConfiguration.root}`,
                      // icon: 'user',
                      title: 'sidebar.workflow_configuration',
                      image:'',
                      authorities: [AUTH.SYSTEM_WORKFLOW.LIST],
                  },
                  {
                      level: 3,
                      path: `${ROUTER_UTILS.trigger.root}`,
                      title: 'Danh sách Trigger',
                      // icon: 'user',
                      image:'',
                      authorities: [AUTH.TRIGGER.LIST],
                  },
                  {
                      level: 3,
                      path: `${ROUTER_UTILS.sla.root}`,
                      title: 'sidebar.sla_management',
                      // icon: 'user',
                      image:'',
                      authorities: [AUTH.SLA.LIST],
                  }
              ]
          },
          {
              level: 2,
              title: 'sidebar.permission_management',
              // icon: 'form',
              authorities: [AUTH.ROLE.LIST, AUTH.GROUP.LIST],
              children: [
                  {
                      level: 3,
                      path: `${ROUTER_UTILS.roleManagement.root}`,
                      title: 'sidebar.role_management',
                      // icon: 'form',
                      image:'',
                      authorities: [AUTH.ROLE.LIST],
                  },
                  {
                      level: 3,
                      path: `${ROUTER_UTILS.userGroup.root}`,
                      title: 'sidebar.user_group_management',
                      // icon: 'team',
                      image:'',
                      authorities: [AUTH.GROUP.LIST],
                  },
              ]
          },
      ]
  },

];
