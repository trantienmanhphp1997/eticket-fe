import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { STATUS } from '@shared/constants/status.constants';
import { AuthService } from '@shared/service/auth/auth.service';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { LoadingService } from '@shared/service/helpers/loading.service';
import { ToastService } from '@shared/service/helpers/toast.service';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { LocalStorageService } from 'ngx-webstorage';
import { AUTH_PATH } from '@shared/constants/base.constant';
import CommonUtil from '@shared/utils/common-utils';
import { ChangePasswordProfileComponent } from 'src/app/pages/profile/change-password/change-password.component';
import { UserService } from '@shared/service/user.service';
import { environment } from '@env/environment';
import { NotificationService } from '@shared/service/notification.service';
import { NOTIFI, WAITING } from '@shared/constants/notification.constant';
import { Subject } from 'rxjs';
import { PAGINATION } from '@shared/constants/pagination.constants';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { LOCAL_STORAGE } from '@shared/constants/local-session-cookies.constants';
import { AngularFireMessaging } from '@angular/fire/messaging';

@Component({
    selector: 'app-ad-header',
    templateUrl: 'ad-header.component.html',
    styleUrls: ['./ad-header.component.css']
})

export class AdminHeaderComponent implements OnInit {
    @Input() isCollapsed = false
    @Input() title = ''
    @Input() breadcrum: any
    @Input() currentUser: any
    @Output() collapsedEvent = new EventEmitter<boolean>(); 
    scrollNotify = new Subject();
    commonUtil: any = CommonUtil;
    visible = false;
    visibleNotify = false;
    currentLanguage = 'vi';
    isDashboard = false;
    imgFile: any;
    urlgetway = environment.fileDomain
    VI = 'vi';
    EN = 'en';
    message:any;
    dataProfile: any;
    is_user_sync = environment.is_user_sync
    notifiList: any = [];
    msgInput: string = 'lorem ipsum';
    totalNotifi = 0;
    private readonly _stop = new Subject<void>();
    private readonly _start = new Subject<void>();
    isLoading = false;
    limit = PAGINATION.SIZE_DEFAULT;
    total = 0;
    routeDetail = `${ROUTER_UTILS.ticket.root}/${ROUTER_UTILS.ticket.view}/`
    ticketId: any;
    pageIndex=PAGINATION.PAGE_DEFAULT;
    constructor(
        public loadingService: LoadingService,
        private authService: AuthService,
        private router: Router,
        private toast: ToastService,
        private localStorage: LocalStorageService,
        private modalService: NzModalService,
        private userService: UserService,
        private noticationService: NotificationService,
        private route: ActivatedRoute,
        private afMessaging: AngularFireMessaging,
    ) {}
    ngOnInit() {
      
        this.imgFile = this.currentUser?.avatarProfile?.data?.url;
        this.scrollNotify.pipe(
            debounceTime(100),
            distinctUntilChanged())
            .subscribe(value => {
                this.onScroll(value) ;
            });
            const notification = this.localStorage.retrieve(LOCAL_STORAGE.NOTICE)
            this.totalNotifi=notification?.unread||0;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.afMessaging.messages.subscribe(
            (payload: any) => {
                if (this.visibleNotify) {
                    this.pageIndex=PAGINATION.PAGE_DEFAULT
                    this.getNotification()
                } else {
                    this.total++;
                    this.totalNotifi++
                    this.localStorage.store(LOCAL_STORAGE.NOTICE, { unread: this.totalNotifi });
                }
            });
        // this.getDataUser();
    }

    // getDataUser() {
    //     this.userService.find(this.currentUser.id, true).subscribe((res) => {
    //         this.dataProfile = res.body?.data;
    //         this.imgFile = this.dataProfile?.avatarProfile?.data?.url;
    //     });
    // }
    collapseMenu(value: boolean) {
        this.collapsedEvent.emit(value);
    }
    onChangeLanguage(language: string): void {
        if (this.currentLanguage !== language) {
            // this.localStorage.store(LOCAL_STORAGE.LANGUAGE, language);
            location.reload();
        } else {
            this.visible = false;
        }
    }
    logout(): void {
        this.localStorage.clear(LOCAL_STORAGE.URL);
        this.localStorage.clear(LOCAL_STORAGE.QUY);
        this.authService.clear();
        this.router.navigate([AUTH_PATH]);
        this.toast.success('model.logout.success.authenticate');
        // this.authService.logout().subscribe((response: any) => {
        //   if (response.code === STATUS.SUCCESS_200) {
        //     this.authService.clear();
        //     this.router.navigate(['/authentication/login']);
        //     this.toast.success('model.logout.success.authenticate');
        //   }
        // });
    }
    toEndUser() {
        this.localStorage.clear(LOCAL_STORAGE.URL);
        this.localStorage.clear(LOCAL_STORAGE.QUY);
        this.router.navigate([`${ROUTER_UTILS.endUserTicket.root}`]);
    }
    onUpdateProfile() {
        this.localStorage.clear(LOCAL_STORAGE.URL);
        this.localStorage.clear(LOCAL_STORAGE.QUY);
        this.router.navigate([`admin/${ROUTER_UTILS.profile.root}/${ROUTER_UTILS.profile.detail}/${this.currentUser.id}`]);
    }
    onChangePassword() {
        const base = CommonUtil.modalBase(ChangePasswordProfileComponent, { profileId: this.currentUser.id }, '35%');
        const modal: NzModalRef = this.modalService.create(base);
        modal.afterClose.subscribe(result => {
        });
    }
    getNotification() {
        if(this.pageIndex==PAGINATION.PAGE_DEFAULT){
            this.notifiList=[];
        }
        this.noticationService.getNotifi({ limit: PAGINATION.SIZE_DEFAULT,page: this.pageIndex}).subscribe((res: any) => {
            res?.body.data.forEach((item:any)=>{
                 this.notifiList.push(item)
            });
            this.totalNotifi = res.body.meta.unread;
            this.isLoading = false;
            this.localStorage.store('notification', { unread: res.body.meta.unread });
            this.total = res?.body.meta.pagination.total
        })
    }
    viewTicket(item: any) {
        this.visibleNotify = false
        if (item.read_at) {
            this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}/${ROUTER_UTILS.ticket.view}/${item?.data?.ticket_id}`]);
        } else {
            this.noticationService.readNotifi(item?.id).subscribe((res: any) => {
                this.totalNotifi--;
                this.getNotification()
                setTimeout(()=>{
                this.router.navigate([`admin/${ROUTER_UTILS.ticket.root}/${ROUTER_UTILS.ticket.view}/${item?.data?.ticket_id}`]);
                },400)
                
            });
            
        }
    }
    onViewAll() {
        if (this.totalNotifi == 0) {
            return
        }
        this.noticationService.getNotifiAll().subscribe((res: any) => {
            if(res.body){
                this.pageIndex = PAGINATION.PAGE_DEFAULT;
                this.getNotification()
            }
        });
    }
    sendButtonClick() {
        this.noticationService.sendMessage(this.msgInput);
    }
    getTitleNotifi(item: any) {
        var title = '';
        if (item?.message_type == WAITING) {
            title = '<b>' + item?.message_type + ' <span class="text-danger">' + item?.message_level + '</span></b>'
        } else if (item?.message_type == NOTIFI) {
            title = '<b>' + item?.message_type + ' ' + item?.message_level + '</b>';
        }
        return title;
    }
    onScroll(event: any) {
        if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight - 10) {
            if (this.limit >= this.total) {
                return
            }
            this.limit += 10;
            this.pageIndex++;
            setTimeout(() => {
                this.isLoading = true;
                this.getNotification()
            }, 800)
        }
    }
    change($event: any) {
        if ($event) {
            this.getNotification()
        }
    }
}