import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROUTER_UTILS } from '@shared/utils/router.utils';
import { AdminLayoutComponent } from './layouts/AdminLayout/admin-layout.component';
import { DefaultLayoutComponent } from './layouts/DefaultLayout/default-layout.component';
import { EnduserLayoutComponent } from './layouts/EndUserLayout/enduser-layout.component';
import { Page403Component } from './pages/authentication/page403/page403.component';
import { Page404Component } from './pages/authentication/page404/page404.component';

const routes: Routes = [
  {
    path: ROUTER_UTILS.base.home,
    component: AdminLayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: '/admin/dashboard' }, //dashboard
      {
        path: ROUTER_UTILS.base.dashboard,
        loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule)
      },
      {
        path: ROUTER_UTILS.ticket.root,
        loadChildren: () => import('./pages/ticket/ticket.module').then(m => m.TicketModule)
      },
      {
        path: ROUTER_UTILS.organization.root,
        loadChildren: () => import('./pages/department/department.module').then(m => m.DeparmentModule)
      },
      {
        path: ROUTER_UTILS.user.root,
        loadChildren: () => import('./pages/user/user.module').then(m => m.UserModule)
      },
      {
        path: ROUTER_UTILS.authorizationMng.root,
        loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule)
      },
      {
        path: ROUTER_UTILS.serviceProducts.root,
        loadChildren: () => import('./pages/service-product/service-product.module').then(m => m.ServiceProductModule)
      },
      {
        path: ROUTER_UTILS.reportTickets.root,
        loadChildren: () => import('./pages/report/report-tickets/report-tickets.module').then(m => m.ReportTicketsModule)
      },
      {
        path: ROUTER_UTILS.reportKpi.root,
        loadChildren: () => import('./pages/report/report-kpi/report-kpi.module').then(m => m.ReportKpiModule)
      },
      {
        path: ROUTER_UTILS.reportOutdate.root,
        loadChildren: () => import('./pages/report/report-outdate/report-outdate.module').then(m => m.ReportOutdateModule)
      },
      {
        path: ROUTER_UTILS.reportRequestUpgrade.root,
        loadChildren: () => import('./pages/report/report-request-upgrade/report-request-upgrade.module').then(m => m.ReportRequestUpgradeModule)
      },
      {
        path: ROUTER_UTILS.flowConfiguration.root,
        loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule)
      },
      {
        path: ROUTER_UTILS.customer.root,
        loadChildren: () => import('./pages/customer/customer.module').then(m => m.CustomerModule)
      },
      {
        path: ROUTER_UTILS.sla.root,
        loadChildren: () => import('./pages/sla/sla.module').then(m => m.SlaModule)
      },
      {
        path: ROUTER_UTILS.slaInternal.root,
        loadChildren: () => import('./pages/sla-internal/sla-internal.module').then(m => m.SlaInternalModule)
      },
      {
        path: ROUTER_UTILS.ticketCategory.root,
        loadChildren: () => import('./pages/ticket-config/ticket-category/ticket-category.module').then(m => m.TicketCategoryModule)
      },
      {
        path: ROUTER_UTILS.ticketSource.root,
        loadChildren: () => import('./pages/ticket-config/ticket-source/ticket-source.module').then(m => m.TicketSourceModule)
      },
      {
        path: ROUTER_UTILS.ticketPriority.root,
        loadChildren: () => import('./pages/ticket-config/ticket-priority/ticket-priority.module').then(m => m.TicketPriorityModule)
      },
      {
        path: ROUTER_UTILS.ticketState.root,
        loadChildren: () => import('./pages/ticket-config/ticket-state/ticket-state.module').then(m => m.TicketStateModule)
      },
      {
        path: ROUTER_UTILS.userGroup.root,
        loadChildren: () => import('./pages/user-group/user-group.module').then(m => m.UserGroupModule)
      },
      {
        path: ROUTER_UTILS.roleManagement.root,
        loadChildren: () => import('./pages/role/role.module').then(m => m.RoleModule)
      },
      {
        path: ROUTER_UTILS.trigger.root,
        loadChildren: () => import('./pages/trigger/trigger.module').then(m => m.TriggerModule)
      },
      {
        path: ROUTER_UTILS.workflowConfiguration.root,
        loadChildren: () => import('./pages/workflow/workflow.module').then(m => m.WorkflowModule)
      },
      {
        path: ROUTER_UTILS.profile.root,
        loadChildren: () => import('./pages/profile/profile-detail.module').then(m => m.ProfileDetailModule)
      },
      // {
      //   path: ROUTER_UTILS.task.root,
      //   loadChildren: () => import('./pages/task/task.module').then(m => m.TicketModule)
      // },
    ]
  },
  {
    path: ROUTER_UTILS.authentication.root,
    component: DefaultLayoutComponent,
    loadChildren: () => import('./pages/authentication/authentication.module').then((m) => m.AuthenticationModule),
  },
  {
    path: ROUTER_UTILS.endUser.root,
    component: EnduserLayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'ticket' },
      {
        path: ROUTER_UTILS.endUserTicket.root,
        loadChildren: () => import('./pages/end-user/ticket/ticket-module').then(m => m.EndUserTicketModule)
      },
      {
        path: ROUTER_UTILS.endUserProfile.root,
        loadChildren: () => import('./pages/end-user/profile/profile-detail-module').then(m => m.EndUserProfileDetailModule)
      },
    ]
  },
  { path: ROUTER_UTILS.error.permissionDenied, component: Page403Component },
  { path: ROUTER_UTILS.error.notFound, component: Page404Component },
  { path: ROUTER_UTILS.base.freeRoute, component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'corrected' })],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
